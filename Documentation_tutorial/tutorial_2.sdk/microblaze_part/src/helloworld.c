// File: helloworld.c
// Designer: Nilles kevin

#include <stdio.h>
#include "xil_printf.h"
#include "sleep.h"
#include "xparameters.h" 		// Defines addresses
#include "xmbox.h" 				// Mailbox driver
#include "microblaze_sleep.h" 	// Needed for the "Sleep" function
#include "xil_io.h"
#include "PWM_Led_2.h"

#define printf xil_printf	/* A smaller footprint printf */

int main()
{   					// die benutzten funktionen f�r die mbox findet man in xmbox.h
						// in der file system.mss im bsp folder kann man auch ein example programm �ffnen um
						// zu schauen wie die initialisation und ansteuerung der mailboxes sind.
	XMbox Mbox0;
	XMbox Mbox1;
	XMbox_Config *ConfigPtr0;
	XMbox_Config *ConfigPtr1;

	int Status;
	u32 Reg32Data0 = 0; 				// data from the ARM core.
	u32 Reg32Data1 = 0; 				// momentan status of the LED.
	u32 Reg0, Reg1, Reg2, Reg3, Reg4; 	// register variabeln mit dem die AXI register beschrieben werden.

	// Configure Mailbox0
	ConfigPtr0 = XMbox_LookupConfig(XPAR_MBOX_0_DEVICE_ID ); 	//Die Device ID ist in der file xparameters zu finden
	if (ConfigPtr0 == (XMbox_Config *)NULL) {
		printf ("LookupConfig0 Failed.\r\n");
		return XST_FAILURE;
	}
	// Configure Mailbox1
	ConfigPtr1 = XMbox_LookupConfig(XPAR_MBOX_1_DEVICE_ID );	//Die Device ID ist in der file xparameters zu finden
	if (ConfigPtr1 == (XMbox_Config *)NULL) {
		printf ("LookupConfig1 Failed.\r\n");
		return XST_FAILURE;
	}
	// Initialize Mailbox0
	Status = XMbox_CfgInitialize(&Mbox0, ConfigPtr0, ConfigPtr0->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	// Initialize Mailbox1
	Status = XMbox_CfgInitialize(&Mbox1, ConfigPtr1, ConfigPtr1->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	while(1)
	{
		// Wait for new data received from the Mailbox
		XMbox_ReadBlocking(&Mbox0,(u32*)(&Reg32Data0),4);
		char *payloadpointer = &Reg32Data0;		// l�st den pointer auf die adresse des Reg32Data0 zeigen.
		//payloadpointer = Reg32Data0;
		char buffer[4]; // 8bit nothing, 8 bit LEDID 1,2,3,4, 8 bit mode 1,0, 8 bit value 0 - 255
		for(int i=0; i<4; i++) {				// die loop speichert die von der mailbox 0 empfangenen daten in einen buffer ab.
			buffer[i] = *payloadpointer;
			payloadpointer++;
		}
		// Sleep for 20 us
		usleep(20);
		/* abarbeiten der empfangenen daten. dazu muss definiert werden wie die Daten in die Mailbox gelegt werden.
		 * buffer [2] = anzusteuernde LED. 1, 2, 3, 4
		 * buffer [1] = Pin mode = 0, PWM mode = 1
		 * buffer [0] = bei pin mode ( led on = 1, led off = 0), bei PWM mode( 0 dunkel => 255 hell)
		 */
		if(buffer[2] == 0x01){						// LED 1 = 0x01 = dec 1
			if (buffer[1] == 0x00){					// PIN Mode
				Reg0 = Reg0 & 0xFFFFFFFE;			// Reg0 = config register f�r die LEDs. E = 1111 1110
				if(buffer[0] == 0x01){				// Setzt LED auf on
					Reg1 = Reg1 | 0x00000100;
				} else if (buffer[0] == 0x00){		// Setzt LED auf off
					Reg1 = Reg1 | 0xFFFFFEFF;
				}
			} else if (buffer[1] == 0x01){			// PWM Mode
				Reg0 = Reg0 | 0x00000001;			// setzt config register LED 1 auf PWM mode
				Reg1 = Reg1 & 0xFFFFFF00;			// l�scht das LED value Register (nur PWM value teil)
				Reg1 = Reg1 | buffer[0];			// Laden des neuen wertes in das Value Register
			}
		} else if (buffer[2] == 0x02) {				// LED 2 = 0x02 = dec 2
			if (buffer[1] == 0x00){					// PIN Mode
				Reg0 = Reg0 & 0xFFFFFFFD;			// Reg0 = config register f�r die LEDs. E = 1111 1101
				if(buffer[0] == 0x01){				// Setzt LED auf on
					Reg2 = Reg2 | 0x00000100;
				} else if (buffer[0] == 0x00){		// Setzt LED auf off
					Reg2 = Reg2 | 0xFFFFFEFF;
				}
			} else if (buffer[1] == 0x01){			// PWM Mode
				Reg0 = Reg0 | 0x00000002;			// setzt config register LED 2 auf PWM mode
				Reg2 = Reg2 & 0xFFFFFF00;			// l�scht das LED value Register (nur PWM value teil)
				Reg2 = Reg2 | buffer[0];			// Laden des neuen wertes in das Value Register
			}
		} else if (buffer[2] == 0x03) {				// LED 3 = 0x03 = dec 3
			if (buffer[1] == 0x00){					// PIN Mode
				Reg0 = Reg0 & 0xFFFFFFFB;			// Reg0 = config register f�r die LEDs. E = 1111 1011
				if(buffer[0] == 0x01){				// Setzt LED auf on
					Reg3 = Reg3 | 0x00000100;
				} else if (buffer[0] == 0x00){		// Setzt LED auf off
					Reg3 = Reg3 | 0xFFFFFEFF;
				}
			} else if (buffer[1] == 0x01){			// PWM Mode
				Reg0 = Reg0 | 0x00000004;			// setzt config register LED 3 auf PWM mode
				Reg3 = Reg3 & 0xFFFFFF00;			// l�scht das LED value Register (nur PWM value teil)
				Reg3 = Reg3 | buffer[0];			// Laden des neuen wertes in das Value Register
			}
		} else if (buffer[2] == 0x04) {				// LED 4 = 0x04 = dec 4
			if (buffer[1] == 0x00){					// PIN Mode
				Reg0 = Reg0 & 0xFFFFFFF7;			// Reg0 = config register f�r die LEDs. E = 1111 0111
				if(buffer[0] == 0x01){				// Setzt LED auf on
					Reg4 = Reg4 | 0x00000100;
				} else if (buffer[0] == 0x00){		// Setzt LED auf off
					Reg4 = Reg4 | 0xFFFFFEFF;
				}
			} else if (buffer[1] == 0x01){			// PWM Mode
				Reg0 = Reg0 | 0x00000008;			// setzt config register LED 4 auf PWM mode
				Reg4 = Reg4 & 0xFFFFFF00;			// l�scht das LED value Register (nur PWM value teil)
				Reg4 = Reg4 | buffer[0];			// Laden des neuen wertes in das Value Register
			}
		}
		// schreibt die neuen werte in die jeweiligen AXI register die mit dem PL teil verbunden sind.
		PWM_LED_2_mWriteReg(XPAR_PWM_LED_2_0_S00_AXI_BASEADDR, PWM_LED_2_S00_AXI_SLV_REG0_OFFSET, Reg0);
		PWM_LED_2_mWriteReg(XPAR_PWM_LED_2_0_S00_AXI_BASEADDR, PWM_LED_2_S00_AXI_SLV_REG1_OFFSET, Reg1);
		PWM_LED_2_mWriteReg(XPAR_PWM_LED_2_0_S00_AXI_BASEADDR, PWM_LED_2_S00_AXI_SLV_REG2_OFFSET, Reg2);
		PWM_LED_2_mWriteReg(XPAR_PWM_LED_2_0_S00_AXI_BASEADDR, PWM_LED_2_S00_AXI_SLV_REG3_OFFSET, Reg3);
		PWM_LED_2_mWriteReg(XPAR_PWM_LED_2_0_S00_AXI_BASEADDR, PWM_LED_2_S00_AXI_SLV_REG4_OFFSET, Reg4);
		// Send current state of switches to ARM via Mailbox
		Reg32Data1 = Reg32Data0;
		// hier kann �ber die mailbox 1 eine nachrricht zum Arm core zur�ck gesendet werden.
		XMbox_WriteBlocking(&Mbox1,(u32*)(&Reg32Data1),4);

		// Sleep for 10 us
		usleep(10);
	}

	return 0;
}
