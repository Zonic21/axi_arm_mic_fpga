// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.4 (win64) Build 1756540 Mon Jan 23 19:11:23 MST 2017
// Date        : Thu Jun 01 11:49:53 2017
// Host        : DESKTOP-CLNFD4A running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               c:/Users/Zonic/tutorial_2/tutorial_2.srcs/sources_1/bd/design_1/ip/design_1_mailbox_1_0/design_1_mailbox_1_0_sim_netlist.v
// Design      : design_1_mailbox_1_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_mailbox_1_0,mailbox,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "mailbox,Vivado 2016.4" *) 
(* NotValidForBitStream *)
module design_1_mailbox_1_0
   (S0_AXI_ACLK,
    S0_AXI_ARESETN,
    S0_AXI_AWADDR,
    S0_AXI_AWVALID,
    S0_AXI_AWREADY,
    S0_AXI_WDATA,
    S0_AXI_WSTRB,
    S0_AXI_WVALID,
    S0_AXI_WREADY,
    S0_AXI_BRESP,
    S0_AXI_BVALID,
    S0_AXI_BREADY,
    S0_AXI_ARADDR,
    S0_AXI_ARVALID,
    S0_AXI_ARREADY,
    S0_AXI_RDATA,
    S0_AXI_RRESP,
    S0_AXI_RVALID,
    S0_AXI_RREADY,
    S1_AXI_ACLK,
    S1_AXI_ARESETN,
    S1_AXI_AWADDR,
    S1_AXI_AWVALID,
    S1_AXI_AWREADY,
    S1_AXI_WDATA,
    S1_AXI_WSTRB,
    S1_AXI_WVALID,
    S1_AXI_WREADY,
    S1_AXI_BRESP,
    S1_AXI_BVALID,
    S1_AXI_BREADY,
    S1_AXI_ARADDR,
    S1_AXI_ARVALID,
    S1_AXI_ARREADY,
    S1_AXI_RDATA,
    S1_AXI_RRESP,
    S1_AXI_RVALID,
    S1_AXI_RREADY,
    Interrupt_0,
    Interrupt_1);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 CLK.S0_AXI_ACLK CLK" *) input S0_AXI_ACLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 RST.S0_AXI_ARESETN RST" *) input S0_AXI_ARESETN;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI AWADDR" *) input [31:0]S0_AXI_AWADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI AWVALID" *) input S0_AXI_AWVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI AWREADY" *) output S0_AXI_AWREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI WDATA" *) input [31:0]S0_AXI_WDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI WSTRB" *) input [3:0]S0_AXI_WSTRB;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI WVALID" *) input S0_AXI_WVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI WREADY" *) output S0_AXI_WREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI BRESP" *) output [1:0]S0_AXI_BRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI BVALID" *) output S0_AXI_BVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI BREADY" *) input S0_AXI_BREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI ARADDR" *) input [31:0]S0_AXI_ARADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI ARVALID" *) input S0_AXI_ARVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI ARREADY" *) output S0_AXI_ARREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI RDATA" *) output [31:0]S0_AXI_RDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI RRESP" *) output [1:0]S0_AXI_RRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI RVALID" *) output S0_AXI_RVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI RREADY" *) input S0_AXI_RREADY;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 CLK.S1_AXI_ACLK CLK" *) input S1_AXI_ACLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 RST.S1_AXI_ARESETN RST" *) input S1_AXI_ARESETN;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI AWADDR" *) input [31:0]S1_AXI_AWADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI AWVALID" *) input S1_AXI_AWVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI AWREADY" *) output S1_AXI_AWREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI WDATA" *) input [31:0]S1_AXI_WDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI WSTRB" *) input [3:0]S1_AXI_WSTRB;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI WVALID" *) input S1_AXI_WVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI WREADY" *) output S1_AXI_WREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI BRESP" *) output [1:0]S1_AXI_BRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI BVALID" *) output S1_AXI_BVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI BREADY" *) input S1_AXI_BREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI ARADDR" *) input [31:0]S1_AXI_ARADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI ARVALID" *) input S1_AXI_ARVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI ARREADY" *) output S1_AXI_ARREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI RDATA" *) output [31:0]S1_AXI_RDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI RRESP" *) output [1:0]S1_AXI_RRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI RVALID" *) output S1_AXI_RVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI RREADY" *) input S1_AXI_RREADY;
  (* x_interface_info = "xilinx.com:signal:interrupt:1.0 INTERRUPT.Interrupt_0 INTERRUPT" *) output Interrupt_0;
  (* x_interface_info = "xilinx.com:signal:interrupt:1.0 INTERRUPT.Interrupt_1 INTERRUPT" *) output Interrupt_1;

  wire Interrupt_0;
  wire Interrupt_1;
  wire S0_AXI_ACLK;
  wire [31:0]S0_AXI_ARADDR;
  wire S0_AXI_ARESETN;
  wire S0_AXI_ARREADY;
  wire S0_AXI_ARVALID;
  wire [31:0]S0_AXI_AWADDR;
  wire S0_AXI_AWREADY;
  wire S0_AXI_AWVALID;
  wire S0_AXI_BREADY;
  wire [1:0]S0_AXI_BRESP;
  wire S0_AXI_BVALID;
  wire [31:0]S0_AXI_RDATA;
  wire S0_AXI_RREADY;
  wire [1:0]S0_AXI_RRESP;
  wire S0_AXI_RVALID;
  wire [31:0]S0_AXI_WDATA;
  wire S0_AXI_WREADY;
  wire [3:0]S0_AXI_WSTRB;
  wire S0_AXI_WVALID;
  wire S1_AXI_ACLK;
  wire [31:0]S1_AXI_ARADDR;
  wire S1_AXI_ARESETN;
  wire S1_AXI_ARREADY;
  wire S1_AXI_ARVALID;
  wire [31:0]S1_AXI_AWADDR;
  wire S1_AXI_AWREADY;
  wire S1_AXI_AWVALID;
  wire S1_AXI_BREADY;
  wire [1:0]S1_AXI_BRESP;
  wire S1_AXI_BVALID;
  wire [31:0]S1_AXI_RDATA;
  wire S1_AXI_RREADY;
  wire [1:0]S1_AXI_RRESP;
  wire S1_AXI_RVALID;
  wire [31:0]S1_AXI_WDATA;
  wire S1_AXI_WREADY;
  wire [3:0]S1_AXI_WSTRB;
  wire S1_AXI_WVALID;
  wire NLW_U0_M0_AXIS_TLAST_UNCONNECTED;
  wire NLW_U0_M0_AXIS_TVALID_UNCONNECTED;
  wire NLW_U0_M1_AXIS_TLAST_UNCONNECTED;
  wire NLW_U0_M1_AXIS_TVALID_UNCONNECTED;
  wire NLW_U0_S0_AXIS_TREADY_UNCONNECTED;
  wire NLW_U0_S1_AXIS_TREADY_UNCONNECTED;
  wire [31:0]NLW_U0_M0_AXIS_TDATA_UNCONNECTED;
  wire [31:0]NLW_U0_M1_AXIS_TDATA_UNCONNECTED;

  (* C_ASYNC_CLKS = "0" *) 
  (* C_ENABLE_BUS_ERROR = "0" *) 
  (* C_EXT_RESET_HIGH = "1" *) 
  (* C_FAMILY = "zynq" *) 
  (* C_IMPL_STYLE = "0" *) 
  (* C_INTERCONNECT_PORT_0 = "2" *) 
  (* C_INTERCONNECT_PORT_1 = "2" *) 
  (* C_M0_AXIS_DATA_WIDTH = "32" *) 
  (* C_M1_AXIS_DATA_WIDTH = "32" *) 
  (* C_MAILBOX_DEPTH = "16" *) 
  (* C_NUM_SYNC_FF = "2" *) 
  (* C_S0_AXIS_DATA_WIDTH = "32" *) 
  (* C_S0_AXI_ADDR_WIDTH = "32" *) 
  (* C_S0_AXI_BASEADDR = "1132593152" *) 
  (* C_S0_AXI_DATA_WIDTH = "32" *) 
  (* C_S0_AXI_HIGHADDR = "1132658687" *) 
  (* C_S1_AXIS_DATA_WIDTH = "32" *) 
  (* C_S1_AXI_ADDR_WIDTH = "32" *) 
  (* C_S1_AXI_BASEADDR = "1132658688" *) 
  (* C_S1_AXI_DATA_WIDTH = "32" *) 
  (* C_S1_AXI_HIGHADDR = "1132724223" *) 
  design_1_mailbox_1_0_mailbox U0
       (.Interrupt_0(Interrupt_0),
        .Interrupt_1(Interrupt_1),
        .M0_AXIS_ACLK(1'b0),
        .M0_AXIS_TDATA(NLW_U0_M0_AXIS_TDATA_UNCONNECTED[31:0]),
        .M0_AXIS_TLAST(NLW_U0_M0_AXIS_TLAST_UNCONNECTED),
        .M0_AXIS_TREADY(1'b0),
        .M0_AXIS_TVALID(NLW_U0_M0_AXIS_TVALID_UNCONNECTED),
        .M1_AXIS_ACLK(1'b0),
        .M1_AXIS_TDATA(NLW_U0_M1_AXIS_TDATA_UNCONNECTED[31:0]),
        .M1_AXIS_TLAST(NLW_U0_M1_AXIS_TLAST_UNCONNECTED),
        .M1_AXIS_TREADY(1'b0),
        .M1_AXIS_TVALID(NLW_U0_M1_AXIS_TVALID_UNCONNECTED),
        .S0_AXIS_ACLK(1'b0),
        .S0_AXIS_TDATA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .S0_AXIS_TLAST(1'b0),
        .S0_AXIS_TREADY(NLW_U0_S0_AXIS_TREADY_UNCONNECTED),
        .S0_AXIS_TVALID(1'b0),
        .S0_AXI_ACLK(S0_AXI_ACLK),
        .S0_AXI_ARADDR(S0_AXI_ARADDR),
        .S0_AXI_ARESETN(S0_AXI_ARESETN),
        .S0_AXI_ARREADY(S0_AXI_ARREADY),
        .S0_AXI_ARVALID(S0_AXI_ARVALID),
        .S0_AXI_AWADDR(S0_AXI_AWADDR),
        .S0_AXI_AWREADY(S0_AXI_AWREADY),
        .S0_AXI_AWVALID(S0_AXI_AWVALID),
        .S0_AXI_BREADY(S0_AXI_BREADY),
        .S0_AXI_BRESP(S0_AXI_BRESP),
        .S0_AXI_BVALID(S0_AXI_BVALID),
        .S0_AXI_RDATA(S0_AXI_RDATA),
        .S0_AXI_RREADY(S0_AXI_RREADY),
        .S0_AXI_RRESP(S0_AXI_RRESP),
        .S0_AXI_RVALID(S0_AXI_RVALID),
        .S0_AXI_WDATA(S0_AXI_WDATA),
        .S0_AXI_WREADY(S0_AXI_WREADY),
        .S0_AXI_WSTRB(S0_AXI_WSTRB),
        .S0_AXI_WVALID(S0_AXI_WVALID),
        .S1_AXIS_ACLK(1'b0),
        .S1_AXIS_TDATA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .S1_AXIS_TLAST(1'b0),
        .S1_AXIS_TREADY(NLW_U0_S1_AXIS_TREADY_UNCONNECTED),
        .S1_AXIS_TVALID(1'b0),
        .S1_AXI_ACLK(S1_AXI_ACLK),
        .S1_AXI_ARADDR(S1_AXI_ARADDR),
        .S1_AXI_ARESETN(S1_AXI_ARESETN),
        .S1_AXI_ARREADY(S1_AXI_ARREADY),
        .S1_AXI_ARVALID(S1_AXI_ARVALID),
        .S1_AXI_AWADDR(S1_AXI_AWADDR),
        .S1_AXI_AWREADY(S1_AXI_AWREADY),
        .S1_AXI_AWVALID(S1_AXI_AWVALID),
        .S1_AXI_BREADY(S1_AXI_BREADY),
        .S1_AXI_BRESP(S1_AXI_BRESP),
        .S1_AXI_BVALID(S1_AXI_BVALID),
        .S1_AXI_RDATA(S1_AXI_RDATA),
        .S1_AXI_RREADY(S1_AXI_RREADY),
        .S1_AXI_RRESP(S1_AXI_RRESP),
        .S1_AXI_RVALID(S1_AXI_RVALID),
        .S1_AXI_WDATA(S1_AXI_WDATA),
        .S1_AXI_WREADY(S1_AXI_WREADY),
        .S1_AXI_WSTRB(S1_AXI_WSTRB),
        .S1_AXI_WVALID(S1_AXI_WVALID),
        .SYS_Rst(1'b0));
endmodule

(* ORIG_REF_NAME = "SRL_FIFO" *) 
module design_1_mailbox_1_0_SRL_FIFO
   (write_fsl_error_d1_reg,
    \Addr_Counters[0].FDRE_I_0 ,
    \s_axi_rdata_i_reg[3] ,
    Q,
    D,
    data_Exists_I_reg_0,
    rit_detect_d0,
    rit_detect_d1_reg,
    sit_detect_d0,
    SR,
    S0_AXI_ACLK,
    CI,
    S1_AXI_WDATA,
    next_Data_Exists,
    \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ,
    FDRE_I1_0,
    data_Exists_I_reg_1,
    Bus_RNW_reg,
    \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ,
    Bus_RNW_reg_reg,
    \rit_register_reg[0] ,
    \rit_register_reg[3] ,
    \sit_register_reg[3] ,
    \sit_register_reg[0] ,
    E);
  output write_fsl_error_d1_reg;
  output \Addr_Counters[0].FDRE_I_0 ;
  output [3:0]\s_axi_rdata_i_reg[3] ;
  output [2:0]Q;
  output [27:0]D;
  output data_Exists_I_reg_0;
  output rit_detect_d0;
  output rit_detect_d1_reg;
  output sit_detect_d0;
  input [0:0]SR;
  input S0_AXI_ACLK;
  input CI;
  input [31:0]S1_AXI_WDATA;
  input next_Data_Exists;
  input \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ;
  input FDRE_I1_0;
  input data_Exists_I_reg_1;
  input Bus_RNW_reg;
  input \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  input Bus_RNW_reg_reg;
  input [1:0]\rit_register_reg[0] ;
  input \rit_register_reg[3] ;
  input \sit_register_reg[3] ;
  input [1:0]\sit_register_reg[0] ;
  input [0:0]E;

  wire Addr_0;
  wire Addr_1;
  wire Addr_2;
  wire Addr_3;
  wire \Addr_Counters[0].FDRE_I_0 ;
  wire \Addr_Counters[3].XORCY_I_i_1__0_n_0 ;
  wire Bus_RNW_reg;
  wire Bus_RNW_reg_reg;
  wire CI;
  wire [27:0]D;
  wire D_0;
  wire [0:0]E;
  wire FDRE_I1_0;
  wire [0:27]FSL0_S_Data_I;
  wire \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ;
  wire LI;
  wire LI0_out;
  wire LI1_out;
  wire [2:0]Q;
  wire S0_AXI_ACLK;
  wire [31:0]S1_AXI_WDATA;
  wire [0:0]SR;
  wire addr_cy_0;
  wire addr_cy_1;
  wire addr_cy_2;
  wire data_Exists_I_reg_0;
  wire data_Exists_I_reg_1;
  wire \fifo_length_i[0]_i_2__0_n_0 ;
  wire \fifo_length_i[1]_i_1__0_n_0 ;
  wire \fifo_length_i[2]_i_1__0_n_0 ;
  wire \fifo_length_i[3]_i_1__0_n_0 ;
  wire \fifo_length_i[4]_i_1__0_n_0 ;
  wire [0:1]fifo_length_i_reg__0;
  wire next_Data_Exists;
  wire rit_detect_d0;
  wire rit_detect_d1_reg;
  wire [1:0]\rit_register_reg[0] ;
  wire \rit_register_reg[3] ;
  wire [3:0]\s_axi_rdata_i_reg[3] ;
  wire sit_detect_d0;
  wire [1:0]\sit_register_reg[0] ;
  wire \sit_register_reg[3] ;
  wire sum_A_0;
  wire sum_A_1;
  wire sum_A_2;
  wire sum_A_3;
  wire write_fsl_error_d1_reg;
  wire [3:3]\NLW_Addr_Counters[0].Used_MuxCY.MUXCY_L_I_CARRY4_CO_UNCONNECTED ;
  wire [3:3]\NLW_Addr_Counters[0].Used_MuxCY.MUXCY_L_I_CARRY4_DI_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b0),
    .IS_D_INVERTED(1'b0),
    .IS_R_INVERTED(1'b0)) 
    \Addr_Counters[0].FDRE_I 
       (.C(S0_AXI_ACLK),
        .CE(\Addr_Counters[0].FDRE_I_0 ),
        .D(sum_A_3),
        .Q(Addr_3),
        .R(SR));
  (* XILINX_LEGACY_PRIM = "(MUXCY,XORCY)" *) 
  (* XILINX_TRANSFORM_PINMAP = "LO:O" *) 
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \Addr_Counters[0].Used_MuxCY.MUXCY_L_I_CARRY4 
       (.CI(1'b0),
        .CO({\NLW_Addr_Counters[0].Used_MuxCY.MUXCY_L_I_CARRY4_CO_UNCONNECTED [3],addr_cy_0,addr_cy_1,addr_cy_2}),
        .CYINIT(CI),
        .DI({\NLW_Addr_Counters[0].Used_MuxCY.MUXCY_L_I_CARRY4_DI_UNCONNECTED [3],Addr_1,Addr_2,Addr_3}),
        .O({sum_A_0,sum_A_1,sum_A_2,sum_A_3}),
        .S({\Addr_Counters[3].XORCY_I_i_1__0_n_0 ,LI0_out,LI1_out,LI}));
  LUT6 #(
    .INIT(64'h9999999899999999)) 
    \Addr_Counters[0].XORCY_I_i_1__0 
       (.I0(Addr_3),
        .I1(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ),
        .I2(Addr_0),
        .I3(Addr_1),
        .I4(Addr_2),
        .I5(FDRE_I1_0),
        .O(LI));
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b0),
    .IS_D_INVERTED(1'b0),
    .IS_R_INVERTED(1'b0)) 
    \Addr_Counters[1].FDRE_I 
       (.C(S0_AXI_ACLK),
        .CE(\Addr_Counters[0].FDRE_I_0 ),
        .D(sum_A_2),
        .Q(Addr_2),
        .R(SR));
  LUT6 #(
    .INIT(64'h9999999899999999)) 
    \Addr_Counters[1].XORCY_I_i_1__0 
       (.I0(Addr_2),
        .I1(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ),
        .I2(Addr_0),
        .I3(Addr_3),
        .I4(Addr_1),
        .I5(FDRE_I1_0),
        .O(LI1_out));
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b0),
    .IS_D_INVERTED(1'b0),
    .IS_R_INVERTED(1'b0)) 
    \Addr_Counters[2].FDRE_I 
       (.C(S0_AXI_ACLK),
        .CE(\Addr_Counters[0].FDRE_I_0 ),
        .D(sum_A_1),
        .Q(Addr_1),
        .R(SR));
  LUT6 #(
    .INIT(64'h9999999899999999)) 
    \Addr_Counters[2].XORCY_I_i_1__0 
       (.I0(Addr_1),
        .I1(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ),
        .I2(Addr_0),
        .I3(Addr_3),
        .I4(Addr_2),
        .I5(FDRE_I1_0),
        .O(LI0_out));
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b0),
    .IS_D_INVERTED(1'b0),
    .IS_R_INVERTED(1'b0)) 
    \Addr_Counters[3].FDRE_I 
       (.C(S0_AXI_ACLK),
        .CE(\Addr_Counters[0].FDRE_I_0 ),
        .D(sum_A_0),
        .Q(Addr_0),
        .R(SR));
  LUT6 #(
    .INIT(64'h9999999899999999)) 
    \Addr_Counters[3].XORCY_I_i_1__0 
       (.I0(Addr_0),
        .I1(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ),
        .I2(Addr_3),
        .I3(Addr_1),
        .I4(Addr_2),
        .I5(FDRE_I1_0),
        .O(\Addr_Counters[3].XORCY_I_i_1__0_n_0 ));
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b0),
    .IS_D_INVERTED(1'b0),
    .IS_R_INVERTED(1'b0)) 
    FDRE_I1
       (.C(S0_AXI_ACLK),
        .CE(\Addr_Counters[0].FDRE_I_0 ),
        .D(D_0),
        .Q(write_fsl_error_d1_reg),
        .R(SR));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[0].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[0].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[31]),
        .Q(FSL0_S_Data_I[0]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[10].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[10].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[21]),
        .Q(FSL0_S_Data_I[10]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[11].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[11].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[20]),
        .Q(FSL0_S_Data_I[11]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[12].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[12].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[19]),
        .Q(FSL0_S_Data_I[12]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[13].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[13].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[18]),
        .Q(FSL0_S_Data_I[13]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[14].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[14].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[17]),
        .Q(FSL0_S_Data_I[14]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[15].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[15].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[16]),
        .Q(FSL0_S_Data_I[15]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[16].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[16].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[15]),
        .Q(FSL0_S_Data_I[16]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[17].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[17].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[14]),
        .Q(FSL0_S_Data_I[17]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[18].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[18].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[13]),
        .Q(FSL0_S_Data_I[18]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[19].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[19].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[12]),
        .Q(FSL0_S_Data_I[19]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[1].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[1].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[30]),
        .Q(FSL0_S_Data_I[1]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[20].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[20].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[11]),
        .Q(FSL0_S_Data_I[20]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[21].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[21].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[10]),
        .Q(FSL0_S_Data_I[21]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[22].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[22].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[9]),
        .Q(FSL0_S_Data_I[22]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[23].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[23].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[8]),
        .Q(FSL0_S_Data_I[23]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[24].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[24].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[7]),
        .Q(FSL0_S_Data_I[24]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[25].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[25].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[6]),
        .Q(FSL0_S_Data_I[25]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[26].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[26].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[5]),
        .Q(FSL0_S_Data_I[26]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[27].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[27].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[4]),
        .Q(FSL0_S_Data_I[27]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[28].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[28].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[3]),
        .Q(\s_axi_rdata_i_reg[3] [3]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[29].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[29].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[2]),
        .Q(\s_axi_rdata_i_reg[3] [2]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[2].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[2].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[29]),
        .Q(FSL0_S_Data_I[2]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[30].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[30].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[1]),
        .Q(\s_axi_rdata_i_reg[3] [1]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[31].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[31].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[0]),
        .Q(\s_axi_rdata_i_reg[3] [0]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[3].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[3].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[28]),
        .Q(FSL0_S_Data_I[3]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[4].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[4].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[27]),
        .Q(FSL0_S_Data_I[4]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[5].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[5].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[26]),
        .Q(FSL0_S_Data_I[5]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[6].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[6].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[25]),
        .Q(FSL0_S_Data_I[6]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[7].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[7].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[24]),
        .Q(FSL0_S_Data_I[7]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[8].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[8].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[23]),
        .Q(FSL0_S_Data_I[8]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_1_to_0/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[9].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[9].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S1_AXI_WDATA[22]),
        .Q(FSL0_S_Data_I[9]));
  LUT4 #(
    .INIT(16'h8000)) 
    buffer_full_early
       (.I0(sum_A_3),
        .I1(sum_A_2),
        .I2(sum_A_0),
        .I3(sum_A_1),
        .O(D_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    data_Exists_I_i_2__0
       (.I0(Addr_0),
        .I1(Addr_3),
        .I2(Addr_1),
        .I3(Addr_2),
        .O(data_Exists_I_reg_0));
  FDCE data_Exists_I_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .CLR(SR),
        .D(next_Data_Exists),
        .Q(\Addr_Counters[0].FDRE_I_0 ));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAA9)) 
    \fifo_length_i[0]_i_2__0 
       (.I0(fifo_length_i_reg__0[0]),
        .I1(fifo_length_i_reg__0[1]),
        .I2(Q[1]),
        .I3(Q[2]),
        .I4(data_Exists_I_reg_1),
        .I5(Q[0]),
        .O(\fifo_length_i[0]_i_2__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT5 #(
    .INIT(32'h6AAAAAA9)) 
    \fifo_length_i[1]_i_1__0 
       (.I0(fifo_length_i_reg__0[1]),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(data_Exists_I_reg_1),
        .I4(Q[0]),
        .O(\fifo_length_i[1]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT4 #(
    .INIT(16'h78E1)) 
    \fifo_length_i[2]_i_1__0 
       (.I0(Q[0]),
        .I1(data_Exists_I_reg_1),
        .I2(Q[2]),
        .I3(Q[1]),
        .O(\fifo_length_i[2]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA955555556AAA)) 
    \fifo_length_i[3]_i_1__0 
       (.I0(Q[0]),
        .I1(\Addr_Counters[0].FDRE_I_0 ),
        .I2(Bus_RNW_reg),
        .I3(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .I4(FDRE_I1_0),
        .I5(Q[1]),
        .O(\fifo_length_i[3]_i_1__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \fifo_length_i[4]_i_1__0 
       (.I0(Q[0]),
        .O(\fifo_length_i[4]_i_1__0_n_0 ));
  FDRE \fifo_length_i_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(E),
        .D(\fifo_length_i[0]_i_2__0_n_0 ),
        .Q(fifo_length_i_reg__0[0]),
        .R(SR));
  FDRE \fifo_length_i_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(E),
        .D(\fifo_length_i[1]_i_1__0_n_0 ),
        .Q(fifo_length_i_reg__0[1]),
        .R(SR));
  FDRE \fifo_length_i_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(E),
        .D(\fifo_length_i[2]_i_1__0_n_0 ),
        .Q(Q[2]),
        .R(SR));
  FDRE \fifo_length_i_reg[3] 
       (.C(S0_AXI_ACLK),
        .CE(E),
        .D(\fifo_length_i[3]_i_1__0_n_0 ),
        .Q(Q[1]),
        .R(SR));
  FDRE \fifo_length_i_reg[4] 
       (.C(S0_AXI_ACLK),
        .CE(E),
        .D(\fifo_length_i[4]_i_1__0_n_0 ),
        .Q(Q[0]),
        .R(SR));
  LUT1 #(
    .INIT(2'h1)) 
    rit_detect_d1_i_1
       (.I0(rit_detect_d1_reg),
        .O(rit_detect_d0));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[10]_i_1 
       (.I0(FSL0_S_Data_I[21]),
        .I1(Bus_RNW_reg_reg),
        .O(D[6]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[11]_i_1 
       (.I0(FSL0_S_Data_I[20]),
        .I1(Bus_RNW_reg_reg),
        .O(D[7]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[12]_i_1 
       (.I0(FSL0_S_Data_I[19]),
        .I1(Bus_RNW_reg_reg),
        .O(D[8]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[13]_i_1 
       (.I0(FSL0_S_Data_I[18]),
        .I1(Bus_RNW_reg_reg),
        .O(D[9]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[14]_i_1 
       (.I0(FSL0_S_Data_I[17]),
        .I1(Bus_RNW_reg_reg),
        .O(D[10]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[15]_i_1 
       (.I0(FSL0_S_Data_I[16]),
        .I1(Bus_RNW_reg_reg),
        .O(D[11]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[16]_i_1 
       (.I0(FSL0_S_Data_I[15]),
        .I1(Bus_RNW_reg_reg),
        .O(D[12]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[17]_i_1 
       (.I0(FSL0_S_Data_I[14]),
        .I1(Bus_RNW_reg_reg),
        .O(D[13]));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[18]_i_1 
       (.I0(FSL0_S_Data_I[13]),
        .I1(Bus_RNW_reg_reg),
        .O(D[14]));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[19]_i_1 
       (.I0(FSL0_S_Data_I[12]),
        .I1(Bus_RNW_reg_reg),
        .O(D[15]));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[20]_i_1 
       (.I0(FSL0_S_Data_I[11]),
        .I1(Bus_RNW_reg_reg),
        .O(D[16]));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[21]_i_1 
       (.I0(FSL0_S_Data_I[10]),
        .I1(Bus_RNW_reg_reg),
        .O(D[17]));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[22]_i_1 
       (.I0(FSL0_S_Data_I[9]),
        .I1(Bus_RNW_reg_reg),
        .O(D[18]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[23]_i_1 
       (.I0(FSL0_S_Data_I[8]),
        .I1(Bus_RNW_reg_reg),
        .O(D[19]));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[24]_i_1 
       (.I0(FSL0_S_Data_I[7]),
        .I1(Bus_RNW_reg_reg),
        .O(D[20]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[25]_i_1 
       (.I0(FSL0_S_Data_I[6]),
        .I1(Bus_RNW_reg_reg),
        .O(D[21]));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[26]_i_1 
       (.I0(FSL0_S_Data_I[5]),
        .I1(Bus_RNW_reg_reg),
        .O(D[22]));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[27]_i_1 
       (.I0(FSL0_S_Data_I[4]),
        .I1(Bus_RNW_reg_reg),
        .O(D[23]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[28]_i_1 
       (.I0(FSL0_S_Data_I[3]),
        .I1(Bus_RNW_reg_reg),
        .O(D[24]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[29]_i_1 
       (.I0(FSL0_S_Data_I[2]),
        .I1(Bus_RNW_reg_reg),
        .O(D[25]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[30]_i_1 
       (.I0(FSL0_S_Data_I[1]),
        .I1(Bus_RNW_reg_reg),
        .O(D[26]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[31]_i_2 
       (.I0(FSL0_S_Data_I[0]),
        .I1(Bus_RNW_reg_reg),
        .O(D[27]));
  LUT6 #(
    .INIT(64'h00000000F4FF00F4)) 
    \s_axi_rdata_i[3]_i_2 
       (.I0(Q[2]),
        .I1(\rit_register_reg[0] [0]),
        .I2(\rit_register_reg[3] ),
        .I3(fifo_length_i_reg__0[1]),
        .I4(\rit_register_reg[0] [1]),
        .I5(fifo_length_i_reg__0[0]),
        .O(rit_detect_d1_reg));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[4]_i_1 
       (.I0(FSL0_S_Data_I[27]),
        .I1(Bus_RNW_reg_reg),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[5]_i_1 
       (.I0(FSL0_S_Data_I[26]),
        .I1(Bus_RNW_reg_reg),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[6]_i_1 
       (.I0(FSL0_S_Data_I[25]),
        .I1(Bus_RNW_reg_reg),
        .O(D[2]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[7]_i_1 
       (.I0(FSL0_S_Data_I[24]),
        .I1(Bus_RNW_reg_reg),
        .O(D[3]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[8]_i_1 
       (.I0(FSL0_S_Data_I[23]),
        .I1(Bus_RNW_reg_reg),
        .O(D[4]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[9]_i_1 
       (.I0(FSL0_S_Data_I[22]),
        .I1(Bus_RNW_reg_reg),
        .O(D[5]));
  LUT6 #(
    .INIT(64'h4504450405004504)) 
    sit_detect_d1_i_1__0
       (.I0(fifo_length_i_reg__0[0]),
        .I1(\sit_register_reg[3] ),
        .I2(fifo_length_i_reg__0[1]),
        .I3(\sit_register_reg[0] [1]),
        .I4(Q[2]),
        .I5(\sit_register_reg[0] [0]),
        .O(sit_detect_d0));
endmodule

(* ORIG_REF_NAME = "SRL_FIFO" *) 
module design_1_mailbox_1_0_SRL_FIFO_2
   (write_fsl_error_d1_reg,
    \Addr_Counters[0].FDRE_I_0 ,
    \s_axi_rdata_i_reg[3] ,
    Q,
    D,
    data_Exists_I_reg_0,
    sit_detect_d0,
    rit_detect_d0,
    rit_detect_d1_reg,
    SR,
    S0_AXI_ACLK,
    CI,
    S0_AXI_WDATA,
    next_Data_Exists,
    \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ,
    FDRE_I1_0,
    data_Exists_I_reg_1,
    Bus_RNW_reg,
    \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ,
    Bus_RNW_reg_reg,
    \sit_register_reg[3] ,
    \sit_register_reg[0] ,
    \rit_register_reg[0] ,
    \rit_register_reg[3] ,
    E);
  output write_fsl_error_d1_reg;
  output \Addr_Counters[0].FDRE_I_0 ;
  output [3:0]\s_axi_rdata_i_reg[3] ;
  output [2:0]Q;
  output [27:0]D;
  output data_Exists_I_reg_0;
  output sit_detect_d0;
  output rit_detect_d0;
  output rit_detect_d1_reg;
  input [0:0]SR;
  input S0_AXI_ACLK;
  input CI;
  input [31:0]S0_AXI_WDATA;
  input next_Data_Exists;
  input \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ;
  input FDRE_I1_0;
  input data_Exists_I_reg_1;
  input Bus_RNW_reg;
  input \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  input Bus_RNW_reg_reg;
  input \sit_register_reg[3] ;
  input [1:0]\sit_register_reg[0] ;
  input [1:0]\rit_register_reg[0] ;
  input \rit_register_reg[3] ;
  input [0:0]E;

  wire Addr_0;
  wire Addr_1;
  wire Addr_2;
  wire Addr_3;
  wire \Addr_Counters[0].FDRE_I_0 ;
  wire \Addr_Counters[3].XORCY_I_i_1_n_0 ;
  wire Bus_RNW_reg;
  wire Bus_RNW_reg_reg;
  wire CI;
  wire [27:0]D;
  wire D_0;
  wire [0:0]E;
  wire FDRE_I1_0;
  wire [0:27]FSL1_S_Data_I;
  wire \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ;
  wire LI;
  wire LI0_out;
  wire LI1_out;
  wire [2:0]Q;
  wire S0_AXI_ACLK;
  wire [31:0]S0_AXI_WDATA;
  wire [0:0]SR;
  wire addr_cy_0;
  wire addr_cy_1;
  wire addr_cy_2;
  wire data_Exists_I_reg_0;
  wire data_Exists_I_reg_1;
  wire \fifo_length_i[0]_i_2_n_0 ;
  wire \fifo_length_i[1]_i_1_n_0 ;
  wire \fifo_length_i[2]_i_1_n_0 ;
  wire \fifo_length_i[3]_i_1_n_0 ;
  wire \fifo_length_i[4]_i_1_n_0 ;
  wire [0:1]fifo_length_i_reg__0;
  wire next_Data_Exists;
  wire rit_detect_d0;
  wire rit_detect_d1_reg;
  wire [1:0]\rit_register_reg[0] ;
  wire \rit_register_reg[3] ;
  wire [3:0]\s_axi_rdata_i_reg[3] ;
  wire sit_detect_d0;
  wire [1:0]\sit_register_reg[0] ;
  wire \sit_register_reg[3] ;
  wire sum_A_0;
  wire sum_A_1;
  wire sum_A_2;
  wire sum_A_3;
  wire write_fsl_error_d1_reg;
  wire [3:3]\NLW_Addr_Counters[0].Used_MuxCY.MUXCY_L_I_CARRY4_CO_UNCONNECTED ;
  wire [3:3]\NLW_Addr_Counters[0].Used_MuxCY.MUXCY_L_I_CARRY4_DI_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b0),
    .IS_D_INVERTED(1'b0),
    .IS_R_INVERTED(1'b0)) 
    \Addr_Counters[0].FDRE_I 
       (.C(S0_AXI_ACLK),
        .CE(\Addr_Counters[0].FDRE_I_0 ),
        .D(sum_A_3),
        .Q(Addr_3),
        .R(SR));
  (* XILINX_LEGACY_PRIM = "(MUXCY,XORCY)" *) 
  (* XILINX_TRANSFORM_PINMAP = "LO:O" *) 
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \Addr_Counters[0].Used_MuxCY.MUXCY_L_I_CARRY4 
       (.CI(1'b0),
        .CO({\NLW_Addr_Counters[0].Used_MuxCY.MUXCY_L_I_CARRY4_CO_UNCONNECTED [3],addr_cy_0,addr_cy_1,addr_cy_2}),
        .CYINIT(CI),
        .DI({\NLW_Addr_Counters[0].Used_MuxCY.MUXCY_L_I_CARRY4_DI_UNCONNECTED [3],Addr_1,Addr_2,Addr_3}),
        .O({sum_A_0,sum_A_1,sum_A_2,sum_A_3}),
        .S({\Addr_Counters[3].XORCY_I_i_1_n_0 ,LI0_out,LI1_out,LI}));
  LUT6 #(
    .INIT(64'h9999999899999999)) 
    \Addr_Counters[0].XORCY_I_i_1 
       (.I0(Addr_3),
        .I1(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ),
        .I2(Addr_0),
        .I3(Addr_1),
        .I4(Addr_2),
        .I5(FDRE_I1_0),
        .O(LI));
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b0),
    .IS_D_INVERTED(1'b0),
    .IS_R_INVERTED(1'b0)) 
    \Addr_Counters[1].FDRE_I 
       (.C(S0_AXI_ACLK),
        .CE(\Addr_Counters[0].FDRE_I_0 ),
        .D(sum_A_2),
        .Q(Addr_2),
        .R(SR));
  LUT6 #(
    .INIT(64'h9999999899999999)) 
    \Addr_Counters[1].XORCY_I_i_1 
       (.I0(Addr_2),
        .I1(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ),
        .I2(Addr_0),
        .I3(Addr_3),
        .I4(Addr_1),
        .I5(FDRE_I1_0),
        .O(LI1_out));
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b0),
    .IS_D_INVERTED(1'b0),
    .IS_R_INVERTED(1'b0)) 
    \Addr_Counters[2].FDRE_I 
       (.C(S0_AXI_ACLK),
        .CE(\Addr_Counters[0].FDRE_I_0 ),
        .D(sum_A_1),
        .Q(Addr_1),
        .R(SR));
  LUT6 #(
    .INIT(64'h9999999899999999)) 
    \Addr_Counters[2].XORCY_I_i_1 
       (.I0(Addr_1),
        .I1(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ),
        .I2(Addr_0),
        .I3(Addr_3),
        .I4(Addr_2),
        .I5(FDRE_I1_0),
        .O(LI0_out));
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b0),
    .IS_D_INVERTED(1'b0),
    .IS_R_INVERTED(1'b0)) 
    \Addr_Counters[3].FDRE_I 
       (.C(S0_AXI_ACLK),
        .CE(\Addr_Counters[0].FDRE_I_0 ),
        .D(sum_A_0),
        .Q(Addr_0),
        .R(SR));
  LUT6 #(
    .INIT(64'h9999999899999999)) 
    \Addr_Counters[3].XORCY_I_i_1 
       (.I0(Addr_0),
        .I1(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ),
        .I2(Addr_3),
        .I3(Addr_1),
        .I4(Addr_2),
        .I5(FDRE_I1_0),
        .O(\Addr_Counters[3].XORCY_I_i_1_n_0 ));
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b0),
    .IS_D_INVERTED(1'b0),
    .IS_R_INVERTED(1'b0)) 
    FDRE_I1
       (.C(S0_AXI_ACLK),
        .CE(\Addr_Counters[0].FDRE_I_0 ),
        .D(D_0),
        .Q(write_fsl_error_d1_reg),
        .R(SR));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[0].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[0].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[31]),
        .Q(FSL1_S_Data_I[0]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[10].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[10].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[21]),
        .Q(FSL1_S_Data_I[10]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[11].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[11].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[20]),
        .Q(FSL1_S_Data_I[11]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[12].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[12].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[19]),
        .Q(FSL1_S_Data_I[12]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[13].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[13].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[18]),
        .Q(FSL1_S_Data_I[13]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[14].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[14].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[17]),
        .Q(FSL1_S_Data_I[14]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[15].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[15].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[16]),
        .Q(FSL1_S_Data_I[15]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[16].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[16].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[15]),
        .Q(FSL1_S_Data_I[16]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[17].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[17].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[14]),
        .Q(FSL1_S_Data_I[17]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[18].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[18].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[13]),
        .Q(FSL1_S_Data_I[18]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[19].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[19].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[12]),
        .Q(FSL1_S_Data_I[19]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[1].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[1].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[30]),
        .Q(FSL1_S_Data_I[1]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[20].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[20].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[11]),
        .Q(FSL1_S_Data_I[20]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[21].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[21].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[10]),
        .Q(FSL1_S_Data_I[21]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[22].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[22].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[9]),
        .Q(FSL1_S_Data_I[22]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[23].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[23].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[8]),
        .Q(FSL1_S_Data_I[23]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[24].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[24].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[7]),
        .Q(FSL1_S_Data_I[24]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[25].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[25].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[6]),
        .Q(FSL1_S_Data_I[25]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[26].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[26].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[5]),
        .Q(FSL1_S_Data_I[26]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[27].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[27].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[4]),
        .Q(FSL1_S_Data_I[27]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[28].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[28].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[3]),
        .Q(\s_axi_rdata_i_reg[3] [3]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[29].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[29].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[2]),
        .Q(\s_axi_rdata_i_reg[3] [2]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[2].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[2].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[29]),
        .Q(FSL1_S_Data_I[2]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[30].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[30].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[1]),
        .Q(\s_axi_rdata_i_reg[3] [1]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[31].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[31].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[0]),
        .Q(\s_axi_rdata_i_reg[3] [0]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[3].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[3].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[28]),
        .Q(FSL1_S_Data_I[3]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[4].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[4].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[27]),
        .Q(FSL1_S_Data_I[4]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[5].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[5].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[26]),
        .Q(FSL1_S_Data_I[5]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[6].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[6].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[25]),
        .Q(FSL1_S_Data_I[6]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[7].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[7].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[24]),
        .Q(FSL1_S_Data_I[7]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[8].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[8].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[23]),
        .Q(FSL1_S_Data_I[8]));
  (* box_type = "PRIMITIVE" *) 
  (* srl_bus_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM " *) 
  (* srl_name = "U0/\fsl_0_to_1/Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/FIFO_RAM[9].SRL16E_I " *) 
  SRL16E #(
    .INIT(16'h0000),
    .IS_CLK_INVERTED(1'b0)) 
    \FIFO_RAM[9].SRL16E_I 
       (.A0(Addr_3),
        .A1(Addr_2),
        .A2(Addr_1),
        .A3(Addr_0),
        .CE(CI),
        .CLK(S0_AXI_ACLK),
        .D(S0_AXI_WDATA[22]),
        .Q(FSL1_S_Data_I[9]));
  LUT4 #(
    .INIT(16'h8000)) 
    buffer_full_early
       (.I0(sum_A_3),
        .I1(sum_A_2),
        .I2(sum_A_0),
        .I3(sum_A_1),
        .O(D_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    data_Exists_I_i_2
       (.I0(Addr_0),
        .I1(Addr_3),
        .I2(Addr_1),
        .I3(Addr_2),
        .O(data_Exists_I_reg_0));
  FDCE data_Exists_I_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .CLR(SR),
        .D(next_Data_Exists),
        .Q(\Addr_Counters[0].FDRE_I_0 ));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAA9)) 
    \fifo_length_i[0]_i_2 
       (.I0(fifo_length_i_reg__0[0]),
        .I1(fifo_length_i_reg__0[1]),
        .I2(Q[1]),
        .I3(Q[2]),
        .I4(data_Exists_I_reg_1),
        .I5(Q[0]),
        .O(\fifo_length_i[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT5 #(
    .INIT(32'h6AAAAAA9)) 
    \fifo_length_i[1]_i_1 
       (.I0(fifo_length_i_reg__0[1]),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(data_Exists_I_reg_1),
        .I4(Q[0]),
        .O(\fifo_length_i[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT4 #(
    .INIT(16'h78E1)) 
    \fifo_length_i[2]_i_1 
       (.I0(Q[0]),
        .I1(data_Exists_I_reg_1),
        .I2(Q[2]),
        .I3(Q[1]),
        .O(\fifo_length_i[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA955555556AAA)) 
    \fifo_length_i[3]_i_1 
       (.I0(Q[0]),
        .I1(\Addr_Counters[0].FDRE_I_0 ),
        .I2(Bus_RNW_reg),
        .I3(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .I4(FDRE_I1_0),
        .I5(Q[1]),
        .O(\fifo_length_i[3]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \fifo_length_i[4]_i_1 
       (.I0(Q[0]),
        .O(\fifo_length_i[4]_i_1_n_0 ));
  FDRE \fifo_length_i_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(E),
        .D(\fifo_length_i[0]_i_2_n_0 ),
        .Q(fifo_length_i_reg__0[0]),
        .R(SR));
  FDRE \fifo_length_i_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(E),
        .D(\fifo_length_i[1]_i_1_n_0 ),
        .Q(fifo_length_i_reg__0[1]),
        .R(SR));
  FDRE \fifo_length_i_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(E),
        .D(\fifo_length_i[2]_i_1_n_0 ),
        .Q(Q[2]),
        .R(SR));
  FDRE \fifo_length_i_reg[3] 
       (.C(S0_AXI_ACLK),
        .CE(E),
        .D(\fifo_length_i[3]_i_1_n_0 ),
        .Q(Q[1]),
        .R(SR));
  FDRE \fifo_length_i_reg[4] 
       (.C(S0_AXI_ACLK),
        .CE(E),
        .D(\fifo_length_i[4]_i_1_n_0 ),
        .Q(Q[0]),
        .R(SR));
  LUT1 #(
    .INIT(2'h1)) 
    rit_detect_d1_i_1__0
       (.I0(rit_detect_d1_reg),
        .O(rit_detect_d0));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[10]_i_1__0 
       (.I0(FSL1_S_Data_I[21]),
        .I1(Bus_RNW_reg_reg),
        .O(D[6]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[11]_i_1__0 
       (.I0(FSL1_S_Data_I[20]),
        .I1(Bus_RNW_reg_reg),
        .O(D[7]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[12]_i_1__0 
       (.I0(FSL1_S_Data_I[19]),
        .I1(Bus_RNW_reg_reg),
        .O(D[8]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[13]_i_1__0 
       (.I0(FSL1_S_Data_I[18]),
        .I1(Bus_RNW_reg_reg),
        .O(D[9]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[14]_i_1__0 
       (.I0(FSL1_S_Data_I[17]),
        .I1(Bus_RNW_reg_reg),
        .O(D[10]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[15]_i_1__0 
       (.I0(FSL1_S_Data_I[16]),
        .I1(Bus_RNW_reg_reg),
        .O(D[11]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[16]_i_1__0 
       (.I0(FSL1_S_Data_I[15]),
        .I1(Bus_RNW_reg_reg),
        .O(D[12]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[17]_i_1__0 
       (.I0(FSL1_S_Data_I[14]),
        .I1(Bus_RNW_reg_reg),
        .O(D[13]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[18]_i_1__0 
       (.I0(FSL1_S_Data_I[13]),
        .I1(Bus_RNW_reg_reg),
        .O(D[14]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[19]_i_1__0 
       (.I0(FSL1_S_Data_I[12]),
        .I1(Bus_RNW_reg_reg),
        .O(D[15]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[20]_i_1__0 
       (.I0(FSL1_S_Data_I[11]),
        .I1(Bus_RNW_reg_reg),
        .O(D[16]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[21]_i_1__0 
       (.I0(FSL1_S_Data_I[10]),
        .I1(Bus_RNW_reg_reg),
        .O(D[17]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[22]_i_1__0 
       (.I0(FSL1_S_Data_I[9]),
        .I1(Bus_RNW_reg_reg),
        .O(D[18]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[23]_i_1__0 
       (.I0(FSL1_S_Data_I[8]),
        .I1(Bus_RNW_reg_reg),
        .O(D[19]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[24]_i_1__0 
       (.I0(FSL1_S_Data_I[7]),
        .I1(Bus_RNW_reg_reg),
        .O(D[20]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[25]_i_1__0 
       (.I0(FSL1_S_Data_I[6]),
        .I1(Bus_RNW_reg_reg),
        .O(D[21]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[26]_i_1__0 
       (.I0(FSL1_S_Data_I[5]),
        .I1(Bus_RNW_reg_reg),
        .O(D[22]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[27]_i_1__0 
       (.I0(FSL1_S_Data_I[4]),
        .I1(Bus_RNW_reg_reg),
        .O(D[23]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[28]_i_1__0 
       (.I0(FSL1_S_Data_I[3]),
        .I1(Bus_RNW_reg_reg),
        .O(D[24]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[29]_i_1__0 
       (.I0(FSL1_S_Data_I[2]),
        .I1(Bus_RNW_reg_reg),
        .O(D[25]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[30]_i_1__0 
       (.I0(FSL1_S_Data_I[1]),
        .I1(Bus_RNW_reg_reg),
        .O(D[26]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[31]_i_2__0 
       (.I0(FSL1_S_Data_I[0]),
        .I1(Bus_RNW_reg_reg),
        .O(D[27]));
  LUT6 #(
    .INIT(64'h00000000F4FF00F4)) 
    \s_axi_rdata_i[3]_i_2__0 
       (.I0(Q[2]),
        .I1(\rit_register_reg[0] [0]),
        .I2(\rit_register_reg[3] ),
        .I3(fifo_length_i_reg__0[1]),
        .I4(\rit_register_reg[0] [1]),
        .I5(fifo_length_i_reg__0[0]),
        .O(rit_detect_d1_reg));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[4]_i_1__0 
       (.I0(FSL1_S_Data_I[27]),
        .I1(Bus_RNW_reg_reg),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[5]_i_1__0 
       (.I0(FSL1_S_Data_I[26]),
        .I1(Bus_RNW_reg_reg),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[6]_i_1__0 
       (.I0(FSL1_S_Data_I[25]),
        .I1(Bus_RNW_reg_reg),
        .O(D[2]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[7]_i_1__0 
       (.I0(FSL1_S_Data_I[24]),
        .I1(Bus_RNW_reg_reg),
        .O(D[3]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[8]_i_1__0 
       (.I0(FSL1_S_Data_I[23]),
        .I1(Bus_RNW_reg_reg),
        .O(D[4]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[9]_i_1__0 
       (.I0(FSL1_S_Data_I[22]),
        .I1(Bus_RNW_reg_reg),
        .O(D[5]));
  LUT6 #(
    .INIT(64'h4504450405004504)) 
    sit_detect_d1_i_1
       (.I0(fifo_length_i_reg__0[0]),
        .I1(\sit_register_reg[3] ),
        .I2(fifo_length_i_reg__0[1]),
        .I3(\sit_register_reg[0] [1]),
        .I4(Q[2]),
        .I5(\sit_register_reg[0] [0]),
        .O(sit_detect_d0));
endmodule

(* ORIG_REF_NAME = "Sync_FIFO" *) 
module design_1_mailbox_1_0_Sync_FIFO
   (write_fsl_error_d1_reg,
    \Addr_Counters[0].FDRE_I ,
    \s_axi_rdata_i_reg[3] ,
    Q,
    D,
    data_Exists_I_reg,
    rit_detect_d0,
    rit_detect_d1_reg,
    sit_detect_d0,
    SR,
    S0_AXI_ACLK,
    CI,
    S1_AXI_WDATA,
    next_Data_Exists,
    \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ,
    FDRE_I1,
    data_Exists_I_reg_0,
    Bus_RNW_reg,
    \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ,
    Bus_RNW_reg_reg,
    \rit_register_reg[0] ,
    \rit_register_reg[3] ,
    \sit_register_reg[3] ,
    \sit_register_reg[0] ,
    E);
  output write_fsl_error_d1_reg;
  output \Addr_Counters[0].FDRE_I ;
  output [3:0]\s_axi_rdata_i_reg[3] ;
  output [2:0]Q;
  output [27:0]D;
  output data_Exists_I_reg;
  output rit_detect_d0;
  output rit_detect_d1_reg;
  output sit_detect_d0;
  input [0:0]SR;
  input S0_AXI_ACLK;
  input CI;
  input [31:0]S1_AXI_WDATA;
  input next_Data_Exists;
  input \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ;
  input FDRE_I1;
  input data_Exists_I_reg_0;
  input Bus_RNW_reg;
  input \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  input Bus_RNW_reg_reg;
  input [1:0]\rit_register_reg[0] ;
  input \rit_register_reg[3] ;
  input \sit_register_reg[3] ;
  input [1:0]\sit_register_reg[0] ;
  input [0:0]E;

  wire \Addr_Counters[0].FDRE_I ;
  wire Bus_RNW_reg;
  wire Bus_RNW_reg_reg;
  wire CI;
  wire [27:0]D;
  wire [0:0]E;
  wire FDRE_I1;
  wire \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ;
  wire [2:0]Q;
  wire S0_AXI_ACLK;
  wire [31:0]S1_AXI_WDATA;
  wire [0:0]SR;
  wire data_Exists_I_reg;
  wire data_Exists_I_reg_0;
  wire next_Data_Exists;
  wire rit_detect_d0;
  wire rit_detect_d1_reg;
  wire [1:0]\rit_register_reg[0] ;
  wire \rit_register_reg[3] ;
  wire [3:0]\s_axi_rdata_i_reg[3] ;
  wire sit_detect_d0;
  wire [1:0]\sit_register_reg[0] ;
  wire \sit_register_reg[3] ;
  wire write_fsl_error_d1_reg;

  design_1_mailbox_1_0_SRL_FIFO \Sync_FIFO_I.srl_fifo_i.FSL_FIFO 
       (.\Addr_Counters[0].FDRE_I_0 (\Addr_Counters[0].FDRE_I ),
        .Bus_RNW_reg(Bus_RNW_reg),
        .Bus_RNW_reg_reg(Bus_RNW_reg_reg),
        .CI(CI),
        .D(D),
        .E(E),
        .FDRE_I1_0(FDRE_I1),
        .\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg (\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] (\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ),
        .Q(Q),
        .S0_AXI_ACLK(S0_AXI_ACLK),
        .S1_AXI_WDATA(S1_AXI_WDATA),
        .SR(SR),
        .data_Exists_I_reg_0(data_Exists_I_reg),
        .data_Exists_I_reg_1(data_Exists_I_reg_0),
        .next_Data_Exists(next_Data_Exists),
        .rit_detect_d0(rit_detect_d0),
        .rit_detect_d1_reg(rit_detect_d1_reg),
        .\rit_register_reg[0] (\rit_register_reg[0] ),
        .\rit_register_reg[3] (\rit_register_reg[3] ),
        .\s_axi_rdata_i_reg[3] (\s_axi_rdata_i_reg[3] ),
        .sit_detect_d0(sit_detect_d0),
        .\sit_register_reg[0] (\sit_register_reg[0] ),
        .\sit_register_reg[3] (\sit_register_reg[3] ),
        .write_fsl_error_d1_reg(write_fsl_error_d1_reg));
endmodule

(* ORIG_REF_NAME = "Sync_FIFO" *) 
module design_1_mailbox_1_0_Sync_FIFO_1
   (write_fsl_error_d1_reg,
    \Addr_Counters[0].FDRE_I ,
    \s_axi_rdata_i_reg[3] ,
    Q,
    D,
    data_Exists_I_reg,
    sit_detect_d0,
    rit_detect_d0,
    rit_detect_d1_reg,
    SR,
    S0_AXI_ACLK,
    CI,
    S0_AXI_WDATA,
    next_Data_Exists,
    \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ,
    FDRE_I1,
    data_Exists_I_reg_0,
    Bus_RNW_reg,
    \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ,
    Bus_RNW_reg_reg,
    \sit_register_reg[3] ,
    \sit_register_reg[0] ,
    \rit_register_reg[0] ,
    \rit_register_reg[3] ,
    E);
  output write_fsl_error_d1_reg;
  output \Addr_Counters[0].FDRE_I ;
  output [3:0]\s_axi_rdata_i_reg[3] ;
  output [2:0]Q;
  output [27:0]D;
  output data_Exists_I_reg;
  output sit_detect_d0;
  output rit_detect_d0;
  output rit_detect_d1_reg;
  input [0:0]SR;
  input S0_AXI_ACLK;
  input CI;
  input [31:0]S0_AXI_WDATA;
  input next_Data_Exists;
  input \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ;
  input FDRE_I1;
  input data_Exists_I_reg_0;
  input Bus_RNW_reg;
  input \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  input Bus_RNW_reg_reg;
  input \sit_register_reg[3] ;
  input [1:0]\sit_register_reg[0] ;
  input [1:0]\rit_register_reg[0] ;
  input \rit_register_reg[3] ;
  input [0:0]E;

  wire \Addr_Counters[0].FDRE_I ;
  wire Bus_RNW_reg;
  wire Bus_RNW_reg_reg;
  wire CI;
  wire [27:0]D;
  wire [0:0]E;
  wire FDRE_I1;
  wire \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ;
  wire [2:0]Q;
  wire S0_AXI_ACLK;
  wire [31:0]S0_AXI_WDATA;
  wire [0:0]SR;
  wire data_Exists_I_reg;
  wire data_Exists_I_reg_0;
  wire next_Data_Exists;
  wire rit_detect_d0;
  wire rit_detect_d1_reg;
  wire [1:0]\rit_register_reg[0] ;
  wire \rit_register_reg[3] ;
  wire [3:0]\s_axi_rdata_i_reg[3] ;
  wire sit_detect_d0;
  wire [1:0]\sit_register_reg[0] ;
  wire \sit_register_reg[3] ;
  wire write_fsl_error_d1_reg;

  design_1_mailbox_1_0_SRL_FIFO_2 \Sync_FIFO_I.srl_fifo_i.FSL_FIFO 
       (.\Addr_Counters[0].FDRE_I_0 (\Addr_Counters[0].FDRE_I ),
        .Bus_RNW_reg(Bus_RNW_reg),
        .Bus_RNW_reg_reg(Bus_RNW_reg_reg),
        .CI(CI),
        .D(D),
        .E(E),
        .FDRE_I1_0(FDRE_I1),
        .\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg (\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] (\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ),
        .Q(Q),
        .S0_AXI_ACLK(S0_AXI_ACLK),
        .S0_AXI_WDATA(S0_AXI_WDATA),
        .SR(SR),
        .data_Exists_I_reg_0(data_Exists_I_reg),
        .data_Exists_I_reg_1(data_Exists_I_reg_0),
        .next_Data_Exists(next_Data_Exists),
        .rit_detect_d0(rit_detect_d0),
        .rit_detect_d1_reg(rit_detect_d1_reg),
        .\rit_register_reg[0] (\rit_register_reg[0] ),
        .\rit_register_reg[3] (\rit_register_reg[3] ),
        .\s_axi_rdata_i_reg[3] (\s_axi_rdata_i_reg[3] ),
        .sit_detect_d0(sit_detect_d0),
        .\sit_register_reg[0] (\sit_register_reg[0] ),
        .\sit_register_reg[3] (\sit_register_reg[3] ),
        .write_fsl_error_d1_reg(write_fsl_error_d1_reg));
endmodule

(* ORIG_REF_NAME = "address_decoder" *) 
module design_1_mailbox_1_0_address_decoder
   (\ie_register_reg[2] ,
    \is_register_reg[2] ,
    read_fsl_error_d1_reg,
    Bus_RNW_reg_reg_0,
    error_detect,
    write_fsl_error,
    D,
    S0_AXI_WREADY,
    S0_AXI_ARREADY,
    CI,
    \fifo_length_i_reg[3] ,
    \s_axi_rdata_i_reg[3] ,
    \s_axi_rdata_i_reg[3]_0 ,
    next_Data_Exists,
    \fifo_length_i_reg[2] ,
    E,
    \sit_register_reg[3] ,
    \rit_register_reg[3] ,
    s_axi_rvalid_i_reg,
    s_axi_bvalid_i_reg,
    full_error_reg,
    \Addr_Counters[0].FDRE_I ,
    empty_error_reg,
    read_fsl_error_d1_reg_0,
    Q,
    S0_AXI_ACLK,
    write_fsl_error_d1,
    read_fsl_error_d1,
    FSL0_S_Exists_I,
    FDRE_I1,
    SYS_Rst_I,
    \state_reg[1] ,
    \state_reg[0] ,
    S0_AXI_ARVALID,
    is_write_reg,
    FSL0_S_Data_I,
    full_error,
    FDRE_I1_0,
    \Addr_Counters[3].FDRE_I ,
    s_axi_rvalid_i_reg_0,
    S0_AXI_WVALID,
    is_read,
    \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] ,
    empty_error,
    p_6_in,
    \sit_register_reg[0] ,
    \rit_register_reg[0] ,
    sit_detect_d0,
    \fifo_length_i_reg[2]_0 ,
    ie_register,
    S0_AXI_RREADY,
    s_axi_rvalid_i_reg_1,
    S0_AXI_BREADY,
    s_axi_bvalid_i_reg_0,
    \bus2ip_addr_i_reg[5] ,
    bus2ip_rnw_i);
  output \ie_register_reg[2] ;
  output \is_register_reg[2] ;
  output read_fsl_error_d1_reg;
  output Bus_RNW_reg_reg_0;
  output error_detect;
  output write_fsl_error;
  output [1:0]D;
  output S0_AXI_WREADY;
  output S0_AXI_ARREADY;
  output CI;
  output \fifo_length_i_reg[3] ;
  output [3:0]\s_axi_rdata_i_reg[3] ;
  output \s_axi_rdata_i_reg[3]_0 ;
  output next_Data_Exists;
  output \fifo_length_i_reg[2] ;
  output [0:0]E;
  output [0:0]\sit_register_reg[3] ;
  output [0:0]\rit_register_reg[3] ;
  output s_axi_rvalid_i_reg;
  output s_axi_bvalid_i_reg;
  output full_error_reg;
  output \Addr_Counters[0].FDRE_I ;
  output empty_error_reg;
  output read_fsl_error_d1_reg_0;
  input Q;
  input S0_AXI_ACLK;
  input write_fsl_error_d1;
  input read_fsl_error_d1;
  input FSL0_S_Exists_I;
  input FDRE_I1;
  input SYS_Rst_I;
  input [1:0]\state_reg[1] ;
  input \state_reg[0] ;
  input S0_AXI_ARVALID;
  input is_write_reg;
  input [3:0]FSL0_S_Data_I;
  input full_error;
  input FDRE_I1_0;
  input \Addr_Counters[3].FDRE_I ;
  input s_axi_rvalid_i_reg_0;
  input S0_AXI_WVALID;
  input is_read;
  input [4:0]\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] ;
  input empty_error;
  input [2:0]p_6_in;
  input [3:0]\sit_register_reg[0] ;
  input [3:0]\rit_register_reg[0] ;
  input sit_detect_d0;
  input \fifo_length_i_reg[2]_0 ;
  input [0:2]ie_register;
  input S0_AXI_RREADY;
  input s_axi_rvalid_i_reg_1;
  input S0_AXI_BREADY;
  input s_axi_bvalid_i_reg_0;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input bus2ip_rnw_i;

  wire \Addr_Counters[0].FDRE_I ;
  wire \Addr_Counters[3].FDRE_I ;
  wire Bus_RNW_reg_i_1_n_0;
  wire Bus_RNW_reg_reg_0;
  wire CI;
  wire [1:0]D;
  wire [0:0]E;
  wire FDRE_I1;
  wire FDRE_I1_0;
  wire [3:0]FSL0_S_Data_I;
  wire FSL0_S_Exists_I;
  wire \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[6].ce_out_i[6]_i_1_n_0 ;
  wire \GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[8].ce_out_i[8]_i_1_n_0 ;
  wire \GEN_BKEND_CE_REGISTERS[9].ce_out_i[9]_i_1_n_0 ;
  wire [4:0]\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] ;
  wire \MEM_DECODE_GEN[0].PER_CE_GEN[0].MULTIPLE_CES_THIS_CS_GEN.CE_I_n_0 ;
  wire Q;
  wire S0_AXI_ACLK;
  wire S0_AXI_ARREADY;
  wire S0_AXI_ARREADY_INST_0_i_1_n_0;
  wire S0_AXI_ARREADY_INST_0_i_2_n_0;
  wire S0_AXI_ARVALID;
  wire S0_AXI_BREADY;
  wire S0_AXI_RREADY;
  wire S0_AXI_WREADY;
  wire S0_AXI_WREADY_INST_0_i_1_n_0;
  wire S0_AXI_WREADY_INST_0_i_2_n_0;
  wire S0_AXI_WVALID;
  wire SYS_Rst_I;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire bus2ip_rnw_i;
  wire ce_expnd_i_0;
  wire ce_expnd_i_3;
  wire ce_expnd_i_5;
  wire ce_expnd_i_6;
  wire ce_expnd_i_8;
  wire cs_ce_clr;
  wire empty_error;
  wire empty_error_reg;
  wire error_detect;
  wire \fifo_length_i_reg[2] ;
  wire \fifo_length_i_reg[2]_0 ;
  wire \fifo_length_i_reg[3] ;
  wire full_error;
  wire full_error_reg;
  wire [0:2]ie_register;
  wire \ie_register_reg[2] ;
  wire is_read;
  wire \is_register_reg[2] ;
  wire is_write_reg;
  wire next_Data_Exists;
  wire [2:0]p_6_in;
  wire read_fsl_error_d1;
  wire read_fsl_error_d1_reg;
  wire read_fsl_error_d1_reg_0;
  wire [3:0]\rit_register_reg[0] ;
  wire [0:0]\rit_register_reg[3] ;
  wire s_axi_bvalid_i_reg;
  wire s_axi_bvalid_i_reg_0;
  wire \s_axi_rdata_i[0]_i_2_n_0 ;
  wire \s_axi_rdata_i[0]_i_3_n_0 ;
  wire \s_axi_rdata_i[0]_i_4_n_0 ;
  wire \s_axi_rdata_i[1]_i_2_n_0 ;
  wire \s_axi_rdata_i[1]_i_3_n_0 ;
  wire \s_axi_rdata_i[1]_i_4__0_n_0 ;
  wire \s_axi_rdata_i[2]_i_2_n_0 ;
  wire \s_axi_rdata_i[2]_i_3_n_0 ;
  wire \s_axi_rdata_i[2]_i_4_n_0 ;
  wire \s_axi_rdata_i[2]_i_5_n_0 ;
  wire \s_axi_rdata_i[2]_i_6_n_0 ;
  wire \s_axi_rdata_i[2]_i_7_n_0 ;
  wire \s_axi_rdata_i[2]_i_8_n_0 ;
  wire \s_axi_rdata_i[3]_i_3_n_0 ;
  wire [3:0]\s_axi_rdata_i_reg[3] ;
  wire \s_axi_rdata_i_reg[3]_0 ;
  wire s_axi_rvalid_i_reg;
  wire s_axi_rvalid_i_reg_0;
  wire s_axi_rvalid_i_reg_1;
  wire sit_detect_d0;
  wire [3:0]\sit_register_reg[0] ;
  wire [0:0]\sit_register_reg[3] ;
  wire \state_reg[0] ;
  wire [1:0]\state_reg[1] ;
  wire write_fsl_error;
  wire write_fsl_error_d1;

  LUT4 #(
    .INIT(16'h0004)) 
    \Addr_Counters[0].XORCY_I_i_2 
       (.I0(Bus_RNW_reg_reg_0),
        .I1(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I2(SYS_Rst_I),
        .I3(FDRE_I1),
        .O(CI));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \Addr_Counters[0].XORCY_I_i_3__0 
       (.I0(read_fsl_error_d1_reg),
        .I1(Bus_RNW_reg_reg_0),
        .I2(FSL0_S_Exists_I),
        .O(\Addr_Counters[0].FDRE_I ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'hFFEF)) 
    \Addr_Counters[0].XORCY_I_i_4 
       (.I0(FDRE_I1),
        .I1(SYS_Rst_I),
        .I2(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I3(Bus_RNW_reg_reg_0),
        .O(\fifo_length_i_reg[3] ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    Bus_RNW_reg_i_1
       (.I0(bus2ip_rnw_i),
        .I1(Q),
        .I2(Bus_RNW_reg_reg_0),
        .O(Bus_RNW_reg_i_1_n_0));
  FDRE Bus_RNW_reg_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(Bus_RNW_reg_i_1_n_0),
        .Q(Bus_RNW_reg_reg_0),
        .R(1'b0));
  FDRE \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(Q),
        .D(\MEM_DECODE_GEN[0].PER_CE_GEN[0].MULTIPLE_CES_THIS_CS_GEN.CE_I_n_0 ),
        .Q(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .R(cs_ce_clr));
  LUT3 #(
    .INIT(8'hFE)) 
    \GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1 
       (.I0(S0_AXI_ARREADY),
        .I1(S0_AXI_WREADY),
        .I2(SYS_Rst_I),
        .O(cs_ce_clr));
  FDRE \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg[10] 
       (.C(S0_AXI_ACLK),
        .CE(Q),
        .D(ce_expnd_i_0),
        .Q(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .R(cs_ce_clr));
  FDRE \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(Q),
        .D(ce_expnd_i_8),
        .Q(read_fsl_error_d1_reg),
        .R(cs_ce_clr));
  FDRE \GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg[4] 
       (.C(S0_AXI_ACLK),
        .CE(Q),
        .D(ce_expnd_i_6),
        .Q(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .R(cs_ce_clr));
  FDRE \GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg[5] 
       (.C(S0_AXI_ACLK),
        .CE(Q),
        .D(ce_expnd_i_5),
        .Q(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .R(cs_ce_clr));
  LUT5 #(
    .INIT(32'h10000000)) 
    \GEN_BKEND_CE_REGISTERS[6].ce_out_i[6]_i_1 
       (.I0(\bus2ip_addr_i_reg[5] [0]),
        .I1(\bus2ip_addr_i_reg[5] [3]),
        .I2(Q),
        .I3(\bus2ip_addr_i_reg[5] [1]),
        .I4(\bus2ip_addr_i_reg[5] [2]),
        .O(\GEN_BKEND_CE_REGISTERS[6].ce_out_i[6]_i_1_n_0 ));
  FDRE \GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg[6] 
       (.C(S0_AXI_ACLK),
        .CE(Q),
        .D(\GEN_BKEND_CE_REGISTERS[6].ce_out_i[6]_i_1_n_0 ),
        .Q(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .R(cs_ce_clr));
  FDRE \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg[7] 
       (.C(S0_AXI_ACLK),
        .CE(Q),
        .D(ce_expnd_i_3),
        .Q(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .R(cs_ce_clr));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h01000000)) 
    \GEN_BKEND_CE_REGISTERS[8].ce_out_i[8]_i_1 
       (.I0(\bus2ip_addr_i_reg[5] [1]),
        .I1(\bus2ip_addr_i_reg[5] [2]),
        .I2(\bus2ip_addr_i_reg[5] [0]),
        .I3(Q),
        .I4(\bus2ip_addr_i_reg[5] [3]),
        .O(\GEN_BKEND_CE_REGISTERS[8].ce_out_i[8]_i_1_n_0 ));
  FDRE \GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg[8] 
       (.C(S0_AXI_ACLK),
        .CE(Q),
        .D(\GEN_BKEND_CE_REGISTERS[8].ce_out_i[8]_i_1_n_0 ),
        .Q(\is_register_reg[2] ),
        .R(cs_ce_clr));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h10000000)) 
    \GEN_BKEND_CE_REGISTERS[9].ce_out_i[9]_i_1 
       (.I0(\bus2ip_addr_i_reg[5] [1]),
        .I1(\bus2ip_addr_i_reg[5] [2]),
        .I2(\bus2ip_addr_i_reg[5] [3]),
        .I3(\bus2ip_addr_i_reg[5] [0]),
        .I4(Q),
        .O(\GEN_BKEND_CE_REGISTERS[9].ce_out_i[9]_i_1_n_0 ));
  FDRE \GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg[9] 
       (.C(S0_AXI_ACLK),
        .CE(Q),
        .D(\GEN_BKEND_CE_REGISTERS[9].ce_out_i[9]_i_1_n_0 ),
        .Q(\ie_register_reg[2] ),
        .R(cs_ce_clr));
  design_1_mailbox_1_0_pselect_f_3 \MEM_DECODE_GEN[0].PER_CE_GEN[0].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] (\MEM_DECODE_GEN[0].PER_CE_GEN[0].MULTIPLE_CES_THIS_CS_GEN.CE_I_n_0 ),
        .Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ));
  design_1_mailbox_1_0_pselect_f__parameterized9 \MEM_DECODE_GEN[0].PER_CE_GEN[10].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_0(ce_expnd_i_0));
  design_1_mailbox_1_0_pselect_f__parameterized1 \MEM_DECODE_GEN[0].PER_CE_GEN[2].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_8(ce_expnd_i_8));
  design_1_mailbox_1_0_pselect_f__parameterized3 \MEM_DECODE_GEN[0].PER_CE_GEN[4].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_6(ce_expnd_i_6));
  design_1_mailbox_1_0_pselect_f__parameterized4 \MEM_DECODE_GEN[0].PER_CE_GEN[5].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_5(ce_expnd_i_5));
  design_1_mailbox_1_0_pselect_f__parameterized6 \MEM_DECODE_GEN[0].PER_CE_GEN[7].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_3(ce_expnd_i_3));
  LUT6 #(
    .INIT(64'hFFFF7F777F777F77)) 
    S0_AXI_ARREADY_INST_0
       (.I0(S0_AXI_ARREADY_INST_0_i_1_n_0),
        .I1(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .I2(S0_AXI_WREADY_INST_0_i_1_n_0),
        .I3(is_read),
        .I4(Bus_RNW_reg_reg_0),
        .I5(read_fsl_error_d1_reg),
        .O(S0_AXI_ARREADY));
  LUT6 #(
    .INIT(64'h0000FFFF0001FFFF)) 
    S0_AXI_ARREADY_INST_0_i_1
       (.I0(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I1(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .I2(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I3(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I4(Bus_RNW_reg_reg_0),
        .I5(\is_register_reg[2] ),
        .O(S0_AXI_ARREADY_INST_0_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h1F)) 
    S0_AXI_ARREADY_INST_0_i_2
       (.I0(\ie_register_reg[2] ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(Bus_RNW_reg_reg_0),
        .O(S0_AXI_ARREADY_INST_0_i_2_n_0));
  LUT3 #(
    .INIT(8'hF4)) 
    S0_AXI_WREADY_INST_0
       (.I0(S0_AXI_WREADY_INST_0_i_1_n_0),
        .I1(is_write_reg),
        .I2(S0_AXI_WREADY_INST_0_i_2_n_0),
        .O(S0_AXI_WREADY));
  LUT5 #(
    .INIT(32'hFFFEFFFF)) 
    S0_AXI_WREADY_INST_0_i_1
       (.I0(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] [0]),
        .I1(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] [1]),
        .I2(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] [3]),
        .I3(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] [2]),
        .I4(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] [4]),
        .O(S0_AXI_WREADY_INST_0_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000FFFF0000FFFE)) 
    S0_AXI_WREADY_INST_0_i_2
       (.I0(\ie_register_reg[2] ),
        .I1(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I2(\is_register_reg[2] ),
        .I3(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I4(Bus_RNW_reg_reg_0),
        .I5(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .O(S0_AXI_WREADY_INST_0_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hFF7F000F)) 
    data_Exists_I_i_1__0
       (.I0(Bus_RNW_reg_reg_0),
        .I1(read_fsl_error_d1_reg),
        .I2(FDRE_I1_0),
        .I3(\Addr_Counters[3].FDRE_I ),
        .I4(FSL0_S_Exists_I),
        .O(next_Data_Exists));
  LUT6 #(
    .INIT(64'h0000000000AABAAA)) 
    empty_error_i_1__0
       (.I0(empty_error),
        .I1(FSL0_S_Exists_I),
        .I2(read_fsl_error_d1_reg),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I5(SYS_Rst_I),
        .O(empty_error_reg));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h807F)) 
    \fifo_length_i[0]_i_1__0 
       (.I0(FSL0_S_Exists_I),
        .I1(Bus_RNW_reg_reg_0),
        .I2(read_fsl_error_d1_reg),
        .I3(FDRE_I1_0),
        .O(E));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h007F)) 
    \fifo_length_i[0]_i_3__0 
       (.I0(FSL0_S_Exists_I),
        .I1(Bus_RNW_reg_reg_0),
        .I2(read_fsl_error_d1_reg),
        .I3(FDRE_I1_0),
        .O(\fifo_length_i_reg[2] ));
  LUT6 #(
    .INIT(64'h0000000000EAAAEA)) 
    full_error_i_1
       (.I0(full_error),
        .I1(FDRE_I1),
        .I2(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I5(SYS_Rst_I),
        .O(full_error_reg));
  LUT6 #(
    .INIT(64'h444F444444444444)) 
    \is_register[0]_i_2 
       (.I0(write_fsl_error_d1),
        .I1(write_fsl_error),
        .I2(read_fsl_error_d1),
        .I3(FSL0_S_Exists_I),
        .I4(read_fsl_error_d1_reg),
        .I5(Bus_RNW_reg_reg_0),
        .O(error_detect));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h40)) 
    read_fsl_error_d1_i_1__0
       (.I0(FSL0_S_Exists_I),
        .I1(read_fsl_error_d1_reg),
        .I2(Bus_RNW_reg_reg_0),
        .O(read_fsl_error_d1_reg_0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \rit_register[0]_i_1 
       (.I0(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I1(Bus_RNW_reg_reg_0),
        .O(\rit_register_reg[3] ));
  LUT5 #(
    .INIT(32'h5D550C00)) 
    s_axi_bvalid_i_i_1
       (.I0(S0_AXI_BREADY),
        .I1(\state_reg[1] [1]),
        .I2(\state_reg[1] [0]),
        .I3(S0_AXI_WREADY),
        .I4(s_axi_bvalid_i_reg_0),
        .O(s_axi_bvalid_i_reg));
  LUT6 #(
    .INIT(64'hFFFFFFFF4C080808)) 
    \s_axi_rdata_i[0]_i_1 
       (.I0(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .I1(Bus_RNW_reg_reg_0),
        .I2(FSL0_S_Exists_I),
        .I3(empty_error),
        .I4(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I5(\s_axi_rdata_i[0]_i_2_n_0 ),
        .O(\s_axi_rdata_i_reg[3] [0]));
  LUT6 #(
    .INIT(64'hF200FFFFF200F200)) 
    \s_axi_rdata_i[0]_i_2 
       (.I0(p_6_in[0]),
        .I1(\s_axi_rdata_i[2]_i_6_n_0 ),
        .I2(\s_axi_rdata_i[0]_i_3_n_0 ),
        .I3(\s_axi_rdata_i[2]_i_4_n_0 ),
        .I4(\s_axi_rdata_i[0]_i_4_n_0 ),
        .I5(S0_AXI_ARREADY_INST_0_i_1_n_0),
        .O(\s_axi_rdata_i[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hB8008800)) 
    \s_axi_rdata_i[0]_i_3 
       (.I0(\sit_register_reg[0] [0]),
        .I1(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I2(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\rit_register_reg[0] [0]),
        .O(\s_axi_rdata_i[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0155FD553155FD55)) 
    \s_axi_rdata_i[0]_i_4 
       (.I0(FSL0_S_Data_I[0]),
        .I1(\ie_register_reg[2] ),
        .I2(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I3(Bus_RNW_reg_reg_0),
        .I4(ie_register[2]),
        .I5(p_6_in[0]),
        .O(\s_axi_rdata_i[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFF4FFF4FFFFFFF4)) 
    \s_axi_rdata_i[1]_i_1 
       (.I0(\s_axi_rdata_i_reg[3]_0 ),
        .I1(FSL0_S_Data_I[1]),
        .I2(\s_axi_rdata_i[1]_i_2_n_0 ),
        .I3(\s_axi_rdata_i[1]_i_3_n_0 ),
        .I4(\s_axi_rdata_i[2]_i_4_n_0 ),
        .I5(\s_axi_rdata_i[1]_i_4__0_n_0 ),
        .O(\s_axi_rdata_i_reg[3] [1]));
  LUT6 #(
    .INIT(64'hF080F080F0800080)) 
    \s_axi_rdata_i[1]_i_2 
       (.I0(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I1(full_error),
        .I2(Bus_RNW_reg_reg_0),
        .I3(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .I4(SYS_Rst_I),
        .I5(FDRE_I1),
        .O(\s_axi_rdata_i[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h8880880000000000)) 
    \s_axi_rdata_i[1]_i_3 
       (.I0(S0_AXI_ARREADY_INST_0_i_1_n_0),
        .I1(ie_register[1]),
        .I2(p_6_in[1]),
        .I3(\ie_register_reg[2] ),
        .I4(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I5(Bus_RNW_reg_reg_0),
        .O(\s_axi_rdata_i[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hDD0DDD0D0000DD0D)) 
    \s_axi_rdata_i[1]_i_4__0 
       (.I0(p_6_in[1]),
        .I1(\s_axi_rdata_i[2]_i_6_n_0 ),
        .I2(\rit_register_reg[0] [1]),
        .I3(\s_axi_rdata_i[2]_i_7_n_0 ),
        .I4(\sit_register_reg[0] [1]),
        .I5(\s_axi_rdata_i[2]_i_8_n_0 ),
        .O(\s_axi_rdata_i[1]_i_4__0_n_0 ));
  LUT6 #(
    .INIT(64'h88F888F8FFFF88F8)) 
    \s_axi_rdata_i[2]_i_1 
       (.I0(\s_axi_rdata_i[2]_i_2_n_0 ),
        .I1(sit_detect_d0),
        .I2(S0_AXI_ARREADY_INST_0_i_1_n_0),
        .I3(\s_axi_rdata_i[2]_i_3_n_0 ),
        .I4(\s_axi_rdata_i[2]_i_4_n_0 ),
        .I5(\s_axi_rdata_i[2]_i_5_n_0 ),
        .O(\s_axi_rdata_i_reg[3] [2]));
  LUT2 #(
    .INIT(4'h8)) 
    \s_axi_rdata_i[2]_i_2 
       (.I0(Bus_RNW_reg_reg_0),
        .I1(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .O(\s_axi_rdata_i[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h01553155FD55FD55)) 
    \s_axi_rdata_i[2]_i_3 
       (.I0(FSL0_S_Data_I[2]),
        .I1(\ie_register_reg[2] ),
        .I2(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I3(Bus_RNW_reg_reg_0),
        .I4(p_6_in[2]),
        .I5(ie_register[0]),
        .O(\s_axi_rdata_i[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h1111000011100000)) 
    \s_axi_rdata_i[2]_i_4 
       (.I0(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I1(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .I2(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I3(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I4(Bus_RNW_reg_reg_0),
        .I5(\is_register_reg[2] ),
        .O(\s_axi_rdata_i[2]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hDD0DDD0D0000DD0D)) 
    \s_axi_rdata_i[2]_i_5 
       (.I0(p_6_in[2]),
        .I1(\s_axi_rdata_i[2]_i_6_n_0 ),
        .I2(\rit_register_reg[0] [2]),
        .I3(\s_axi_rdata_i[2]_i_7_n_0 ),
        .I4(\sit_register_reg[0] [2]),
        .I5(\s_axi_rdata_i[2]_i_8_n_0 ),
        .O(\s_axi_rdata_i[2]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hEFFF)) 
    \s_axi_rdata_i[2]_i_6 
       (.I0(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I1(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I2(\is_register_reg[2] ),
        .I3(Bus_RNW_reg_reg_0),
        .O(\s_axi_rdata_i[2]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hBF)) 
    \s_axi_rdata_i[2]_i_7 
       (.I0(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I1(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I2(Bus_RNW_reg_reg_0),
        .O(\s_axi_rdata_i[2]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \s_axi_rdata_i[2]_i_8 
       (.I0(Bus_RNW_reg_reg_0),
        .I1(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .O(\s_axi_rdata_i[2]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'hA8FF)) 
    \s_axi_rdata_i[31]_i_3 
       (.I0(Bus_RNW_reg_reg_0),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\ie_register_reg[2] ),
        .I3(S0_AXI_ARREADY_INST_0_i_1_n_0),
        .O(\s_axi_rdata_i_reg[3]_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF4444F444)) 
    \s_axi_rdata_i[3]_i_1 
       (.I0(\s_axi_rdata_i_reg[3]_0 ),
        .I1(FSL0_S_Data_I[3]),
        .I2(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\fifo_length_i_reg[2]_0 ),
        .I5(\s_axi_rdata_i[3]_i_3_n_0 ),
        .O(\s_axi_rdata_i_reg[3] [3]));
  LUT6 #(
    .INIT(64'hA0A0800000008000)) 
    \s_axi_rdata_i[3]_i_3 
       (.I0(\s_axi_rdata_i[2]_i_4_n_0 ),
        .I1(\rit_register_reg[0] [3]),
        .I2(Bus_RNW_reg_reg_0),
        .I3(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I4(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I5(\sit_register_reg[0] [3]),
        .O(\s_axi_rdata_i[3]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h5D550C00)) 
    s_axi_rvalid_i_i_1
       (.I0(S0_AXI_RREADY),
        .I1(\state_reg[1] [0]),
        .I2(\state_reg[1] [1]),
        .I3(S0_AXI_ARREADY),
        .I4(s_axi_rvalid_i_reg_1),
        .O(s_axi_rvalid_i_reg));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \sit_register[0]_i_1 
       (.I0(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I1(Bus_RNW_reg_reg_0),
        .O(\sit_register_reg[3] ));
  LUT5 #(
    .INIT(32'hF4F4FFF0)) 
    \state[0]_i_1 
       (.I0(\state_reg[1] [0]),
        .I1(S0_AXI_WREADY),
        .I2(\state_reg[0] ),
        .I3(S0_AXI_ARVALID),
        .I4(\state_reg[1] [1]),
        .O(D[0]));
  LUT5 #(
    .INIT(32'h3F883FBB)) 
    \state[1]_i_1__0 
       (.I0(S0_AXI_ARREADY),
        .I1(\state_reg[1] [0]),
        .I2(s_axi_rvalid_i_reg_0),
        .I3(\state_reg[1] [1]),
        .I4(S0_AXI_WVALID),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h00E0)) 
    write_fsl_error_d1_i_1
       (.I0(FDRE_I1),
        .I1(SYS_Rst_I),
        .I2(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I3(Bus_RNW_reg_reg_0),
        .O(write_fsl_error));
endmodule

(* ORIG_REF_NAME = "address_decoder" *) 
module design_1_mailbox_1_0_address_decoder__parameterized0
   (\ie_register_reg[2] ,
    \is_register_reg[2] ,
    read_fsl_error_d1_reg,
    Bus_RNW_reg_reg_0,
    next_Data_Exists,
    \fifo_length_i_reg[2] ,
    E,
    error_detect,
    write_fsl_error,
    S1_AXI_ARREADY,
    S1_AXI_WREADY,
    D,
    CI,
    \fifo_length_i_reg[3] ,
    \s_axi_rdata_i_reg[3] ,
    \s_axi_rdata_i_reg[3]_0 ,
    \sit_register_reg[3] ,
    \rit_register_reg[3] ,
    s_axi_rvalid_i_reg,
    s_axi_bvalid_i_reg,
    \Addr_Counters[0].FDRE_I ,
    empty_error_reg,
    read_fsl_error_d1_reg_0,
    full_error_reg,
    Q,
    S1_AXI_ACLK,
    FDRE_I1,
    \Addr_Counters[3].FDRE_I ,
    FSL1_S_Exists_I,
    write_fsl_error_d1,
    read_fsl_error_d1,
    SYS_Rst_I,
    \state_reg[1] ,
    s_axi_rvalid_i_reg_0,
    S1_AXI_WVALID,
    is_read,
    FDRE_I1_0,
    \state_reg[0] ,
    S1_AXI_ARVALID,
    is_write_reg,
    FSL1_S_Data_I,
    full_error_reg_0,
    \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] ,
    p_6_in,
    \sit_register_reg[0] ,
    \rit_register_reg[0] ,
    sit_detect_d0,
    \fifo_length_i_reg[2]_0 ,
    ie_register,
    empty_error_reg_0,
    S1_AXI_RREADY,
    s_axi_rvalid_i_reg_1,
    S1_AXI_BREADY,
    s_axi_bvalid_i_reg_0,
    \bus2ip_addr_i_reg[5] ,
    bus2ip_rnw_i_reg);
  output \ie_register_reg[2] ;
  output \is_register_reg[2] ;
  output read_fsl_error_d1_reg;
  output Bus_RNW_reg_reg_0;
  output next_Data_Exists;
  output \fifo_length_i_reg[2] ;
  output [0:0]E;
  output error_detect;
  output write_fsl_error;
  output S1_AXI_ARREADY;
  output S1_AXI_WREADY;
  output [1:0]D;
  output CI;
  output \fifo_length_i_reg[3] ;
  output [3:0]\s_axi_rdata_i_reg[3] ;
  output \s_axi_rdata_i_reg[3]_0 ;
  output [0:0]\sit_register_reg[3] ;
  output [0:0]\rit_register_reg[3] ;
  output s_axi_rvalid_i_reg;
  output s_axi_bvalid_i_reg;
  output \Addr_Counters[0].FDRE_I ;
  output empty_error_reg;
  output read_fsl_error_d1_reg_0;
  output full_error_reg;
  input Q;
  input S1_AXI_ACLK;
  input FDRE_I1;
  input \Addr_Counters[3].FDRE_I ;
  input FSL1_S_Exists_I;
  input write_fsl_error_d1;
  input read_fsl_error_d1;
  input SYS_Rst_I;
  input [1:0]\state_reg[1] ;
  input s_axi_rvalid_i_reg_0;
  input S1_AXI_WVALID;
  input is_read;
  input FDRE_I1_0;
  input \state_reg[0] ;
  input S1_AXI_ARVALID;
  input is_write_reg;
  input [3:0]FSL1_S_Data_I;
  input full_error_reg_0;
  input [4:0]\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] ;
  input [2:0]p_6_in;
  input [3:0]\sit_register_reg[0] ;
  input [3:0]\rit_register_reg[0] ;
  input sit_detect_d0;
  input \fifo_length_i_reg[2]_0 ;
  input [0:2]ie_register;
  input empty_error_reg_0;
  input S1_AXI_RREADY;
  input s_axi_rvalid_i_reg_1;
  input S1_AXI_BREADY;
  input s_axi_bvalid_i_reg_0;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input bus2ip_rnw_i_reg;

  wire \Addr_Counters[0].FDRE_I ;
  wire \Addr_Counters[3].FDRE_I ;
  wire Bus_RNW_reg_i_1__0_n_0;
  wire Bus_RNW_reg_reg_0;
  wire CI;
  wire [1:0]D;
  wire [0:0]E;
  wire FDRE_I1;
  wire FDRE_I1_0;
  wire [3:0]FSL1_S_Data_I;
  wire FSL1_S_Exists_I;
  wire \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[6].ce_out_i[6]_i_1__0_n_0 ;
  wire \GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[8].ce_out_i[8]_i_1__0_n_0 ;
  wire \GEN_BKEND_CE_REGISTERS[9].ce_out_i[9]_i_1__0_n_0 ;
  wire [4:0]\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] ;
  wire \MEM_DECODE_GEN[0].PER_CE_GEN[0].MULTIPLE_CES_THIS_CS_GEN.CE_I_n_0 ;
  wire Q;
  wire S1_AXI_ACLK;
  wire S1_AXI_ARREADY;
  wire S1_AXI_ARREADY_INST_0_i_1_n_0;
  wire S1_AXI_ARREADY_INST_0_i_2_n_0;
  wire S1_AXI_ARVALID;
  wire S1_AXI_BREADY;
  wire S1_AXI_RREADY;
  wire S1_AXI_WREADY;
  wire S1_AXI_WREADY_INST_0_i_1_n_0;
  wire S1_AXI_WREADY_INST_0_i_2_n_0;
  wire S1_AXI_WVALID;
  wire SYS_Rst_I;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire bus2ip_rnw_i_reg;
  wire ce_expnd_i_0;
  wire ce_expnd_i_3;
  wire ce_expnd_i_5;
  wire ce_expnd_i_6;
  wire ce_expnd_i_8;
  wire cs_ce_clr;
  wire empty_error_reg;
  wire empty_error_reg_0;
  wire error_detect;
  wire \fifo_length_i_reg[2] ;
  wire \fifo_length_i_reg[2]_0 ;
  wire \fifo_length_i_reg[3] ;
  wire full_error_reg;
  wire full_error_reg_0;
  wire [0:2]ie_register;
  wire \ie_register_reg[2] ;
  wire is_read;
  wire \is_register_reg[2] ;
  wire is_write_reg;
  wire next_Data_Exists;
  wire [2:0]p_6_in;
  wire read_fsl_error_d1;
  wire read_fsl_error_d1_reg;
  wire read_fsl_error_d1_reg_0;
  wire [3:0]\rit_register_reg[0] ;
  wire [0:0]\rit_register_reg[3] ;
  wire s_axi_bvalid_i_reg;
  wire s_axi_bvalid_i_reg_0;
  wire \s_axi_rdata_i[0]_i_2__0_n_0 ;
  wire \s_axi_rdata_i[0]_i_3__0_n_0 ;
  wire \s_axi_rdata_i[0]_i_4__0_n_0 ;
  wire \s_axi_rdata_i[1]_i_2__0_n_0 ;
  wire \s_axi_rdata_i[1]_i_3__0_n_0 ;
  wire \s_axi_rdata_i[1]_i_4_n_0 ;
  wire \s_axi_rdata_i[2]_i_2__0_n_0 ;
  wire \s_axi_rdata_i[2]_i_3__0_n_0 ;
  wire \s_axi_rdata_i[2]_i_4__0_n_0 ;
  wire \s_axi_rdata_i[2]_i_5__0_n_0 ;
  wire \s_axi_rdata_i[2]_i_6__0_n_0 ;
  wire \s_axi_rdata_i[2]_i_7__0_n_0 ;
  wire \s_axi_rdata_i[2]_i_8__0_n_0 ;
  wire \s_axi_rdata_i[3]_i_3__0_n_0 ;
  wire [3:0]\s_axi_rdata_i_reg[3] ;
  wire \s_axi_rdata_i_reg[3]_0 ;
  wire s_axi_rvalid_i_reg;
  wire s_axi_rvalid_i_reg_0;
  wire s_axi_rvalid_i_reg_1;
  wire sit_detect_d0;
  wire [3:0]\sit_register_reg[0] ;
  wire [0:0]\sit_register_reg[3] ;
  wire \state_reg[0] ;
  wire [1:0]\state_reg[1] ;
  wire write_fsl_error;
  wire write_fsl_error_d1;

  LUT4 #(
    .INIT(16'h0004)) 
    \Addr_Counters[0].XORCY_I_i_2__0 
       (.I0(Bus_RNW_reg_reg_0),
        .I1(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I2(SYS_Rst_I),
        .I3(FDRE_I1_0),
        .O(CI));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \Addr_Counters[0].XORCY_I_i_3 
       (.I0(read_fsl_error_d1_reg),
        .I1(Bus_RNW_reg_reg_0),
        .I2(FSL1_S_Exists_I),
        .O(\Addr_Counters[0].FDRE_I ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT4 #(
    .INIT(16'hFFEF)) 
    \Addr_Counters[0].XORCY_I_i_4__0 
       (.I0(FDRE_I1_0),
        .I1(SYS_Rst_I),
        .I2(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I3(Bus_RNW_reg_reg_0),
        .O(\fifo_length_i_reg[3] ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    Bus_RNW_reg_i_1__0
       (.I0(bus2ip_rnw_i_reg),
        .I1(Q),
        .I2(Bus_RNW_reg_reg_0),
        .O(Bus_RNW_reg_i_1__0_n_0));
  FDRE Bus_RNW_reg_reg
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(Bus_RNW_reg_i_1__0_n_0),
        .Q(Bus_RNW_reg_reg_0),
        .R(1'b0));
  FDRE \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(Q),
        .D(\MEM_DECODE_GEN[0].PER_CE_GEN[0].MULTIPLE_CES_THIS_CS_GEN.CE_I_n_0 ),
        .Q(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .R(cs_ce_clr));
  LUT3 #(
    .INIT(8'hFE)) 
    \GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1__0 
       (.I0(S1_AXI_ARREADY),
        .I1(S1_AXI_WREADY),
        .I2(SYS_Rst_I),
        .O(cs_ce_clr));
  FDRE \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg[10] 
       (.C(S1_AXI_ACLK),
        .CE(Q),
        .D(ce_expnd_i_0),
        .Q(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .R(cs_ce_clr));
  FDRE \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(Q),
        .D(ce_expnd_i_8),
        .Q(read_fsl_error_d1_reg),
        .R(cs_ce_clr));
  FDRE \GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg[4] 
       (.C(S1_AXI_ACLK),
        .CE(Q),
        .D(ce_expnd_i_6),
        .Q(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .R(cs_ce_clr));
  FDRE \GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg[5] 
       (.C(S1_AXI_ACLK),
        .CE(Q),
        .D(ce_expnd_i_5),
        .Q(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .R(cs_ce_clr));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'h10000000)) 
    \GEN_BKEND_CE_REGISTERS[6].ce_out_i[6]_i_1__0 
       (.I0(\bus2ip_addr_i_reg[5] [0]),
        .I1(\bus2ip_addr_i_reg[5] [3]),
        .I2(Q),
        .I3(\bus2ip_addr_i_reg[5] [1]),
        .I4(\bus2ip_addr_i_reg[5] [2]),
        .O(\GEN_BKEND_CE_REGISTERS[6].ce_out_i[6]_i_1__0_n_0 ));
  FDRE \GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg[6] 
       (.C(S1_AXI_ACLK),
        .CE(Q),
        .D(\GEN_BKEND_CE_REGISTERS[6].ce_out_i[6]_i_1__0_n_0 ),
        .Q(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .R(cs_ce_clr));
  FDRE \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg[7] 
       (.C(S1_AXI_ACLK),
        .CE(Q),
        .D(ce_expnd_i_3),
        .Q(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .R(cs_ce_clr));
  LUT5 #(
    .INIT(32'h01000000)) 
    \GEN_BKEND_CE_REGISTERS[8].ce_out_i[8]_i_1__0 
       (.I0(\bus2ip_addr_i_reg[5] [1]),
        .I1(\bus2ip_addr_i_reg[5] [2]),
        .I2(\bus2ip_addr_i_reg[5] [0]),
        .I3(Q),
        .I4(\bus2ip_addr_i_reg[5] [3]),
        .O(\GEN_BKEND_CE_REGISTERS[8].ce_out_i[8]_i_1__0_n_0 ));
  FDRE \GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg[8] 
       (.C(S1_AXI_ACLK),
        .CE(Q),
        .D(\GEN_BKEND_CE_REGISTERS[8].ce_out_i[8]_i_1__0_n_0 ),
        .Q(\is_register_reg[2] ),
        .R(cs_ce_clr));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'h10000000)) 
    \GEN_BKEND_CE_REGISTERS[9].ce_out_i[9]_i_1__0 
       (.I0(\bus2ip_addr_i_reg[5] [1]),
        .I1(\bus2ip_addr_i_reg[5] [2]),
        .I2(\bus2ip_addr_i_reg[5] [3]),
        .I3(\bus2ip_addr_i_reg[5] [0]),
        .I4(Q),
        .O(\GEN_BKEND_CE_REGISTERS[9].ce_out_i[9]_i_1__0_n_0 ));
  FDRE \GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg[9] 
       (.C(S1_AXI_ACLK),
        .CE(Q),
        .D(\GEN_BKEND_CE_REGISTERS[9].ce_out_i[9]_i_1__0_n_0 ),
        .Q(\ie_register_reg[2] ),
        .R(cs_ce_clr));
  design_1_mailbox_1_0_pselect_f \MEM_DECODE_GEN[0].PER_CE_GEN[0].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] (\MEM_DECODE_GEN[0].PER_CE_GEN[0].MULTIPLE_CES_THIS_CS_GEN.CE_I_n_0 ),
        .Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ));
  design_1_mailbox_1_0_pselect_f__parameterized19 \MEM_DECODE_GEN[0].PER_CE_GEN[10].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_0(ce_expnd_i_0));
  design_1_mailbox_1_0_pselect_f__parameterized11 \MEM_DECODE_GEN[0].PER_CE_GEN[2].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_8(ce_expnd_i_8));
  design_1_mailbox_1_0_pselect_f__parameterized13 \MEM_DECODE_GEN[0].PER_CE_GEN[4].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_6(ce_expnd_i_6));
  design_1_mailbox_1_0_pselect_f__parameterized14 \MEM_DECODE_GEN[0].PER_CE_GEN[5].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_5(ce_expnd_i_5));
  design_1_mailbox_1_0_pselect_f__parameterized16 \MEM_DECODE_GEN[0].PER_CE_GEN[7].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_3(ce_expnd_i_3));
  LUT6 #(
    .INIT(64'hFFFF7F777F777F77)) 
    S1_AXI_ARREADY_INST_0
       (.I0(S1_AXI_ARREADY_INST_0_i_1_n_0),
        .I1(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .I2(S1_AXI_WREADY_INST_0_i_1_n_0),
        .I3(is_read),
        .I4(Bus_RNW_reg_reg_0),
        .I5(read_fsl_error_d1_reg),
        .O(S1_AXI_ARREADY));
  LUT6 #(
    .INIT(64'h0000FFFF0001FFFF)) 
    S1_AXI_ARREADY_INST_0_i_1
       (.I0(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I1(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .I2(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I3(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I4(Bus_RNW_reg_reg_0),
        .I5(\is_register_reg[2] ),
        .O(S1_AXI_ARREADY_INST_0_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h1F)) 
    S1_AXI_ARREADY_INST_0_i_2
       (.I0(\ie_register_reg[2] ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(Bus_RNW_reg_reg_0),
        .O(S1_AXI_ARREADY_INST_0_i_2_n_0));
  LUT3 #(
    .INIT(8'hF4)) 
    S1_AXI_WREADY_INST_0
       (.I0(S1_AXI_WREADY_INST_0_i_1_n_0),
        .I1(is_write_reg),
        .I2(S1_AXI_WREADY_INST_0_i_2_n_0),
        .O(S1_AXI_WREADY));
  LUT5 #(
    .INIT(32'hFFFEFFFF)) 
    S1_AXI_WREADY_INST_0_i_1
       (.I0(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] [0]),
        .I1(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] [1]),
        .I2(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] [3]),
        .I3(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] [2]),
        .I4(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] [4]),
        .O(S1_AXI_WREADY_INST_0_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000FFFF0000FFFE)) 
    S1_AXI_WREADY_INST_0_i_2
       (.I0(\ie_register_reg[2] ),
        .I1(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I2(\is_register_reg[2] ),
        .I3(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I4(Bus_RNW_reg_reg_0),
        .I5(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .O(S1_AXI_WREADY_INST_0_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'hFF7F000F)) 
    data_Exists_I_i_1
       (.I0(Bus_RNW_reg_reg_0),
        .I1(read_fsl_error_d1_reg),
        .I2(FDRE_I1),
        .I3(\Addr_Counters[3].FDRE_I ),
        .I4(FSL1_S_Exists_I),
        .O(next_Data_Exists));
  LUT6 #(
    .INIT(64'h0000000000AABAAA)) 
    empty_error_i_1
       (.I0(empty_error_reg_0),
        .I1(FSL1_S_Exists_I),
        .I2(read_fsl_error_d1_reg),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I5(SYS_Rst_I),
        .O(empty_error_reg));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT4 #(
    .INIT(16'h807F)) 
    \fifo_length_i[0]_i_1 
       (.I0(FSL1_S_Exists_I),
        .I1(Bus_RNW_reg_reg_0),
        .I2(read_fsl_error_d1_reg),
        .I3(FDRE_I1),
        .O(E));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT4 #(
    .INIT(16'h007F)) 
    \fifo_length_i[0]_i_3 
       (.I0(FSL1_S_Exists_I),
        .I1(Bus_RNW_reg_reg_0),
        .I2(read_fsl_error_d1_reg),
        .I3(FDRE_I1),
        .O(\fifo_length_i_reg[2] ));
  LUT6 #(
    .INIT(64'h0000000000EAAAEA)) 
    full_error_i_1__0
       (.I0(full_error_reg_0),
        .I1(FDRE_I1_0),
        .I2(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I5(SYS_Rst_I),
        .O(full_error_reg));
  LUT6 #(
    .INIT(64'h444F444444444444)) 
    \is_register[0]_i_2__0 
       (.I0(write_fsl_error_d1),
        .I1(write_fsl_error),
        .I2(read_fsl_error_d1),
        .I3(FSL1_S_Exists_I),
        .I4(read_fsl_error_d1_reg),
        .I5(Bus_RNW_reg_reg_0),
        .O(error_detect));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'h40)) 
    read_fsl_error_d1_i_1
       (.I0(FSL1_S_Exists_I),
        .I1(read_fsl_error_d1_reg),
        .I2(Bus_RNW_reg_reg_0),
        .O(read_fsl_error_d1_reg_0));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \rit_register[0]_i_1__0 
       (.I0(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I1(Bus_RNW_reg_reg_0),
        .O(\rit_register_reg[3] ));
  LUT5 #(
    .INIT(32'h5D550C00)) 
    s_axi_bvalid_i_i_1__0
       (.I0(S1_AXI_BREADY),
        .I1(\state_reg[1] [1]),
        .I2(\state_reg[1] [0]),
        .I3(S1_AXI_WREADY),
        .I4(s_axi_bvalid_i_reg_0),
        .O(s_axi_bvalid_i_reg));
  LUT5 #(
    .INIT(32'hF4F4FFF4)) 
    \s_axi_rdata_i[0]_i_1__0 
       (.I0(\s_axi_rdata_i[0]_i_2__0_n_0 ),
        .I1(S1_AXI_ARREADY_INST_0_i_1_n_0),
        .I2(\s_axi_rdata_i[0]_i_3__0_n_0 ),
        .I3(\s_axi_rdata_i[2]_i_4__0_n_0 ),
        .I4(\s_axi_rdata_i[0]_i_4__0_n_0 ),
        .O(\s_axi_rdata_i_reg[3] [0]));
  LUT6 #(
    .INIT(64'h01553155FD55FD55)) 
    \s_axi_rdata_i[0]_i_2__0 
       (.I0(FSL1_S_Data_I[0]),
        .I1(\ie_register_reg[2] ),
        .I2(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I3(Bus_RNW_reg_reg_0),
        .I4(p_6_in[0]),
        .I5(ie_register[2]),
        .O(\s_axi_rdata_i[0]_i_2__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'h74004400)) 
    \s_axi_rdata_i[0]_i_3__0 
       (.I0(FSL1_S_Exists_I),
        .I1(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .I2(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I3(Bus_RNW_reg_reg_0),
        .I4(empty_error_reg_0),
        .O(\s_axi_rdata_i[0]_i_3__0_n_0 ));
  LUT6 #(
    .INIT(64'hDD0DDD0D0000DD0D)) 
    \s_axi_rdata_i[0]_i_4__0 
       (.I0(p_6_in[0]),
        .I1(\s_axi_rdata_i[2]_i_6__0_n_0 ),
        .I2(\sit_register_reg[0] [0]),
        .I3(\s_axi_rdata_i[2]_i_8__0_n_0 ),
        .I4(\rit_register_reg[0] [0]),
        .I5(\s_axi_rdata_i[2]_i_7__0_n_0 ),
        .O(\s_axi_rdata_i[0]_i_4__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFF4FFF4FFFFFFF4)) 
    \s_axi_rdata_i[1]_i_1__0 
       (.I0(\s_axi_rdata_i[1]_i_2__0_n_0 ),
        .I1(\s_axi_rdata_i[2]_i_4__0_n_0 ),
        .I2(\s_axi_rdata_i[1]_i_3__0_n_0 ),
        .I3(\s_axi_rdata_i[1]_i_4_n_0 ),
        .I4(FSL1_S_Data_I[1]),
        .I5(\s_axi_rdata_i_reg[3]_0 ),
        .O(\s_axi_rdata_i_reg[3] [1]));
  LUT6 #(
    .INIT(64'hDD0DDD0D0000DD0D)) 
    \s_axi_rdata_i[1]_i_2__0 
       (.I0(p_6_in[1]),
        .I1(\s_axi_rdata_i[2]_i_6__0_n_0 ),
        .I2(\sit_register_reg[0] [1]),
        .I3(\s_axi_rdata_i[2]_i_8__0_n_0 ),
        .I4(\rit_register_reg[0] [1]),
        .I5(\s_axi_rdata_i[2]_i_7__0_n_0 ),
        .O(\s_axi_rdata_i[1]_i_2__0_n_0 ));
  LUT6 #(
    .INIT(64'h8880880000000000)) 
    \s_axi_rdata_i[1]_i_3__0 
       (.I0(S1_AXI_ARREADY_INST_0_i_1_n_0),
        .I1(ie_register[1]),
        .I2(p_6_in[1]),
        .I3(\ie_register_reg[2] ),
        .I4(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I5(Bus_RNW_reg_reg_0),
        .O(\s_axi_rdata_i[1]_i_3__0_n_0 ));
  LUT6 #(
    .INIT(64'hEFE00000E0E00000)) 
    \s_axi_rdata_i[1]_i_4 
       (.I0(SYS_Rst_I),
        .I1(FDRE_I1_0),
        .I2(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .I3(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I4(Bus_RNW_reg_reg_0),
        .I5(full_error_reg_0),
        .O(\s_axi_rdata_i[1]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h88F888F8FFFF88F8)) 
    \s_axi_rdata_i[2]_i_1__0 
       (.I0(\s_axi_rdata_i[2]_i_2__0_n_0 ),
        .I1(sit_detect_d0),
        .I2(S1_AXI_ARREADY_INST_0_i_1_n_0),
        .I3(\s_axi_rdata_i[2]_i_3__0_n_0 ),
        .I4(\s_axi_rdata_i[2]_i_4__0_n_0 ),
        .I5(\s_axi_rdata_i[2]_i_5__0_n_0 ),
        .O(\s_axi_rdata_i_reg[3] [2]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s_axi_rdata_i[2]_i_2__0 
       (.I0(Bus_RNW_reg_reg_0),
        .I1(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .O(\s_axi_rdata_i[2]_i_2__0_n_0 ));
  LUT6 #(
    .INIT(64'h01553155FD55FD55)) 
    \s_axi_rdata_i[2]_i_3__0 
       (.I0(FSL1_S_Data_I[2]),
        .I1(\ie_register_reg[2] ),
        .I2(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I3(Bus_RNW_reg_reg_0),
        .I4(p_6_in[2]),
        .I5(ie_register[0]),
        .O(\s_axi_rdata_i[2]_i_3__0_n_0 ));
  LUT6 #(
    .INIT(64'h1111000011100000)) 
    \s_axi_rdata_i[2]_i_4__0 
       (.I0(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I1(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .I2(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I3(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I4(Bus_RNW_reg_reg_0),
        .I5(\is_register_reg[2] ),
        .O(\s_axi_rdata_i[2]_i_4__0_n_0 ));
  LUT6 #(
    .INIT(64'hDD0DDD0D0000DD0D)) 
    \s_axi_rdata_i[2]_i_5__0 
       (.I0(p_6_in[2]),
        .I1(\s_axi_rdata_i[2]_i_6__0_n_0 ),
        .I2(\rit_register_reg[0] [2]),
        .I3(\s_axi_rdata_i[2]_i_7__0_n_0 ),
        .I4(\sit_register_reg[0] [2]),
        .I5(\s_axi_rdata_i[2]_i_8__0_n_0 ),
        .O(\s_axi_rdata_i[2]_i_5__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'hEFFF)) 
    \s_axi_rdata_i[2]_i_6__0 
       (.I0(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I1(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I2(\is_register_reg[2] ),
        .I3(Bus_RNW_reg_reg_0),
        .O(\s_axi_rdata_i[2]_i_6__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hBF)) 
    \s_axi_rdata_i[2]_i_7__0 
       (.I0(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I1(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I2(Bus_RNW_reg_reg_0),
        .O(\s_axi_rdata_i[2]_i_7__0_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \s_axi_rdata_i[2]_i_8__0 
       (.I0(Bus_RNW_reg_reg_0),
        .I1(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .O(\s_axi_rdata_i[2]_i_8__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT4 #(
    .INIT(16'hA8FF)) 
    \s_axi_rdata_i[31]_i_3__0 
       (.I0(Bus_RNW_reg_reg_0),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\ie_register_reg[2] ),
        .I3(S1_AXI_ARREADY_INST_0_i_1_n_0),
        .O(\s_axi_rdata_i_reg[3]_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF4040FF40)) 
    \s_axi_rdata_i[3]_i_1__0 
       (.I0(\fifo_length_i_reg[2]_0 ),
        .I1(Bus_RNW_reg_reg_0),
        .I2(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .I3(FSL1_S_Data_I[3]),
        .I4(\s_axi_rdata_i_reg[3]_0 ),
        .I5(\s_axi_rdata_i[3]_i_3__0_n_0 ),
        .O(\s_axi_rdata_i_reg[3] [3]));
  LUT6 #(
    .INIT(64'hA0A0800000008000)) 
    \s_axi_rdata_i[3]_i_3__0 
       (.I0(\s_axi_rdata_i[2]_i_4__0_n_0 ),
        .I1(\rit_register_reg[0] [3]),
        .I2(Bus_RNW_reg_reg_0),
        .I3(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I4(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I5(\sit_register_reg[0] [3]),
        .O(\s_axi_rdata_i[3]_i_3__0_n_0 ));
  LUT5 #(
    .INIT(32'h5D550C00)) 
    s_axi_rvalid_i_i_1__0
       (.I0(S1_AXI_RREADY),
        .I1(\state_reg[1] [0]),
        .I2(\state_reg[1] [1]),
        .I3(S1_AXI_ARREADY),
        .I4(s_axi_rvalid_i_reg_1),
        .O(s_axi_rvalid_i_reg));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \sit_register[0]_i_1__0 
       (.I0(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I1(Bus_RNW_reg_reg_0),
        .O(\sit_register_reg[3] ));
  LUT5 #(
    .INIT(32'hF4F4FFF0)) 
    \state[0]_i_1__0 
       (.I0(\state_reg[1] [0]),
        .I1(S1_AXI_WREADY),
        .I2(\state_reg[0] ),
        .I3(S1_AXI_ARVALID),
        .I4(\state_reg[1] [1]),
        .O(D[0]));
  LUT5 #(
    .INIT(32'h3F883FBB)) 
    \state[1]_i_1 
       (.I0(S1_AXI_ARREADY),
        .I1(\state_reg[1] [0]),
        .I2(s_axi_rvalid_i_reg_0),
        .I3(\state_reg[1] [1]),
        .I4(S1_AXI_WVALID),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT4 #(
    .INIT(16'h00E0)) 
    write_fsl_error_d1_i_1__0
       (.I0(FDRE_I1_0),
        .I1(SYS_Rst_I),
        .I2(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I3(Bus_RNW_reg_reg_0),
        .O(write_fsl_error));
endmodule

(* ORIG_REF_NAME = "axi_lite_ipif" *) 
module design_1_mailbox_1_0_axi_lite_ipif
   (\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ,
    \GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ,
    read_fsl_error_d1_reg,
    Bus_RNW_reg_reg,
    S0_AXI_RVALID,
    S0_AXI_BVALID,
    error_detect,
    write_fsl_error,
    S0_AXI_WREADY,
    S0_AXI_ARREADY,
    CI,
    \fifo_length_i_reg[3] ,
    \s_axi_rdata_i_reg[3] ,
    next_Data_Exists,
    \fifo_length_i_reg[2] ,
    E,
    \sit_register_reg[3] ,
    \rit_register_reg[3] ,
    rit_detect_d1_reg,
    sit_detect_d1_reg,
    full_error_reg,
    \Addr_Counters[0].FDRE_I ,
    empty_error_reg,
    read_fsl_error_d1_reg_0,
    S0_AXI_RDATA,
    SYS_Rst_I,
    S0_AXI_ACLK,
    write_fsl_error_d1,
    read_fsl_error_d1,
    FSL0_S_Exists_I,
    FDRE_I1,
    S0_AXI_ARVALID,
    FSL0_S_Data_I,
    full_error,
    FDRE_I1_0,
    \Addr_Counters[3].FDRE_I ,
    empty_error,
    p_6_in,
    Q,
    \rit_register_reg[0] ,
    sit_detect_d0,
    \fifo_length_i_reg[2]_0 ,
    ie_register,
    S0_AXI_WVALID,
    S0_AXI_AWVALID,
    S0_AXI_ARADDR,
    S0_AXI_AWADDR,
    S0_AXI_RREADY,
    S0_AXI_BREADY,
    \fifo_length_i_reg[2]_1 ,
    \fifo_length_i_reg[2]_2 ,
    D);
  output \GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ;
  output \GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ;
  output read_fsl_error_d1_reg;
  output Bus_RNW_reg_reg;
  output S0_AXI_RVALID;
  output S0_AXI_BVALID;
  output error_detect;
  output write_fsl_error;
  output S0_AXI_WREADY;
  output S0_AXI_ARREADY;
  output CI;
  output \fifo_length_i_reg[3] ;
  output \s_axi_rdata_i_reg[3] ;
  output next_Data_Exists;
  output \fifo_length_i_reg[2] ;
  output [0:0]E;
  output [0:0]\sit_register_reg[3] ;
  output [0:0]\rit_register_reg[3] ;
  output rit_detect_d1_reg;
  output sit_detect_d1_reg;
  output full_error_reg;
  output \Addr_Counters[0].FDRE_I ;
  output empty_error_reg;
  output read_fsl_error_d1_reg_0;
  output [31:0]S0_AXI_RDATA;
  input SYS_Rst_I;
  input S0_AXI_ACLK;
  input write_fsl_error_d1;
  input read_fsl_error_d1;
  input FSL0_S_Exists_I;
  input FDRE_I1;
  input S0_AXI_ARVALID;
  input [3:0]FSL0_S_Data_I;
  input full_error;
  input FDRE_I1_0;
  input \Addr_Counters[3].FDRE_I ;
  input empty_error;
  input [2:0]p_6_in;
  input [3:0]Q;
  input [3:0]\rit_register_reg[0] ;
  input sit_detect_d0;
  input \fifo_length_i_reg[2]_0 ;
  input [0:2]ie_register;
  input S0_AXI_WVALID;
  input S0_AXI_AWVALID;
  input [3:0]S0_AXI_ARADDR;
  input [3:0]S0_AXI_AWADDR;
  input S0_AXI_RREADY;
  input S0_AXI_BREADY;
  input [2:0]\fifo_length_i_reg[2]_1 ;
  input [2:0]\fifo_length_i_reg[2]_2 ;
  input [27:0]D;

  wire \Addr_Counters[0].FDRE_I ;
  wire \Addr_Counters[3].FDRE_I ;
  wire Bus_RNW_reg_reg;
  wire CI;
  wire [27:0]D;
  wire [0:0]E;
  wire FDRE_I1;
  wire FDRE_I1_0;
  wire [3:0]FSL0_S_Data_I;
  wire FSL0_S_Exists_I;
  wire \GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ;
  wire [3:0]Q;
  wire S0_AXI_ACLK;
  wire [3:0]S0_AXI_ARADDR;
  wire S0_AXI_ARREADY;
  wire S0_AXI_ARVALID;
  wire [3:0]S0_AXI_AWADDR;
  wire S0_AXI_AWVALID;
  wire S0_AXI_BREADY;
  wire S0_AXI_BVALID;
  wire [31:0]S0_AXI_RDATA;
  wire S0_AXI_RREADY;
  wire S0_AXI_RVALID;
  wire S0_AXI_WREADY;
  wire S0_AXI_WVALID;
  wire SYS_Rst_I;
  wire empty_error;
  wire empty_error_reg;
  wire error_detect;
  wire \fifo_length_i_reg[2] ;
  wire \fifo_length_i_reg[2]_0 ;
  wire [2:0]\fifo_length_i_reg[2]_1 ;
  wire [2:0]\fifo_length_i_reg[2]_2 ;
  wire \fifo_length_i_reg[3] ;
  wire full_error;
  wire full_error_reg;
  wire [0:2]ie_register;
  wire next_Data_Exists;
  wire [2:0]p_6_in;
  wire read_fsl_error_d1;
  wire read_fsl_error_d1_reg;
  wire read_fsl_error_d1_reg_0;
  wire rit_detect_d1_reg;
  wire [3:0]\rit_register_reg[0] ;
  wire [0:0]\rit_register_reg[3] ;
  wire \s_axi_rdata_i_reg[3] ;
  wire sit_detect_d0;
  wire sit_detect_d1_reg;
  wire [0:0]\sit_register_reg[3] ;
  wire write_fsl_error;
  wire write_fsl_error_d1;

  design_1_mailbox_1_0_slave_attachment I_SLAVE_ATTACHMENT
       (.\Addr_Counters[0].FDRE_I (\Addr_Counters[0].FDRE_I ),
        .\Addr_Counters[3].FDRE_I (\Addr_Counters[3].FDRE_I ),
        .Bus_RNW_reg_reg(Bus_RNW_reg_reg),
        .CI(CI),
        .D(D),
        .E(E),
        .FDRE_I1(FDRE_I1),
        .FDRE_I1_0(FDRE_I1_0),
        .FSL0_S_Data_I(FSL0_S_Data_I),
        .FSL0_S_Exists_I(FSL0_S_Exists_I),
        .Q(Q),
        .S0_AXI_ACLK(S0_AXI_ACLK),
        .S0_AXI_ARADDR(S0_AXI_ARADDR),
        .S0_AXI_ARREADY(S0_AXI_ARREADY),
        .S0_AXI_ARVALID(S0_AXI_ARVALID),
        .S0_AXI_AWADDR(S0_AXI_AWADDR),
        .S0_AXI_AWVALID(S0_AXI_AWVALID),
        .S0_AXI_BREADY(S0_AXI_BREADY),
        .S0_AXI_BVALID(S0_AXI_BVALID),
        .S0_AXI_RDATA(S0_AXI_RDATA),
        .S0_AXI_RREADY(S0_AXI_RREADY),
        .S0_AXI_RVALID(S0_AXI_RVALID),
        .S0_AXI_WREADY(S0_AXI_WREADY),
        .S0_AXI_WVALID(S0_AXI_WVALID),
        .SYS_Rst_I(SYS_Rst_I),
        .empty_error(empty_error),
        .empty_error_reg(empty_error_reg),
        .error_detect(error_detect),
        .\fifo_length_i_reg[2] (\fifo_length_i_reg[2] ),
        .\fifo_length_i_reg[2]_0 (\fifo_length_i_reg[2]_0 ),
        .\fifo_length_i_reg[2]_1 (\fifo_length_i_reg[2]_1 ),
        .\fifo_length_i_reg[2]_2 (\fifo_length_i_reg[2]_2 ),
        .\fifo_length_i_reg[3] (\fifo_length_i_reg[3] ),
        .full_error(full_error),
        .full_error_reg(full_error_reg),
        .ie_register(ie_register),
        .\ie_register_reg[2] (\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .\is_register_reg[2] (\GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .next_Data_Exists(next_Data_Exists),
        .p_6_in(p_6_in),
        .read_fsl_error_d1(read_fsl_error_d1),
        .read_fsl_error_d1_reg(read_fsl_error_d1_reg),
        .read_fsl_error_d1_reg_0(read_fsl_error_d1_reg_0),
        .rit_detect_d1_reg(rit_detect_d1_reg),
        .\rit_register_reg[0] (\rit_register_reg[0] ),
        .\rit_register_reg[3] (\rit_register_reg[3] ),
        .\s_axi_rdata_i_reg[3]_0 (\s_axi_rdata_i_reg[3] ),
        .sit_detect_d0(sit_detect_d0),
        .sit_detect_d1_reg(sit_detect_d1_reg),
        .\sit_register_reg[3] (\sit_register_reg[3] ),
        .write_fsl_error(write_fsl_error),
        .write_fsl_error_d1(write_fsl_error_d1));
endmodule

(* ORIG_REF_NAME = "axi_lite_ipif" *) 
module design_1_mailbox_1_0_axi_lite_ipif__parameterized1
   (\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ,
    \GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ,
    read_fsl_error_d1_reg,
    Bus_RNW_reg_reg,
    S1_AXI_RVALID,
    S1_AXI_BVALID,
    next_Data_Exists,
    \fifo_length_i_reg[2] ,
    E,
    error_detect,
    write_fsl_error,
    S1_AXI_ARREADY,
    S1_AXI_WREADY,
    CI,
    \fifo_length_i_reg[3] ,
    \s_axi_rdata_i_reg[3] ,
    \sit_register_reg[3] ,
    \rit_register_reg[3] ,
    rit_detect_d1_reg,
    sit_detect_d1_reg,
    \Addr_Counters[0].FDRE_I ,
    empty_error_reg,
    read_fsl_error_d1_reg_0,
    full_error_reg,
    S1_AXI_RDATA,
    SYS_Rst_I,
    S1_AXI_ACLK,
    FDRE_I1,
    \Addr_Counters[3].FDRE_I ,
    FSL1_S_Exists_I,
    write_fsl_error_d1,
    read_fsl_error_d1,
    FDRE_I1_0,
    S1_AXI_ARVALID,
    FSL1_S_Data_I,
    full_error_reg_0,
    p_6_in,
    Q,
    \rit_register_reg[0] ,
    sit_detect_d0,
    \fifo_length_i_reg[2]_0 ,
    ie_register,
    empty_error_reg_0,
    S1_AXI_WVALID,
    S1_AXI_AWVALID,
    S1_AXI_ARADDR,
    S1_AXI_AWADDR,
    S1_AXI_RREADY,
    S1_AXI_BREADY,
    \fifo_length_i_reg[2]_1 ,
    \fifo_length_i_reg[2]_2 ,
    D);
  output \GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ;
  output \GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ;
  output read_fsl_error_d1_reg;
  output Bus_RNW_reg_reg;
  output S1_AXI_RVALID;
  output S1_AXI_BVALID;
  output next_Data_Exists;
  output \fifo_length_i_reg[2] ;
  output [0:0]E;
  output error_detect;
  output write_fsl_error;
  output S1_AXI_ARREADY;
  output S1_AXI_WREADY;
  output CI;
  output \fifo_length_i_reg[3] ;
  output \s_axi_rdata_i_reg[3] ;
  output [0:0]\sit_register_reg[3] ;
  output [0:0]\rit_register_reg[3] ;
  output rit_detect_d1_reg;
  output sit_detect_d1_reg;
  output \Addr_Counters[0].FDRE_I ;
  output empty_error_reg;
  output read_fsl_error_d1_reg_0;
  output full_error_reg;
  output [31:0]S1_AXI_RDATA;
  input SYS_Rst_I;
  input S1_AXI_ACLK;
  input FDRE_I1;
  input \Addr_Counters[3].FDRE_I ;
  input FSL1_S_Exists_I;
  input write_fsl_error_d1;
  input read_fsl_error_d1;
  input FDRE_I1_0;
  input S1_AXI_ARVALID;
  input [3:0]FSL1_S_Data_I;
  input full_error_reg_0;
  input [2:0]p_6_in;
  input [3:0]Q;
  input [3:0]\rit_register_reg[0] ;
  input sit_detect_d0;
  input \fifo_length_i_reg[2]_0 ;
  input [0:2]ie_register;
  input empty_error_reg_0;
  input S1_AXI_WVALID;
  input S1_AXI_AWVALID;
  input [3:0]S1_AXI_ARADDR;
  input [3:0]S1_AXI_AWADDR;
  input S1_AXI_RREADY;
  input S1_AXI_BREADY;
  input [2:0]\fifo_length_i_reg[2]_1 ;
  input [2:0]\fifo_length_i_reg[2]_2 ;
  input [27:0]D;

  wire \Addr_Counters[0].FDRE_I ;
  wire \Addr_Counters[3].FDRE_I ;
  wire Bus_RNW_reg_reg;
  wire CI;
  wire [27:0]D;
  wire [0:0]E;
  wire FDRE_I1;
  wire FDRE_I1_0;
  wire [3:0]FSL1_S_Data_I;
  wire FSL1_S_Exists_I;
  wire \GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ;
  wire [3:0]Q;
  wire S1_AXI_ACLK;
  wire [3:0]S1_AXI_ARADDR;
  wire S1_AXI_ARREADY;
  wire S1_AXI_ARVALID;
  wire [3:0]S1_AXI_AWADDR;
  wire S1_AXI_AWVALID;
  wire S1_AXI_BREADY;
  wire S1_AXI_BVALID;
  wire [31:0]S1_AXI_RDATA;
  wire S1_AXI_RREADY;
  wire S1_AXI_RVALID;
  wire S1_AXI_WREADY;
  wire S1_AXI_WVALID;
  wire SYS_Rst_I;
  wire empty_error_reg;
  wire empty_error_reg_0;
  wire error_detect;
  wire \fifo_length_i_reg[2] ;
  wire \fifo_length_i_reg[2]_0 ;
  wire [2:0]\fifo_length_i_reg[2]_1 ;
  wire [2:0]\fifo_length_i_reg[2]_2 ;
  wire \fifo_length_i_reg[3] ;
  wire full_error_reg;
  wire full_error_reg_0;
  wire [0:2]ie_register;
  wire next_Data_Exists;
  wire [2:0]p_6_in;
  wire read_fsl_error_d1;
  wire read_fsl_error_d1_reg;
  wire read_fsl_error_d1_reg_0;
  wire rit_detect_d1_reg;
  wire [3:0]\rit_register_reg[0] ;
  wire [0:0]\rit_register_reg[3] ;
  wire \s_axi_rdata_i_reg[3] ;
  wire sit_detect_d0;
  wire sit_detect_d1_reg;
  wire [0:0]\sit_register_reg[3] ;
  wire write_fsl_error;
  wire write_fsl_error_d1;

  design_1_mailbox_1_0_slave_attachment__parameterized0 I_SLAVE_ATTACHMENT
       (.\Addr_Counters[0].FDRE_I (\Addr_Counters[0].FDRE_I ),
        .\Addr_Counters[3].FDRE_I (\Addr_Counters[3].FDRE_I ),
        .Bus_RNW_reg_reg(Bus_RNW_reg_reg),
        .CI(CI),
        .D(D),
        .E(E),
        .FDRE_I1(FDRE_I1),
        .FDRE_I1_0(FDRE_I1_0),
        .FSL1_S_Data_I(FSL1_S_Data_I),
        .FSL1_S_Exists_I(FSL1_S_Exists_I),
        .Q(Q),
        .S1_AXI_ACLK(S1_AXI_ACLK),
        .S1_AXI_ARADDR(S1_AXI_ARADDR),
        .S1_AXI_ARREADY(S1_AXI_ARREADY),
        .S1_AXI_ARVALID(S1_AXI_ARVALID),
        .S1_AXI_AWADDR(S1_AXI_AWADDR),
        .S1_AXI_AWVALID(S1_AXI_AWVALID),
        .S1_AXI_BREADY(S1_AXI_BREADY),
        .S1_AXI_BVALID(S1_AXI_BVALID),
        .S1_AXI_RDATA(S1_AXI_RDATA),
        .S1_AXI_RREADY(S1_AXI_RREADY),
        .S1_AXI_RVALID(S1_AXI_RVALID),
        .S1_AXI_WREADY(S1_AXI_WREADY),
        .S1_AXI_WVALID(S1_AXI_WVALID),
        .SYS_Rst_I(SYS_Rst_I),
        .empty_error_reg(empty_error_reg),
        .empty_error_reg_0(empty_error_reg_0),
        .error_detect(error_detect),
        .\fifo_length_i_reg[2] (\fifo_length_i_reg[2] ),
        .\fifo_length_i_reg[2]_0 (\fifo_length_i_reg[2]_0 ),
        .\fifo_length_i_reg[2]_1 (\fifo_length_i_reg[2]_1 ),
        .\fifo_length_i_reg[2]_2 (\fifo_length_i_reg[2]_2 ),
        .\fifo_length_i_reg[3] (\fifo_length_i_reg[3] ),
        .full_error_reg(full_error_reg),
        .full_error_reg_0(full_error_reg_0),
        .ie_register(ie_register),
        .\ie_register_reg[2] (\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .\is_register_reg[2] (\GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .next_Data_Exists(next_Data_Exists),
        .p_6_in(p_6_in),
        .read_fsl_error_d1(read_fsl_error_d1),
        .read_fsl_error_d1_reg(read_fsl_error_d1_reg),
        .read_fsl_error_d1_reg_0(read_fsl_error_d1_reg_0),
        .rit_detect_d1_reg(rit_detect_d1_reg),
        .\rit_register_reg[0] (\rit_register_reg[0] ),
        .\rit_register_reg[3] (\rit_register_reg[3] ),
        .\s_axi_rdata_i_reg[3]_0 (\s_axi_rdata_i_reg[3] ),
        .sit_detect_d0(sit_detect_d0),
        .sit_detect_d1_reg(sit_detect_d1_reg),
        .\sit_register_reg[3] (\sit_register_reg[3] ),
        .write_fsl_error(write_fsl_error),
        .write_fsl_error_d1(write_fsl_error_d1));
endmodule

(* ORIG_REF_NAME = "fsl_v20" *) 
module design_1_mailbox_1_0_fsl_v20
   (write_fsl_error_d1_reg,
    FSL1_S_Exists_I,
    \s_axi_rdata_i_reg[3] ,
    Q,
    D,
    data_Exists_I_reg,
    sit_detect_d0,
    rit_detect_d0,
    rit_detect_d1_reg,
    SR,
    S0_AXI_ACLK,
    CI,
    S0_AXI_WDATA,
    next_Data_Exists,
    \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ,
    FDRE_I1,
    data_Exists_I_reg_0,
    Bus_RNW_reg,
    \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ,
    Bus_RNW_reg_reg,
    \sit_register_reg[3] ,
    \sit_register_reg[0] ,
    \rit_register_reg[0] ,
    \rit_register_reg[3] ,
    E);
  output write_fsl_error_d1_reg;
  output FSL1_S_Exists_I;
  output [3:0]\s_axi_rdata_i_reg[3] ;
  output [2:0]Q;
  output [27:0]D;
  output data_Exists_I_reg;
  output sit_detect_d0;
  output rit_detect_d0;
  output rit_detect_d1_reg;
  input [0:0]SR;
  input S0_AXI_ACLK;
  input CI;
  input [31:0]S0_AXI_WDATA;
  input next_Data_Exists;
  input \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ;
  input FDRE_I1;
  input data_Exists_I_reg_0;
  input Bus_RNW_reg;
  input \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  input Bus_RNW_reg_reg;
  input \sit_register_reg[3] ;
  input [1:0]\sit_register_reg[0] ;
  input [1:0]\rit_register_reg[0] ;
  input \rit_register_reg[3] ;
  input [0:0]E;

  wire Bus_RNW_reg;
  wire Bus_RNW_reg_reg;
  wire CI;
  wire [27:0]D;
  wire [0:0]E;
  wire FDRE_I1;
  wire FSL1_S_Exists_I;
  wire \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ;
  wire [2:0]Q;
  wire S0_AXI_ACLK;
  wire [31:0]S0_AXI_WDATA;
  wire [0:0]SR;
  wire data_Exists_I_reg;
  wire data_Exists_I_reg_0;
  wire next_Data_Exists;
  wire rit_detect_d0;
  wire rit_detect_d1_reg;
  wire [1:0]\rit_register_reg[0] ;
  wire \rit_register_reg[3] ;
  wire [3:0]\s_axi_rdata_i_reg[3] ;
  wire sit_detect_d0;
  wire [1:0]\sit_register_reg[0] ;
  wire \sit_register_reg[3] ;
  wire write_fsl_error_d1_reg;

  design_1_mailbox_1_0_Sync_FIFO_1 \Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1 
       (.\Addr_Counters[0].FDRE_I (FSL1_S_Exists_I),
        .Bus_RNW_reg(Bus_RNW_reg),
        .Bus_RNW_reg_reg(Bus_RNW_reg_reg),
        .CI(CI),
        .D(D),
        .E(E),
        .FDRE_I1(FDRE_I1),
        .\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg (\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] (\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ),
        .Q(Q),
        .S0_AXI_ACLK(S0_AXI_ACLK),
        .S0_AXI_WDATA(S0_AXI_WDATA),
        .SR(SR),
        .data_Exists_I_reg(data_Exists_I_reg),
        .data_Exists_I_reg_0(data_Exists_I_reg_0),
        .next_Data_Exists(next_Data_Exists),
        .rit_detect_d0(rit_detect_d0),
        .rit_detect_d1_reg(rit_detect_d1_reg),
        .\rit_register_reg[0] (\rit_register_reg[0] ),
        .\rit_register_reg[3] (\rit_register_reg[3] ),
        .\s_axi_rdata_i_reg[3] (\s_axi_rdata_i_reg[3] ),
        .sit_detect_d0(sit_detect_d0),
        .\sit_register_reg[0] (\sit_register_reg[0] ),
        .\sit_register_reg[3] (\sit_register_reg[3] ),
        .write_fsl_error_d1_reg(write_fsl_error_d1_reg));
endmodule

(* ORIG_REF_NAME = "fsl_v20" *) 
module design_1_mailbox_1_0_fsl_v20_0
   (write_fsl_error_d1_reg,
    FSL0_S_Exists_I,
    \s_axi_rdata_i_reg[3] ,
    Q,
    D,
    data_Exists_I_reg,
    rit_detect_d0,
    rit_detect_d1_reg,
    sit_detect_d0,
    SR,
    S0_AXI_ACLK,
    CI,
    S1_AXI_WDATA,
    next_Data_Exists,
    \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ,
    FDRE_I1,
    data_Exists_I_reg_0,
    Bus_RNW_reg,
    \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ,
    Bus_RNW_reg_reg,
    \rit_register_reg[0] ,
    \rit_register_reg[3] ,
    \sit_register_reg[3] ,
    \sit_register_reg[0] ,
    E);
  output write_fsl_error_d1_reg;
  output FSL0_S_Exists_I;
  output [3:0]\s_axi_rdata_i_reg[3] ;
  output [2:0]Q;
  output [27:0]D;
  output data_Exists_I_reg;
  output rit_detect_d0;
  output rit_detect_d1_reg;
  output sit_detect_d0;
  input [0:0]SR;
  input S0_AXI_ACLK;
  input CI;
  input [31:0]S1_AXI_WDATA;
  input next_Data_Exists;
  input \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ;
  input FDRE_I1;
  input data_Exists_I_reg_0;
  input Bus_RNW_reg;
  input \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  input Bus_RNW_reg_reg;
  input [1:0]\rit_register_reg[0] ;
  input \rit_register_reg[3] ;
  input \sit_register_reg[3] ;
  input [1:0]\sit_register_reg[0] ;
  input [0:0]E;

  wire Bus_RNW_reg;
  wire Bus_RNW_reg_reg;
  wire CI;
  wire [27:0]D;
  wire [0:0]E;
  wire FDRE_I1;
  wire FSL0_S_Exists_I;
  wire \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ;
  wire [2:0]Q;
  wire S0_AXI_ACLK;
  wire [31:0]S1_AXI_WDATA;
  wire [0:0]SR;
  wire data_Exists_I_reg;
  wire data_Exists_I_reg_0;
  wire next_Data_Exists;
  wire rit_detect_d0;
  wire rit_detect_d1_reg;
  wire [1:0]\rit_register_reg[0] ;
  wire \rit_register_reg[3] ;
  wire [3:0]\s_axi_rdata_i_reg[3] ;
  wire sit_detect_d0;
  wire [1:0]\sit_register_reg[0] ;
  wire \sit_register_reg[3] ;
  wire write_fsl_error_d1_reg;

  design_1_mailbox_1_0_Sync_FIFO \Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1 
       (.\Addr_Counters[0].FDRE_I (FSL0_S_Exists_I),
        .Bus_RNW_reg(Bus_RNW_reg),
        .Bus_RNW_reg_reg(Bus_RNW_reg_reg),
        .CI(CI),
        .D(D),
        .E(E),
        .FDRE_I1(FDRE_I1),
        .\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg (\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] (\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] ),
        .Q(Q),
        .S0_AXI_ACLK(S0_AXI_ACLK),
        .S1_AXI_WDATA(S1_AXI_WDATA),
        .SR(SR),
        .data_Exists_I_reg(data_Exists_I_reg),
        .data_Exists_I_reg_0(data_Exists_I_reg_0),
        .next_Data_Exists(next_Data_Exists),
        .rit_detect_d0(rit_detect_d0),
        .rit_detect_d1_reg(rit_detect_d1_reg),
        .\rit_register_reg[0] (\rit_register_reg[0] ),
        .\rit_register_reg[3] (\rit_register_reg[3] ),
        .\s_axi_rdata_i_reg[3] (\s_axi_rdata_i_reg[3] ),
        .sit_detect_d0(sit_detect_d0),
        .\sit_register_reg[0] (\sit_register_reg[0] ),
        .\sit_register_reg[3] (\sit_register_reg[3] ),
        .write_fsl_error_d1_reg(write_fsl_error_d1_reg));
endmodule

(* ORIG_REF_NAME = "if_decode" *) 
module design_1_mailbox_1_0_if_decode
   (\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ,
    Bus_RNW_reg,
    S0_AXI_RVALID,
    S0_AXI_BVALID,
    S0_AXI_WREADY,
    S0_AXI_ARREADY,
    CI,
    \fifo_length_i_reg[3] ,
    \s_axi_rdata_i_reg[3] ,
    next_Data_Exists,
    \fifo_length_i_reg[2] ,
    E,
    Q,
    \s_axi_rdata_i_reg[3]_0 ,
    rit_detect_d1_reg_0,
    sit_detect_d1_reg_0,
    Interrupt_0,
    \Addr_Counters[0].FDRE_I ,
    S0_AXI_RDATA,
    SYS_Rst_I,
    S0_AXI_ACLK,
    sit_detect_d0,
    rit_detect_d0,
    FSL0_S_Exists_I,
    FDRE_I1,
    S0_AXI_ARVALID,
    FSL0_S_Data_I,
    FDRE_I1_0,
    \Addr_Counters[3].FDRE_I ,
    \fifo_length_i_reg[2]_0 ,
    S0_AXI_WVALID,
    S0_AXI_AWVALID,
    S0_AXI_ARADDR,
    S0_AXI_AWADDR,
    S0_AXI_RREADY,
    S0_AXI_BREADY,
    \fifo_length_i_reg[2]_1 ,
    \fifo_length_i_reg[2]_2 ,
    S0_AXI_WDATA,
    D);
  output \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  output Bus_RNW_reg;
  output S0_AXI_RVALID;
  output S0_AXI_BVALID;
  output S0_AXI_WREADY;
  output S0_AXI_ARREADY;
  output CI;
  output \fifo_length_i_reg[3] ;
  output \s_axi_rdata_i_reg[3] ;
  output next_Data_Exists;
  output \fifo_length_i_reg[2] ;
  output [0:0]E;
  output [1:0]Q;
  output [1:0]\s_axi_rdata_i_reg[3]_0 ;
  output rit_detect_d1_reg_0;
  output sit_detect_d1_reg_0;
  output Interrupt_0;
  output \Addr_Counters[0].FDRE_I ;
  output [31:0]S0_AXI_RDATA;
  input SYS_Rst_I;
  input S0_AXI_ACLK;
  input sit_detect_d0;
  input rit_detect_d0;
  input FSL0_S_Exists_I;
  input FDRE_I1;
  input S0_AXI_ARVALID;
  input [3:0]FSL0_S_Data_I;
  input FDRE_I1_0;
  input \Addr_Counters[3].FDRE_I ;
  input \fifo_length_i_reg[2]_0 ;
  input S0_AXI_WVALID;
  input S0_AXI_AWVALID;
  input [3:0]S0_AXI_ARADDR;
  input [3:0]S0_AXI_AWADDR;
  input S0_AXI_RREADY;
  input S0_AXI_BREADY;
  input [2:0]\fifo_length_i_reg[2]_1 ;
  input [2:0]\fifo_length_i_reg[2]_2 ;
  input [3:0]S0_AXI_WDATA;
  input [27:0]D;

  wire \Addr_Counters[0].FDRE_I ;
  wire \Addr_Counters[3].FDRE_I ;
  wire Bus_RNW_reg;
  wire CI;
  wire [27:0]D;
  wire [0:0]E;
  wire FDRE_I1;
  wire FDRE_I1_0;
  wire [3:0]FSL0_S_Data_I;
  wire FSL0_S_Exists_I;
  wire \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  wire \I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ;
  wire \I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ;
  wire Interrupt_0;
  wire [1:0]Q;
  wire S0_AXI_ACLK;
  wire [3:0]S0_AXI_ARADDR;
  wire S0_AXI_ARREADY;
  wire S0_AXI_ARVALID;
  wire [3:0]S0_AXI_AWADDR;
  wire S0_AXI_AWVALID;
  wire S0_AXI_BREADY;
  wire S0_AXI_BVALID;
  wire [31:0]S0_AXI_RDATA;
  wire S0_AXI_RREADY;
  wire S0_AXI_RVALID;
  wire [3:0]S0_AXI_WDATA;
  wire S0_AXI_WREADY;
  wire S0_AXI_WVALID;
  wire SYS_Rst_I;
  wire \Using_AXI.AXI4_If_n_20 ;
  wire \Using_AXI.AXI4_If_n_22 ;
  wire \Using_AXI.AXI4_If_n_23 ;
  wire empty_error;
  wire error_detect;
  wire \fifo_length_i_reg[2] ;
  wire \fifo_length_i_reg[2]_0 ;
  wire [2:0]\fifo_length_i_reg[2]_1 ;
  wire [2:0]\fifo_length_i_reg[2]_2 ;
  wire \fifo_length_i_reg[3] ;
  wire full_error;
  wire [0:2]ie_register;
  wire \ie_register[0]_i_1_n_0 ;
  wire \ie_register[1]_i_1_n_0 ;
  wire \ie_register[2]_i_1_n_0 ;
  wire \is_register[0]_i_1_n_0 ;
  wire \is_register[1]_i_1_n_0 ;
  wire \is_register[2]_i_1_n_0 ;
  wire next_Data_Exists;
  wire p_0_in1_in;
  wire p_1_in2_in;
  wire [2:0]p_6_in;
  wire read_fsl_error_d1;
  wire rit_detect_d0;
  wire rit_detect_d1;
  wire rit_detect_d1_reg_0;
  wire [2:3]rit_register;
  wire \s_axi_rdata_i_reg[3] ;
  wire [1:0]\s_axi_rdata_i_reg[3]_0 ;
  wire sit_detect_d0;
  wire sit_detect_d1;
  wire sit_detect_d1_reg_0;
  wire [2:3]sit_register;
  wire write_fsl_error;
  wire write_fsl_error_d1;

  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    Interrupt_0_INST_0
       (.I0(p_6_in[0]),
        .I1(ie_register[2]),
        .I2(ie_register[1]),
        .I3(p_6_in[1]),
        .I4(ie_register[0]),
        .I5(p_6_in[2]),
        .O(Interrupt_0));
  design_1_mailbox_1_0_axi_lite_ipif \Using_AXI.AXI4_If 
       (.\Addr_Counters[0].FDRE_I (\Addr_Counters[0].FDRE_I ),
        .\Addr_Counters[3].FDRE_I (\Addr_Counters[3].FDRE_I ),
        .Bus_RNW_reg_reg(Bus_RNW_reg),
        .CI(CI),
        .D(D),
        .E(E),
        .FDRE_I1(FDRE_I1),
        .FDRE_I1_0(FDRE_I1_0),
        .FSL0_S_Data_I(FSL0_S_Data_I),
        .FSL0_S_Exists_I(FSL0_S_Exists_I),
        .\GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg (\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg (\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .Q({Q,sit_register[2],sit_register[3]}),
        .S0_AXI_ACLK(S0_AXI_ACLK),
        .S0_AXI_ARADDR(S0_AXI_ARADDR),
        .S0_AXI_ARREADY(S0_AXI_ARREADY),
        .S0_AXI_ARVALID(S0_AXI_ARVALID),
        .S0_AXI_AWADDR(S0_AXI_AWADDR),
        .S0_AXI_AWVALID(S0_AXI_AWVALID),
        .S0_AXI_BREADY(S0_AXI_BREADY),
        .S0_AXI_BVALID(S0_AXI_BVALID),
        .S0_AXI_RDATA(S0_AXI_RDATA),
        .S0_AXI_RREADY(S0_AXI_RREADY),
        .S0_AXI_RVALID(S0_AXI_RVALID),
        .S0_AXI_WREADY(S0_AXI_WREADY),
        .S0_AXI_WVALID(S0_AXI_WVALID),
        .SYS_Rst_I(SYS_Rst_I),
        .empty_error(empty_error),
        .empty_error_reg(\Using_AXI.AXI4_If_n_22 ),
        .error_detect(error_detect),
        .\fifo_length_i_reg[2] (\fifo_length_i_reg[2] ),
        .\fifo_length_i_reg[2]_0 (\fifo_length_i_reg[2]_0 ),
        .\fifo_length_i_reg[2]_1 (\fifo_length_i_reg[2]_1 ),
        .\fifo_length_i_reg[2]_2 (\fifo_length_i_reg[2]_2 ),
        .\fifo_length_i_reg[3] (\fifo_length_i_reg[3] ),
        .full_error(full_error),
        .full_error_reg(\Using_AXI.AXI4_If_n_20 ),
        .ie_register(ie_register),
        .next_Data_Exists(next_Data_Exists),
        .p_6_in(p_6_in),
        .read_fsl_error_d1(read_fsl_error_d1),
        .read_fsl_error_d1_reg(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .read_fsl_error_d1_reg_0(\Using_AXI.AXI4_If_n_23 ),
        .rit_detect_d1_reg(rit_detect_d1_reg_0),
        .\rit_register_reg[0] ({\s_axi_rdata_i_reg[3]_0 ,rit_register[2],rit_register[3]}),
        .\rit_register_reg[3] (p_1_in2_in),
        .\s_axi_rdata_i_reg[3] (\s_axi_rdata_i_reg[3] ),
        .sit_detect_d0(sit_detect_d0),
        .sit_detect_d1_reg(sit_detect_d1_reg_0),
        .\sit_register_reg[3] (p_0_in1_in),
        .write_fsl_error(write_fsl_error),
        .write_fsl_error_d1(write_fsl_error_d1));
  FDRE empty_error_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\Using_AXI.AXI4_If_n_22 ),
        .Q(empty_error),
        .R(1'b0));
  FDRE full_error_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\Using_AXI.AXI4_If_n_20 ),
        .Q(full_error),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hFB08)) 
    \ie_register[0]_i_1 
       (.I0(S0_AXI_WDATA[2]),
        .I1(\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(ie_register[0]),
        .O(\ie_register[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \ie_register[1]_i_1 
       (.I0(S0_AXI_WDATA[1]),
        .I1(\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(ie_register[1]),
        .O(\ie_register[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \ie_register[2]_i_1 
       (.I0(S0_AXI_WDATA[0]),
        .I1(\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(ie_register[2]),
        .O(\ie_register[2]_i_1_n_0 ));
  FDRE \ie_register_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\ie_register[0]_i_1_n_0 ),
        .Q(ie_register[0]),
        .R(SYS_Rst_I));
  FDRE \ie_register_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\ie_register[1]_i_1_n_0 ),
        .Q(ie_register[1]),
        .R(SYS_Rst_I));
  FDRE \ie_register_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\ie_register[2]_i_1_n_0 ),
        .Q(ie_register[2]),
        .R(SYS_Rst_I));
  LUT5 #(
    .INIT(32'hFBFFAAAA)) 
    \is_register[0]_i_1 
       (.I0(error_detect),
        .I1(\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(S0_AXI_WDATA[2]),
        .I4(p_6_in[2]),
        .O(\is_register[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFF1FFFFF11111111)) 
    \is_register[1]_i_1 
       (.I0(\fifo_length_i_reg[2]_0 ),
        .I1(rit_detect_d1),
        .I2(\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .I3(Bus_RNW_reg),
        .I4(S0_AXI_WDATA[1]),
        .I5(p_6_in[1]),
        .O(\is_register[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFF4FFFFF44444444)) 
    \is_register[2]_i_1 
       (.I0(sit_detect_d1),
        .I1(sit_detect_d0),
        .I2(\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .I3(Bus_RNW_reg),
        .I4(S0_AXI_WDATA[0]),
        .I5(p_6_in[0]),
        .O(\is_register[2]_i_1_n_0 ));
  FDRE \is_register_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\is_register[0]_i_1_n_0 ),
        .Q(p_6_in[2]),
        .R(SYS_Rst_I));
  FDRE \is_register_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\is_register[1]_i_1_n_0 ),
        .Q(p_6_in[1]),
        .R(SYS_Rst_I));
  FDRE \is_register_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\is_register[2]_i_1_n_0 ),
        .Q(p_6_in[0]),
        .R(SYS_Rst_I));
  FDRE read_fsl_error_d1_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\Using_AXI.AXI4_If_n_23 ),
        .Q(read_fsl_error_d1),
        .R(SYS_Rst_I));
  FDRE rit_detect_d1_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(rit_detect_d0),
        .Q(rit_detect_d1),
        .R(1'b0));
  FDRE \rit_register_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(p_1_in2_in),
        .D(S0_AXI_WDATA[3]),
        .Q(\s_axi_rdata_i_reg[3]_0 [1]),
        .R(SYS_Rst_I));
  FDRE \rit_register_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(p_1_in2_in),
        .D(S0_AXI_WDATA[2]),
        .Q(\s_axi_rdata_i_reg[3]_0 [0]),
        .R(SYS_Rst_I));
  FDRE \rit_register_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(p_1_in2_in),
        .D(S0_AXI_WDATA[1]),
        .Q(rit_register[2]),
        .R(SYS_Rst_I));
  FDRE \rit_register_reg[3] 
       (.C(S0_AXI_ACLK),
        .CE(p_1_in2_in),
        .D(S0_AXI_WDATA[0]),
        .Q(rit_register[3]),
        .R(SYS_Rst_I));
  FDRE sit_detect_d1_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(sit_detect_d0),
        .Q(sit_detect_d1),
        .R(1'b0));
  FDRE \sit_register_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(p_0_in1_in),
        .D(S0_AXI_WDATA[3]),
        .Q(Q[1]),
        .R(SYS_Rst_I));
  FDRE \sit_register_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(p_0_in1_in),
        .D(S0_AXI_WDATA[2]),
        .Q(Q[0]),
        .R(SYS_Rst_I));
  FDRE \sit_register_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(p_0_in1_in),
        .D(S0_AXI_WDATA[1]),
        .Q(sit_register[2]),
        .R(SYS_Rst_I));
  FDRE \sit_register_reg[3] 
       (.C(S0_AXI_ACLK),
        .CE(p_0_in1_in),
        .D(S0_AXI_WDATA[0]),
        .Q(sit_register[3]),
        .R(SYS_Rst_I));
  FDRE write_fsl_error_d1_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(write_fsl_error),
        .Q(write_fsl_error_d1),
        .R(SYS_Rst_I));
endmodule

(* ORIG_REF_NAME = "if_decode" *) 
module design_1_mailbox_1_0_if_decode__parameterized1
   (\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ,
    Bus_RNW_reg,
    S1_AXI_RVALID,
    S1_AXI_BVALID,
    next_Data_Exists,
    \fifo_length_i_reg[2] ,
    E,
    S1_AXI_ARREADY,
    S1_AXI_WREADY,
    CI,
    \fifo_length_i_reg[3] ,
    \s_axi_rdata_i_reg[3] ,
    Q,
    sit_detect_d1_reg_0,
    rit_detect_d1_reg_0,
    sit_detect_d1_reg_1,
    Interrupt_1,
    \Addr_Counters[0].FDRE_I ,
    S1_AXI_RDATA,
    SYS_Rst_I,
    S1_AXI_ACLK,
    sit_detect_d0,
    rit_detect_d0,
    FDRE_I1,
    \Addr_Counters[3].FDRE_I ,
    FSL1_S_Exists_I,
    FDRE_I1_0,
    S1_AXI_ARVALID,
    FSL1_S_Data_I,
    \fifo_length_i_reg[2]_0 ,
    S1_AXI_WVALID,
    S1_AXI_AWVALID,
    S1_AXI_ARADDR,
    S1_AXI_AWADDR,
    S1_AXI_RREADY,
    S1_AXI_BREADY,
    \fifo_length_i_reg[2]_1 ,
    \fifo_length_i_reg[2]_2 ,
    S1_AXI_WDATA,
    D);
  output \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  output Bus_RNW_reg;
  output S1_AXI_RVALID;
  output S1_AXI_BVALID;
  output next_Data_Exists;
  output \fifo_length_i_reg[2] ;
  output [0:0]E;
  output S1_AXI_ARREADY;
  output S1_AXI_WREADY;
  output CI;
  output \fifo_length_i_reg[3] ;
  output \s_axi_rdata_i_reg[3] ;
  output [1:0]Q;
  output [1:0]sit_detect_d1_reg_0;
  output rit_detect_d1_reg_0;
  output sit_detect_d1_reg_1;
  output Interrupt_1;
  output \Addr_Counters[0].FDRE_I ;
  output [31:0]S1_AXI_RDATA;
  input SYS_Rst_I;
  input S1_AXI_ACLK;
  input sit_detect_d0;
  input rit_detect_d0;
  input FDRE_I1;
  input \Addr_Counters[3].FDRE_I ;
  input FSL1_S_Exists_I;
  input FDRE_I1_0;
  input S1_AXI_ARVALID;
  input [3:0]FSL1_S_Data_I;
  input \fifo_length_i_reg[2]_0 ;
  input S1_AXI_WVALID;
  input S1_AXI_AWVALID;
  input [3:0]S1_AXI_ARADDR;
  input [3:0]S1_AXI_AWADDR;
  input S1_AXI_RREADY;
  input S1_AXI_BREADY;
  input [2:0]\fifo_length_i_reg[2]_1 ;
  input [2:0]\fifo_length_i_reg[2]_2 ;
  input [3:0]S1_AXI_WDATA;
  input [27:0]D;

  wire \Addr_Counters[0].FDRE_I ;
  wire \Addr_Counters[3].FDRE_I ;
  wire Bus_RNW_reg;
  wire CI;
  wire [27:0]D;
  wire [0:0]E;
  wire FDRE_I1;
  wire FDRE_I1_0;
  wire [3:0]FSL1_S_Data_I;
  wire FSL1_S_Exists_I;
  wire \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  wire \I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ;
  wire \I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ;
  wire Interrupt_1;
  wire [1:0]Q;
  wire S1_AXI_ACLK;
  wire [3:0]S1_AXI_ARADDR;
  wire S1_AXI_ARREADY;
  wire S1_AXI_ARVALID;
  wire [3:0]S1_AXI_AWADDR;
  wire S1_AXI_AWVALID;
  wire S1_AXI_BREADY;
  wire S1_AXI_BVALID;
  wire [31:0]S1_AXI_RDATA;
  wire S1_AXI_RREADY;
  wire S1_AXI_RVALID;
  wire [3:0]S1_AXI_WDATA;
  wire S1_AXI_WREADY;
  wire S1_AXI_WVALID;
  wire SYS_Rst_I;
  wire \Using_AXI.AXI4_If_n_21 ;
  wire \Using_AXI.AXI4_If_n_22 ;
  wire \Using_AXI.AXI4_If_n_23 ;
  wire empty_error_reg_n_0;
  wire error_detect;
  wire \fifo_length_i_reg[2] ;
  wire \fifo_length_i_reg[2]_0 ;
  wire [2:0]\fifo_length_i_reg[2]_1 ;
  wire [2:0]\fifo_length_i_reg[2]_2 ;
  wire \fifo_length_i_reg[3] ;
  wire full_error_reg_n_0;
  wire [0:2]ie_register;
  wire \ie_register[0]_i_1_n_0 ;
  wire \ie_register[1]_i_1_n_0 ;
  wire \ie_register[2]_i_1_n_0 ;
  wire \is_register[0]_i_1_n_0 ;
  wire \is_register[1]_i_1_n_0 ;
  wire \is_register[2]_i_1_n_0 ;
  wire next_Data_Exists;
  wire p_0_in1_in;
  wire p_1_in2_in;
  wire [2:0]p_6_in;
  wire read_fsl_error_d1;
  wire rit_detect_d0;
  wire rit_detect_d1;
  wire rit_detect_d1_reg_0;
  wire \rit_register_reg_n_0_[2] ;
  wire \rit_register_reg_n_0_[3] ;
  wire \s_axi_rdata_i_reg[3] ;
  wire sit_detect_d0;
  wire sit_detect_d1;
  wire [1:0]sit_detect_d1_reg_0;
  wire sit_detect_d1_reg_1;
  wire \sit_register_reg_n_0_[2] ;
  wire \sit_register_reg_n_0_[3] ;
  wire write_fsl_error;
  wire write_fsl_error_d1;

  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    Interrupt_1_INST_0
       (.I0(p_6_in[0]),
        .I1(ie_register[2]),
        .I2(ie_register[1]),
        .I3(p_6_in[1]),
        .I4(ie_register[0]),
        .I5(p_6_in[2]),
        .O(Interrupt_1));
  design_1_mailbox_1_0_axi_lite_ipif__parameterized1 \Using_AXI.AXI4_If 
       (.\Addr_Counters[0].FDRE_I (\Addr_Counters[0].FDRE_I ),
        .\Addr_Counters[3].FDRE_I (\Addr_Counters[3].FDRE_I ),
        .Bus_RNW_reg_reg(Bus_RNW_reg),
        .CI(CI),
        .D(D),
        .E(E),
        .FDRE_I1(FDRE_I1),
        .FDRE_I1_0(FDRE_I1_0),
        .FSL1_S_Data_I(FSL1_S_Data_I),
        .FSL1_S_Exists_I(FSL1_S_Exists_I),
        .\GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg (\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg (\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .Q({sit_detect_d1_reg_0,\sit_register_reg_n_0_[2] ,\sit_register_reg_n_0_[3] }),
        .S1_AXI_ACLK(S1_AXI_ACLK),
        .S1_AXI_ARADDR(S1_AXI_ARADDR),
        .S1_AXI_ARREADY(S1_AXI_ARREADY),
        .S1_AXI_ARVALID(S1_AXI_ARVALID),
        .S1_AXI_AWADDR(S1_AXI_AWADDR),
        .S1_AXI_AWVALID(S1_AXI_AWVALID),
        .S1_AXI_BREADY(S1_AXI_BREADY),
        .S1_AXI_BVALID(S1_AXI_BVALID),
        .S1_AXI_RDATA(S1_AXI_RDATA),
        .S1_AXI_RREADY(S1_AXI_RREADY),
        .S1_AXI_RVALID(S1_AXI_RVALID),
        .S1_AXI_WREADY(S1_AXI_WREADY),
        .S1_AXI_WVALID(S1_AXI_WVALID),
        .SYS_Rst_I(SYS_Rst_I),
        .empty_error_reg(\Using_AXI.AXI4_If_n_21 ),
        .empty_error_reg_0(empty_error_reg_n_0),
        .error_detect(error_detect),
        .\fifo_length_i_reg[2] (\fifo_length_i_reg[2] ),
        .\fifo_length_i_reg[2]_0 (\fifo_length_i_reg[2]_0 ),
        .\fifo_length_i_reg[2]_1 (\fifo_length_i_reg[2]_1 ),
        .\fifo_length_i_reg[2]_2 (\fifo_length_i_reg[2]_2 ),
        .\fifo_length_i_reg[3] (\fifo_length_i_reg[3] ),
        .full_error_reg(\Using_AXI.AXI4_If_n_23 ),
        .full_error_reg_0(full_error_reg_n_0),
        .ie_register(ie_register),
        .next_Data_Exists(next_Data_Exists),
        .p_6_in(p_6_in),
        .read_fsl_error_d1(read_fsl_error_d1),
        .read_fsl_error_d1_reg(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .read_fsl_error_d1_reg_0(\Using_AXI.AXI4_If_n_22 ),
        .rit_detect_d1_reg(rit_detect_d1_reg_0),
        .\rit_register_reg[0] ({Q,\rit_register_reg_n_0_[2] ,\rit_register_reg_n_0_[3] }),
        .\rit_register_reg[3] (p_1_in2_in),
        .\s_axi_rdata_i_reg[3] (\s_axi_rdata_i_reg[3] ),
        .sit_detect_d0(sit_detect_d0),
        .sit_detect_d1_reg(sit_detect_d1_reg_1),
        .\sit_register_reg[3] (p_0_in1_in),
        .write_fsl_error(write_fsl_error),
        .write_fsl_error_d1(write_fsl_error_d1));
  FDRE empty_error_reg
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\Using_AXI.AXI4_If_n_21 ),
        .Q(empty_error_reg_n_0),
        .R(1'b0));
  FDRE full_error_reg
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\Using_AXI.AXI4_If_n_23 ),
        .Q(full_error_reg_n_0),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hFB08)) 
    \ie_register[0]_i_1 
       (.I0(S1_AXI_WDATA[2]),
        .I1(\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(ie_register[0]),
        .O(\ie_register[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \ie_register[1]_i_1 
       (.I0(S1_AXI_WDATA[1]),
        .I1(\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(ie_register[1]),
        .O(\ie_register[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \ie_register[2]_i_1 
       (.I0(S1_AXI_WDATA[0]),
        .I1(\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(ie_register[2]),
        .O(\ie_register[2]_i_1_n_0 ));
  FDRE \ie_register_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\ie_register[0]_i_1_n_0 ),
        .Q(ie_register[0]),
        .R(SYS_Rst_I));
  FDRE \ie_register_reg[1] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\ie_register[1]_i_1_n_0 ),
        .Q(ie_register[1]),
        .R(SYS_Rst_I));
  FDRE \ie_register_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\ie_register[2]_i_1_n_0 ),
        .Q(ie_register[2]),
        .R(SYS_Rst_I));
  LUT5 #(
    .INIT(32'hFBFFAAAA)) 
    \is_register[0]_i_1 
       (.I0(error_detect),
        .I1(\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(S1_AXI_WDATA[2]),
        .I4(p_6_in[2]),
        .O(\is_register[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFF1FFFFF11111111)) 
    \is_register[1]_i_1 
       (.I0(\fifo_length_i_reg[2]_0 ),
        .I1(rit_detect_d1),
        .I2(\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .I3(Bus_RNW_reg),
        .I4(S1_AXI_WDATA[1]),
        .I5(p_6_in[1]),
        .O(\is_register[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFF4FFFFF44444444)) 
    \is_register[2]_i_1 
       (.I0(sit_detect_d1),
        .I1(sit_detect_d0),
        .I2(\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .I3(Bus_RNW_reg),
        .I4(S1_AXI_WDATA[0]),
        .I5(p_6_in[0]),
        .O(\is_register[2]_i_1_n_0 ));
  FDRE \is_register_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\is_register[0]_i_1_n_0 ),
        .Q(p_6_in[2]),
        .R(SYS_Rst_I));
  FDRE \is_register_reg[1] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\is_register[1]_i_1_n_0 ),
        .Q(p_6_in[1]),
        .R(SYS_Rst_I));
  FDRE \is_register_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\is_register[2]_i_1_n_0 ),
        .Q(p_6_in[0]),
        .R(SYS_Rst_I));
  FDRE read_fsl_error_d1_reg
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\Using_AXI.AXI4_If_n_22 ),
        .Q(read_fsl_error_d1),
        .R(SYS_Rst_I));
  FDRE rit_detect_d1_reg
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(rit_detect_d0),
        .Q(rit_detect_d1),
        .R(1'b0));
  FDRE \rit_register_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(p_1_in2_in),
        .D(S1_AXI_WDATA[3]),
        .Q(Q[1]),
        .R(SYS_Rst_I));
  FDRE \rit_register_reg[1] 
       (.C(S1_AXI_ACLK),
        .CE(p_1_in2_in),
        .D(S1_AXI_WDATA[2]),
        .Q(Q[0]),
        .R(SYS_Rst_I));
  FDRE \rit_register_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(p_1_in2_in),
        .D(S1_AXI_WDATA[1]),
        .Q(\rit_register_reg_n_0_[2] ),
        .R(SYS_Rst_I));
  FDRE \rit_register_reg[3] 
       (.C(S1_AXI_ACLK),
        .CE(p_1_in2_in),
        .D(S1_AXI_WDATA[0]),
        .Q(\rit_register_reg_n_0_[3] ),
        .R(SYS_Rst_I));
  FDRE sit_detect_d1_reg
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(sit_detect_d0),
        .Q(sit_detect_d1),
        .R(1'b0));
  FDRE \sit_register_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(p_0_in1_in),
        .D(S1_AXI_WDATA[3]),
        .Q(sit_detect_d1_reg_0[1]),
        .R(SYS_Rst_I));
  FDRE \sit_register_reg[1] 
       (.C(S1_AXI_ACLK),
        .CE(p_0_in1_in),
        .D(S1_AXI_WDATA[2]),
        .Q(sit_detect_d1_reg_0[0]),
        .R(SYS_Rst_I));
  FDRE \sit_register_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(p_0_in1_in),
        .D(S1_AXI_WDATA[1]),
        .Q(\sit_register_reg_n_0_[2] ),
        .R(SYS_Rst_I));
  FDRE \sit_register_reg[3] 
       (.C(S1_AXI_ACLK),
        .CE(p_0_in1_in),
        .D(S1_AXI_WDATA[0]),
        .Q(\sit_register_reg_n_0_[3] ),
        .R(SYS_Rst_I));
  FDRE write_fsl_error_d1_reg
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(write_fsl_error),
        .Q(write_fsl_error_d1),
        .R(SYS_Rst_I));
endmodule

(* C_ASYNC_CLKS = "0" *) (* C_ENABLE_BUS_ERROR = "0" *) (* C_EXT_RESET_HIGH = "1" *) 
(* C_FAMILY = "zynq" *) (* C_IMPL_STYLE = "0" *) (* C_INTERCONNECT_PORT_0 = "2" *) 
(* C_INTERCONNECT_PORT_1 = "2" *) (* C_M0_AXIS_DATA_WIDTH = "32" *) (* C_M1_AXIS_DATA_WIDTH = "32" *) 
(* C_MAILBOX_DEPTH = "16" *) (* C_NUM_SYNC_FF = "2" *) (* C_S0_AXIS_DATA_WIDTH = "32" *) 
(* C_S0_AXI_ADDR_WIDTH = "32" *) (* C_S0_AXI_BASEADDR = "1132593152" *) (* C_S0_AXI_DATA_WIDTH = "32" *) 
(* C_S0_AXI_HIGHADDR = "1132658687" *) (* C_S1_AXIS_DATA_WIDTH = "32" *) (* C_S1_AXI_ADDR_WIDTH = "32" *) 
(* C_S1_AXI_BASEADDR = "1132658688" *) (* C_S1_AXI_DATA_WIDTH = "32" *) (* C_S1_AXI_HIGHADDR = "1132724223" *) 
(* ORIG_REF_NAME = "mailbox" *) 
module design_1_mailbox_1_0_mailbox
   (SYS_Rst,
    S0_AXI_ACLK,
    S0_AXI_ARESETN,
    S0_AXI_AWADDR,
    S0_AXI_AWVALID,
    S0_AXI_AWREADY,
    S0_AXI_WDATA,
    S0_AXI_WSTRB,
    S0_AXI_WVALID,
    S0_AXI_WREADY,
    S0_AXI_BRESP,
    S0_AXI_BVALID,
    S0_AXI_BREADY,
    S0_AXI_ARADDR,
    S0_AXI_ARVALID,
    S0_AXI_ARREADY,
    S0_AXI_RDATA,
    S0_AXI_RRESP,
    S0_AXI_RVALID,
    S0_AXI_RREADY,
    S1_AXI_ACLK,
    S1_AXI_ARESETN,
    S1_AXI_AWADDR,
    S1_AXI_AWVALID,
    S1_AXI_AWREADY,
    S1_AXI_WDATA,
    S1_AXI_WSTRB,
    S1_AXI_WVALID,
    S1_AXI_WREADY,
    S1_AXI_BRESP,
    S1_AXI_BVALID,
    S1_AXI_BREADY,
    S1_AXI_ARADDR,
    S1_AXI_ARVALID,
    S1_AXI_ARREADY,
    S1_AXI_RDATA,
    S1_AXI_RRESP,
    S1_AXI_RVALID,
    S1_AXI_RREADY,
    M0_AXIS_ACLK,
    M0_AXIS_TLAST,
    M0_AXIS_TDATA,
    M0_AXIS_TVALID,
    M0_AXIS_TREADY,
    S0_AXIS_ACLK,
    S0_AXIS_TLAST,
    S0_AXIS_TDATA,
    S0_AXIS_TVALID,
    S0_AXIS_TREADY,
    M1_AXIS_ACLK,
    M1_AXIS_TLAST,
    M1_AXIS_TDATA,
    M1_AXIS_TVALID,
    M1_AXIS_TREADY,
    S1_AXIS_ACLK,
    S1_AXIS_TLAST,
    S1_AXIS_TDATA,
    S1_AXIS_TVALID,
    S1_AXIS_TREADY,
    Interrupt_0,
    Interrupt_1);
  input SYS_Rst;
  input S0_AXI_ACLK;
  input S0_AXI_ARESETN;
  input [31:0]S0_AXI_AWADDR;
  input S0_AXI_AWVALID;
  output S0_AXI_AWREADY;
  input [31:0]S0_AXI_WDATA;
  input [3:0]S0_AXI_WSTRB;
  input S0_AXI_WVALID;
  output S0_AXI_WREADY;
  output [1:0]S0_AXI_BRESP;
  output S0_AXI_BVALID;
  input S0_AXI_BREADY;
  input [31:0]S0_AXI_ARADDR;
  input S0_AXI_ARVALID;
  output S0_AXI_ARREADY;
  output [31:0]S0_AXI_RDATA;
  output [1:0]S0_AXI_RRESP;
  output S0_AXI_RVALID;
  input S0_AXI_RREADY;
  input S1_AXI_ACLK;
  input S1_AXI_ARESETN;
  input [31:0]S1_AXI_AWADDR;
  input S1_AXI_AWVALID;
  output S1_AXI_AWREADY;
  input [31:0]S1_AXI_WDATA;
  input [3:0]S1_AXI_WSTRB;
  input S1_AXI_WVALID;
  output S1_AXI_WREADY;
  output [1:0]S1_AXI_BRESP;
  output S1_AXI_BVALID;
  input S1_AXI_BREADY;
  input [31:0]S1_AXI_ARADDR;
  input S1_AXI_ARVALID;
  output S1_AXI_ARREADY;
  output [31:0]S1_AXI_RDATA;
  output [1:0]S1_AXI_RRESP;
  output S1_AXI_RVALID;
  input S1_AXI_RREADY;
  input M0_AXIS_ACLK;
  output M0_AXIS_TLAST;
  output [31:0]M0_AXIS_TDATA;
  output M0_AXIS_TVALID;
  input M0_AXIS_TREADY;
  input S0_AXIS_ACLK;
  input S0_AXIS_TLAST;
  input [31:0]S0_AXIS_TDATA;
  input S0_AXIS_TVALID;
  output S0_AXIS_TREADY;
  input M1_AXIS_ACLK;
  output M1_AXIS_TLAST;
  output [31:0]M1_AXIS_TDATA;
  output M1_AXIS_TVALID;
  input M1_AXIS_TREADY;
  input S1_AXIS_ACLK;
  input S1_AXIS_TLAST;
  input [31:0]S1_AXIS_TDATA;
  input S1_AXIS_TVALID;
  output S1_AXIS_TREADY;
  output Interrupt_0;
  output Interrupt_1;

  wire \<const0> ;
  wire [28:31]FSL0_S_Data_I;
  wire FSL0_S_Exists_I;
  wire [28:31]FSL1_S_Data_I;
  wire FSL1_S_Exists_I;
  wire Interrupt_0;
  wire Interrupt_1;
  wire \Rst_Sync.SYS_Rst_I_i_1_n_0 ;
  wire S0_AXI_ACLK;
  wire [31:0]S0_AXI_ARADDR;
  wire S0_AXI_ARESETN;
  wire S0_AXI_ARREADY;
  wire S0_AXI_ARVALID;
  wire [31:0]S0_AXI_AWADDR;
  wire S0_AXI_AWVALID;
  wire S0_AXI_BREADY;
  wire S0_AXI_BVALID;
  wire [31:0]S0_AXI_RDATA;
  wire S0_AXI_RREADY;
  wire S0_AXI_RVALID;
  wire [31:0]S0_AXI_WDATA;
  wire S0_AXI_WREADY;
  wire S0_AXI_WVALID;
  wire S1_AXI_ACLK;
  wire [31:0]S1_AXI_ARADDR;
  wire S1_AXI_ARESETN;
  wire S1_AXI_ARREADY;
  wire S1_AXI_ARVALID;
  wire [31:0]S1_AXI_AWADDR;
  wire S1_AXI_AWVALID;
  wire S1_AXI_BREADY;
  wire S1_AXI_BVALID;
  wire [31:0]S1_AXI_RDATA;
  wire S1_AXI_RREADY;
  wire S1_AXI_RVALID;
  wire [31:0]S1_AXI_WDATA;
  wire S1_AXI_WREADY;
  wire S1_AXI_WVALID;
  wire SYS_Rst_I;
  wire SYS_Rst_Input;
  (* async_reg = "true" *) wire SYS_Rst_Input_d1;
  (* async_reg = "true" *) wire SYS_Rst_Input_d2;
  wire \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/Bus_RNW_reg ;
  wire \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/Bus_RNW_reg_2 ;
  wire \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  wire \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg_3 ;
  wire \Using_Bus_0.Bus0_If_n_10 ;
  wire \Using_Bus_0.Bus0_If_n_11 ;
  wire \Using_Bus_0.Bus0_If_n_16 ;
  wire \Using_Bus_0.Bus0_If_n_17 ;
  wire \Using_Bus_0.Bus0_If_n_19 ;
  wire \Using_Bus_0.Bus0_If_n_7 ;
  wire \Using_Bus_0.Bus0_If_n_8 ;
  wire \Using_Bus_1.Bus1_If_n_10 ;
  wire \Using_Bus_1.Bus1_If_n_11 ;
  wire \Using_Bus_1.Bus1_If_n_12 ;
  wire \Using_Bus_1.Bus1_If_n_13 ;
  wire \Using_Bus_1.Bus1_If_n_14 ;
  wire \Using_Bus_1.Bus1_If_n_15 ;
  wire \Using_Bus_1.Bus1_If_n_16 ;
  wire \Using_Bus_1.Bus1_If_n_17 ;
  wire \Using_Bus_1.Bus1_If_n_19 ;
  wire \Using_Bus_1.Bus1_If_n_5 ;
  wire \Using_Bus_1.Bus1_If_n_6 ;
  wire \Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/CI ;
  wire \Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/CI_0 ;
  wire [2:4]\Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/fifo_length_i_reg ;
  wire [2:4]\Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/fifo_length_i_reg_6 ;
  wire \Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/next_Data_Exists ;
  wire \Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/next_Data_Exists_1 ;
  wire fsl_0_to_1_n_0;
  wire fsl_0_to_1_n_10;
  wire fsl_0_to_1_n_11;
  wire fsl_0_to_1_n_12;
  wire fsl_0_to_1_n_13;
  wire fsl_0_to_1_n_14;
  wire fsl_0_to_1_n_15;
  wire fsl_0_to_1_n_16;
  wire fsl_0_to_1_n_17;
  wire fsl_0_to_1_n_18;
  wire fsl_0_to_1_n_19;
  wire fsl_0_to_1_n_20;
  wire fsl_0_to_1_n_21;
  wire fsl_0_to_1_n_22;
  wire fsl_0_to_1_n_23;
  wire fsl_0_to_1_n_24;
  wire fsl_0_to_1_n_25;
  wire fsl_0_to_1_n_26;
  wire fsl_0_to_1_n_27;
  wire fsl_0_to_1_n_28;
  wire fsl_0_to_1_n_29;
  wire fsl_0_to_1_n_30;
  wire fsl_0_to_1_n_31;
  wire fsl_0_to_1_n_32;
  wire fsl_0_to_1_n_33;
  wire fsl_0_to_1_n_34;
  wire fsl_0_to_1_n_35;
  wire fsl_0_to_1_n_36;
  wire fsl_0_to_1_n_37;
  wire fsl_0_to_1_n_40;
  wire fsl_0_to_1_n_9;
  wire fsl_1_to_0_n_0;
  wire fsl_1_to_0_n_10;
  wire fsl_1_to_0_n_11;
  wire fsl_1_to_0_n_12;
  wire fsl_1_to_0_n_13;
  wire fsl_1_to_0_n_14;
  wire fsl_1_to_0_n_15;
  wire fsl_1_to_0_n_16;
  wire fsl_1_to_0_n_17;
  wire fsl_1_to_0_n_18;
  wire fsl_1_to_0_n_19;
  wire fsl_1_to_0_n_20;
  wire fsl_1_to_0_n_21;
  wire fsl_1_to_0_n_22;
  wire fsl_1_to_0_n_23;
  wire fsl_1_to_0_n_24;
  wire fsl_1_to_0_n_25;
  wire fsl_1_to_0_n_26;
  wire fsl_1_to_0_n_27;
  wire fsl_1_to_0_n_28;
  wire fsl_1_to_0_n_29;
  wire fsl_1_to_0_n_30;
  wire fsl_1_to_0_n_31;
  wire fsl_1_to_0_n_32;
  wire fsl_1_to_0_n_33;
  wire fsl_1_to_0_n_34;
  wire fsl_1_to_0_n_35;
  wire fsl_1_to_0_n_36;
  wire fsl_1_to_0_n_37;
  wire fsl_1_to_0_n_39;
  wire fsl_1_to_0_n_9;
  wire rit_detect_d0;
  wire rit_detect_d0_5;
  wire [0:1]rit_register;
  wire sit_detect_d0;
  wire sit_detect_d0_4;
  wire [0:1]sit_register;

  assign M0_AXIS_TDATA[31] = \<const0> ;
  assign M0_AXIS_TDATA[30] = \<const0> ;
  assign M0_AXIS_TDATA[29] = \<const0> ;
  assign M0_AXIS_TDATA[28] = \<const0> ;
  assign M0_AXIS_TDATA[27] = \<const0> ;
  assign M0_AXIS_TDATA[26] = \<const0> ;
  assign M0_AXIS_TDATA[25] = \<const0> ;
  assign M0_AXIS_TDATA[24] = \<const0> ;
  assign M0_AXIS_TDATA[23] = \<const0> ;
  assign M0_AXIS_TDATA[22] = \<const0> ;
  assign M0_AXIS_TDATA[21] = \<const0> ;
  assign M0_AXIS_TDATA[20] = \<const0> ;
  assign M0_AXIS_TDATA[19] = \<const0> ;
  assign M0_AXIS_TDATA[18] = \<const0> ;
  assign M0_AXIS_TDATA[17] = \<const0> ;
  assign M0_AXIS_TDATA[16] = \<const0> ;
  assign M0_AXIS_TDATA[15] = \<const0> ;
  assign M0_AXIS_TDATA[14] = \<const0> ;
  assign M0_AXIS_TDATA[13] = \<const0> ;
  assign M0_AXIS_TDATA[12] = \<const0> ;
  assign M0_AXIS_TDATA[11] = \<const0> ;
  assign M0_AXIS_TDATA[10] = \<const0> ;
  assign M0_AXIS_TDATA[9] = \<const0> ;
  assign M0_AXIS_TDATA[8] = \<const0> ;
  assign M0_AXIS_TDATA[7] = \<const0> ;
  assign M0_AXIS_TDATA[6] = \<const0> ;
  assign M0_AXIS_TDATA[5] = \<const0> ;
  assign M0_AXIS_TDATA[4] = \<const0> ;
  assign M0_AXIS_TDATA[3] = \<const0> ;
  assign M0_AXIS_TDATA[2] = \<const0> ;
  assign M0_AXIS_TDATA[1] = \<const0> ;
  assign M0_AXIS_TDATA[0] = \<const0> ;
  assign M0_AXIS_TLAST = \<const0> ;
  assign M0_AXIS_TVALID = \<const0> ;
  assign M1_AXIS_TDATA[31] = \<const0> ;
  assign M1_AXIS_TDATA[30] = \<const0> ;
  assign M1_AXIS_TDATA[29] = \<const0> ;
  assign M1_AXIS_TDATA[28] = \<const0> ;
  assign M1_AXIS_TDATA[27] = \<const0> ;
  assign M1_AXIS_TDATA[26] = \<const0> ;
  assign M1_AXIS_TDATA[25] = \<const0> ;
  assign M1_AXIS_TDATA[24] = \<const0> ;
  assign M1_AXIS_TDATA[23] = \<const0> ;
  assign M1_AXIS_TDATA[22] = \<const0> ;
  assign M1_AXIS_TDATA[21] = \<const0> ;
  assign M1_AXIS_TDATA[20] = \<const0> ;
  assign M1_AXIS_TDATA[19] = \<const0> ;
  assign M1_AXIS_TDATA[18] = \<const0> ;
  assign M1_AXIS_TDATA[17] = \<const0> ;
  assign M1_AXIS_TDATA[16] = \<const0> ;
  assign M1_AXIS_TDATA[15] = \<const0> ;
  assign M1_AXIS_TDATA[14] = \<const0> ;
  assign M1_AXIS_TDATA[13] = \<const0> ;
  assign M1_AXIS_TDATA[12] = \<const0> ;
  assign M1_AXIS_TDATA[11] = \<const0> ;
  assign M1_AXIS_TDATA[10] = \<const0> ;
  assign M1_AXIS_TDATA[9] = \<const0> ;
  assign M1_AXIS_TDATA[8] = \<const0> ;
  assign M1_AXIS_TDATA[7] = \<const0> ;
  assign M1_AXIS_TDATA[6] = \<const0> ;
  assign M1_AXIS_TDATA[5] = \<const0> ;
  assign M1_AXIS_TDATA[4] = \<const0> ;
  assign M1_AXIS_TDATA[3] = \<const0> ;
  assign M1_AXIS_TDATA[2] = \<const0> ;
  assign M1_AXIS_TDATA[1] = \<const0> ;
  assign M1_AXIS_TDATA[0] = \<const0> ;
  assign M1_AXIS_TLAST = \<const0> ;
  assign M1_AXIS_TVALID = \<const0> ;
  assign S0_AXIS_TREADY = \<const0> ;
  assign S0_AXI_AWREADY = S0_AXI_WREADY;
  assign S0_AXI_BRESP[1] = \<const0> ;
  assign S0_AXI_BRESP[0] = \<const0> ;
  assign S0_AXI_RRESP[1] = \<const0> ;
  assign S0_AXI_RRESP[0] = \<const0> ;
  assign S1_AXIS_TREADY = \<const0> ;
  assign S1_AXI_AWREADY = S1_AXI_WREADY;
  assign S1_AXI_BRESP[1] = \<const0> ;
  assign S1_AXI_BRESP[0] = \<const0> ;
  assign S1_AXI_RRESP[1] = \<const0> ;
  assign S1_AXI_RRESP[0] = \<const0> ;
  assign SYS_Rst_Input = SYS_Rst;
  GND GND
       (.G(\<const0> ));
  LUT3 #(
    .INIT(8'hBF)) 
    \Rst_Sync.SYS_Rst_I_i_1 
       (.I0(SYS_Rst_Input_d2),
        .I1(S0_AXI_ARESETN),
        .I2(S1_AXI_ARESETN),
        .O(\Rst_Sync.SYS_Rst_I_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \Rst_Sync.SYS_Rst_I_reg 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\Rst_Sync.SYS_Rst_I_i_1_n_0 ),
        .Q(SYS_Rst_I),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Rst_Sync.SYS_Rst_Input_d1_reg 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(SYS_Rst_Input),
        .Q(SYS_Rst_Input_d1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Rst_Sync.SYS_Rst_Input_d2_reg 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(SYS_Rst_Input_d1),
        .Q(SYS_Rst_Input_d2),
        .R(1'b0));
  design_1_mailbox_1_0_if_decode \Using_Bus_0.Bus0_If 
       (.\Addr_Counters[0].FDRE_I (\Using_Bus_0.Bus0_If_n_19 ),
        .\Addr_Counters[3].FDRE_I (fsl_1_to_0_n_37),
        .Bus_RNW_reg(\Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/Bus_RNW_reg ),
        .CI(\Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/CI ),
        .D({fsl_1_to_0_n_9,fsl_1_to_0_n_10,fsl_1_to_0_n_11,fsl_1_to_0_n_12,fsl_1_to_0_n_13,fsl_1_to_0_n_14,fsl_1_to_0_n_15,fsl_1_to_0_n_16,fsl_1_to_0_n_17,fsl_1_to_0_n_18,fsl_1_to_0_n_19,fsl_1_to_0_n_20,fsl_1_to_0_n_21,fsl_1_to_0_n_22,fsl_1_to_0_n_23,fsl_1_to_0_n_24,fsl_1_to_0_n_25,fsl_1_to_0_n_26,fsl_1_to_0_n_27,fsl_1_to_0_n_28,fsl_1_to_0_n_29,fsl_1_to_0_n_30,fsl_1_to_0_n_31,fsl_1_to_0_n_32,fsl_1_to_0_n_33,fsl_1_to_0_n_34,fsl_1_to_0_n_35,fsl_1_to_0_n_36}),
        .E(\Using_Bus_0.Bus0_If_n_11 ),
        .FDRE_I1(fsl_0_to_1_n_0),
        .FDRE_I1_0(\Using_Bus_1.Bus1_If_n_10 ),
        .FSL0_S_Data_I({FSL0_S_Data_I[28],FSL0_S_Data_I[29],FSL0_S_Data_I[30],FSL0_S_Data_I[31]}),
        .FSL0_S_Exists_I(FSL0_S_Exists_I),
        .\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg (\Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .Interrupt_0(Interrupt_0),
        .Q({sit_register[0],sit_register[1]}),
        .S0_AXI_ACLK(S0_AXI_ACLK),
        .S0_AXI_ARADDR(S0_AXI_ARADDR[5:2]),
        .S0_AXI_ARREADY(S0_AXI_ARREADY),
        .S0_AXI_ARVALID(S0_AXI_ARVALID),
        .S0_AXI_AWADDR(S0_AXI_AWADDR[5:2]),
        .S0_AXI_AWVALID(S0_AXI_AWVALID),
        .S0_AXI_BREADY(S0_AXI_BREADY),
        .S0_AXI_BVALID(S0_AXI_BVALID),
        .S0_AXI_RDATA(S0_AXI_RDATA),
        .S0_AXI_RREADY(S0_AXI_RREADY),
        .S0_AXI_RVALID(S0_AXI_RVALID),
        .S0_AXI_WDATA(S0_AXI_WDATA[3:0]),
        .S0_AXI_WREADY(S0_AXI_WREADY),
        .S0_AXI_WVALID(S0_AXI_WVALID),
        .SYS_Rst_I(SYS_Rst_I),
        .\fifo_length_i_reg[2] (\Using_Bus_0.Bus0_If_n_10 ),
        .\fifo_length_i_reg[2]_0 (fsl_1_to_0_n_39),
        .\fifo_length_i_reg[2]_1 ({\Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/fifo_length_i_reg_6 [2],\Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/fifo_length_i_reg_6 [3],\Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/fifo_length_i_reg_6 [4]}),
        .\fifo_length_i_reg[2]_2 ({\Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/fifo_length_i_reg [2],\Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/fifo_length_i_reg [3],\Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/fifo_length_i_reg [4]}),
        .\fifo_length_i_reg[3] (\Using_Bus_0.Bus0_If_n_7 ),
        .next_Data_Exists(\Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/next_Data_Exists ),
        .rit_detect_d0(rit_detect_d0_5),
        .rit_detect_d1_reg_0(\Using_Bus_0.Bus0_If_n_16 ),
        .\s_axi_rdata_i_reg[3] (\Using_Bus_0.Bus0_If_n_8 ),
        .\s_axi_rdata_i_reg[3]_0 ({rit_register[0],rit_register[1]}),
        .sit_detect_d0(sit_detect_d0),
        .sit_detect_d1_reg_0(\Using_Bus_0.Bus0_If_n_17 ));
  design_1_mailbox_1_0_if_decode__parameterized1 \Using_Bus_1.Bus1_If 
       (.\Addr_Counters[0].FDRE_I (\Using_Bus_1.Bus1_If_n_19 ),
        .\Addr_Counters[3].FDRE_I (fsl_0_to_1_n_37),
        .Bus_RNW_reg(\Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/Bus_RNW_reg_2 ),
        .CI(\Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/CI_0 ),
        .D({fsl_0_to_1_n_9,fsl_0_to_1_n_10,fsl_0_to_1_n_11,fsl_0_to_1_n_12,fsl_0_to_1_n_13,fsl_0_to_1_n_14,fsl_0_to_1_n_15,fsl_0_to_1_n_16,fsl_0_to_1_n_17,fsl_0_to_1_n_18,fsl_0_to_1_n_19,fsl_0_to_1_n_20,fsl_0_to_1_n_21,fsl_0_to_1_n_22,fsl_0_to_1_n_23,fsl_0_to_1_n_24,fsl_0_to_1_n_25,fsl_0_to_1_n_26,fsl_0_to_1_n_27,fsl_0_to_1_n_28,fsl_0_to_1_n_29,fsl_0_to_1_n_30,fsl_0_to_1_n_31,fsl_0_to_1_n_32,fsl_0_to_1_n_33,fsl_0_to_1_n_34,fsl_0_to_1_n_35,fsl_0_to_1_n_36}),
        .E(\Using_Bus_1.Bus1_If_n_6 ),
        .FDRE_I1(\Using_Bus_0.Bus0_If_n_7 ),
        .FDRE_I1_0(fsl_1_to_0_n_0),
        .FSL1_S_Data_I({FSL1_S_Data_I[28],FSL1_S_Data_I[29],FSL1_S_Data_I[30],FSL1_S_Data_I[31]}),
        .FSL1_S_Exists_I(FSL1_S_Exists_I),
        .\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg (\Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg_3 ),
        .Interrupt_1(Interrupt_1),
        .Q({\Using_Bus_1.Bus1_If_n_12 ,\Using_Bus_1.Bus1_If_n_13 }),
        .S1_AXI_ACLK(S1_AXI_ACLK),
        .S1_AXI_ARADDR(S1_AXI_ARADDR[5:2]),
        .S1_AXI_ARREADY(S1_AXI_ARREADY),
        .S1_AXI_ARVALID(S1_AXI_ARVALID),
        .S1_AXI_AWADDR(S1_AXI_AWADDR[5:2]),
        .S1_AXI_AWVALID(S1_AXI_AWVALID),
        .S1_AXI_BREADY(S1_AXI_BREADY),
        .S1_AXI_BVALID(S1_AXI_BVALID),
        .S1_AXI_RDATA(S1_AXI_RDATA),
        .S1_AXI_RREADY(S1_AXI_RREADY),
        .S1_AXI_RVALID(S1_AXI_RVALID),
        .S1_AXI_WDATA(S1_AXI_WDATA[3:0]),
        .S1_AXI_WREADY(S1_AXI_WREADY),
        .S1_AXI_WVALID(S1_AXI_WVALID),
        .SYS_Rst_I(SYS_Rst_I),
        .\fifo_length_i_reg[2] (\Using_Bus_1.Bus1_If_n_5 ),
        .\fifo_length_i_reg[2]_0 (fsl_0_to_1_n_40),
        .\fifo_length_i_reg[2]_1 ({\Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/fifo_length_i_reg [2],\Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/fifo_length_i_reg [3],\Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/fifo_length_i_reg [4]}),
        .\fifo_length_i_reg[2]_2 ({\Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/fifo_length_i_reg_6 [2],\Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/fifo_length_i_reg_6 [3],\Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/fifo_length_i_reg_6 [4]}),
        .\fifo_length_i_reg[3] (\Using_Bus_1.Bus1_If_n_10 ),
        .next_Data_Exists(\Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/next_Data_Exists_1 ),
        .rit_detect_d0(rit_detect_d0),
        .rit_detect_d1_reg_0(\Using_Bus_1.Bus1_If_n_16 ),
        .\s_axi_rdata_i_reg[3] (\Using_Bus_1.Bus1_If_n_11 ),
        .sit_detect_d0(sit_detect_d0_4),
        .sit_detect_d1_reg_0({\Using_Bus_1.Bus1_If_n_14 ,\Using_Bus_1.Bus1_If_n_15 }),
        .sit_detect_d1_reg_1(\Using_Bus_1.Bus1_If_n_17 ));
  design_1_mailbox_1_0_fsl_v20 fsl_0_to_1
       (.Bus_RNW_reg(\Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/Bus_RNW_reg_2 ),
        .Bus_RNW_reg_reg(\Using_Bus_1.Bus1_If_n_11 ),
        .CI(\Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/CI ),
        .D({fsl_0_to_1_n_9,fsl_0_to_1_n_10,fsl_0_to_1_n_11,fsl_0_to_1_n_12,fsl_0_to_1_n_13,fsl_0_to_1_n_14,fsl_0_to_1_n_15,fsl_0_to_1_n_16,fsl_0_to_1_n_17,fsl_0_to_1_n_18,fsl_0_to_1_n_19,fsl_0_to_1_n_20,fsl_0_to_1_n_21,fsl_0_to_1_n_22,fsl_0_to_1_n_23,fsl_0_to_1_n_24,fsl_0_to_1_n_25,fsl_0_to_1_n_26,fsl_0_to_1_n_27,fsl_0_to_1_n_28,fsl_0_to_1_n_29,fsl_0_to_1_n_30,fsl_0_to_1_n_31,fsl_0_to_1_n_32,fsl_0_to_1_n_33,fsl_0_to_1_n_34,fsl_0_to_1_n_35,fsl_0_to_1_n_36}),
        .E(\Using_Bus_1.Bus1_If_n_6 ),
        .FDRE_I1(\Using_Bus_0.Bus0_If_n_7 ),
        .FSL1_S_Exists_I(FSL1_S_Exists_I),
        .\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg (\Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg_3 ),
        .\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] (\Using_Bus_1.Bus1_If_n_19 ),
        .Q({\Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/fifo_length_i_reg [2],\Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/fifo_length_i_reg [3],\Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/fifo_length_i_reg [4]}),
        .S0_AXI_ACLK(S0_AXI_ACLK),
        .S0_AXI_WDATA(S0_AXI_WDATA),
        .SR(SYS_Rst_I),
        .data_Exists_I_reg(fsl_0_to_1_n_37),
        .data_Exists_I_reg_0(\Using_Bus_1.Bus1_If_n_5 ),
        .next_Data_Exists(\Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/next_Data_Exists_1 ),
        .rit_detect_d0(rit_detect_d0),
        .rit_detect_d1_reg(fsl_0_to_1_n_40),
        .\rit_register_reg[0] ({\Using_Bus_1.Bus1_If_n_12 ,\Using_Bus_1.Bus1_If_n_13 }),
        .\rit_register_reg[3] (\Using_Bus_1.Bus1_If_n_16 ),
        .\s_axi_rdata_i_reg[3] ({FSL1_S_Data_I[28],FSL1_S_Data_I[29],FSL1_S_Data_I[30],FSL1_S_Data_I[31]}),
        .sit_detect_d0(sit_detect_d0),
        .\sit_register_reg[0] ({sit_register[0],sit_register[1]}),
        .\sit_register_reg[3] (\Using_Bus_0.Bus0_If_n_17 ),
        .write_fsl_error_d1_reg(fsl_0_to_1_n_0));
  design_1_mailbox_1_0_fsl_v20_0 fsl_1_to_0
       (.Bus_RNW_reg(\Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/Bus_RNW_reg ),
        .Bus_RNW_reg_reg(\Using_Bus_0.Bus0_If_n_8 ),
        .CI(\Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/CI_0 ),
        .D({fsl_1_to_0_n_9,fsl_1_to_0_n_10,fsl_1_to_0_n_11,fsl_1_to_0_n_12,fsl_1_to_0_n_13,fsl_1_to_0_n_14,fsl_1_to_0_n_15,fsl_1_to_0_n_16,fsl_1_to_0_n_17,fsl_1_to_0_n_18,fsl_1_to_0_n_19,fsl_1_to_0_n_20,fsl_1_to_0_n_21,fsl_1_to_0_n_22,fsl_1_to_0_n_23,fsl_1_to_0_n_24,fsl_1_to_0_n_25,fsl_1_to_0_n_26,fsl_1_to_0_n_27,fsl_1_to_0_n_28,fsl_1_to_0_n_29,fsl_1_to_0_n_30,fsl_1_to_0_n_31,fsl_1_to_0_n_32,fsl_1_to_0_n_33,fsl_1_to_0_n_34,fsl_1_to_0_n_35,fsl_1_to_0_n_36}),
        .E(\Using_Bus_0.Bus0_If_n_11 ),
        .FDRE_I1(\Using_Bus_1.Bus1_If_n_10 ),
        .FSL0_S_Exists_I(FSL0_S_Exists_I),
        .\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg (\Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] (\Using_Bus_0.Bus0_If_n_19 ),
        .Q({\Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/fifo_length_i_reg_6 [2],\Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/fifo_length_i_reg_6 [3],\Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/fifo_length_i_reg_6 [4]}),
        .S0_AXI_ACLK(S0_AXI_ACLK),
        .S1_AXI_WDATA(S1_AXI_WDATA),
        .SR(SYS_Rst_I),
        .data_Exists_I_reg(fsl_1_to_0_n_37),
        .data_Exists_I_reg_0(\Using_Bus_0.Bus0_If_n_10 ),
        .next_Data_Exists(\Using_FIFO.Sync_FIFO_Gen.Sync_FIFO_I1/Sync_FIFO_I.srl_fifo_i.FSL_FIFO/next_Data_Exists ),
        .rit_detect_d0(rit_detect_d0_5),
        .rit_detect_d1_reg(fsl_1_to_0_n_39),
        .\rit_register_reg[0] ({rit_register[0],rit_register[1]}),
        .\rit_register_reg[3] (\Using_Bus_0.Bus0_If_n_16 ),
        .\s_axi_rdata_i_reg[3] ({FSL0_S_Data_I[28],FSL0_S_Data_I[29],FSL0_S_Data_I[30],FSL0_S_Data_I[31]}),
        .sit_detect_d0(sit_detect_d0_4),
        .\sit_register_reg[0] ({\Using_Bus_1.Bus1_If_n_14 ,\Using_Bus_1.Bus1_If_n_15 }),
        .\sit_register_reg[3] (\Using_Bus_1.Bus1_If_n_17 ),
        .write_fsl_error_d1_reg(fsl_1_to_0_n_0));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module design_1_mailbox_1_0_pselect_f
   (\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ,
    Q,
    \bus2ip_addr_i_reg[5] );
  output \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ;
  input Q;
  input [3:0]\bus2ip_addr_i_reg[5] ;

  wire \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ;
  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;

  LUT5 #(
    .INIT(32'h00000002)) 
    CS
       (.I0(Q),
        .I1(\bus2ip_addr_i_reg[5] [2]),
        .I2(\bus2ip_addr_i_reg[5] [1]),
        .I3(\bus2ip_addr_i_reg[5] [3]),
        .I4(\bus2ip_addr_i_reg[5] [0]),
        .O(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module design_1_mailbox_1_0_pselect_f_3
   (\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ,
    Q,
    \bus2ip_addr_i_reg[5] );
  output \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ;
  input Q;
  input [3:0]\bus2ip_addr_i_reg[5] ;

  wire \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ;
  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;

  LUT5 #(
    .INIT(32'h00000002)) 
    CS
       (.I0(Q),
        .I1(\bus2ip_addr_i_reg[5] [2]),
        .I2(\bus2ip_addr_i_reg[5] [1]),
        .I3(\bus2ip_addr_i_reg[5] [3]),
        .I4(\bus2ip_addr_i_reg[5] [0]),
        .O(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module design_1_mailbox_1_0_pselect_f__parameterized1
   (ce_expnd_i_8,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_8;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_8;

  LUT5 #(
    .INIT(32'h01000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [3]),
        .I1(\bus2ip_addr_i_reg[5] [2]),
        .I2(\bus2ip_addr_i_reg[5] [0]),
        .I3(\bus2ip_addr_i_reg[5] [1]),
        .I4(Q),
        .O(ce_expnd_i_8));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module design_1_mailbox_1_0_pselect_f__parameterized11
   (ce_expnd_i_8,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_8;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_8;

  LUT5 #(
    .INIT(32'h01000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [3]),
        .I1(\bus2ip_addr_i_reg[5] [2]),
        .I2(\bus2ip_addr_i_reg[5] [0]),
        .I3(\bus2ip_addr_i_reg[5] [1]),
        .I4(Q),
        .O(ce_expnd_i_8));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module design_1_mailbox_1_0_pselect_f__parameterized13
   (ce_expnd_i_6,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_6;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_6;

  LUT5 #(
    .INIT(32'h01000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [3]),
        .I1(\bus2ip_addr_i_reg[5] [1]),
        .I2(\bus2ip_addr_i_reg[5] [0]),
        .I3(\bus2ip_addr_i_reg[5] [2]),
        .I4(Q),
        .O(ce_expnd_i_6));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module design_1_mailbox_1_0_pselect_f__parameterized14
   (ce_expnd_i_5,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_5;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_5;

  LUT5 #(
    .INIT(32'h10000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [3]),
        .I1(\bus2ip_addr_i_reg[5] [1]),
        .I2(Q),
        .I3(\bus2ip_addr_i_reg[5] [0]),
        .I4(\bus2ip_addr_i_reg[5] [2]),
        .O(ce_expnd_i_5));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module design_1_mailbox_1_0_pselect_f__parameterized16
   (ce_expnd_i_3,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_3;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_3;

  LUT5 #(
    .INIT(32'h40000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [3]),
        .I1(Q),
        .I2(\bus2ip_addr_i_reg[5] [2]),
        .I3(\bus2ip_addr_i_reg[5] [0]),
        .I4(\bus2ip_addr_i_reg[5] [1]),
        .O(ce_expnd_i_3));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module design_1_mailbox_1_0_pselect_f__parameterized19
   (ce_expnd_i_0,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_0;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_0;

  LUT5 #(
    .INIT(32'h10000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [2]),
        .I1(\bus2ip_addr_i_reg[5] [0]),
        .I2(\bus2ip_addr_i_reg[5] [3]),
        .I3(\bus2ip_addr_i_reg[5] [1]),
        .I4(Q),
        .O(ce_expnd_i_0));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module design_1_mailbox_1_0_pselect_f__parameterized3
   (ce_expnd_i_6,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_6;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_6;

  LUT5 #(
    .INIT(32'h01000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [3]),
        .I1(\bus2ip_addr_i_reg[5] [1]),
        .I2(\bus2ip_addr_i_reg[5] [0]),
        .I3(\bus2ip_addr_i_reg[5] [2]),
        .I4(Q),
        .O(ce_expnd_i_6));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module design_1_mailbox_1_0_pselect_f__parameterized4
   (ce_expnd_i_5,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_5;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_5;

  LUT5 #(
    .INIT(32'h10000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [3]),
        .I1(\bus2ip_addr_i_reg[5] [1]),
        .I2(Q),
        .I3(\bus2ip_addr_i_reg[5] [0]),
        .I4(\bus2ip_addr_i_reg[5] [2]),
        .O(ce_expnd_i_5));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module design_1_mailbox_1_0_pselect_f__parameterized6
   (ce_expnd_i_3,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_3;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_3;

  LUT5 #(
    .INIT(32'h40000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [3]),
        .I1(Q),
        .I2(\bus2ip_addr_i_reg[5] [2]),
        .I3(\bus2ip_addr_i_reg[5] [0]),
        .I4(\bus2ip_addr_i_reg[5] [1]),
        .O(ce_expnd_i_3));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module design_1_mailbox_1_0_pselect_f__parameterized9
   (ce_expnd_i_0,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_0;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_0;

  LUT5 #(
    .INIT(32'h10000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [2]),
        .I1(\bus2ip_addr_i_reg[5] [0]),
        .I2(\bus2ip_addr_i_reg[5] [3]),
        .I3(\bus2ip_addr_i_reg[5] [1]),
        .I4(Q),
        .O(ce_expnd_i_0));
endmodule

(* ORIG_REF_NAME = "slave_attachment" *) 
module design_1_mailbox_1_0_slave_attachment
   (\ie_register_reg[2] ,
    \is_register_reg[2] ,
    read_fsl_error_d1_reg,
    Bus_RNW_reg_reg,
    S0_AXI_RVALID,
    S0_AXI_BVALID,
    error_detect,
    write_fsl_error,
    S0_AXI_WREADY,
    S0_AXI_ARREADY,
    CI,
    \fifo_length_i_reg[3] ,
    \s_axi_rdata_i_reg[3]_0 ,
    next_Data_Exists,
    \fifo_length_i_reg[2] ,
    E,
    \sit_register_reg[3] ,
    \rit_register_reg[3] ,
    rit_detect_d1_reg,
    sit_detect_d1_reg,
    full_error_reg,
    \Addr_Counters[0].FDRE_I ,
    empty_error_reg,
    read_fsl_error_d1_reg_0,
    S0_AXI_RDATA,
    SYS_Rst_I,
    S0_AXI_ACLK,
    write_fsl_error_d1,
    read_fsl_error_d1,
    FSL0_S_Exists_I,
    FDRE_I1,
    S0_AXI_ARVALID,
    FSL0_S_Data_I,
    full_error,
    FDRE_I1_0,
    \Addr_Counters[3].FDRE_I ,
    empty_error,
    p_6_in,
    Q,
    \rit_register_reg[0] ,
    sit_detect_d0,
    \fifo_length_i_reg[2]_0 ,
    ie_register,
    S0_AXI_WVALID,
    S0_AXI_AWVALID,
    S0_AXI_ARADDR,
    S0_AXI_AWADDR,
    S0_AXI_RREADY,
    S0_AXI_BREADY,
    \fifo_length_i_reg[2]_1 ,
    \fifo_length_i_reg[2]_2 ,
    D);
  output \ie_register_reg[2] ;
  output \is_register_reg[2] ;
  output read_fsl_error_d1_reg;
  output Bus_RNW_reg_reg;
  output S0_AXI_RVALID;
  output S0_AXI_BVALID;
  output error_detect;
  output write_fsl_error;
  output S0_AXI_WREADY;
  output S0_AXI_ARREADY;
  output CI;
  output \fifo_length_i_reg[3] ;
  output \s_axi_rdata_i_reg[3]_0 ;
  output next_Data_Exists;
  output \fifo_length_i_reg[2] ;
  output [0:0]E;
  output [0:0]\sit_register_reg[3] ;
  output [0:0]\rit_register_reg[3] ;
  output rit_detect_d1_reg;
  output sit_detect_d1_reg;
  output full_error_reg;
  output \Addr_Counters[0].FDRE_I ;
  output empty_error_reg;
  output read_fsl_error_d1_reg_0;
  output [31:0]S0_AXI_RDATA;
  input SYS_Rst_I;
  input S0_AXI_ACLK;
  input write_fsl_error_d1;
  input read_fsl_error_d1;
  input FSL0_S_Exists_I;
  input FDRE_I1;
  input S0_AXI_ARVALID;
  input [3:0]FSL0_S_Data_I;
  input full_error;
  input FDRE_I1_0;
  input \Addr_Counters[3].FDRE_I ;
  input empty_error;
  input [2:0]p_6_in;
  input [3:0]Q;
  input [3:0]\rit_register_reg[0] ;
  input sit_detect_d0;
  input \fifo_length_i_reg[2]_0 ;
  input [0:2]ie_register;
  input S0_AXI_WVALID;
  input S0_AXI_AWVALID;
  input [3:0]S0_AXI_ARADDR;
  input [3:0]S0_AXI_AWADDR;
  input S0_AXI_RREADY;
  input S0_AXI_BREADY;
  input [2:0]\fifo_length_i_reg[2]_1 ;
  input [2:0]\fifo_length_i_reg[2]_2 ;
  input [27:0]D;

  wire \Addr_Counters[0].FDRE_I ;
  wire \Addr_Counters[3].FDRE_I ;
  wire Bus_RNW_reg_reg;
  wire CI;
  wire [27:0]D;
  wire [0:0]E;
  wire FDRE_I1;
  wire FDRE_I1_0;
  wire [3:0]FSL0_S_Data_I;
  wire FSL0_S_Exists_I;
  wire \INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1_n_0 ;
  wire [4:0]\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 ;
  wire I_DECODER_n_12;
  wire I_DECODER_n_13;
  wire I_DECODER_n_14;
  wire I_DECODER_n_15;
  wire I_DECODER_n_22;
  wire I_DECODER_n_23;
  wire I_DECODER_n_6;
  wire I_DECODER_n_7;
  wire [3:0]Q;
  wire S0_AXI_ACLK;
  wire [3:0]S0_AXI_ARADDR;
  wire S0_AXI_ARREADY;
  wire S0_AXI_ARVALID;
  wire [3:0]S0_AXI_AWADDR;
  wire S0_AXI_AWVALID;
  wire S0_AXI_BREADY;
  wire S0_AXI_BVALID;
  wire [31:0]S0_AXI_RDATA;
  wire S0_AXI_RREADY;
  wire S0_AXI_RVALID;
  wire S0_AXI_WREADY;
  wire S0_AXI_WVALID;
  wire SYS_Rst_I;
  wire \bus2ip_addr_i[5]_i_1_n_0 ;
  wire \bus2ip_addr_i_reg_n_0_[2] ;
  wire \bus2ip_addr_i_reg_n_0_[3] ;
  wire \bus2ip_addr_i_reg_n_0_[4] ;
  wire \bus2ip_addr_i_reg_n_0_[5] ;
  wire bus2ip_rnw_i;
  wire bus2ip_rnw_i06_out;
  wire empty_error;
  wire empty_error_reg;
  wire error_detect;
  wire \fifo_length_i_reg[2] ;
  wire \fifo_length_i_reg[2]_0 ;
  wire [2:0]\fifo_length_i_reg[2]_1 ;
  wire [2:0]\fifo_length_i_reg[2]_2 ;
  wire \fifo_length_i_reg[3] ;
  wire full_error;
  wire full_error_reg;
  wire [0:2]ie_register;
  wire \ie_register_reg[2] ;
  wire is_read;
  wire is_read_i_1_n_0;
  wire is_read_i_2_n_0;
  wire \is_register_reg[2] ;
  wire is_write_i_1_n_0;
  wire is_write_reg_n_0;
  wire next_Data_Exists;
  wire [5:2]p_1_in;
  wire [2:0]p_6_in;
  wire [4:0]plusOp;
  wire read_fsl_error_d1;
  wire read_fsl_error_d1_reg;
  wire read_fsl_error_d1_reg_0;
  wire rit_detect_d1_reg;
  wire [3:0]\rit_register_reg[0] ;
  wire [0:0]\rit_register_reg[3] ;
  wire rst;
  wire s_axi_rdata_i;
  wire \s_axi_rdata_i_reg[3]_0 ;
  wire sit_detect_d0;
  wire sit_detect_d1_reg;
  wire [0:0]\sit_register_reg[3] ;
  wire start2;
  wire start2_i_1_n_0;
  wire [1:0]state;
  wire \state[0]_i_2_n_0 ;
  wire \state[1]_i_2_n_0 ;
  wire \state[1]_i_3_n_0 ;
  wire write_fsl_error;
  wire write_fsl_error_d1;

  LUT1 #(
    .INIT(2'h1)) 
    \INCLUDE_DPHASE_TIMER.dpto_cnt[0]_i_1 
       (.I0(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [0]),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \INCLUDE_DPHASE_TIMER.dpto_cnt[1]_i_1 
       (.I0(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [1]),
        .I1(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [0]),
        .O(plusOp[1]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \INCLUDE_DPHASE_TIMER.dpto_cnt[2]_i_1 
       (.I0(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [2]),
        .I1(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [0]),
        .I2(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [1]),
        .O(plusOp[2]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \INCLUDE_DPHASE_TIMER.dpto_cnt[3]_i_1 
       (.I0(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [3]),
        .I1(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [1]),
        .I2(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [0]),
        .I3(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [2]),
        .O(plusOp[3]));
  LUT2 #(
    .INIT(4'h9)) 
    \INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1 
       (.I0(state[1]),
        .I1(state[0]),
        .O(\INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_2 
       (.I0(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [4]),
        .I1(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [2]),
        .I2(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [0]),
        .I3(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [1]),
        .I4(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [3]),
        .O(plusOp[4]));
  FDRE \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(plusOp[0]),
        .Q(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [0]),
        .R(\INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1_n_0 ));
  FDRE \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(plusOp[1]),
        .Q(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [1]),
        .R(\INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1_n_0 ));
  FDRE \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(plusOp[2]),
        .Q(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [2]),
        .R(\INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1_n_0 ));
  FDRE \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[3] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(plusOp[3]),
        .Q(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [3]),
        .R(\INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1_n_0 ));
  FDRE \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(plusOp[4]),
        .Q(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [4]),
        .R(\INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1_n_0 ));
  design_1_mailbox_1_0_address_decoder I_DECODER
       (.\Addr_Counters[0].FDRE_I (\Addr_Counters[0].FDRE_I ),
        .\Addr_Counters[3].FDRE_I (\Addr_Counters[3].FDRE_I ),
        .Bus_RNW_reg_reg_0(Bus_RNW_reg_reg),
        .CI(CI),
        .D({I_DECODER_n_6,I_DECODER_n_7}),
        .E(E),
        .FDRE_I1(FDRE_I1),
        .FDRE_I1_0(FDRE_I1_0),
        .FSL0_S_Data_I(FSL0_S_Data_I),
        .FSL0_S_Exists_I(FSL0_S_Exists_I),
        .\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] (\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 ),
        .Q(start2),
        .S0_AXI_ACLK(S0_AXI_ACLK),
        .S0_AXI_ARREADY(S0_AXI_ARREADY),
        .S0_AXI_ARVALID(S0_AXI_ARVALID),
        .S0_AXI_BREADY(S0_AXI_BREADY),
        .S0_AXI_RREADY(S0_AXI_RREADY),
        .S0_AXI_WREADY(S0_AXI_WREADY),
        .S0_AXI_WVALID(\state[1]_i_3_n_0 ),
        .SYS_Rst_I(SYS_Rst_I),
        .\bus2ip_addr_i_reg[5] ({\bus2ip_addr_i_reg_n_0_[5] ,\bus2ip_addr_i_reg_n_0_[4] ,\bus2ip_addr_i_reg_n_0_[3] ,\bus2ip_addr_i_reg_n_0_[2] }),
        .bus2ip_rnw_i(bus2ip_rnw_i),
        .empty_error(empty_error),
        .empty_error_reg(empty_error_reg),
        .error_detect(error_detect),
        .\fifo_length_i_reg[2] (\fifo_length_i_reg[2] ),
        .\fifo_length_i_reg[2]_0 (\fifo_length_i_reg[2]_0 ),
        .\fifo_length_i_reg[3] (\fifo_length_i_reg[3] ),
        .full_error(full_error),
        .full_error_reg(full_error_reg),
        .ie_register(ie_register),
        .\ie_register_reg[2] (\ie_register_reg[2] ),
        .is_read(is_read),
        .\is_register_reg[2] (\is_register_reg[2] ),
        .is_write_reg(is_write_reg_n_0),
        .next_Data_Exists(next_Data_Exists),
        .p_6_in(p_6_in),
        .read_fsl_error_d1(read_fsl_error_d1),
        .read_fsl_error_d1_reg(read_fsl_error_d1_reg),
        .read_fsl_error_d1_reg_0(read_fsl_error_d1_reg_0),
        .\rit_register_reg[0] (\rit_register_reg[0] ),
        .\rit_register_reg[3] (\rit_register_reg[3] ),
        .s_axi_bvalid_i_reg(I_DECODER_n_23),
        .s_axi_bvalid_i_reg_0(S0_AXI_BVALID),
        .\s_axi_rdata_i_reg[3] ({I_DECODER_n_12,I_DECODER_n_13,I_DECODER_n_14,I_DECODER_n_15}),
        .\s_axi_rdata_i_reg[3]_0 (\s_axi_rdata_i_reg[3]_0 ),
        .s_axi_rvalid_i_reg(I_DECODER_n_22),
        .s_axi_rvalid_i_reg_0(\state[1]_i_2_n_0 ),
        .s_axi_rvalid_i_reg_1(S0_AXI_RVALID),
        .sit_detect_d0(sit_detect_d0),
        .\sit_register_reg[0] (Q),
        .\sit_register_reg[3] (\sit_register_reg[3] ),
        .\state_reg[0] (\state[0]_i_2_n_0 ),
        .\state_reg[1] (state),
        .write_fsl_error(write_fsl_error),
        .write_fsl_error_d1(write_fsl_error_d1));
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    \bus2ip_addr_i[2]_i_1 
       (.I0(S0_AXI_ARADDR[0]),
        .I1(state[1]),
        .I2(S0_AXI_ARVALID),
        .I3(state[0]),
        .I4(S0_AXI_AWADDR[0]),
        .O(p_1_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    \bus2ip_addr_i[3]_i_1 
       (.I0(S0_AXI_ARADDR[1]),
        .I1(state[1]),
        .I2(S0_AXI_ARVALID),
        .I3(state[0]),
        .I4(S0_AXI_AWADDR[1]),
        .O(p_1_in[3]));
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    \bus2ip_addr_i[4]_i_1 
       (.I0(S0_AXI_ARADDR[2]),
        .I1(state[1]),
        .I2(S0_AXI_ARVALID),
        .I3(state[0]),
        .I4(S0_AXI_AWADDR[2]),
        .O(p_1_in[4]));
  LUT5 #(
    .INIT(32'h000000EA)) 
    \bus2ip_addr_i[5]_i_1 
       (.I0(S0_AXI_ARVALID),
        .I1(S0_AXI_WVALID),
        .I2(S0_AXI_AWVALID),
        .I3(state[1]),
        .I4(state[0]),
        .O(\bus2ip_addr_i[5]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    \bus2ip_addr_i[5]_i_2 
       (.I0(S0_AXI_ARADDR[3]),
        .I1(state[1]),
        .I2(S0_AXI_ARVALID),
        .I3(state[0]),
        .I4(S0_AXI_AWADDR[3]),
        .O(p_1_in[5]));
  FDRE \bus2ip_addr_i_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(\bus2ip_addr_i[5]_i_1_n_0 ),
        .D(p_1_in[2]),
        .Q(\bus2ip_addr_i_reg_n_0_[2] ),
        .R(rst));
  FDRE \bus2ip_addr_i_reg[3] 
       (.C(S0_AXI_ACLK),
        .CE(\bus2ip_addr_i[5]_i_1_n_0 ),
        .D(p_1_in[3]),
        .Q(\bus2ip_addr_i_reg_n_0_[3] ),
        .R(rst));
  FDRE \bus2ip_addr_i_reg[4] 
       (.C(S0_AXI_ACLK),
        .CE(\bus2ip_addr_i[5]_i_1_n_0 ),
        .D(p_1_in[4]),
        .Q(\bus2ip_addr_i_reg_n_0_[4] ),
        .R(rst));
  FDRE \bus2ip_addr_i_reg[5] 
       (.C(S0_AXI_ACLK),
        .CE(\bus2ip_addr_i[5]_i_1_n_0 ),
        .D(p_1_in[5]),
        .Q(\bus2ip_addr_i_reg_n_0_[5] ),
        .R(rst));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h04)) 
    bus2ip_rnw_i_i_1
       (.I0(state[1]),
        .I1(S0_AXI_ARVALID),
        .I2(state[0]),
        .O(bus2ip_rnw_i06_out));
  FDRE bus2ip_rnw_i_reg
       (.C(S0_AXI_ACLK),
        .CE(\bus2ip_addr_i[5]_i_1_n_0 ),
        .D(bus2ip_rnw_i06_out),
        .Q(bus2ip_rnw_i),
        .R(rst));
  LUT4 #(
    .INIT(16'h2F20)) 
    is_read_i_1
       (.I0(S0_AXI_ARVALID),
        .I1(state[1]),
        .I2(is_read_i_2_n_0),
        .I3(is_read),
        .O(is_read_i_1_n_0));
  LUT6 #(
    .INIT(64'hF88800000000FFFF)) 
    is_read_i_2
       (.I0(S0_AXI_BREADY),
        .I1(S0_AXI_BVALID),
        .I2(S0_AXI_RREADY),
        .I3(S0_AXI_RVALID),
        .I4(state[1]),
        .I5(state[0]),
        .O(is_read_i_2_n_0));
  FDRE is_read_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(is_read_i_1_n_0),
        .Q(is_read),
        .R(rst));
  LUT6 #(
    .INIT(64'h0040FFFF00400000)) 
    is_write_i_1
       (.I0(state[1]),
        .I1(S0_AXI_WVALID),
        .I2(S0_AXI_AWVALID),
        .I3(S0_AXI_ARVALID),
        .I4(is_read_i_2_n_0),
        .I5(is_write_reg_n_0),
        .O(is_write_i_1_n_0));
  FDRE is_write_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(is_write_i_1_n_0),
        .Q(is_write_reg_n_0),
        .R(rst));
  FDRE rst_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(SYS_Rst_I),
        .Q(rst),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    s_axi_bvalid_i_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(I_DECODER_n_23),
        .Q(S0_AXI_BVALID),
        .R(rst));
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[31]_i_1 
       (.I0(state[0]),
        .I1(state[1]),
        .O(s_axi_rdata_i));
  LUT6 #(
    .INIT(64'hBB0BFF0F0000BB0B)) 
    \s_axi_rdata_i[3]_i_4 
       (.I0(\rit_register_reg[0] [0]),
        .I1(\fifo_length_i_reg[2]_1 [0]),
        .I2(\fifo_length_i_reg[2]_1 [2]),
        .I3(\rit_register_reg[0] [2]),
        .I4(\fifo_length_i_reg[2]_1 [1]),
        .I5(\rit_register_reg[0] [1]),
        .O(rit_detect_d1_reg));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(I_DECODER_n_15),
        .Q(S0_AXI_RDATA[0]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[10] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[6]),
        .Q(S0_AXI_RDATA[10]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[11] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[7]),
        .Q(S0_AXI_RDATA[11]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[12] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[8]),
        .Q(S0_AXI_RDATA[12]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[13] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[9]),
        .Q(S0_AXI_RDATA[13]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[14] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[10]),
        .Q(S0_AXI_RDATA[14]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[15] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[11]),
        .Q(S0_AXI_RDATA[15]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[16] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[12]),
        .Q(S0_AXI_RDATA[16]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[17] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[13]),
        .Q(S0_AXI_RDATA[17]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[18] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[14]),
        .Q(S0_AXI_RDATA[18]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[19] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[15]),
        .Q(S0_AXI_RDATA[19]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(I_DECODER_n_14),
        .Q(S0_AXI_RDATA[1]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[20] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[16]),
        .Q(S0_AXI_RDATA[20]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[21] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[17]),
        .Q(S0_AXI_RDATA[21]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[22] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[18]),
        .Q(S0_AXI_RDATA[22]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[23] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[19]),
        .Q(S0_AXI_RDATA[23]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[24] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[20]),
        .Q(S0_AXI_RDATA[24]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[25] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[21]),
        .Q(S0_AXI_RDATA[25]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[26] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[22]),
        .Q(S0_AXI_RDATA[26]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[27] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[23]),
        .Q(S0_AXI_RDATA[27]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[28] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[24]),
        .Q(S0_AXI_RDATA[28]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[29] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[25]),
        .Q(S0_AXI_RDATA[29]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(I_DECODER_n_13),
        .Q(S0_AXI_RDATA[2]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[30] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[26]),
        .Q(S0_AXI_RDATA[30]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[31] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[27]),
        .Q(S0_AXI_RDATA[31]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[3] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(I_DECODER_n_12),
        .Q(S0_AXI_RDATA[3]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[4] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[0]),
        .Q(S0_AXI_RDATA[4]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[5] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[1]),
        .Q(S0_AXI_RDATA[5]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[6] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[2]),
        .Q(S0_AXI_RDATA[6]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[7] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[3]),
        .Q(S0_AXI_RDATA[7]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[8] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[4]),
        .Q(S0_AXI_RDATA[8]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[9] 
       (.C(S0_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[5]),
        .Q(S0_AXI_RDATA[9]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    s_axi_rvalid_i_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(I_DECODER_n_22),
        .Q(S0_AXI_RVALID),
        .R(rst));
  LUT6 #(
    .INIT(64'hBBFB00F0FFFFBBFB)) 
    sit_detect_d1_i_2
       (.I0(Q[0]),
        .I1(\fifo_length_i_reg[2]_2 [0]),
        .I2(Q[2]),
        .I3(\fifo_length_i_reg[2]_2 [2]),
        .I4(Q[1]),
        .I5(\fifo_length_i_reg[2]_2 [1]),
        .O(sit_detect_d1_reg));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT5 #(
    .INIT(32'h00000F08)) 
    start2_i_1
       (.I0(S0_AXI_WVALID),
        .I1(S0_AXI_AWVALID),
        .I2(state[0]),
        .I3(S0_AXI_ARVALID),
        .I4(state[1]),
        .O(start2_i_1_n_0));
  FDRE start2_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(start2_i_1_n_0),
        .Q(start2),
        .R(rst));
  LUT6 #(
    .INIT(64'h22222AAA2AAA2AAA)) 
    \state[0]_i_2 
       (.I0(state[0]),
        .I1(state[1]),
        .I2(S0_AXI_RVALID),
        .I3(S0_AXI_RREADY),
        .I4(S0_AXI_BVALID),
        .I5(S0_AXI_BREADY),
        .O(\state[0]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \state[1]_i_2 
       (.I0(S0_AXI_RVALID),
        .I1(S0_AXI_RREADY),
        .I2(S0_AXI_BVALID),
        .I3(S0_AXI_BREADY),
        .O(\state[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hBF)) 
    \state[1]_i_3 
       (.I0(S0_AXI_ARVALID),
        .I1(S0_AXI_AWVALID),
        .I2(S0_AXI_WVALID),
        .O(\state[1]_i_3_n_0 ));
  FDRE \state_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(I_DECODER_n_7),
        .Q(state[0]),
        .R(rst));
  FDRE \state_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(I_DECODER_n_6),
        .Q(state[1]),
        .R(rst));
endmodule

(* ORIG_REF_NAME = "slave_attachment" *) 
module design_1_mailbox_1_0_slave_attachment__parameterized0
   (\ie_register_reg[2] ,
    \is_register_reg[2] ,
    read_fsl_error_d1_reg,
    Bus_RNW_reg_reg,
    S1_AXI_RVALID,
    S1_AXI_BVALID,
    next_Data_Exists,
    \fifo_length_i_reg[2] ,
    E,
    error_detect,
    write_fsl_error,
    S1_AXI_ARREADY,
    S1_AXI_WREADY,
    CI,
    \fifo_length_i_reg[3] ,
    \s_axi_rdata_i_reg[3]_0 ,
    \sit_register_reg[3] ,
    \rit_register_reg[3] ,
    rit_detect_d1_reg,
    sit_detect_d1_reg,
    \Addr_Counters[0].FDRE_I ,
    empty_error_reg,
    read_fsl_error_d1_reg_0,
    full_error_reg,
    S1_AXI_RDATA,
    SYS_Rst_I,
    S1_AXI_ACLK,
    FDRE_I1,
    \Addr_Counters[3].FDRE_I ,
    FSL1_S_Exists_I,
    write_fsl_error_d1,
    read_fsl_error_d1,
    FDRE_I1_0,
    S1_AXI_ARVALID,
    FSL1_S_Data_I,
    full_error_reg_0,
    p_6_in,
    Q,
    \rit_register_reg[0] ,
    sit_detect_d0,
    \fifo_length_i_reg[2]_0 ,
    ie_register,
    empty_error_reg_0,
    S1_AXI_WVALID,
    S1_AXI_AWVALID,
    S1_AXI_ARADDR,
    S1_AXI_AWADDR,
    S1_AXI_RREADY,
    S1_AXI_BREADY,
    \fifo_length_i_reg[2]_1 ,
    \fifo_length_i_reg[2]_2 ,
    D);
  output \ie_register_reg[2] ;
  output \is_register_reg[2] ;
  output read_fsl_error_d1_reg;
  output Bus_RNW_reg_reg;
  output S1_AXI_RVALID;
  output S1_AXI_BVALID;
  output next_Data_Exists;
  output \fifo_length_i_reg[2] ;
  output [0:0]E;
  output error_detect;
  output write_fsl_error;
  output S1_AXI_ARREADY;
  output S1_AXI_WREADY;
  output CI;
  output \fifo_length_i_reg[3] ;
  output \s_axi_rdata_i_reg[3]_0 ;
  output [0:0]\sit_register_reg[3] ;
  output [0:0]\rit_register_reg[3] ;
  output rit_detect_d1_reg;
  output sit_detect_d1_reg;
  output \Addr_Counters[0].FDRE_I ;
  output empty_error_reg;
  output read_fsl_error_d1_reg_0;
  output full_error_reg;
  output [31:0]S1_AXI_RDATA;
  input SYS_Rst_I;
  input S1_AXI_ACLK;
  input FDRE_I1;
  input \Addr_Counters[3].FDRE_I ;
  input FSL1_S_Exists_I;
  input write_fsl_error_d1;
  input read_fsl_error_d1;
  input FDRE_I1_0;
  input S1_AXI_ARVALID;
  input [3:0]FSL1_S_Data_I;
  input full_error_reg_0;
  input [2:0]p_6_in;
  input [3:0]Q;
  input [3:0]\rit_register_reg[0] ;
  input sit_detect_d0;
  input \fifo_length_i_reg[2]_0 ;
  input [0:2]ie_register;
  input empty_error_reg_0;
  input S1_AXI_WVALID;
  input S1_AXI_AWVALID;
  input [3:0]S1_AXI_ARADDR;
  input [3:0]S1_AXI_AWADDR;
  input S1_AXI_RREADY;
  input S1_AXI_BREADY;
  input [2:0]\fifo_length_i_reg[2]_1 ;
  input [2:0]\fifo_length_i_reg[2]_2 ;
  input [27:0]D;

  wire \Addr_Counters[0].FDRE_I ;
  wire \Addr_Counters[3].FDRE_I ;
  wire Bus_RNW_reg_reg;
  wire CI;
  wire [27:0]D;
  wire [0:0]E;
  wire FDRE_I1;
  wire FDRE_I1_0;
  wire [3:0]FSL1_S_Data_I;
  wire FSL1_S_Exists_I;
  wire \INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1__0_n_0 ;
  wire [4:0]\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 ;
  wire I_DECODER_n_11;
  wire I_DECODER_n_12;
  wire I_DECODER_n_15;
  wire I_DECODER_n_16;
  wire I_DECODER_n_17;
  wire I_DECODER_n_18;
  wire I_DECODER_n_22;
  wire I_DECODER_n_23;
  wire [3:0]Q;
  wire S1_AXI_ACLK;
  wire [3:0]S1_AXI_ARADDR;
  wire S1_AXI_ARREADY;
  wire S1_AXI_ARVALID;
  wire [3:0]S1_AXI_AWADDR;
  wire S1_AXI_AWVALID;
  wire S1_AXI_BREADY;
  wire S1_AXI_BVALID;
  wire [31:0]S1_AXI_RDATA;
  wire S1_AXI_RREADY;
  wire S1_AXI_RVALID;
  wire S1_AXI_WREADY;
  wire S1_AXI_WVALID;
  wire SYS_Rst_I;
  wire \bus2ip_addr_i[2]_i_1__0_n_0 ;
  wire \bus2ip_addr_i[3]_i_1__0_n_0 ;
  wire \bus2ip_addr_i[4]_i_1__0_n_0 ;
  wire \bus2ip_addr_i[5]_i_1__0_n_0 ;
  wire \bus2ip_addr_i[5]_i_2__0_n_0 ;
  wire \bus2ip_addr_i_reg_n_0_[2] ;
  wire \bus2ip_addr_i_reg_n_0_[3] ;
  wire \bus2ip_addr_i_reg_n_0_[4] ;
  wire \bus2ip_addr_i_reg_n_0_[5] ;
  wire bus2ip_rnw_i06_out;
  wire bus2ip_rnw_i_reg_n_0;
  wire empty_error_reg;
  wire empty_error_reg_0;
  wire error_detect;
  wire \fifo_length_i_reg[2] ;
  wire \fifo_length_i_reg[2]_0 ;
  wire [2:0]\fifo_length_i_reg[2]_1 ;
  wire [2:0]\fifo_length_i_reg[2]_2 ;
  wire \fifo_length_i_reg[3] ;
  wire full_error_reg;
  wire full_error_reg_0;
  wire [0:2]ie_register;
  wire \ie_register_reg[2] ;
  wire is_read;
  wire is_read_i_1__0_n_0;
  wire is_read_i_2__0_n_0;
  wire \is_register_reg[2] ;
  wire is_write_i_1__0_n_0;
  wire is_write_reg_n_0;
  wire next_Data_Exists;
  wire [2:0]p_6_in;
  wire [4:0]plusOp;
  wire read_fsl_error_d1;
  wire read_fsl_error_d1_reg;
  wire read_fsl_error_d1_reg_0;
  wire rit_detect_d1_reg;
  wire [3:0]\rit_register_reg[0] ;
  wire [0:0]\rit_register_reg[3] ;
  wire rst;
  wire s_axi_rdata_i;
  wire \s_axi_rdata_i_reg[3]_0 ;
  wire sit_detect_d0;
  wire sit_detect_d1_reg;
  wire [0:0]\sit_register_reg[3] ;
  wire start2;
  wire start2_i_1_n_0;
  wire [1:0]state;
  wire \state[0]_i_2__0_n_0 ;
  wire \state[1]_i_2__0_n_0 ;
  wire \state[1]_i_3__0_n_0 ;
  wire write_fsl_error;
  wire write_fsl_error_d1;

  LUT1 #(
    .INIT(2'h1)) 
    \INCLUDE_DPHASE_TIMER.dpto_cnt[0]_i_1__0 
       (.I0(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [0]),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \INCLUDE_DPHASE_TIMER.dpto_cnt[1]_i_1__0 
       (.I0(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [1]),
        .I1(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [0]),
        .O(plusOp[1]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \INCLUDE_DPHASE_TIMER.dpto_cnt[2]_i_1__0 
       (.I0(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [2]),
        .I1(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [0]),
        .I2(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [1]),
        .O(plusOp[2]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \INCLUDE_DPHASE_TIMER.dpto_cnt[3]_i_1__0 
       (.I0(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [3]),
        .I1(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [1]),
        .I2(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [0]),
        .I3(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [2]),
        .O(plusOp[3]));
  LUT2 #(
    .INIT(4'h9)) 
    \INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1__0 
       (.I0(state[1]),
        .I1(state[0]),
        .O(\INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_2__0 
       (.I0(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [4]),
        .I1(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [2]),
        .I2(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [0]),
        .I3(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [1]),
        .I4(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [3]),
        .O(plusOp[4]));
  FDRE \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(plusOp[0]),
        .Q(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [0]),
        .R(\INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1__0_n_0 ));
  FDRE \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[1] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(plusOp[1]),
        .Q(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [1]),
        .R(\INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1__0_n_0 ));
  FDRE \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(plusOp[2]),
        .Q(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [2]),
        .R(\INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1__0_n_0 ));
  FDRE \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[3] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(plusOp[3]),
        .Q(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [3]),
        .R(\INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1__0_n_0 ));
  FDRE \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(plusOp[4]),
        .Q(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [4]),
        .R(\INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1__0_n_0 ));
  design_1_mailbox_1_0_address_decoder__parameterized0 I_DECODER
       (.\Addr_Counters[0].FDRE_I (\Addr_Counters[0].FDRE_I ),
        .\Addr_Counters[3].FDRE_I (\Addr_Counters[3].FDRE_I ),
        .Bus_RNW_reg_reg_0(Bus_RNW_reg_reg),
        .CI(CI),
        .D({I_DECODER_n_11,I_DECODER_n_12}),
        .E(E),
        .FDRE_I1(FDRE_I1),
        .FDRE_I1_0(FDRE_I1_0),
        .FSL1_S_Data_I(FSL1_S_Data_I),
        .FSL1_S_Exists_I(FSL1_S_Exists_I),
        .\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] (\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 ),
        .Q(start2),
        .S1_AXI_ACLK(S1_AXI_ACLK),
        .S1_AXI_ARREADY(S1_AXI_ARREADY),
        .S1_AXI_ARVALID(S1_AXI_ARVALID),
        .S1_AXI_BREADY(S1_AXI_BREADY),
        .S1_AXI_RREADY(S1_AXI_RREADY),
        .S1_AXI_WREADY(S1_AXI_WREADY),
        .S1_AXI_WVALID(\state[1]_i_3__0_n_0 ),
        .SYS_Rst_I(SYS_Rst_I),
        .\bus2ip_addr_i_reg[5] ({\bus2ip_addr_i_reg_n_0_[5] ,\bus2ip_addr_i_reg_n_0_[4] ,\bus2ip_addr_i_reg_n_0_[3] ,\bus2ip_addr_i_reg_n_0_[2] }),
        .bus2ip_rnw_i_reg(bus2ip_rnw_i_reg_n_0),
        .empty_error_reg(empty_error_reg),
        .empty_error_reg_0(empty_error_reg_0),
        .error_detect(error_detect),
        .\fifo_length_i_reg[2] (\fifo_length_i_reg[2] ),
        .\fifo_length_i_reg[2]_0 (\fifo_length_i_reg[2]_0 ),
        .\fifo_length_i_reg[3] (\fifo_length_i_reg[3] ),
        .full_error_reg(full_error_reg),
        .full_error_reg_0(full_error_reg_0),
        .ie_register(ie_register),
        .\ie_register_reg[2] (\ie_register_reg[2] ),
        .is_read(is_read),
        .\is_register_reg[2] (\is_register_reg[2] ),
        .is_write_reg(is_write_reg_n_0),
        .next_Data_Exists(next_Data_Exists),
        .p_6_in(p_6_in),
        .read_fsl_error_d1(read_fsl_error_d1),
        .read_fsl_error_d1_reg(read_fsl_error_d1_reg),
        .read_fsl_error_d1_reg_0(read_fsl_error_d1_reg_0),
        .\rit_register_reg[0] (\rit_register_reg[0] ),
        .\rit_register_reg[3] (\rit_register_reg[3] ),
        .s_axi_bvalid_i_reg(I_DECODER_n_23),
        .s_axi_bvalid_i_reg_0(S1_AXI_BVALID),
        .\s_axi_rdata_i_reg[3] ({I_DECODER_n_15,I_DECODER_n_16,I_DECODER_n_17,I_DECODER_n_18}),
        .\s_axi_rdata_i_reg[3]_0 (\s_axi_rdata_i_reg[3]_0 ),
        .s_axi_rvalid_i_reg(I_DECODER_n_22),
        .s_axi_rvalid_i_reg_0(\state[1]_i_2__0_n_0 ),
        .s_axi_rvalid_i_reg_1(S1_AXI_RVALID),
        .sit_detect_d0(sit_detect_d0),
        .\sit_register_reg[0] (Q),
        .\sit_register_reg[3] (\sit_register_reg[3] ),
        .\state_reg[0] (\state[0]_i_2__0_n_0 ),
        .\state_reg[1] (state),
        .write_fsl_error(write_fsl_error),
        .write_fsl_error_d1(write_fsl_error_d1));
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    \bus2ip_addr_i[2]_i_1__0 
       (.I0(S1_AXI_ARADDR[0]),
        .I1(state[1]),
        .I2(S1_AXI_ARVALID),
        .I3(state[0]),
        .I4(S1_AXI_AWADDR[0]),
        .O(\bus2ip_addr_i[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    \bus2ip_addr_i[3]_i_1__0 
       (.I0(S1_AXI_ARADDR[1]),
        .I1(state[1]),
        .I2(S1_AXI_ARVALID),
        .I3(state[0]),
        .I4(S1_AXI_AWADDR[1]),
        .O(\bus2ip_addr_i[3]_i_1__0_n_0 ));
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    \bus2ip_addr_i[4]_i_1__0 
       (.I0(S1_AXI_ARADDR[2]),
        .I1(state[1]),
        .I2(S1_AXI_ARVALID),
        .I3(state[0]),
        .I4(S1_AXI_AWADDR[2]),
        .O(\bus2ip_addr_i[4]_i_1__0_n_0 ));
  LUT5 #(
    .INIT(32'h000000EA)) 
    \bus2ip_addr_i[5]_i_1__0 
       (.I0(S1_AXI_ARVALID),
        .I1(S1_AXI_WVALID),
        .I2(S1_AXI_AWVALID),
        .I3(state[1]),
        .I4(state[0]),
        .O(\bus2ip_addr_i[5]_i_1__0_n_0 ));
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    \bus2ip_addr_i[5]_i_2__0 
       (.I0(S1_AXI_ARADDR[3]),
        .I1(state[1]),
        .I2(S1_AXI_ARVALID),
        .I3(state[0]),
        .I4(S1_AXI_AWADDR[3]),
        .O(\bus2ip_addr_i[5]_i_2__0_n_0 ));
  FDRE \bus2ip_addr_i_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(\bus2ip_addr_i[5]_i_1__0_n_0 ),
        .D(\bus2ip_addr_i[2]_i_1__0_n_0 ),
        .Q(\bus2ip_addr_i_reg_n_0_[2] ),
        .R(rst));
  FDRE \bus2ip_addr_i_reg[3] 
       (.C(S1_AXI_ACLK),
        .CE(\bus2ip_addr_i[5]_i_1__0_n_0 ),
        .D(\bus2ip_addr_i[3]_i_1__0_n_0 ),
        .Q(\bus2ip_addr_i_reg_n_0_[3] ),
        .R(rst));
  FDRE \bus2ip_addr_i_reg[4] 
       (.C(S1_AXI_ACLK),
        .CE(\bus2ip_addr_i[5]_i_1__0_n_0 ),
        .D(\bus2ip_addr_i[4]_i_1__0_n_0 ),
        .Q(\bus2ip_addr_i_reg_n_0_[4] ),
        .R(rst));
  FDRE \bus2ip_addr_i_reg[5] 
       (.C(S1_AXI_ACLK),
        .CE(\bus2ip_addr_i[5]_i_1__0_n_0 ),
        .D(\bus2ip_addr_i[5]_i_2__0_n_0 ),
        .Q(\bus2ip_addr_i_reg_n_0_[5] ),
        .R(rst));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'h04)) 
    bus2ip_rnw_i_i_1__0
       (.I0(state[1]),
        .I1(S1_AXI_ARVALID),
        .I2(state[0]),
        .O(bus2ip_rnw_i06_out));
  FDRE bus2ip_rnw_i_reg
       (.C(S1_AXI_ACLK),
        .CE(\bus2ip_addr_i[5]_i_1__0_n_0 ),
        .D(bus2ip_rnw_i06_out),
        .Q(bus2ip_rnw_i_reg_n_0),
        .R(rst));
  LUT4 #(
    .INIT(16'h2F20)) 
    is_read_i_1__0
       (.I0(S1_AXI_ARVALID),
        .I1(state[1]),
        .I2(is_read_i_2__0_n_0),
        .I3(is_read),
        .O(is_read_i_1__0_n_0));
  LUT6 #(
    .INIT(64'hF88800000000FFFF)) 
    is_read_i_2__0
       (.I0(S1_AXI_BREADY),
        .I1(S1_AXI_BVALID),
        .I2(S1_AXI_RREADY),
        .I3(S1_AXI_RVALID),
        .I4(state[1]),
        .I5(state[0]),
        .O(is_read_i_2__0_n_0));
  FDRE is_read_reg
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(is_read_i_1__0_n_0),
        .Q(is_read),
        .R(rst));
  LUT6 #(
    .INIT(64'h0040FFFF00400000)) 
    is_write_i_1__0
       (.I0(state[1]),
        .I1(S1_AXI_WVALID),
        .I2(S1_AXI_AWVALID),
        .I3(S1_AXI_ARVALID),
        .I4(is_read_i_2__0_n_0),
        .I5(is_write_reg_n_0),
        .O(is_write_i_1__0_n_0));
  FDRE is_write_reg
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(is_write_i_1__0_n_0),
        .Q(is_write_reg_n_0),
        .R(rst));
  FDRE rst_reg
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(SYS_Rst_I),
        .Q(rst),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    s_axi_bvalid_i_reg
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(I_DECODER_n_23),
        .Q(S1_AXI_BVALID),
        .R(rst));
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[31]_i_1__0 
       (.I0(state[0]),
        .I1(state[1]),
        .O(s_axi_rdata_i));
  LUT6 #(
    .INIT(64'hBB0BFF0F0000BB0B)) 
    \s_axi_rdata_i[3]_i_4__0 
       (.I0(\rit_register_reg[0] [0]),
        .I1(\fifo_length_i_reg[2]_1 [0]),
        .I2(\fifo_length_i_reg[2]_1 [2]),
        .I3(\rit_register_reg[0] [2]),
        .I4(\fifo_length_i_reg[2]_1 [1]),
        .I5(\rit_register_reg[0] [1]),
        .O(rit_detect_d1_reg));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(I_DECODER_n_18),
        .Q(S1_AXI_RDATA[0]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[10] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[6]),
        .Q(S1_AXI_RDATA[10]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[11] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[7]),
        .Q(S1_AXI_RDATA[11]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[12] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[8]),
        .Q(S1_AXI_RDATA[12]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[13] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[9]),
        .Q(S1_AXI_RDATA[13]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[14] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[10]),
        .Q(S1_AXI_RDATA[14]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[15] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[11]),
        .Q(S1_AXI_RDATA[15]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[16] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[12]),
        .Q(S1_AXI_RDATA[16]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[17] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[13]),
        .Q(S1_AXI_RDATA[17]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[18] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[14]),
        .Q(S1_AXI_RDATA[18]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[19] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[15]),
        .Q(S1_AXI_RDATA[19]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[1] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(I_DECODER_n_17),
        .Q(S1_AXI_RDATA[1]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[20] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[16]),
        .Q(S1_AXI_RDATA[20]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[21] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[17]),
        .Q(S1_AXI_RDATA[21]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[22] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[18]),
        .Q(S1_AXI_RDATA[22]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[23] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[19]),
        .Q(S1_AXI_RDATA[23]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[24] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[20]),
        .Q(S1_AXI_RDATA[24]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[25] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[21]),
        .Q(S1_AXI_RDATA[25]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[26] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[22]),
        .Q(S1_AXI_RDATA[26]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[27] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[23]),
        .Q(S1_AXI_RDATA[27]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[28] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[24]),
        .Q(S1_AXI_RDATA[28]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[29] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[25]),
        .Q(S1_AXI_RDATA[29]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(I_DECODER_n_16),
        .Q(S1_AXI_RDATA[2]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[30] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[26]),
        .Q(S1_AXI_RDATA[30]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[31] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[27]),
        .Q(S1_AXI_RDATA[31]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[3] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(I_DECODER_n_15),
        .Q(S1_AXI_RDATA[3]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[4] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[0]),
        .Q(S1_AXI_RDATA[4]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[5] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[1]),
        .Q(S1_AXI_RDATA[5]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[6] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[2]),
        .Q(S1_AXI_RDATA[6]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[7] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[3]),
        .Q(S1_AXI_RDATA[7]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[8] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[4]),
        .Q(S1_AXI_RDATA[8]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[9] 
       (.C(S1_AXI_ACLK),
        .CE(s_axi_rdata_i),
        .D(D[5]),
        .Q(S1_AXI_RDATA[9]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    s_axi_rvalid_i_reg
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(I_DECODER_n_22),
        .Q(S1_AXI_RVALID),
        .R(rst));
  LUT6 #(
    .INIT(64'hBBFB00F0FFFFBBFB)) 
    sit_detect_d1_i_2__0
       (.I0(Q[0]),
        .I1(\fifo_length_i_reg[2]_2 [0]),
        .I2(Q[2]),
        .I3(\fifo_length_i_reg[2]_2 [2]),
        .I4(Q[1]),
        .I5(\fifo_length_i_reg[2]_2 [1]),
        .O(sit_detect_d1_reg));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT5 #(
    .INIT(32'h00000F08)) 
    start2_i_1
       (.I0(S1_AXI_WVALID),
        .I1(S1_AXI_AWVALID),
        .I2(state[0]),
        .I3(S1_AXI_ARVALID),
        .I4(state[1]),
        .O(start2_i_1_n_0));
  FDRE start2_reg
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(start2_i_1_n_0),
        .Q(start2),
        .R(rst));
  LUT6 #(
    .INIT(64'h22222AAA2AAA2AAA)) 
    \state[0]_i_2__0 
       (.I0(state[0]),
        .I1(state[1]),
        .I2(S1_AXI_RVALID),
        .I3(S1_AXI_RREADY),
        .I4(S1_AXI_BVALID),
        .I5(S1_AXI_BREADY),
        .O(\state[0]_i_2__0_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \state[1]_i_2__0 
       (.I0(S1_AXI_RVALID),
        .I1(S1_AXI_RREADY),
        .I2(S1_AXI_BVALID),
        .I3(S1_AXI_BREADY),
        .O(\state[1]_i_2__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hBF)) 
    \state[1]_i_3__0 
       (.I0(S1_AXI_ARVALID),
        .I1(S1_AXI_AWVALID),
        .I2(S1_AXI_WVALID),
        .O(\state[1]_i_3__0_n_0 ));
  FDRE \state_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(I_DECODER_n_12),
        .Q(state[0]),
        .R(rst));
  FDRE \state_reg[1] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(I_DECODER_n_11),
        .Q(state[1]),
        .R(rst));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
