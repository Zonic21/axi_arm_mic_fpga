-- Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2016.4 (win64) Build 1756540 Mon Jan 23 19:11:23 MST 2017
-- Date        : Thu May 18 14:25:23 2017
-- Host        : DESKTOP-CLNFD4A running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top design_1_mailbox_0_0 -prefix
--               design_1_mailbox_0_0_ design_1_mailbox_0_0_sim_netlist.vhdl
-- Design      : design_1_mailbox_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_mailbox_0_0_Async_FIFO is
  port (
    \out\ : out STD_LOGIC;
    \write_addr_reg[0]_0\ : out STD_LOGIC;
    sit_detect_d0 : out STD_LOGIC;
    rit_detect_d0 : out STD_LOGIC;
    DataOut : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S0_AXI_ACLK : in STD_LOGIC;
    S1_AXI_ARESETN : in STD_LOGIC;
    S1_AXI_ACLK : in STD_LOGIC;
    empty_allow : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Bus_RNW_reg : in STD_LOGIC;
    \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg\ : in STD_LOGIC;
    \rit_register_reg[0]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg\ : in STD_LOGIC;
    Bus_RNW_reg_0 : in STD_LOGIC;
    S1_AXI_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end design_1_mailbox_0_0_Async_FIFO;

architecture STRUCTURE of design_1_mailbox_0_0_Async_FIFO is
  signal Bin2Gray : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal CI : STD_LOGIC;
  signal Gray2Bin : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \Rd_Length_i[0]_i_1_n_0\ : STD_LOGIC;
  signal \Rd_Length_i[1]_i_1_n_0\ : STD_LOGIC;
  signal \Rd_Length_i[2]_i_1_n_0\ : STD_LOGIC;
  signal \Rd_Length_i[3]_i_1_n_0\ : STD_LOGIC;
  signal \Rd_Length_i[3]_i_2_n_0\ : STD_LOGIC;
  signal \Rd_Length_i_reg_n_0_[0]\ : STD_LOGIC;
  signal \Rd_Length_i_reg_n_0_[1]\ : STD_LOGIC;
  signal \Rd_Length_i_reg_n_0_[2]\ : STD_LOGIC;
  signal \Rd_Length_i_reg_n_0_[3]\ : STD_LOGIC;
  signal S : STD_LOGIC;
  signal S0 : STD_LOGIC;
  signal S00_out : STD_LOGIC;
  signal S01_out : STD_LOGIC;
  signal S02_out : STD_LOGIC;
  signal S04_out : STD_LOGIC;
  signal S05_out : STD_LOGIC;
  signal S06_out : STD_LOGIC;
  signal \Wr_Length_i[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \Wr_Length_i[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \Wr_Length_i[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \Wr_Length_i[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \Wr_Length_i[3]_i_2__0_n_0\ : STD_LOGIC;
  signal \Wr_Length_i_reg_n_0_[0]\ : STD_LOGIC;
  signal \Wr_Length_i_reg_n_0_[1]\ : STD_LOGIC;
  signal \Wr_Length_i_reg_n_0_[2]\ : STD_LOGIC;
  signal \Wr_Length_i_reg_n_0_[3]\ : STD_LOGIC;
  signal empty : STD_LOGIC;
  attribute async_reg : string;
  attribute async_reg of empty : signal is "true";
  signal emptyg : STD_LOGIC;
  signal emuxcyo_0 : STD_LOGIC;
  signal emuxcyo_1 : STD_LOGIC;
  signal fmuxcyo_0 : STD_LOGIC;
  signal fmuxcyo_1 : STD_LOGIC;
  signal fmuxcyo_2 : STD_LOGIC;
  signal \full_i_i_1__0_n_0\ : STD_LOGIC;
  signal fullg : STD_LOGIC;
  signal p_1_in16_in : STD_LOGIC;
  signal p_1_in20_in : STD_LOGIC;
  signal p_2_in17_in : STD_LOGIC;
  signal p_2_in22_in : STD_LOGIC;
  signal p_2_in52_in : STD_LOGIC;
  signal p_2_in57_in : STD_LOGIC;
  signal p_2_in62_in : STD_LOGIC;
  signal plusOp : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \ram_mem_reg_0_15_0_5_i_1__0_n_0\ : STD_LOGIC;
  signal \ram_mem_reg_0_15_0_5_i_2__0_n_0\ : STD_LOGIC;
  signal \ram_mem_reg_0_15_0_5_i_3__0_n_0\ : STD_LOGIC;
  signal \ram_mem_reg_0_15_0_5_i_4__0_n_0\ : STD_LOGIC;
  signal ram_mem_reg_0_15_0_5_n_0 : STD_LOGIC;
  signal ram_mem_reg_0_15_0_5_n_1 : STD_LOGIC;
  signal ram_mem_reg_0_15_0_5_n_2 : STD_LOGIC;
  signal ram_mem_reg_0_15_0_5_n_3 : STD_LOGIC;
  signal ram_mem_reg_0_15_0_5_n_4 : STD_LOGIC;
  signal ram_mem_reg_0_15_0_5_n_5 : STD_LOGIC;
  signal ram_mem_reg_0_15_12_17_n_0 : STD_LOGIC;
  signal ram_mem_reg_0_15_12_17_n_1 : STD_LOGIC;
  signal ram_mem_reg_0_15_12_17_n_2 : STD_LOGIC;
  signal ram_mem_reg_0_15_12_17_n_3 : STD_LOGIC;
  signal ram_mem_reg_0_15_12_17_n_4 : STD_LOGIC;
  signal ram_mem_reg_0_15_12_17_n_5 : STD_LOGIC;
  signal ram_mem_reg_0_15_18_23_n_0 : STD_LOGIC;
  signal ram_mem_reg_0_15_18_23_n_1 : STD_LOGIC;
  signal ram_mem_reg_0_15_18_23_n_2 : STD_LOGIC;
  signal ram_mem_reg_0_15_18_23_n_3 : STD_LOGIC;
  signal ram_mem_reg_0_15_18_23_n_4 : STD_LOGIC;
  signal ram_mem_reg_0_15_18_23_n_5 : STD_LOGIC;
  signal ram_mem_reg_0_15_24_29_n_0 : STD_LOGIC;
  signal ram_mem_reg_0_15_24_29_n_1 : STD_LOGIC;
  signal ram_mem_reg_0_15_24_29_n_2 : STD_LOGIC;
  signal ram_mem_reg_0_15_24_29_n_3 : STD_LOGIC;
  signal ram_mem_reg_0_15_24_29_n_4 : STD_LOGIC;
  signal ram_mem_reg_0_15_24_29_n_5 : STD_LOGIC;
  signal ram_mem_reg_0_15_30_31_n_0 : STD_LOGIC;
  signal ram_mem_reg_0_15_30_31_n_1 : STD_LOGIC;
  signal ram_mem_reg_0_15_6_11_n_0 : STD_LOGIC;
  signal ram_mem_reg_0_15_6_11_n_1 : STD_LOGIC;
  signal ram_mem_reg_0_15_6_11_n_2 : STD_LOGIC;
  signal ram_mem_reg_0_15_6_11_n_3 : STD_LOGIC;
  signal ram_mem_reg_0_15_6_11_n_4 : STD_LOGIC;
  signal ram_mem_reg_0_15_6_11_n_5 : STD_LOGIC;
  signal rd_meta_reset : STD_LOGIC;
  signal rd_reset : STD_LOGIC;
  signal \rd_write_addrgray_dx[0]_4\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg of \rd_write_addrgray_dx[0]_4\ : signal is "true";
  signal \rd_write_addrgray_dx[1]_5\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg of \rd_write_addrgray_dx[1]_5\ : signal is "true";
  signal \rd_write_addrgray_dx[2]_8\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg of \rd_write_addrgray_dx[2]_8\ : signal is "true";
  signal \rd_write_next_reg_n_0_[0]\ : STD_LOGIC;
  signal \rd_write_next_reg_n_0_[1]\ : STD_LOGIC;
  signal \rd_write_next_reg_n_0_[2]\ : STD_LOGIC;
  signal \rd_write_next_reg_n_0_[3]\ : STD_LOGIC;
  signal \rd_write_nextgray_dx[0]_2\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg of \rd_write_nextgray_dx[0]_2\ : signal is "true";
  signal \rd_write_nextgray_dx[1]_3\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg of \rd_write_nextgray_dx[1]_3\ : signal is "true";
  signal \rd_write_nextgray_dx[2]_9\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg of \rd_write_nextgray_dx[2]_9\ : signal is "true";
  signal \read_addr_d1_reg_n_0_[0]\ : STD_LOGIC;
  signal \read_addr_d1_reg_n_0_[1]\ : STD_LOGIC;
  signal \read_addr_d1_reg_n_0_[2]\ : STD_LOGIC;
  signal \read_addr_reg_n_0_[0]\ : STD_LOGIC;
  signal \read_addr_reg_n_0_[3]\ : STD_LOGIC;
  signal \read_addrgray_reg_n_0_[0]\ : STD_LOGIC;
  signal read_lastgray : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal read_nextgray : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \rit_detect_d1_i_2__0_n_0\ : STD_LOGIC;
  signal sit_detect_d1_i_2_n_0 : STD_LOGIC;
  signal wr_meta_reset : STD_LOGIC;
  signal \wr_read_lastgray_dx[0]_6\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg of \wr_read_lastgray_dx[0]_6\ : signal is "true";
  signal \wr_read_lastgray_dx[1]_7\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg of \wr_read_lastgray_dx[1]_7\ : signal is "true";
  signal \wr_read_lastgray_dx[2]_10\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg of \wr_read_lastgray_dx[2]_10\ : signal is "true";
  signal \wr_read_next[0]_i_1_n_0\ : STD_LOGIC;
  signal \wr_read_next[1]_i_1_n_0\ : STD_LOGIC;
  signal \wr_read_next[2]_i_1_n_0\ : STD_LOGIC;
  signal \wr_read_next_reg_n_0_[0]\ : STD_LOGIC;
  signal \wr_read_next_reg_n_0_[1]\ : STD_LOGIC;
  signal \wr_read_next_reg_n_0_[2]\ : STD_LOGIC;
  signal \wr_read_next_reg_n_0_[3]\ : STD_LOGIC;
  signal \wr_read_nextgray_dx[0]_0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg of \wr_read_nextgray_dx[0]_0\ : signal is "true";
  signal \wr_read_nextgray_dx[1]_1\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg of \wr_read_nextgray_dx[1]_1\ : signal is "true";
  signal \wr_read_nextgray_dx[2]_11\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg of \wr_read_nextgray_dx[2]_11\ : signal is "true";
  signal wr_reset : STD_LOGIC;
  signal \write_addr_d1_reg_n_0_[0]\ : STD_LOGIC;
  signal \write_addr_d1_reg_n_0_[1]\ : STD_LOGIC;
  signal \write_addr_d1_reg_n_0_[2]\ : STD_LOGIC;
  signal \^write_addr_reg[0]_0\ : STD_LOGIC;
  attribute async_reg of \write_addr_reg[0]_0\ : signal is "true";
  signal \write_addr_reg_n_0_[0]\ : STD_LOGIC;
  signal \write_addr_reg_n_0_[3]\ : STD_LOGIC;
  signal write_addrgray : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal write_nextgray : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \write_nextgray[0]_i_1_n_0\ : STD_LOGIC;
  signal \write_nextgray[1]_i_1_n_0\ : STD_LOGIC;
  signal \write_nextgray[2]_i_1_n_0\ : STD_LOGIC;
  signal \write_nextgray[2]_i_2_n_0\ : STD_LOGIC;
  signal NLW_emuxcylow_CARRY4_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_fmuxcylow_CARRY4_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_mem_reg_0_15_0_5_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_mem_reg_0_15_12_17_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_mem_reg_0_15_18_23_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_mem_reg_0_15_24_29_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_mem_reg_0_15_30_31_DOB_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_mem_reg_0_15_30_31_DOC_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_mem_reg_0_15_30_31_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_mem_reg_0_15_6_11_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \Rd_Length_i[1]_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \Rd_Length_i[3]_i_2\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \Wr_Length_i[1]_i_1__0\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \Wr_Length_i[3]_i_2__0\ : label is "soft_lutpair35";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of empty_reg : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of empty_reg : label is "yes";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of emuxcylow_CARRY4 : label is "(MUXCY,XORCY)";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of emuxcylow_CARRY4 : label is "LO:O";
  attribute box_type : string;
  attribute box_type of emuxcylow_CARRY4 : label is "PRIMITIVE";
  attribute XILINX_LEGACY_PRIM of fmuxcylow_CARRY4 : label is "(MUXCY,XORCY)";
  attribute XILINX_TRANSFORM_PINMAP of fmuxcylow_CARRY4 : label is "LO:O";
  attribute box_type of fmuxcylow_CARRY4 : label is "PRIMITIVE";
  attribute KEEP of full_i_reg : label is "yes";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_mem_reg_0_15_0_5 : label is "";
  attribute METHODOLOGY_DRC_VIOS of ram_mem_reg_0_15_12_17 : label is "";
  attribute METHODOLOGY_DRC_VIOS of ram_mem_reg_0_15_18_23 : label is "";
  attribute METHODOLOGY_DRC_VIOS of ram_mem_reg_0_15_24_29 : label is "";
  attribute METHODOLOGY_DRC_VIOS of ram_mem_reg_0_15_30_31 : label is "";
  attribute METHODOLOGY_DRC_VIOS of ram_mem_reg_0_15_6_11 : label is "";
  attribute XILINX_LEGACY_PRIM of rd_rst_meta_inst : label is "FDP";
  attribute box_type of rd_rst_meta_inst : label is "PRIMITIVE";
  attribute XILINX_LEGACY_PRIM of rd_rst_sync_inst : label is "FDP";
  attribute box_type of rd_rst_sync_inst : label is "PRIMITIVE";
  attribute ASYNC_REG_boolean of \rd_write_addrgray_dx_reg[0][0]\ : label is std.standard.true;
  attribute KEEP of \rd_write_addrgray_dx_reg[0][0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_addrgray_dx_reg[0][1]\ : label is std.standard.true;
  attribute KEEP of \rd_write_addrgray_dx_reg[0][1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_addrgray_dx_reg[0][2]\ : label is std.standard.true;
  attribute KEEP of \rd_write_addrgray_dx_reg[0][2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_addrgray_dx_reg[0][3]\ : label is std.standard.true;
  attribute KEEP of \rd_write_addrgray_dx_reg[0][3]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_addrgray_dx_reg[1][0]\ : label is std.standard.true;
  attribute KEEP of \rd_write_addrgray_dx_reg[1][0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_addrgray_dx_reg[1][1]\ : label is std.standard.true;
  attribute KEEP of \rd_write_addrgray_dx_reg[1][1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_addrgray_dx_reg[1][2]\ : label is std.standard.true;
  attribute KEEP of \rd_write_addrgray_dx_reg[1][2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_addrgray_dx_reg[1][3]\ : label is std.standard.true;
  attribute KEEP of \rd_write_addrgray_dx_reg[1][3]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_nextgray_dx_reg[0][0]\ : label is std.standard.true;
  attribute KEEP of \rd_write_nextgray_dx_reg[0][0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_nextgray_dx_reg[0][1]\ : label is std.standard.true;
  attribute KEEP of \rd_write_nextgray_dx_reg[0][1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_nextgray_dx_reg[0][2]\ : label is std.standard.true;
  attribute KEEP of \rd_write_nextgray_dx_reg[0][2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_nextgray_dx_reg[0][3]\ : label is std.standard.true;
  attribute KEEP of \rd_write_nextgray_dx_reg[0][3]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_nextgray_dx_reg[1][0]\ : label is std.standard.true;
  attribute KEEP of \rd_write_nextgray_dx_reg[1][0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_nextgray_dx_reg[1][1]\ : label is std.standard.true;
  attribute KEEP of \rd_write_nextgray_dx_reg[1][1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_nextgray_dx_reg[1][2]\ : label is std.standard.true;
  attribute KEEP of \rd_write_nextgray_dx_reg[1][2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_nextgray_dx_reg[1][3]\ : label is std.standard.true;
  attribute KEEP of \rd_write_nextgray_dx_reg[1][3]\ : label is "yes";
  attribute SOFT_HLUTNM of \read_nextgray[0]_i_1__0\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \read_nextgray[1]_i_1__0\ : label is "soft_lutpair38";
  attribute ASYNC_REG_boolean of \wr_read_lastgray_dx_reg[0][0]\ : label is std.standard.true;
  attribute KEEP of \wr_read_lastgray_dx_reg[0][0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_lastgray_dx_reg[0][1]\ : label is std.standard.true;
  attribute KEEP of \wr_read_lastgray_dx_reg[0][1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_lastgray_dx_reg[0][2]\ : label is std.standard.true;
  attribute KEEP of \wr_read_lastgray_dx_reg[0][2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_lastgray_dx_reg[0][3]\ : label is std.standard.true;
  attribute KEEP of \wr_read_lastgray_dx_reg[0][3]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_lastgray_dx_reg[1][0]\ : label is std.standard.true;
  attribute KEEP of \wr_read_lastgray_dx_reg[1][0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_lastgray_dx_reg[1][1]\ : label is std.standard.true;
  attribute KEEP of \wr_read_lastgray_dx_reg[1][1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_lastgray_dx_reg[1][2]\ : label is std.standard.true;
  attribute KEEP of \wr_read_lastgray_dx_reg[1][2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_lastgray_dx_reg[1][3]\ : label is std.standard.true;
  attribute KEEP of \wr_read_lastgray_dx_reg[1][3]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_nextgray_dx_reg[0][0]\ : label is std.standard.true;
  attribute KEEP of \wr_read_nextgray_dx_reg[0][0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_nextgray_dx_reg[0][1]\ : label is std.standard.true;
  attribute KEEP of \wr_read_nextgray_dx_reg[0][1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_nextgray_dx_reg[0][2]\ : label is std.standard.true;
  attribute KEEP of \wr_read_nextgray_dx_reg[0][2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_nextgray_dx_reg[0][3]\ : label is std.standard.true;
  attribute KEEP of \wr_read_nextgray_dx_reg[0][3]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_nextgray_dx_reg[1][0]\ : label is std.standard.true;
  attribute KEEP of \wr_read_nextgray_dx_reg[1][0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_nextgray_dx_reg[1][1]\ : label is std.standard.true;
  attribute KEEP of \wr_read_nextgray_dx_reg[1][1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_nextgray_dx_reg[1][2]\ : label is std.standard.true;
  attribute KEEP of \wr_read_nextgray_dx_reg[1][2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_nextgray_dx_reg[1][3]\ : label is std.standard.true;
  attribute KEEP of \wr_read_nextgray_dx_reg[1][3]\ : label is "yes";
  attribute XILINX_LEGACY_PRIM of wr_rst_meta_inst : label is "FDP";
  attribute box_type of wr_rst_meta_inst : label is "PRIMITIVE";
  attribute XILINX_LEGACY_PRIM of wr_rst_sync_inst : label is "FDP";
  attribute box_type of wr_rst_sync_inst : label is "PRIMITIVE";
  attribute SOFT_HLUTNM of \write_addr[1]_i_1\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \write_addr[2]_i_1\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \write_addr[3]_i_1\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \write_nextgray[0]_i_1\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \write_nextgray[1]_i_1\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \write_nextgray[2]_i_2\ : label is "soft_lutpair37";
begin
  \out\ <= empty;
  \write_addr_reg[0]_0\ <= \^write_addr_reg[0]_0\;
\DataOut_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_0_5_n_1,
      Q => DataOut(0),
      R => '0'
    );
\DataOut_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_6_11_n_5,
      Q => DataOut(10),
      R => '0'
    );
\DataOut_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_6_11_n_4,
      Q => DataOut(11),
      R => '0'
    );
\DataOut_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_12_17_n_1,
      Q => DataOut(12),
      R => '0'
    );
\DataOut_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_12_17_n_0,
      Q => DataOut(13),
      R => '0'
    );
\DataOut_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_12_17_n_3,
      Q => DataOut(14),
      R => '0'
    );
\DataOut_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_12_17_n_2,
      Q => DataOut(15),
      R => '0'
    );
\DataOut_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_12_17_n_5,
      Q => DataOut(16),
      R => '0'
    );
\DataOut_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_12_17_n_4,
      Q => DataOut(17),
      R => '0'
    );
\DataOut_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_18_23_n_1,
      Q => DataOut(18),
      R => '0'
    );
\DataOut_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_18_23_n_0,
      Q => DataOut(19),
      R => '0'
    );
\DataOut_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_0_5_n_0,
      Q => DataOut(1),
      R => '0'
    );
\DataOut_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_18_23_n_3,
      Q => DataOut(20),
      R => '0'
    );
\DataOut_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_18_23_n_2,
      Q => DataOut(21),
      R => '0'
    );
\DataOut_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_18_23_n_5,
      Q => DataOut(22),
      R => '0'
    );
\DataOut_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_18_23_n_4,
      Q => DataOut(23),
      R => '0'
    );
\DataOut_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_24_29_n_1,
      Q => DataOut(24),
      R => '0'
    );
\DataOut_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_24_29_n_0,
      Q => DataOut(25),
      R => '0'
    );
\DataOut_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_24_29_n_3,
      Q => DataOut(26),
      R => '0'
    );
\DataOut_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_24_29_n_2,
      Q => DataOut(27),
      R => '0'
    );
\DataOut_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_24_29_n_5,
      Q => DataOut(28),
      R => '0'
    );
\DataOut_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_24_29_n_4,
      Q => DataOut(29),
      R => '0'
    );
\DataOut_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_0_5_n_3,
      Q => DataOut(2),
      R => '0'
    );
\DataOut_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_30_31_n_1,
      Q => DataOut(30),
      R => '0'
    );
\DataOut_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_30_31_n_0,
      Q => DataOut(31),
      R => '0'
    );
\DataOut_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_0_5_n_2,
      Q => DataOut(3),
      R => '0'
    );
\DataOut_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_0_5_n_5,
      Q => DataOut(4),
      R => '0'
    );
\DataOut_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_0_5_n_4,
      Q => DataOut(5),
      R => '0'
    );
\DataOut_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_6_11_n_1,
      Q => DataOut(6),
      R => '0'
    );
\DataOut_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_6_11_n_0,
      Q => DataOut(7),
      R => '0'
    );
\DataOut_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_6_11_n_3,
      Q => DataOut(8),
      R => '0'
    );
\DataOut_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => ram_mem_reg_0_15_6_11_n_2,
      Q => DataOut(9),
      R => '0'
    );
\Gen_emuxcy[1].emuxcy_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9A95"
    )
        port map (
      I0 => \rd_write_addrgray_dx[1]_5\(1),
      I1 => p_2_in52_in,
      I2 => empty,
      I3 => read_nextgray(1),
      O => S04_out
    );
\Gen_emuxcy[2].emuxcy_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9A95"
    )
        port map (
      I0 => \rd_write_addrgray_dx[1]_5\(2),
      I1 => p_2_in57_in,
      I2 => empty,
      I3 => read_nextgray(2),
      O => S05_out
    );
\Gen_fmuxcy[1].fmuxcy_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9A95"
    )
        port map (
      I0 => \wr_read_lastgray_dx[1]_7\(1),
      I1 => write_addrgray(1),
      I2 => \^write_addr_reg[0]_0\,
      I3 => write_nextgray(1),
      O => S00_out
    );
\Gen_fmuxcy[2].fmuxcy_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9A95"
    )
        port map (
      I0 => \wr_read_lastgray_dx[1]_7\(2),
      I1 => write_addrgray(2),
      I2 => \^write_addr_reg[0]_0\,
      I3 => write_nextgray(2),
      O => S01_out
    );
\Rd_Length_i[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \rd_write_next_reg_n_0_[0]\,
      I1 => \read_addr_d1_reg_n_0_[0]\,
      O => \Rd_Length_i[0]_i_1_n_0\
    );
\Rd_Length_i[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => \read_addr_d1_reg_n_0_[0]\,
      I1 => \rd_write_next_reg_n_0_[0]\,
      I2 => \read_addr_d1_reg_n_0_[1]\,
      I3 => \rd_write_next_reg_n_0_[1]\,
      O => \Rd_Length_i[1]_i_1_n_0\
    );
\Rd_Length_i[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F04B0FBB0FB4F04"
    )
        port map (
      I0 => \rd_write_next_reg_n_0_[0]\,
      I1 => \read_addr_d1_reg_n_0_[0]\,
      I2 => \rd_write_next_reg_n_0_[1]\,
      I3 => \read_addr_d1_reg_n_0_[1]\,
      I4 => \read_addr_d1_reg_n_0_[2]\,
      I5 => \rd_write_next_reg_n_0_[2]\,
      O => \Rd_Length_i[2]_i_1_n_0\
    );
\Rd_Length_i[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"718E8E71"
    )
        port map (
      I0 => \Rd_Length_i[3]_i_2_n_0\,
      I1 => \rd_write_next_reg_n_0_[2]\,
      I2 => \read_addr_d1_reg_n_0_[2]\,
      I3 => read_nextgray(3),
      I4 => \rd_write_next_reg_n_0_[3]\,
      O => \Rd_Length_i[3]_i_1_n_0\
    );
\Rd_Length_i[3]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DD4D"
    )
        port map (
      I0 => \read_addr_d1_reg_n_0_[1]\,
      I1 => \rd_write_next_reg_n_0_[1]\,
      I2 => \read_addr_d1_reg_n_0_[0]\,
      I3 => \rd_write_next_reg_n_0_[0]\,
      O => \Rd_Length_i[3]_i_2_n_0\
    );
\Rd_Length_i_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      CLR => rd_reset,
      D => \Rd_Length_i[0]_i_1_n_0\,
      Q => \Rd_Length_i_reg_n_0_[0]\
    );
\Rd_Length_i_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      CLR => rd_reset,
      D => \Rd_Length_i[1]_i_1_n_0\,
      Q => \Rd_Length_i_reg_n_0_[1]\
    );
\Rd_Length_i_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      CLR => rd_reset,
      D => \Rd_Length_i[2]_i_1_n_0\,
      Q => \Rd_Length_i_reg_n_0_[2]\
    );
\Rd_Length_i_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      CLR => rd_reset,
      D => \Rd_Length_i[3]_i_1_n_0\,
      Q => \Rd_Length_i_reg_n_0_[3]\
    );
\Wr_Length_i[0]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \write_addr_d1_reg_n_0_[0]\,
      I1 => \wr_read_next_reg_n_0_[0]\,
      O => \Wr_Length_i[0]_i_1__0_n_0\
    );
\Wr_Length_i[1]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => \wr_read_next_reg_n_0_[0]\,
      I1 => \write_addr_d1_reg_n_0_[0]\,
      I2 => \wr_read_next_reg_n_0_[1]\,
      I3 => \write_addr_d1_reg_n_0_[1]\,
      O => \Wr_Length_i[1]_i_1__0_n_0\
    );
\Wr_Length_i[2]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F04B0FBB0FB4F04"
    )
        port map (
      I0 => \write_addr_d1_reg_n_0_[0]\,
      I1 => \wr_read_next_reg_n_0_[0]\,
      I2 => \write_addr_d1_reg_n_0_[1]\,
      I3 => \wr_read_next_reg_n_0_[1]\,
      I4 => \wr_read_next_reg_n_0_[2]\,
      I5 => \write_addr_d1_reg_n_0_[2]\,
      O => \Wr_Length_i[2]_i_1__0_n_0\
    );
\Wr_Length_i[3]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"718E8E71"
    )
        port map (
      I0 => \Wr_Length_i[3]_i_2__0_n_0\,
      I1 => \write_addr_d1_reg_n_0_[2]\,
      I2 => \wr_read_next_reg_n_0_[2]\,
      I3 => \wr_read_next_reg_n_0_[3]\,
      I4 => write_nextgray(3),
      O => \Wr_Length_i[3]_i_1__0_n_0\
    );
\Wr_Length_i[3]_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DD4D"
    )
        port map (
      I0 => \wr_read_next_reg_n_0_[1]\,
      I1 => \write_addr_d1_reg_n_0_[1]\,
      I2 => \wr_read_next_reg_n_0_[0]\,
      I3 => \write_addr_d1_reg_n_0_[0]\,
      O => \Wr_Length_i[3]_i_2__0_n_0\
    );
\Wr_Length_i_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      CLR => wr_reset,
      D => \Wr_Length_i[0]_i_1__0_n_0\,
      Q => \Wr_Length_i_reg_n_0_[0]\
    );
\Wr_Length_i_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      CLR => wr_reset,
      D => \Wr_Length_i[1]_i_1__0_n_0\,
      Q => \Wr_Length_i_reg_n_0_[1]\
    );
\Wr_Length_i_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      CLR => wr_reset,
      D => \Wr_Length_i[2]_i_1__0_n_0\,
      Q => \Wr_Length_i_reg_n_0_[2]\
    );
\Wr_Length_i_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      CLR => wr_reset,
      D => \Wr_Length_i[3]_i_1__0_n_0\,
      Q => \Wr_Length_i_reg_n_0_[3]\
    );
empty_reg: unisim.vcomponents.FDPE
     port map (
      C => S0_AXI_ACLK,
      CE => empty_allow,
      D => emptyg,
      PRE => rd_reset,
      Q => empty
    );
\emuxcyhigh_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9A95"
    )
        port map (
      I0 => \rd_write_addrgray_dx[1]_5\(3),
      I1 => p_2_in62_in,
      I2 => empty,
      I3 => read_nextgray(3),
      O => S06_out
    );
emuxcylow_CARRY4: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => emptyg,
      CO(2) => CI,
      CO(1) => emuxcyo_1,
      CO(0) => emuxcyo_0,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_emuxcylow_CARRY4_O_UNCONNECTED(3 downto 0),
      S(3) => S06_out,
      S(2) => S05_out,
      S(1) => S04_out,
      S(0) => S
    );
\emuxcylow_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9A95"
    )
        port map (
      I0 => \rd_write_addrgray_dx[1]_5\(0),
      I1 => \read_addrgray_reg_n_0_[0]\,
      I2 => empty,
      I3 => read_nextgray(0),
      O => S
    );
fmuxcyhigh_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9A95"
    )
        port map (
      I0 => \wr_read_lastgray_dx[1]_7\(3),
      I1 => write_addrgray(3),
      I2 => \^write_addr_reg[0]_0\,
      I3 => write_nextgray(3),
      O => S02_out
    );
fmuxcylow_CARRY4: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => fullg,
      CO(2) => fmuxcyo_2,
      CO(1) => fmuxcyo_1,
      CO(0) => fmuxcyo_0,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_fmuxcylow_CARRY4_O_UNCONNECTED(3 downto 0),
      S(3) => S02_out,
      S(2) => S01_out,
      S(1) => S00_out,
      S(0) => S0
    );
fmuxcylow_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9A95"
    )
        port map (
      I0 => \wr_read_lastgray_dx[1]_7\(0),
      I1 => write_addrgray(0),
      I2 => \^write_addr_reg[0]_0\,
      I3 => write_nextgray(0),
      O => S0
    );
\full_i_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88888A88"
    )
        port map (
      I0 => fullg,
      I1 => \^write_addr_reg[0]_0\,
      I2 => \^write_addr_reg[0]_0\,
      I3 => \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg\,
      I4 => Bus_RNW_reg_0,
      O => \full_i_i_1__0_n_0\
    );
full_i_reg: unisim.vcomponents.FDPE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \full_i_i_1__0_n_0\,
      PRE => wr_reset,
      Q => \^write_addr_reg[0]_0\
    );
i_0: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \rd_write_addrgray_dx[2]_8\(3)
    );
i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \rd_write_addrgray_dx[2]_8\(2)
    );
i_10: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \wr_read_lastgray_dx[2]_10\(1)
    );
i_11: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \wr_read_lastgray_dx[2]_10\(0)
    );
i_12: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \wr_read_nextgray_dx[2]_11\(3)
    );
i_13: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \wr_read_nextgray_dx[2]_11\(2)
    );
i_14: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \wr_read_nextgray_dx[2]_11\(1)
    );
i_15: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \wr_read_nextgray_dx[2]_11\(0)
    );
i_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \rd_write_addrgray_dx[2]_8\(1)
    );
i_3: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \rd_write_addrgray_dx[2]_8\(0)
    );
i_4: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \rd_write_nextgray_dx[2]_9\(3)
    );
i_5: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \rd_write_nextgray_dx[2]_9\(2)
    );
i_6: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \rd_write_nextgray_dx[2]_9\(1)
    );
i_7: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \rd_write_nextgray_dx[2]_9\(0)
    );
i_8: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \wr_read_lastgray_dx[2]_10\(3)
    );
i_9: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \wr_read_lastgray_dx[2]_10\(2)
    );
ram_mem_reg_0_15_0_5: unisim.vcomponents.RAM32M
     port map (
      ADDRA(4) => '0',
      ADDRA(3) => \ram_mem_reg_0_15_0_5_i_1__0_n_0\,
      ADDRA(2) => \ram_mem_reg_0_15_0_5_i_2__0_n_0\,
      ADDRA(1) => \ram_mem_reg_0_15_0_5_i_3__0_n_0\,
      ADDRA(0) => \ram_mem_reg_0_15_0_5_i_4__0_n_0\,
      ADDRB(4) => '0',
      ADDRB(3) => \ram_mem_reg_0_15_0_5_i_1__0_n_0\,
      ADDRB(2) => \ram_mem_reg_0_15_0_5_i_2__0_n_0\,
      ADDRB(1) => \ram_mem_reg_0_15_0_5_i_3__0_n_0\,
      ADDRB(0) => \ram_mem_reg_0_15_0_5_i_4__0_n_0\,
      ADDRC(4) => '0',
      ADDRC(3) => \ram_mem_reg_0_15_0_5_i_1__0_n_0\,
      ADDRC(2) => \ram_mem_reg_0_15_0_5_i_2__0_n_0\,
      ADDRC(1) => \ram_mem_reg_0_15_0_5_i_3__0_n_0\,
      ADDRC(0) => \ram_mem_reg_0_15_0_5_i_4__0_n_0\,
      ADDRD(4) => '0',
      ADDRD(3) => \write_addr_reg_n_0_[3]\,
      ADDRD(2) => p_2_in17_in,
      ADDRD(1) => p_1_in16_in,
      ADDRD(0) => \write_addr_reg_n_0_[0]\,
      DIA(1 downto 0) => S1_AXI_WDATA(1 downto 0),
      DIB(1 downto 0) => S1_AXI_WDATA(3 downto 2),
      DIC(1 downto 0) => S1_AXI_WDATA(5 downto 4),
      DID(1 downto 0) => B"00",
      DOA(1) => ram_mem_reg_0_15_0_5_n_0,
      DOA(0) => ram_mem_reg_0_15_0_5_n_1,
      DOB(1) => ram_mem_reg_0_15_0_5_n_2,
      DOB(0) => ram_mem_reg_0_15_0_5_n_3,
      DOC(1) => ram_mem_reg_0_15_0_5_n_4,
      DOC(0) => ram_mem_reg_0_15_0_5_n_5,
      DOD(1 downto 0) => NLW_ram_mem_reg_0_15_0_5_DOD_UNCONNECTED(1 downto 0),
      WCLK => S1_AXI_ACLK,
      WE => \write_nextgray[2]_i_1_n_0\
    );
\ram_mem_reg_0_15_0_5_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => p_1_in20_in,
      I1 => \read_addr_reg_n_0_[0]\,
      I2 => E(0),
      I3 => p_2_in22_in,
      I4 => \read_addr_reg_n_0_[3]\,
      O => \ram_mem_reg_0_15_0_5_i_1__0_n_0\
    );
\ram_mem_reg_0_15_0_5_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF40000000"
    )
        port map (
      I0 => empty,
      I1 => Bus_RNW_reg,
      I2 => \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg\,
      I3 => \read_addr_reg_n_0_[0]\,
      I4 => p_1_in20_in,
      I5 => p_2_in22_in,
      O => \ram_mem_reg_0_15_0_5_i_2__0_n_0\
    );
\ram_mem_reg_0_15_0_5_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF7F0080"
    )
        port map (
      I0 => \read_addr_reg_n_0_[0]\,
      I1 => \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg\,
      I2 => Bus_RNW_reg,
      I3 => empty,
      I4 => p_1_in20_in,
      O => \ram_mem_reg_0_15_0_5_i_3__0_n_0\
    );
\ram_mem_reg_0_15_0_5_i_4__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9AAA"
    )
        port map (
      I0 => \read_addr_reg_n_0_[0]\,
      I1 => empty,
      I2 => Bus_RNW_reg,
      I3 => \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg\,
      O => \ram_mem_reg_0_15_0_5_i_4__0_n_0\
    );
ram_mem_reg_0_15_12_17: unisim.vcomponents.RAM32M
     port map (
      ADDRA(4) => '0',
      ADDRA(3) => \ram_mem_reg_0_15_0_5_i_1__0_n_0\,
      ADDRA(2) => \ram_mem_reg_0_15_0_5_i_2__0_n_0\,
      ADDRA(1) => \ram_mem_reg_0_15_0_5_i_3__0_n_0\,
      ADDRA(0) => \ram_mem_reg_0_15_0_5_i_4__0_n_0\,
      ADDRB(4) => '0',
      ADDRB(3) => \ram_mem_reg_0_15_0_5_i_1__0_n_0\,
      ADDRB(2) => \ram_mem_reg_0_15_0_5_i_2__0_n_0\,
      ADDRB(1) => \ram_mem_reg_0_15_0_5_i_3__0_n_0\,
      ADDRB(0) => \ram_mem_reg_0_15_0_5_i_4__0_n_0\,
      ADDRC(4) => '0',
      ADDRC(3) => \ram_mem_reg_0_15_0_5_i_1__0_n_0\,
      ADDRC(2) => \ram_mem_reg_0_15_0_5_i_2__0_n_0\,
      ADDRC(1) => \ram_mem_reg_0_15_0_5_i_3__0_n_0\,
      ADDRC(0) => \ram_mem_reg_0_15_0_5_i_4__0_n_0\,
      ADDRD(4) => '0',
      ADDRD(3) => \write_addr_reg_n_0_[3]\,
      ADDRD(2) => p_2_in17_in,
      ADDRD(1) => p_1_in16_in,
      ADDRD(0) => \write_addr_reg_n_0_[0]\,
      DIA(1 downto 0) => S1_AXI_WDATA(13 downto 12),
      DIB(1 downto 0) => S1_AXI_WDATA(15 downto 14),
      DIC(1 downto 0) => S1_AXI_WDATA(17 downto 16),
      DID(1 downto 0) => B"00",
      DOA(1) => ram_mem_reg_0_15_12_17_n_0,
      DOA(0) => ram_mem_reg_0_15_12_17_n_1,
      DOB(1) => ram_mem_reg_0_15_12_17_n_2,
      DOB(0) => ram_mem_reg_0_15_12_17_n_3,
      DOC(1) => ram_mem_reg_0_15_12_17_n_4,
      DOC(0) => ram_mem_reg_0_15_12_17_n_5,
      DOD(1 downto 0) => NLW_ram_mem_reg_0_15_12_17_DOD_UNCONNECTED(1 downto 0),
      WCLK => S1_AXI_ACLK,
      WE => \write_nextgray[2]_i_1_n_0\
    );
ram_mem_reg_0_15_18_23: unisim.vcomponents.RAM32M
     port map (
      ADDRA(4) => '0',
      ADDRA(3) => \ram_mem_reg_0_15_0_5_i_1__0_n_0\,
      ADDRA(2) => \ram_mem_reg_0_15_0_5_i_2__0_n_0\,
      ADDRA(1) => \ram_mem_reg_0_15_0_5_i_3__0_n_0\,
      ADDRA(0) => \ram_mem_reg_0_15_0_5_i_4__0_n_0\,
      ADDRB(4) => '0',
      ADDRB(3) => \ram_mem_reg_0_15_0_5_i_1__0_n_0\,
      ADDRB(2) => \ram_mem_reg_0_15_0_5_i_2__0_n_0\,
      ADDRB(1) => \ram_mem_reg_0_15_0_5_i_3__0_n_0\,
      ADDRB(0) => \ram_mem_reg_0_15_0_5_i_4__0_n_0\,
      ADDRC(4) => '0',
      ADDRC(3) => \ram_mem_reg_0_15_0_5_i_1__0_n_0\,
      ADDRC(2) => \ram_mem_reg_0_15_0_5_i_2__0_n_0\,
      ADDRC(1) => \ram_mem_reg_0_15_0_5_i_3__0_n_0\,
      ADDRC(0) => \ram_mem_reg_0_15_0_5_i_4__0_n_0\,
      ADDRD(4) => '0',
      ADDRD(3) => \write_addr_reg_n_0_[3]\,
      ADDRD(2) => p_2_in17_in,
      ADDRD(1) => p_1_in16_in,
      ADDRD(0) => \write_addr_reg_n_0_[0]\,
      DIA(1 downto 0) => S1_AXI_WDATA(19 downto 18),
      DIB(1 downto 0) => S1_AXI_WDATA(21 downto 20),
      DIC(1 downto 0) => S1_AXI_WDATA(23 downto 22),
      DID(1 downto 0) => B"00",
      DOA(1) => ram_mem_reg_0_15_18_23_n_0,
      DOA(0) => ram_mem_reg_0_15_18_23_n_1,
      DOB(1) => ram_mem_reg_0_15_18_23_n_2,
      DOB(0) => ram_mem_reg_0_15_18_23_n_3,
      DOC(1) => ram_mem_reg_0_15_18_23_n_4,
      DOC(0) => ram_mem_reg_0_15_18_23_n_5,
      DOD(1 downto 0) => NLW_ram_mem_reg_0_15_18_23_DOD_UNCONNECTED(1 downto 0),
      WCLK => S1_AXI_ACLK,
      WE => \write_nextgray[2]_i_1_n_0\
    );
ram_mem_reg_0_15_24_29: unisim.vcomponents.RAM32M
     port map (
      ADDRA(4) => '0',
      ADDRA(3) => \ram_mem_reg_0_15_0_5_i_1__0_n_0\,
      ADDRA(2) => \ram_mem_reg_0_15_0_5_i_2__0_n_0\,
      ADDRA(1) => \ram_mem_reg_0_15_0_5_i_3__0_n_0\,
      ADDRA(0) => \ram_mem_reg_0_15_0_5_i_4__0_n_0\,
      ADDRB(4) => '0',
      ADDRB(3) => \ram_mem_reg_0_15_0_5_i_1__0_n_0\,
      ADDRB(2) => \ram_mem_reg_0_15_0_5_i_2__0_n_0\,
      ADDRB(1) => \ram_mem_reg_0_15_0_5_i_3__0_n_0\,
      ADDRB(0) => \ram_mem_reg_0_15_0_5_i_4__0_n_0\,
      ADDRC(4) => '0',
      ADDRC(3) => \ram_mem_reg_0_15_0_5_i_1__0_n_0\,
      ADDRC(2) => \ram_mem_reg_0_15_0_5_i_2__0_n_0\,
      ADDRC(1) => \ram_mem_reg_0_15_0_5_i_3__0_n_0\,
      ADDRC(0) => \ram_mem_reg_0_15_0_5_i_4__0_n_0\,
      ADDRD(4) => '0',
      ADDRD(3) => \write_addr_reg_n_0_[3]\,
      ADDRD(2) => p_2_in17_in,
      ADDRD(1) => p_1_in16_in,
      ADDRD(0) => \write_addr_reg_n_0_[0]\,
      DIA(1 downto 0) => S1_AXI_WDATA(25 downto 24),
      DIB(1 downto 0) => S1_AXI_WDATA(27 downto 26),
      DIC(1 downto 0) => S1_AXI_WDATA(29 downto 28),
      DID(1 downto 0) => B"00",
      DOA(1) => ram_mem_reg_0_15_24_29_n_0,
      DOA(0) => ram_mem_reg_0_15_24_29_n_1,
      DOB(1) => ram_mem_reg_0_15_24_29_n_2,
      DOB(0) => ram_mem_reg_0_15_24_29_n_3,
      DOC(1) => ram_mem_reg_0_15_24_29_n_4,
      DOC(0) => ram_mem_reg_0_15_24_29_n_5,
      DOD(1 downto 0) => NLW_ram_mem_reg_0_15_24_29_DOD_UNCONNECTED(1 downto 0),
      WCLK => S1_AXI_ACLK,
      WE => \write_nextgray[2]_i_1_n_0\
    );
ram_mem_reg_0_15_30_31: unisim.vcomponents.RAM32M
     port map (
      ADDRA(4) => '0',
      ADDRA(3) => \ram_mem_reg_0_15_0_5_i_1__0_n_0\,
      ADDRA(2) => \ram_mem_reg_0_15_0_5_i_2__0_n_0\,
      ADDRA(1) => \ram_mem_reg_0_15_0_5_i_3__0_n_0\,
      ADDRA(0) => \ram_mem_reg_0_15_0_5_i_4__0_n_0\,
      ADDRB(4) => '0',
      ADDRB(3) => \ram_mem_reg_0_15_0_5_i_1__0_n_0\,
      ADDRB(2) => \ram_mem_reg_0_15_0_5_i_2__0_n_0\,
      ADDRB(1) => \ram_mem_reg_0_15_0_5_i_3__0_n_0\,
      ADDRB(0) => \ram_mem_reg_0_15_0_5_i_4__0_n_0\,
      ADDRC(4) => '0',
      ADDRC(3) => \ram_mem_reg_0_15_0_5_i_1__0_n_0\,
      ADDRC(2) => \ram_mem_reg_0_15_0_5_i_2__0_n_0\,
      ADDRC(1) => \ram_mem_reg_0_15_0_5_i_3__0_n_0\,
      ADDRC(0) => \ram_mem_reg_0_15_0_5_i_4__0_n_0\,
      ADDRD(4) => '0',
      ADDRD(3) => \write_addr_reg_n_0_[3]\,
      ADDRD(2) => p_2_in17_in,
      ADDRD(1) => p_1_in16_in,
      ADDRD(0) => \write_addr_reg_n_0_[0]\,
      DIA(1 downto 0) => S1_AXI_WDATA(31 downto 30),
      DIB(1 downto 0) => B"00",
      DIC(1 downto 0) => B"00",
      DID(1 downto 0) => B"00",
      DOA(1) => ram_mem_reg_0_15_30_31_n_0,
      DOA(0) => ram_mem_reg_0_15_30_31_n_1,
      DOB(1 downto 0) => NLW_ram_mem_reg_0_15_30_31_DOB_UNCONNECTED(1 downto 0),
      DOC(1 downto 0) => NLW_ram_mem_reg_0_15_30_31_DOC_UNCONNECTED(1 downto 0),
      DOD(1 downto 0) => NLW_ram_mem_reg_0_15_30_31_DOD_UNCONNECTED(1 downto 0),
      WCLK => S1_AXI_ACLK,
      WE => \write_nextgray[2]_i_1_n_0\
    );
ram_mem_reg_0_15_6_11: unisim.vcomponents.RAM32M
     port map (
      ADDRA(4) => '0',
      ADDRA(3) => \ram_mem_reg_0_15_0_5_i_1__0_n_0\,
      ADDRA(2) => \ram_mem_reg_0_15_0_5_i_2__0_n_0\,
      ADDRA(1) => \ram_mem_reg_0_15_0_5_i_3__0_n_0\,
      ADDRA(0) => \ram_mem_reg_0_15_0_5_i_4__0_n_0\,
      ADDRB(4) => '0',
      ADDRB(3) => \ram_mem_reg_0_15_0_5_i_1__0_n_0\,
      ADDRB(2) => \ram_mem_reg_0_15_0_5_i_2__0_n_0\,
      ADDRB(1) => \ram_mem_reg_0_15_0_5_i_3__0_n_0\,
      ADDRB(0) => \ram_mem_reg_0_15_0_5_i_4__0_n_0\,
      ADDRC(4) => '0',
      ADDRC(3) => \ram_mem_reg_0_15_0_5_i_1__0_n_0\,
      ADDRC(2) => \ram_mem_reg_0_15_0_5_i_2__0_n_0\,
      ADDRC(1) => \ram_mem_reg_0_15_0_5_i_3__0_n_0\,
      ADDRC(0) => \ram_mem_reg_0_15_0_5_i_4__0_n_0\,
      ADDRD(4) => '0',
      ADDRD(3) => \write_addr_reg_n_0_[3]\,
      ADDRD(2) => p_2_in17_in,
      ADDRD(1) => p_1_in16_in,
      ADDRD(0) => \write_addr_reg_n_0_[0]\,
      DIA(1 downto 0) => S1_AXI_WDATA(7 downto 6),
      DIB(1 downto 0) => S1_AXI_WDATA(9 downto 8),
      DIC(1 downto 0) => S1_AXI_WDATA(11 downto 10),
      DID(1 downto 0) => B"00",
      DOA(1) => ram_mem_reg_0_15_6_11_n_0,
      DOA(0) => ram_mem_reg_0_15_6_11_n_1,
      DOB(1) => ram_mem_reg_0_15_6_11_n_2,
      DOB(0) => ram_mem_reg_0_15_6_11_n_3,
      DOC(1) => ram_mem_reg_0_15_6_11_n_4,
      DOC(0) => ram_mem_reg_0_15_6_11_n_5,
      DOD(1 downto 0) => NLW_ram_mem_reg_0_15_6_11_DOD_UNCONNECTED(1 downto 0),
      WCLK => S1_AXI_ACLK,
      WE => \write_nextgray[2]_i_1_n_0\
    );
rd_rst_meta_inst: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => '0',
      PRE => S1_AXI_ARESETN,
      Q => rd_meta_reset
    );
rd_rst_sync_inst: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => rd_meta_reset,
      PRE => S1_AXI_ARESETN,
      Q => rd_reset
    );
\rd_write_addrgray_dx_reg[0][0]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => write_addrgray(0),
      Q => \rd_write_addrgray_dx[0]_4\(0),
      R => '0'
    );
\rd_write_addrgray_dx_reg[0][1]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => write_addrgray(1),
      Q => \rd_write_addrgray_dx[0]_4\(1),
      R => '0'
    );
\rd_write_addrgray_dx_reg[0][2]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => write_addrgray(2),
      Q => \rd_write_addrgray_dx[0]_4\(2),
      R => '0'
    );
\rd_write_addrgray_dx_reg[0][3]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => write_addrgray(3),
      Q => \rd_write_addrgray_dx[0]_4\(3),
      R => '0'
    );
\rd_write_addrgray_dx_reg[1][0]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \rd_write_addrgray_dx[0]_4\(0),
      Q => \rd_write_addrgray_dx[1]_5\(0),
      R => '0'
    );
\rd_write_addrgray_dx_reg[1][1]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \rd_write_addrgray_dx[0]_4\(1),
      Q => \rd_write_addrgray_dx[1]_5\(1),
      R => '0'
    );
\rd_write_addrgray_dx_reg[1][2]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \rd_write_addrgray_dx[0]_4\(2),
      Q => \rd_write_addrgray_dx[1]_5\(2),
      R => '0'
    );
\rd_write_addrgray_dx_reg[1][3]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \rd_write_addrgray_dx[0]_4\(3),
      Q => \rd_write_addrgray_dx[1]_5\(3),
      R => '0'
    );
\rd_write_next[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \rd_write_nextgray_dx[1]_3\(2),
      I1 => \rd_write_nextgray_dx[1]_3\(3),
      I2 => \rd_write_nextgray_dx[1]_3\(0),
      I3 => \rd_write_nextgray_dx[1]_3\(1),
      O => Gray2Bin(0)
    );
\rd_write_next[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \rd_write_nextgray_dx[1]_3\(3),
      I1 => \rd_write_nextgray_dx[1]_3\(1),
      I2 => \rd_write_nextgray_dx[1]_3\(2),
      O => Gray2Bin(1)
    );
\rd_write_next[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \rd_write_nextgray_dx[1]_3\(3),
      I1 => \rd_write_nextgray_dx[1]_3\(2),
      O => Gray2Bin(2)
    );
\rd_write_next_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => Gray2Bin(0),
      Q => \rd_write_next_reg_n_0_[0]\,
      R => '0'
    );
\rd_write_next_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => Gray2Bin(1),
      Q => \rd_write_next_reg_n_0_[1]\,
      R => '0'
    );
\rd_write_next_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => Gray2Bin(2),
      Q => \rd_write_next_reg_n_0_[2]\,
      R => '0'
    );
\rd_write_next_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \rd_write_nextgray_dx[1]_3\(3),
      Q => \rd_write_next_reg_n_0_[3]\,
      R => '0'
    );
\rd_write_nextgray_dx_reg[0][0]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => write_nextgray(0),
      Q => \rd_write_nextgray_dx[0]_2\(0),
      R => '0'
    );
\rd_write_nextgray_dx_reg[0][1]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => write_nextgray(1),
      Q => \rd_write_nextgray_dx[0]_2\(1),
      R => '0'
    );
\rd_write_nextgray_dx_reg[0][2]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => write_nextgray(2),
      Q => \rd_write_nextgray_dx[0]_2\(2),
      R => '0'
    );
\rd_write_nextgray_dx_reg[0][3]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => write_nextgray(3),
      Q => \rd_write_nextgray_dx[0]_2\(3),
      R => '0'
    );
\rd_write_nextgray_dx_reg[1][0]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \rd_write_nextgray_dx[0]_2\(0),
      Q => \rd_write_nextgray_dx[1]_3\(0),
      R => '0'
    );
\rd_write_nextgray_dx_reg[1][1]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \rd_write_nextgray_dx[0]_2\(1),
      Q => \rd_write_nextgray_dx[1]_3\(1),
      R => '0'
    );
\rd_write_nextgray_dx_reg[1][2]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \rd_write_nextgray_dx[0]_2\(2),
      Q => \rd_write_nextgray_dx[1]_3\(2),
      R => '0'
    );
\rd_write_nextgray_dx_reg[1][3]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \rd_write_nextgray_dx[0]_2\(3),
      Q => \rd_write_nextgray_dx[1]_3\(3),
      R => '0'
    );
\read_addr_d1_reg[0]\: unisim.vcomponents.FDPE
     port map (
      C => S0_AXI_ACLK,
      CE => E(0),
      D => \read_addr_reg_n_0_[0]\,
      PRE => rd_reset,
      Q => \read_addr_d1_reg_n_0_[0]\
    );
\read_addr_d1_reg[1]\: unisim.vcomponents.FDPE
     port map (
      C => S0_AXI_ACLK,
      CE => E(0),
      D => p_1_in20_in,
      PRE => rd_reset,
      Q => \read_addr_d1_reg_n_0_[1]\
    );
\read_addr_d1_reg[2]\: unisim.vcomponents.FDPE
     port map (
      C => S0_AXI_ACLK,
      CE => E(0),
      D => p_2_in22_in,
      PRE => rd_reset,
      Q => \read_addr_d1_reg_n_0_[2]\
    );
\read_addr_d1_reg[3]\: unisim.vcomponents.FDPE
     port map (
      C => S0_AXI_ACLK,
      CE => E(0),
      D => \read_addr_reg_n_0_[3]\,
      PRE => rd_reset,
      Q => read_nextgray(3)
    );
\read_addr_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      CLR => rd_reset,
      D => \ram_mem_reg_0_15_0_5_i_4__0_n_0\,
      Q => \read_addr_reg_n_0_[0]\
    );
\read_addr_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      CLR => rd_reset,
      D => \ram_mem_reg_0_15_0_5_i_3__0_n_0\,
      Q => p_1_in20_in
    );
\read_addr_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      CLR => rd_reset,
      D => \ram_mem_reg_0_15_0_5_i_2__0_n_0\,
      Q => p_2_in22_in
    );
\read_addr_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      CLR => rd_reset,
      D => \ram_mem_reg_0_15_0_5_i_1__0_n_0\,
      Q => \read_addr_reg_n_0_[3]\
    );
\read_addrgray_reg[0]\: unisim.vcomponents.FDPE
     port map (
      C => S0_AXI_ACLK,
      CE => E(0),
      D => read_nextgray(0),
      PRE => rd_reset,
      Q => \read_addrgray_reg_n_0_[0]\
    );
\read_addrgray_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => S0_AXI_ACLK,
      CE => E(0),
      CLR => rd_reset,
      D => read_nextgray(1),
      Q => p_2_in52_in
    );
\read_addrgray_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => S0_AXI_ACLK,
      CE => E(0),
      CLR => rd_reset,
      D => read_nextgray(2),
      Q => p_2_in57_in
    );
\read_addrgray_reg[3]\: unisim.vcomponents.FDPE
     port map (
      C => S0_AXI_ACLK,
      CE => E(0),
      D => read_nextgray(3),
      PRE => rd_reset,
      Q => p_2_in62_in
    );
\read_lastgray_reg[0]\: unisim.vcomponents.FDPE
     port map (
      C => S0_AXI_ACLK,
      CE => E(0),
      D => \read_addrgray_reg_n_0_[0]\,
      PRE => rd_reset,
      Q => read_lastgray(0)
    );
\read_lastgray_reg[1]\: unisim.vcomponents.FDPE
     port map (
      C => S0_AXI_ACLK,
      CE => E(0),
      D => p_2_in52_in,
      PRE => rd_reset,
      Q => read_lastgray(1)
    );
\read_lastgray_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => S0_AXI_ACLK,
      CE => E(0),
      CLR => rd_reset,
      D => p_2_in57_in,
      Q => read_lastgray(2)
    );
\read_lastgray_reg[3]\: unisim.vcomponents.FDPE
     port map (
      C => S0_AXI_ACLK,
      CE => E(0),
      D => p_2_in62_in,
      PRE => rd_reset,
      Q => read_lastgray(3)
    );
\read_nextgray[0]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_1_in20_in,
      I1 => \read_addr_reg_n_0_[0]\,
      O => Bin2Gray(0)
    );
\read_nextgray[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_2_in22_in,
      I1 => p_1_in20_in,
      O => Bin2Gray(1)
    );
\read_nextgray[2]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_addr_reg_n_0_[3]\,
      I1 => p_2_in22_in,
      O => Bin2Gray(2)
    );
\read_nextgray_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => S0_AXI_ACLK,
      CE => E(0),
      CLR => rd_reset,
      D => Bin2Gray(0),
      Q => read_nextgray(0)
    );
\read_nextgray_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => S0_AXI_ACLK,
      CE => E(0),
      CLR => rd_reset,
      D => Bin2Gray(1),
      Q => read_nextgray(1)
    );
\read_nextgray_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => S0_AXI_ACLK,
      CE => E(0),
      CLR => rd_reset,
      D => Bin2Gray(2),
      Q => read_nextgray(2)
    );
\rit_detect_d1_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \rit_detect_d1_i_2__0_n_0\,
      I1 => \rit_register_reg[0]\(3),
      I2 => \Rd_Length_i_reg_n_0_[3]\,
      O => rit_detect_d0
    );
\rit_detect_d1_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2F02FFFF00002F02"
    )
        port map (
      I0 => \Rd_Length_i_reg_n_0_[0]\,
      I1 => \rit_register_reg[0]\(0),
      I2 => \rit_register_reg[0]\(1),
      I3 => \Rd_Length_i_reg_n_0_[1]\,
      I4 => \rit_register_reg[0]\(2),
      I5 => \Rd_Length_i_reg_n_0_[2]\,
      O => \rit_detect_d1_i_2__0_n_0\
    );
sit_detect_d1_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => sit_detect_d1_i_2_n_0,
      I1 => \Wr_Length_i_reg_n_0_[3]\,
      I2 => Q(3),
      O => sit_detect_d0
    );
sit_detect_d1_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DF0DFFFF0000DF0D"
    )
        port map (
      I0 => \Wr_Length_i_reg_n_0_[0]\,
      I1 => Q(0),
      I2 => \Wr_Length_i_reg_n_0_[1]\,
      I3 => Q(1),
      I4 => \Wr_Length_i_reg_n_0_[2]\,
      I5 => Q(2),
      O => sit_detect_d1_i_2_n_0
    );
\wr_read_lastgray_dx_reg[0][0]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => read_lastgray(0),
      Q => \wr_read_lastgray_dx[0]_6\(0),
      R => '0'
    );
\wr_read_lastgray_dx_reg[0][1]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => read_lastgray(1),
      Q => \wr_read_lastgray_dx[0]_6\(1),
      R => '0'
    );
\wr_read_lastgray_dx_reg[0][2]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => read_lastgray(2),
      Q => \wr_read_lastgray_dx[0]_6\(2),
      R => '0'
    );
\wr_read_lastgray_dx_reg[0][3]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => read_lastgray(3),
      Q => \wr_read_lastgray_dx[0]_6\(3),
      R => '0'
    );
\wr_read_lastgray_dx_reg[1][0]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \wr_read_lastgray_dx[0]_6\(0),
      Q => \wr_read_lastgray_dx[1]_7\(0),
      R => '0'
    );
\wr_read_lastgray_dx_reg[1][1]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \wr_read_lastgray_dx[0]_6\(1),
      Q => \wr_read_lastgray_dx[1]_7\(1),
      R => '0'
    );
\wr_read_lastgray_dx_reg[1][2]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \wr_read_lastgray_dx[0]_6\(2),
      Q => \wr_read_lastgray_dx[1]_7\(2),
      R => '0'
    );
\wr_read_lastgray_dx_reg[1][3]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \wr_read_lastgray_dx[0]_6\(3),
      Q => \wr_read_lastgray_dx[1]_7\(3),
      R => '0'
    );
\wr_read_next[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \wr_read_nextgray_dx[1]_1\(2),
      I1 => \wr_read_nextgray_dx[1]_1\(3),
      I2 => \wr_read_nextgray_dx[1]_1\(0),
      I3 => \wr_read_nextgray_dx[1]_1\(1),
      O => \wr_read_next[0]_i_1_n_0\
    );
\wr_read_next[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \wr_read_nextgray_dx[1]_1\(3),
      I1 => \wr_read_nextgray_dx[1]_1\(1),
      I2 => \wr_read_nextgray_dx[1]_1\(2),
      O => \wr_read_next[1]_i_1_n_0\
    );
\wr_read_next[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \wr_read_nextgray_dx[1]_1\(3),
      I1 => \wr_read_nextgray_dx[1]_1\(2),
      O => \wr_read_next[2]_i_1_n_0\
    );
\wr_read_next_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \wr_read_next[0]_i_1_n_0\,
      Q => \wr_read_next_reg_n_0_[0]\,
      R => '0'
    );
\wr_read_next_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \wr_read_next[1]_i_1_n_0\,
      Q => \wr_read_next_reg_n_0_[1]\,
      R => '0'
    );
\wr_read_next_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \wr_read_next[2]_i_1_n_0\,
      Q => \wr_read_next_reg_n_0_[2]\,
      R => '0'
    );
\wr_read_next_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \wr_read_nextgray_dx[1]_1\(3),
      Q => \wr_read_next_reg_n_0_[3]\,
      R => '0'
    );
\wr_read_nextgray_dx_reg[0][0]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => read_nextgray(0),
      Q => \wr_read_nextgray_dx[0]_0\(0),
      R => '0'
    );
\wr_read_nextgray_dx_reg[0][1]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => read_nextgray(1),
      Q => \wr_read_nextgray_dx[0]_0\(1),
      R => '0'
    );
\wr_read_nextgray_dx_reg[0][2]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => read_nextgray(2),
      Q => \wr_read_nextgray_dx[0]_0\(2),
      R => '0'
    );
\wr_read_nextgray_dx_reg[0][3]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => read_nextgray(3),
      Q => \wr_read_nextgray_dx[0]_0\(3),
      R => '0'
    );
\wr_read_nextgray_dx_reg[1][0]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \wr_read_nextgray_dx[0]_0\(0),
      Q => \wr_read_nextgray_dx[1]_1\(0),
      R => '0'
    );
\wr_read_nextgray_dx_reg[1][1]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \wr_read_nextgray_dx[0]_0\(1),
      Q => \wr_read_nextgray_dx[1]_1\(1),
      R => '0'
    );
\wr_read_nextgray_dx_reg[1][2]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \wr_read_nextgray_dx[0]_0\(2),
      Q => \wr_read_nextgray_dx[1]_1\(2),
      R => '0'
    );
\wr_read_nextgray_dx_reg[1][3]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \wr_read_nextgray_dx[0]_0\(3),
      Q => \wr_read_nextgray_dx[1]_1\(3),
      R => '0'
    );
wr_rst_meta_inst: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => '0',
      PRE => S1_AXI_ARESETN,
      Q => wr_meta_reset
    );
wr_rst_sync_inst: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => wr_meta_reset,
      PRE => S1_AXI_ARESETN,
      Q => wr_reset
    );
\write_addr[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \write_addr_reg_n_0_[0]\,
      O => plusOp(0)
    );
\write_addr[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \write_addr_reg_n_0_[0]\,
      I1 => p_1_in16_in,
      O => plusOp(1)
    );
\write_addr[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \write_addr_reg_n_0_[0]\,
      I1 => p_1_in16_in,
      I2 => p_2_in17_in,
      O => plusOp(2)
    );
\write_addr[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => p_1_in16_in,
      I1 => \write_addr_reg_n_0_[0]\,
      I2 => p_2_in17_in,
      I3 => \write_addr_reg_n_0_[3]\,
      O => plusOp(3)
    );
\write_addr_d1_reg[0]\: unisim.vcomponents.FDPE
     port map (
      C => S1_AXI_ACLK,
      CE => \write_nextgray[2]_i_1_n_0\,
      D => \write_addr_reg_n_0_[0]\,
      PRE => wr_reset,
      Q => \write_addr_d1_reg_n_0_[0]\
    );
\write_addr_d1_reg[1]\: unisim.vcomponents.FDPE
     port map (
      C => S1_AXI_ACLK,
      CE => \write_nextgray[2]_i_1_n_0\,
      D => p_1_in16_in,
      PRE => wr_reset,
      Q => \write_addr_d1_reg_n_0_[1]\
    );
\write_addr_d1_reg[2]\: unisim.vcomponents.FDPE
     port map (
      C => S1_AXI_ACLK,
      CE => \write_nextgray[2]_i_1_n_0\,
      D => p_2_in17_in,
      PRE => wr_reset,
      Q => \write_addr_d1_reg_n_0_[2]\
    );
\write_addr_d1_reg[3]\: unisim.vcomponents.FDPE
     port map (
      C => S1_AXI_ACLK,
      CE => \write_nextgray[2]_i_1_n_0\,
      D => \write_addr_reg_n_0_[3]\,
      PRE => wr_reset,
      Q => write_nextgray(3)
    );
\write_addr_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => S1_AXI_ACLK,
      CE => \write_nextgray[2]_i_1_n_0\,
      CLR => wr_reset,
      D => plusOp(0),
      Q => \write_addr_reg_n_0_[0]\
    );
\write_addr_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => S1_AXI_ACLK,
      CE => \write_nextgray[2]_i_1_n_0\,
      CLR => wr_reset,
      D => plusOp(1),
      Q => p_1_in16_in
    );
\write_addr_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => S1_AXI_ACLK,
      CE => \write_nextgray[2]_i_1_n_0\,
      CLR => wr_reset,
      D => plusOp(2),
      Q => p_2_in17_in
    );
\write_addr_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => S1_AXI_ACLK,
      CE => \write_nextgray[2]_i_1_n_0\,
      CLR => wr_reset,
      D => plusOp(3),
      Q => \write_addr_reg_n_0_[3]\
    );
\write_addrgray_reg[0]\: unisim.vcomponents.FDPE
     port map (
      C => S1_AXI_ACLK,
      CE => \write_nextgray[2]_i_1_n_0\,
      D => write_nextgray(0),
      PRE => wr_reset,
      Q => write_addrgray(0)
    );
\write_addrgray_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => S1_AXI_ACLK,
      CE => \write_nextgray[2]_i_1_n_0\,
      CLR => wr_reset,
      D => write_nextgray(1),
      Q => write_addrgray(1)
    );
\write_addrgray_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => S1_AXI_ACLK,
      CE => \write_nextgray[2]_i_1_n_0\,
      CLR => wr_reset,
      D => write_nextgray(2),
      Q => write_addrgray(2)
    );
\write_addrgray_reg[3]\: unisim.vcomponents.FDPE
     port map (
      C => S1_AXI_ACLK,
      CE => \write_nextgray[2]_i_1_n_0\,
      D => write_nextgray(3),
      PRE => wr_reset,
      Q => write_addrgray(3)
    );
\write_nextgray[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_1_in16_in,
      I1 => \write_addr_reg_n_0_[0]\,
      O => \write_nextgray[0]_i_1_n_0\
    );
\write_nextgray[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_2_in17_in,
      I1 => p_1_in16_in,
      O => \write_nextgray[1]_i_1_n_0\
    );
\write_nextgray[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => \^write_addr_reg[0]_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg\,
      I2 => Bus_RNW_reg_0,
      I3 => \^write_addr_reg[0]_0\,
      O => \write_nextgray[2]_i_1_n_0\
    );
\write_nextgray[2]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \write_addr_reg_n_0_[3]\,
      I1 => p_2_in17_in,
      O => \write_nextgray[2]_i_2_n_0\
    );
\write_nextgray_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => S1_AXI_ACLK,
      CE => \write_nextgray[2]_i_1_n_0\,
      CLR => wr_reset,
      D => \write_nextgray[0]_i_1_n_0\,
      Q => write_nextgray(0)
    );
\write_nextgray_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => S1_AXI_ACLK,
      CE => \write_nextgray[2]_i_1_n_0\,
      CLR => wr_reset,
      D => \write_nextgray[1]_i_1_n_0\,
      Q => write_nextgray(1)
    );
\write_nextgray_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => S1_AXI_ACLK,
      CE => \write_nextgray[2]_i_1_n_0\,
      CLR => wr_reset,
      D => \write_nextgray[2]_i_2_n_0\,
      Q => write_nextgray(2)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_mailbox_0_0_Async_FIFO_1 is
  port (
    rd_rst_meta_inst_0 : out STD_LOGIC;
    \out\ : out STD_LOGIC;
    \write_nextgray_reg[2]_0\ : out STD_LOGIC;
    rit_detect_d0 : out STD_LOGIC;
    sit_detect_d0 : out STD_LOGIC;
    \s_axi_rdata_i_reg[31]\ : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S1_AXI_ACLK : in STD_LOGIC;
    S0_AXI_ACLK : in STD_LOGIC;
    empty_allow : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    Bus_RNW_reg : in STD_LOGIC;
    \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg\ : in STD_LOGIC;
    \rit_register_reg[0]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg\ : in STD_LOGIC;
    Bus_RNW_reg_0 : in STD_LOGIC;
    S0_AXI_ARESETN : in STD_LOGIC;
    SYS_Rst : in STD_LOGIC;
    S1_AXI_ARESETN : in STD_LOGIC;
    S0_AXI_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_mailbox_0_0_Async_FIFO_1 : entity is "Async_FIFO";
end design_1_mailbox_0_0_Async_FIFO_1;

architecture STRUCTURE of design_1_mailbox_0_0_Async_FIFO_1 is
  signal Bin2Gray : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal CI : STD_LOGIC;
  signal DataOut0 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal Gray2Bin : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal Rd_Length : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal Rd_Length_i00_out : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \Rd_Length_i[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \Rd_Length_i[3]_i_2__0_n_0\ : STD_LOGIC;
  signal S : STD_LOGIC;
  signal S0 : STD_LOGIC;
  signal S00_out : STD_LOGIC;
  signal S01_out : STD_LOGIC;
  signal S02_out : STD_LOGIC;
  signal S04_out : STD_LOGIC;
  signal S05_out : STD_LOGIC;
  signal S06_out : STD_LOGIC;
  signal Wr_Length : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal Wr_Length_i01_out : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \Wr_Length_i[0]_i_1_n_0\ : STD_LOGIC;
  signal \Wr_Length_i[3]_i_2_n_0\ : STD_LOGIC;
  signal empty : STD_LOGIC;
  attribute async_reg : string;
  attribute async_reg of empty : signal is "true";
  signal emptyg : STD_LOGIC;
  signal emuxcyo_0 : STD_LOGIC;
  signal emuxcyo_1 : STD_LOGIC;
  signal fmuxcyo_0 : STD_LOGIC;
  signal fmuxcyo_1 : STD_LOGIC;
  signal fmuxcyo_2 : STD_LOGIC;
  signal full_i_i_1_n_0 : STD_LOGIC;
  signal fullg : STD_LOGIC;
  signal p_1_in16_in : STD_LOGIC;
  signal p_1_in20_in : STD_LOGIC;
  signal p_2_in17_in : STD_LOGIC;
  signal p_2_in22_in : STD_LOGIC;
  signal p_2_in52_in : STD_LOGIC;
  signal p_2_in57_in : STD_LOGIC;
  signal p_2_in62_in : STD_LOGIC;
  signal plusOp : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal rd_meta_reset : STD_LOGIC;
  signal rd_reset : STD_LOGIC;
  signal \^rd_rst_meta_inst_0\ : STD_LOGIC;
  signal \rd_write_addrgray_dx[0]_4\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg of \rd_write_addrgray_dx[0]_4\ : signal is "true";
  signal \rd_write_addrgray_dx[1]_5\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg of \rd_write_addrgray_dx[1]_5\ : signal is "true";
  signal \rd_write_addrgray_dx[2]_8\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg of \rd_write_addrgray_dx[2]_8\ : signal is "true";
  signal rd_write_next : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \rd_write_nextgray_dx[0]_2\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg of \rd_write_nextgray_dx[0]_2\ : signal is "true";
  signal \rd_write_nextgray_dx[1]_3\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg of \rd_write_nextgray_dx[1]_3\ : signal is "true";
  signal \rd_write_nextgray_dx[2]_9\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg of \rd_write_nextgray_dx[2]_9\ : signal is "true";
  signal read_addr_d1 : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal read_addr_next : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \read_addr_reg_n_0_[0]\ : STD_LOGIC;
  signal \read_addr_reg_n_0_[3]\ : STD_LOGIC;
  signal \read_addrgray_reg_n_0_[0]\ : STD_LOGIC;
  signal read_lastgray : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal read_nextgray : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal rit_detect_d1_i_2_n_0 : STD_LOGIC;
  signal \sit_detect_d1_i_2__0_n_0\ : STD_LOGIC;
  signal wr_meta_reset : STD_LOGIC;
  signal \wr_read_lastgray_dx[0]_6\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg of \wr_read_lastgray_dx[0]_6\ : signal is "true";
  signal \wr_read_lastgray_dx[1]_7\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg of \wr_read_lastgray_dx[1]_7\ : signal is "true";
  signal \wr_read_lastgray_dx[2]_10\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg of \wr_read_lastgray_dx[2]_10\ : signal is "true";
  signal wr_read_next : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \wr_read_next[0]_i_1_n_0\ : STD_LOGIC;
  signal \wr_read_next[1]_i_1_n_0\ : STD_LOGIC;
  signal \wr_read_next[2]_i_1_n_0\ : STD_LOGIC;
  signal \wr_read_nextgray_dx[0]_0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg of \wr_read_nextgray_dx[0]_0\ : signal is "true";
  signal \wr_read_nextgray_dx[1]_1\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg of \wr_read_nextgray_dx[1]_1\ : signal is "true";
  signal \wr_read_nextgray_dx[2]_11\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg of \wr_read_nextgray_dx[2]_11\ : signal is "true";
  signal wr_reset : STD_LOGIC;
  signal write_addr_d1 : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \write_addr_reg_n_0_[0]\ : STD_LOGIC;
  signal \write_addr_reg_n_0_[3]\ : STD_LOGIC;
  signal write_addrgray : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal write_nextgray : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \write_nextgray[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \write_nextgray[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \write_nextgray[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \write_nextgray[2]_i_2__0_n_0\ : STD_LOGIC;
  signal \^write_nextgray_reg[2]_0\ : STD_LOGIC;
  attribute async_reg of \write_nextgray_reg[2]_0\ : signal is "true";
  signal NLW_emuxcylow_CARRY4_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_fmuxcylow_CARRY4_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_mem_reg_0_15_0_5_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_mem_reg_0_15_12_17_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_mem_reg_0_15_18_23_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_mem_reg_0_15_24_29_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_mem_reg_0_15_30_31_DOB_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_mem_reg_0_15_30_31_DOC_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_mem_reg_0_15_30_31_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_mem_reg_0_15_6_11_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \Rd_Length_i[1]_i_1__0\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \Rd_Length_i[3]_i_2__0\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \Wr_Length_i[1]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \Wr_Length_i[3]_i_2\ : label is "soft_lutpair28";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of empty_reg : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of empty_reg : label is "yes";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of emuxcylow_CARRY4 : label is "(MUXCY,XORCY)";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of emuxcylow_CARRY4 : label is "LO:O";
  attribute box_type : string;
  attribute box_type of emuxcylow_CARRY4 : label is "PRIMITIVE";
  attribute XILINX_LEGACY_PRIM of fmuxcylow_CARRY4 : label is "(MUXCY,XORCY)";
  attribute XILINX_TRANSFORM_PINMAP of fmuxcylow_CARRY4 : label is "LO:O";
  attribute box_type of fmuxcylow_CARRY4 : label is "PRIMITIVE";
  attribute KEEP of full_i_reg : label is "yes";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_mem_reg_0_15_0_5 : label is "";
  attribute METHODOLOGY_DRC_VIOS of ram_mem_reg_0_15_12_17 : label is "";
  attribute METHODOLOGY_DRC_VIOS of ram_mem_reg_0_15_18_23 : label is "";
  attribute METHODOLOGY_DRC_VIOS of ram_mem_reg_0_15_24_29 : label is "";
  attribute METHODOLOGY_DRC_VIOS of ram_mem_reg_0_15_30_31 : label is "";
  attribute METHODOLOGY_DRC_VIOS of ram_mem_reg_0_15_6_11 : label is "";
  attribute XILINX_LEGACY_PRIM of rd_rst_meta_inst : label is "FDP";
  attribute box_type of rd_rst_meta_inst : label is "PRIMITIVE";
  attribute XILINX_LEGACY_PRIM of rd_rst_sync_inst : label is "FDP";
  attribute box_type of rd_rst_sync_inst : label is "PRIMITIVE";
  attribute ASYNC_REG_boolean of \rd_write_addrgray_dx_reg[0][0]\ : label is std.standard.true;
  attribute KEEP of \rd_write_addrgray_dx_reg[0][0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_addrgray_dx_reg[0][1]\ : label is std.standard.true;
  attribute KEEP of \rd_write_addrgray_dx_reg[0][1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_addrgray_dx_reg[0][2]\ : label is std.standard.true;
  attribute KEEP of \rd_write_addrgray_dx_reg[0][2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_addrgray_dx_reg[0][3]\ : label is std.standard.true;
  attribute KEEP of \rd_write_addrgray_dx_reg[0][3]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_addrgray_dx_reg[1][0]\ : label is std.standard.true;
  attribute KEEP of \rd_write_addrgray_dx_reg[1][0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_addrgray_dx_reg[1][1]\ : label is std.standard.true;
  attribute KEEP of \rd_write_addrgray_dx_reg[1][1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_addrgray_dx_reg[1][2]\ : label is std.standard.true;
  attribute KEEP of \rd_write_addrgray_dx_reg[1][2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_addrgray_dx_reg[1][3]\ : label is std.standard.true;
  attribute KEEP of \rd_write_addrgray_dx_reg[1][3]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_nextgray_dx_reg[0][0]\ : label is std.standard.true;
  attribute KEEP of \rd_write_nextgray_dx_reg[0][0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_nextgray_dx_reg[0][1]\ : label is std.standard.true;
  attribute KEEP of \rd_write_nextgray_dx_reg[0][1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_nextgray_dx_reg[0][2]\ : label is std.standard.true;
  attribute KEEP of \rd_write_nextgray_dx_reg[0][2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_nextgray_dx_reg[0][3]\ : label is std.standard.true;
  attribute KEEP of \rd_write_nextgray_dx_reg[0][3]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_nextgray_dx_reg[1][0]\ : label is std.standard.true;
  attribute KEEP of \rd_write_nextgray_dx_reg[1][0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_nextgray_dx_reg[1][1]\ : label is std.standard.true;
  attribute KEEP of \rd_write_nextgray_dx_reg[1][1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_nextgray_dx_reg[1][2]\ : label is std.standard.true;
  attribute KEEP of \rd_write_nextgray_dx_reg[1][2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \rd_write_nextgray_dx_reg[1][3]\ : label is std.standard.true;
  attribute KEEP of \rd_write_nextgray_dx_reg[1][3]\ : label is "yes";
  attribute SOFT_HLUTNM of \read_nextgray[1]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \read_nextgray[2]_i_2\ : label is "soft_lutpair33";
  attribute ASYNC_REG_boolean of \wr_read_lastgray_dx_reg[0][0]\ : label is std.standard.true;
  attribute KEEP of \wr_read_lastgray_dx_reg[0][0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_lastgray_dx_reg[0][1]\ : label is std.standard.true;
  attribute KEEP of \wr_read_lastgray_dx_reg[0][1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_lastgray_dx_reg[0][2]\ : label is std.standard.true;
  attribute KEEP of \wr_read_lastgray_dx_reg[0][2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_lastgray_dx_reg[0][3]\ : label is std.standard.true;
  attribute KEEP of \wr_read_lastgray_dx_reg[0][3]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_lastgray_dx_reg[1][0]\ : label is std.standard.true;
  attribute KEEP of \wr_read_lastgray_dx_reg[1][0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_lastgray_dx_reg[1][1]\ : label is std.standard.true;
  attribute KEEP of \wr_read_lastgray_dx_reg[1][1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_lastgray_dx_reg[1][2]\ : label is std.standard.true;
  attribute KEEP of \wr_read_lastgray_dx_reg[1][2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_lastgray_dx_reg[1][3]\ : label is std.standard.true;
  attribute KEEP of \wr_read_lastgray_dx_reg[1][3]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_nextgray_dx_reg[0][0]\ : label is std.standard.true;
  attribute KEEP of \wr_read_nextgray_dx_reg[0][0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_nextgray_dx_reg[0][1]\ : label is std.standard.true;
  attribute KEEP of \wr_read_nextgray_dx_reg[0][1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_nextgray_dx_reg[0][2]\ : label is std.standard.true;
  attribute KEEP of \wr_read_nextgray_dx_reg[0][2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_nextgray_dx_reg[0][3]\ : label is std.standard.true;
  attribute KEEP of \wr_read_nextgray_dx_reg[0][3]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_nextgray_dx_reg[1][0]\ : label is std.standard.true;
  attribute KEEP of \wr_read_nextgray_dx_reg[1][0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_nextgray_dx_reg[1][1]\ : label is std.standard.true;
  attribute KEEP of \wr_read_nextgray_dx_reg[1][1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_nextgray_dx_reg[1][2]\ : label is std.standard.true;
  attribute KEEP of \wr_read_nextgray_dx_reg[1][2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \wr_read_nextgray_dx_reg[1][3]\ : label is std.standard.true;
  attribute KEEP of \wr_read_nextgray_dx_reg[1][3]\ : label is "yes";
  attribute XILINX_LEGACY_PRIM of wr_rst_meta_inst : label is "FDP";
  attribute box_type of wr_rst_meta_inst : label is "PRIMITIVE";
  attribute XILINX_LEGACY_PRIM of wr_rst_sync_inst : label is "FDP";
  attribute box_type of wr_rst_sync_inst : label is "PRIMITIVE";
  attribute SOFT_HLUTNM of \write_addr[1]_i_1__0\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \write_addr[2]_i_1__0\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \write_addr[3]_i_1__0\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \write_nextgray[0]_i_1__0\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \write_nextgray[1]_i_1__0\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \write_nextgray[2]_i_2__0\ : label is "soft_lutpair32";
begin
  \out\ <= empty;
  rd_rst_meta_inst_0 <= \^rd_rst_meta_inst_0\;
  \write_nextgray_reg[2]_0\ <= \^write_nextgray_reg[2]_0\;
\DataOut_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(0),
      Q => \s_axi_rdata_i_reg[31]\(0),
      R => '0'
    );
\DataOut_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(10),
      Q => \s_axi_rdata_i_reg[31]\(10),
      R => '0'
    );
\DataOut_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(11),
      Q => \s_axi_rdata_i_reg[31]\(11),
      R => '0'
    );
\DataOut_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(12),
      Q => \s_axi_rdata_i_reg[31]\(12),
      R => '0'
    );
\DataOut_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(13),
      Q => \s_axi_rdata_i_reg[31]\(13),
      R => '0'
    );
\DataOut_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(14),
      Q => \s_axi_rdata_i_reg[31]\(14),
      R => '0'
    );
\DataOut_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(15),
      Q => \s_axi_rdata_i_reg[31]\(15),
      R => '0'
    );
\DataOut_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(16),
      Q => \s_axi_rdata_i_reg[31]\(16),
      R => '0'
    );
\DataOut_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(17),
      Q => \s_axi_rdata_i_reg[31]\(17),
      R => '0'
    );
\DataOut_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(18),
      Q => \s_axi_rdata_i_reg[31]\(18),
      R => '0'
    );
\DataOut_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(19),
      Q => \s_axi_rdata_i_reg[31]\(19),
      R => '0'
    );
\DataOut_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(1),
      Q => \s_axi_rdata_i_reg[31]\(1),
      R => '0'
    );
\DataOut_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(20),
      Q => \s_axi_rdata_i_reg[31]\(20),
      R => '0'
    );
\DataOut_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(21),
      Q => \s_axi_rdata_i_reg[31]\(21),
      R => '0'
    );
\DataOut_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(22),
      Q => \s_axi_rdata_i_reg[31]\(22),
      R => '0'
    );
\DataOut_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(23),
      Q => \s_axi_rdata_i_reg[31]\(23),
      R => '0'
    );
\DataOut_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(24),
      Q => \s_axi_rdata_i_reg[31]\(24),
      R => '0'
    );
\DataOut_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(25),
      Q => \s_axi_rdata_i_reg[31]\(25),
      R => '0'
    );
\DataOut_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(26),
      Q => \s_axi_rdata_i_reg[31]\(26),
      R => '0'
    );
\DataOut_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(27),
      Q => \s_axi_rdata_i_reg[31]\(27),
      R => '0'
    );
\DataOut_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(28),
      Q => \s_axi_rdata_i_reg[31]\(28),
      R => '0'
    );
\DataOut_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(29),
      Q => \s_axi_rdata_i_reg[31]\(29),
      R => '0'
    );
\DataOut_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(2),
      Q => \s_axi_rdata_i_reg[31]\(2),
      R => '0'
    );
\DataOut_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(30),
      Q => \s_axi_rdata_i_reg[31]\(30),
      R => '0'
    );
\DataOut_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(31),
      Q => \s_axi_rdata_i_reg[31]\(31),
      R => '0'
    );
\DataOut_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(3),
      Q => \s_axi_rdata_i_reg[31]\(3),
      R => '0'
    );
\DataOut_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(4),
      Q => \s_axi_rdata_i_reg[31]\(4),
      R => '0'
    );
\DataOut_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(5),
      Q => \s_axi_rdata_i_reg[31]\(5),
      R => '0'
    );
\DataOut_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(6),
      Q => \s_axi_rdata_i_reg[31]\(6),
      R => '0'
    );
\DataOut_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(7),
      Q => \s_axi_rdata_i_reg[31]\(7),
      R => '0'
    );
\DataOut_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(8),
      Q => \s_axi_rdata_i_reg[31]\(8),
      R => '0'
    );
\DataOut_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => DataOut0(9),
      Q => \s_axi_rdata_i_reg[31]\(9),
      R => '0'
    );
\Gen_emuxcy[1].emuxcy_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9A95"
    )
        port map (
      I0 => \rd_write_addrgray_dx[1]_5\(1),
      I1 => p_2_in52_in,
      I2 => empty,
      I3 => read_nextgray(1),
      O => S04_out
    );
\Gen_emuxcy[2].emuxcy_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9A95"
    )
        port map (
      I0 => \rd_write_addrgray_dx[1]_5\(2),
      I1 => p_2_in57_in,
      I2 => empty,
      I3 => read_nextgray(2),
      O => S05_out
    );
\Gen_fmuxcy[1].fmuxcy_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9A95"
    )
        port map (
      I0 => \wr_read_lastgray_dx[1]_7\(1),
      I1 => write_addrgray(1),
      I2 => \^write_nextgray_reg[2]_0\,
      I3 => write_nextgray(1),
      O => S00_out
    );
\Gen_fmuxcy[2].fmuxcy_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9A95"
    )
        port map (
      I0 => \wr_read_lastgray_dx[1]_7\(2),
      I1 => write_addrgray(2),
      I2 => \^write_nextgray_reg[2]_0\,
      I3 => write_nextgray(2),
      O => S01_out
    );
\Rd_Length_i[0]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => rd_write_next(0),
      I1 => read_addr_d1(0),
      O => \Rd_Length_i[0]_i_1__0_n_0\
    );
\Rd_Length_i[1]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => read_addr_d1(0),
      I1 => rd_write_next(0),
      I2 => read_addr_d1(1),
      I3 => rd_write_next(1),
      O => Rd_Length_i00_out(1)
    );
\Rd_Length_i[2]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F04B0FBB0FB4F04"
    )
        port map (
      I0 => rd_write_next(0),
      I1 => read_addr_d1(0),
      I2 => rd_write_next(1),
      I3 => read_addr_d1(1),
      I4 => read_addr_d1(2),
      I5 => rd_write_next(2),
      O => Rd_Length_i00_out(2)
    );
\Rd_Length_i[3]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"718E8E71"
    )
        port map (
      I0 => \Rd_Length_i[3]_i_2__0_n_0\,
      I1 => rd_write_next(2),
      I2 => read_addr_d1(2),
      I3 => read_nextgray(3),
      I4 => rd_write_next(3),
      O => Rd_Length_i00_out(3)
    );
\Rd_Length_i[3]_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DD4D"
    )
        port map (
      I0 => read_addr_d1(1),
      I1 => rd_write_next(1),
      I2 => read_addr_d1(0),
      I3 => rd_write_next(0),
      O => \Rd_Length_i[3]_i_2__0_n_0\
    );
\Rd_Length_i_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      CLR => rd_reset,
      D => \Rd_Length_i[0]_i_1__0_n_0\,
      Q => Rd_Length(0)
    );
\Rd_Length_i_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      CLR => rd_reset,
      D => Rd_Length_i00_out(1),
      Q => Rd_Length(1)
    );
\Rd_Length_i_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      CLR => rd_reset,
      D => Rd_Length_i00_out(2),
      Q => Rd_Length(2)
    );
\Rd_Length_i_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      CLR => rd_reset,
      D => Rd_Length_i00_out(3),
      Q => Rd_Length(3)
    );
\Wr_Length_i[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_addr_d1(0),
      I1 => wr_read_next(0),
      O => \Wr_Length_i[0]_i_1_n_0\
    );
\Wr_Length_i[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => wr_read_next(0),
      I1 => write_addr_d1(0),
      I2 => wr_read_next(1),
      I3 => write_addr_d1(1),
      O => Wr_Length_i01_out(1)
    );
\Wr_Length_i[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F04B0FBB0FB4F04"
    )
        port map (
      I0 => write_addr_d1(0),
      I1 => wr_read_next(0),
      I2 => write_addr_d1(1),
      I3 => wr_read_next(1),
      I4 => wr_read_next(2),
      I5 => write_addr_d1(2),
      O => Wr_Length_i01_out(2)
    );
\Wr_Length_i[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"718E8E71"
    )
        port map (
      I0 => \Wr_Length_i[3]_i_2_n_0\,
      I1 => write_addr_d1(2),
      I2 => wr_read_next(2),
      I3 => wr_read_next(3),
      I4 => write_nextgray(3),
      O => Wr_Length_i01_out(3)
    );
\Wr_Length_i[3]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DD4D"
    )
        port map (
      I0 => wr_read_next(1),
      I1 => write_addr_d1(1),
      I2 => wr_read_next(0),
      I3 => write_addr_d1(0),
      O => \Wr_Length_i[3]_i_2_n_0\
    );
\Wr_Length_i_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      CLR => wr_reset,
      D => \Wr_Length_i[0]_i_1_n_0\,
      Q => Wr_Length(0)
    );
\Wr_Length_i_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      CLR => wr_reset,
      D => Wr_Length_i01_out(1),
      Q => Wr_Length(1)
    );
\Wr_Length_i_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      CLR => wr_reset,
      D => Wr_Length_i01_out(2),
      Q => Wr_Length(2)
    );
\Wr_Length_i_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      CLR => wr_reset,
      D => Wr_Length_i01_out(3),
      Q => Wr_Length(3)
    );
empty_reg: unisim.vcomponents.FDPE
     port map (
      C => S1_AXI_ACLK,
      CE => empty_allow,
      D => emptyg,
      PRE => rd_reset,
      Q => empty
    );
emuxcyhigh_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9A95"
    )
        port map (
      I0 => \rd_write_addrgray_dx[1]_5\(3),
      I1 => p_2_in62_in,
      I2 => empty,
      I3 => read_nextgray(3),
      O => S06_out
    );
emuxcylow_CARRY4: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => emptyg,
      CO(2) => CI,
      CO(1) => emuxcyo_1,
      CO(0) => emuxcyo_0,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_emuxcylow_CARRY4_O_UNCONNECTED(3 downto 0),
      S(3) => S06_out,
      S(2) => S05_out,
      S(1) => S04_out,
      S(0) => S
    );
emuxcylow_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9A95"
    )
        port map (
      I0 => \rd_write_addrgray_dx[1]_5\(0),
      I1 => \read_addrgray_reg_n_0_[0]\,
      I2 => empty,
      I3 => read_nextgray(0),
      O => S
    );
\fmuxcyhigh_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9A95"
    )
        port map (
      I0 => \wr_read_lastgray_dx[1]_7\(3),
      I1 => write_addrgray(3),
      I2 => \^write_nextgray_reg[2]_0\,
      I3 => write_nextgray(3),
      O => S02_out
    );
fmuxcylow_CARRY4: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => fullg,
      CO(2) => fmuxcyo_2,
      CO(1) => fmuxcyo_1,
      CO(0) => fmuxcyo_0,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_fmuxcylow_CARRY4_O_UNCONNECTED(3 downto 0),
      S(3) => S02_out,
      S(2) => S01_out,
      S(1) => S00_out,
      S(0) => S0
    );
\fmuxcylow_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9A95"
    )
        port map (
      I0 => \wr_read_lastgray_dx[1]_7\(0),
      I1 => write_addrgray(0),
      I2 => \^write_nextgray_reg[2]_0\,
      I3 => write_nextgray(0),
      O => S0
    );
full_i_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88888A88"
    )
        port map (
      I0 => fullg,
      I1 => \^write_nextgray_reg[2]_0\,
      I2 => \^write_nextgray_reg[2]_0\,
      I3 => \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg\,
      I4 => Bus_RNW_reg_0,
      O => full_i_i_1_n_0
    );
full_i_reg: unisim.vcomponents.FDPE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => full_i_i_1_n_0,
      PRE => wr_reset,
      Q => \^write_nextgray_reg[2]_0\
    );
i_0: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \rd_write_addrgray_dx[2]_8\(3)
    );
i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \rd_write_addrgray_dx[2]_8\(2)
    );
i_10: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \wr_read_lastgray_dx[2]_10\(1)
    );
i_11: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \wr_read_lastgray_dx[2]_10\(0)
    );
i_12: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \wr_read_nextgray_dx[2]_11\(3)
    );
i_13: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \wr_read_nextgray_dx[2]_11\(2)
    );
i_14: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \wr_read_nextgray_dx[2]_11\(1)
    );
i_15: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \wr_read_nextgray_dx[2]_11\(0)
    );
i_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \rd_write_addrgray_dx[2]_8\(1)
    );
i_3: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \rd_write_addrgray_dx[2]_8\(0)
    );
i_4: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \rd_write_nextgray_dx[2]_9\(3)
    );
i_5: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \rd_write_nextgray_dx[2]_9\(2)
    );
i_6: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \rd_write_nextgray_dx[2]_9\(1)
    );
i_7: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \rd_write_nextgray_dx[2]_9\(0)
    );
i_8: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \wr_read_lastgray_dx[2]_10\(3)
    );
i_9: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => \wr_read_lastgray_dx[2]_10\(2)
    );
ram_mem_reg_0_15_0_5: unisim.vcomponents.RAM32M
     port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 0) => read_addr_next(3 downto 0),
      ADDRB(4) => '0',
      ADDRB(3 downto 0) => read_addr_next(3 downto 0),
      ADDRC(4) => '0',
      ADDRC(3 downto 0) => read_addr_next(3 downto 0),
      ADDRD(4) => '0',
      ADDRD(3) => \write_addr_reg_n_0_[3]\,
      ADDRD(2) => p_2_in17_in,
      ADDRD(1) => p_1_in16_in,
      ADDRD(0) => \write_addr_reg_n_0_[0]\,
      DIA(1 downto 0) => S0_AXI_WDATA(1 downto 0),
      DIB(1 downto 0) => S0_AXI_WDATA(3 downto 2),
      DIC(1 downto 0) => S0_AXI_WDATA(5 downto 4),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => DataOut0(1 downto 0),
      DOB(1 downto 0) => DataOut0(3 downto 2),
      DOC(1 downto 0) => DataOut0(5 downto 4),
      DOD(1 downto 0) => NLW_ram_mem_reg_0_15_0_5_DOD_UNCONNECTED(1 downto 0),
      WCLK => S0_AXI_ACLK,
      WE => \write_nextgray[2]_i_1__0_n_0\
    );
ram_mem_reg_0_15_0_5_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => p_1_in20_in,
      I1 => \read_addr_reg_n_0_[0]\,
      I2 => E(0),
      I3 => p_2_in22_in,
      I4 => \read_addr_reg_n_0_[3]\,
      O => read_addr_next(3)
    );
ram_mem_reg_0_15_0_5_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF40000000"
    )
        port map (
      I0 => empty,
      I1 => Bus_RNW_reg,
      I2 => \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg\,
      I3 => \read_addr_reg_n_0_[0]\,
      I4 => p_1_in20_in,
      I5 => p_2_in22_in,
      O => read_addr_next(2)
    );
ram_mem_reg_0_15_0_5_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF7F0080"
    )
        port map (
      I0 => \read_addr_reg_n_0_[0]\,
      I1 => \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg\,
      I2 => Bus_RNW_reg,
      I3 => empty,
      I4 => p_1_in20_in,
      O => read_addr_next(1)
    );
ram_mem_reg_0_15_0_5_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9AAA"
    )
        port map (
      I0 => \read_addr_reg_n_0_[0]\,
      I1 => empty,
      I2 => Bus_RNW_reg,
      I3 => \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg\,
      O => read_addr_next(0)
    );
ram_mem_reg_0_15_12_17: unisim.vcomponents.RAM32M
     port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 0) => read_addr_next(3 downto 0),
      ADDRB(4) => '0',
      ADDRB(3 downto 0) => read_addr_next(3 downto 0),
      ADDRC(4) => '0',
      ADDRC(3 downto 0) => read_addr_next(3 downto 0),
      ADDRD(4) => '0',
      ADDRD(3) => \write_addr_reg_n_0_[3]\,
      ADDRD(2) => p_2_in17_in,
      ADDRD(1) => p_1_in16_in,
      ADDRD(0) => \write_addr_reg_n_0_[0]\,
      DIA(1 downto 0) => S0_AXI_WDATA(13 downto 12),
      DIB(1 downto 0) => S0_AXI_WDATA(15 downto 14),
      DIC(1 downto 0) => S0_AXI_WDATA(17 downto 16),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => DataOut0(13 downto 12),
      DOB(1 downto 0) => DataOut0(15 downto 14),
      DOC(1 downto 0) => DataOut0(17 downto 16),
      DOD(1 downto 0) => NLW_ram_mem_reg_0_15_12_17_DOD_UNCONNECTED(1 downto 0),
      WCLK => S0_AXI_ACLK,
      WE => \write_nextgray[2]_i_1__0_n_0\
    );
ram_mem_reg_0_15_18_23: unisim.vcomponents.RAM32M
     port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 0) => read_addr_next(3 downto 0),
      ADDRB(4) => '0',
      ADDRB(3 downto 0) => read_addr_next(3 downto 0),
      ADDRC(4) => '0',
      ADDRC(3 downto 0) => read_addr_next(3 downto 0),
      ADDRD(4) => '0',
      ADDRD(3) => \write_addr_reg_n_0_[3]\,
      ADDRD(2) => p_2_in17_in,
      ADDRD(1) => p_1_in16_in,
      ADDRD(0) => \write_addr_reg_n_0_[0]\,
      DIA(1 downto 0) => S0_AXI_WDATA(19 downto 18),
      DIB(1 downto 0) => S0_AXI_WDATA(21 downto 20),
      DIC(1 downto 0) => S0_AXI_WDATA(23 downto 22),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => DataOut0(19 downto 18),
      DOB(1 downto 0) => DataOut0(21 downto 20),
      DOC(1 downto 0) => DataOut0(23 downto 22),
      DOD(1 downto 0) => NLW_ram_mem_reg_0_15_18_23_DOD_UNCONNECTED(1 downto 0),
      WCLK => S0_AXI_ACLK,
      WE => \write_nextgray[2]_i_1__0_n_0\
    );
ram_mem_reg_0_15_24_29: unisim.vcomponents.RAM32M
     port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 0) => read_addr_next(3 downto 0),
      ADDRB(4) => '0',
      ADDRB(3 downto 0) => read_addr_next(3 downto 0),
      ADDRC(4) => '0',
      ADDRC(3 downto 0) => read_addr_next(3 downto 0),
      ADDRD(4) => '0',
      ADDRD(3) => \write_addr_reg_n_0_[3]\,
      ADDRD(2) => p_2_in17_in,
      ADDRD(1) => p_1_in16_in,
      ADDRD(0) => \write_addr_reg_n_0_[0]\,
      DIA(1 downto 0) => S0_AXI_WDATA(25 downto 24),
      DIB(1 downto 0) => S0_AXI_WDATA(27 downto 26),
      DIC(1 downto 0) => S0_AXI_WDATA(29 downto 28),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => DataOut0(25 downto 24),
      DOB(1 downto 0) => DataOut0(27 downto 26),
      DOC(1 downto 0) => DataOut0(29 downto 28),
      DOD(1 downto 0) => NLW_ram_mem_reg_0_15_24_29_DOD_UNCONNECTED(1 downto 0),
      WCLK => S0_AXI_ACLK,
      WE => \write_nextgray[2]_i_1__0_n_0\
    );
ram_mem_reg_0_15_30_31: unisim.vcomponents.RAM32M
     port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 0) => read_addr_next(3 downto 0),
      ADDRB(4) => '0',
      ADDRB(3 downto 0) => read_addr_next(3 downto 0),
      ADDRC(4) => '0',
      ADDRC(3 downto 0) => read_addr_next(3 downto 0),
      ADDRD(4) => '0',
      ADDRD(3) => \write_addr_reg_n_0_[3]\,
      ADDRD(2) => p_2_in17_in,
      ADDRD(1) => p_1_in16_in,
      ADDRD(0) => \write_addr_reg_n_0_[0]\,
      DIA(1 downto 0) => S0_AXI_WDATA(31 downto 30),
      DIB(1 downto 0) => B"00",
      DIC(1 downto 0) => B"00",
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => DataOut0(31 downto 30),
      DOB(1 downto 0) => NLW_ram_mem_reg_0_15_30_31_DOB_UNCONNECTED(1 downto 0),
      DOC(1 downto 0) => NLW_ram_mem_reg_0_15_30_31_DOC_UNCONNECTED(1 downto 0),
      DOD(1 downto 0) => NLW_ram_mem_reg_0_15_30_31_DOD_UNCONNECTED(1 downto 0),
      WCLK => S0_AXI_ACLK,
      WE => \write_nextgray[2]_i_1__0_n_0\
    );
ram_mem_reg_0_15_6_11: unisim.vcomponents.RAM32M
     port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 0) => read_addr_next(3 downto 0),
      ADDRB(4) => '0',
      ADDRB(3 downto 0) => read_addr_next(3 downto 0),
      ADDRC(4) => '0',
      ADDRC(3 downto 0) => read_addr_next(3 downto 0),
      ADDRD(4) => '0',
      ADDRD(3) => \write_addr_reg_n_0_[3]\,
      ADDRD(2) => p_2_in17_in,
      ADDRD(1) => p_1_in16_in,
      ADDRD(0) => \write_addr_reg_n_0_[0]\,
      DIA(1 downto 0) => S0_AXI_WDATA(7 downto 6),
      DIB(1 downto 0) => S0_AXI_WDATA(9 downto 8),
      DIC(1 downto 0) => S0_AXI_WDATA(11 downto 10),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => DataOut0(7 downto 6),
      DOB(1 downto 0) => DataOut0(9 downto 8),
      DOC(1 downto 0) => DataOut0(11 downto 10),
      DOD(1 downto 0) => NLW_ram_mem_reg_0_15_6_11_DOD_UNCONNECTED(1 downto 0),
      WCLK => S0_AXI_ACLK,
      WE => \write_nextgray[2]_i_1__0_n_0\
    );
rd_rst_meta_inst: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => '0',
      PRE => \^rd_rst_meta_inst_0\,
      Q => rd_meta_reset
    );
rd_rst_meta_inst_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"DF"
    )
        port map (
      I0 => S0_AXI_ARESETN,
      I1 => SYS_Rst,
      I2 => S1_AXI_ARESETN,
      O => \^rd_rst_meta_inst_0\
    );
rd_rst_sync_inst: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => rd_meta_reset,
      PRE => \^rd_rst_meta_inst_0\,
      Q => rd_reset
    );
\rd_write_addrgray_dx_reg[0][0]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => write_addrgray(0),
      Q => \rd_write_addrgray_dx[0]_4\(0),
      R => '0'
    );
\rd_write_addrgray_dx_reg[0][1]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => write_addrgray(1),
      Q => \rd_write_addrgray_dx[0]_4\(1),
      R => '0'
    );
\rd_write_addrgray_dx_reg[0][2]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => write_addrgray(2),
      Q => \rd_write_addrgray_dx[0]_4\(2),
      R => '0'
    );
\rd_write_addrgray_dx_reg[0][3]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => write_addrgray(3),
      Q => \rd_write_addrgray_dx[0]_4\(3),
      R => '0'
    );
\rd_write_addrgray_dx_reg[1][0]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \rd_write_addrgray_dx[0]_4\(0),
      Q => \rd_write_addrgray_dx[1]_5\(0),
      R => '0'
    );
\rd_write_addrgray_dx_reg[1][1]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \rd_write_addrgray_dx[0]_4\(1),
      Q => \rd_write_addrgray_dx[1]_5\(1),
      R => '0'
    );
\rd_write_addrgray_dx_reg[1][2]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \rd_write_addrgray_dx[0]_4\(2),
      Q => \rd_write_addrgray_dx[1]_5\(2),
      R => '0'
    );
\rd_write_addrgray_dx_reg[1][3]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \rd_write_addrgray_dx[0]_4\(3),
      Q => \rd_write_addrgray_dx[1]_5\(3),
      R => '0'
    );
\rd_write_next[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \rd_write_nextgray_dx[1]_3\(2),
      I1 => \rd_write_nextgray_dx[1]_3\(3),
      I2 => \rd_write_nextgray_dx[1]_3\(0),
      I3 => \rd_write_nextgray_dx[1]_3\(1),
      O => Gray2Bin(0)
    );
\rd_write_next[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \rd_write_nextgray_dx[1]_3\(3),
      I1 => \rd_write_nextgray_dx[1]_3\(1),
      I2 => \rd_write_nextgray_dx[1]_3\(2),
      O => Gray2Bin(1)
    );
\rd_write_next[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \rd_write_nextgray_dx[1]_3\(3),
      I1 => \rd_write_nextgray_dx[1]_3\(2),
      O => Gray2Bin(2)
    );
\rd_write_next_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => Gray2Bin(0),
      Q => rd_write_next(0),
      R => '0'
    );
\rd_write_next_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => Gray2Bin(1),
      Q => rd_write_next(1),
      R => '0'
    );
\rd_write_next_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => Gray2Bin(2),
      Q => rd_write_next(2),
      R => '0'
    );
\rd_write_next_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \rd_write_nextgray_dx[1]_3\(3),
      Q => rd_write_next(3),
      R => '0'
    );
\rd_write_nextgray_dx_reg[0][0]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => write_nextgray(0),
      Q => \rd_write_nextgray_dx[0]_2\(0),
      R => '0'
    );
\rd_write_nextgray_dx_reg[0][1]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => write_nextgray(1),
      Q => \rd_write_nextgray_dx[0]_2\(1),
      R => '0'
    );
\rd_write_nextgray_dx_reg[0][2]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => write_nextgray(2),
      Q => \rd_write_nextgray_dx[0]_2\(2),
      R => '0'
    );
\rd_write_nextgray_dx_reg[0][3]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => write_nextgray(3),
      Q => \rd_write_nextgray_dx[0]_2\(3),
      R => '0'
    );
\rd_write_nextgray_dx_reg[1][0]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \rd_write_nextgray_dx[0]_2\(0),
      Q => \rd_write_nextgray_dx[1]_3\(0),
      R => '0'
    );
\rd_write_nextgray_dx_reg[1][1]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \rd_write_nextgray_dx[0]_2\(1),
      Q => \rd_write_nextgray_dx[1]_3\(1),
      R => '0'
    );
\rd_write_nextgray_dx_reg[1][2]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \rd_write_nextgray_dx[0]_2\(2),
      Q => \rd_write_nextgray_dx[1]_3\(2),
      R => '0'
    );
\rd_write_nextgray_dx_reg[1][3]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \rd_write_nextgray_dx[0]_2\(3),
      Q => \rd_write_nextgray_dx[1]_3\(3),
      R => '0'
    );
\read_addr_d1_reg[0]\: unisim.vcomponents.FDPE
     port map (
      C => S1_AXI_ACLK,
      CE => E(0),
      D => \read_addr_reg_n_0_[0]\,
      PRE => rd_reset,
      Q => read_addr_d1(0)
    );
\read_addr_d1_reg[1]\: unisim.vcomponents.FDPE
     port map (
      C => S1_AXI_ACLK,
      CE => E(0),
      D => p_1_in20_in,
      PRE => rd_reset,
      Q => read_addr_d1(1)
    );
\read_addr_d1_reg[2]\: unisim.vcomponents.FDPE
     port map (
      C => S1_AXI_ACLK,
      CE => E(0),
      D => p_2_in22_in,
      PRE => rd_reset,
      Q => read_addr_d1(2)
    );
\read_addr_d1_reg[3]\: unisim.vcomponents.FDPE
     port map (
      C => S1_AXI_ACLK,
      CE => E(0),
      D => \read_addr_reg_n_0_[3]\,
      PRE => rd_reset,
      Q => read_nextgray(3)
    );
\read_addr_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      CLR => rd_reset,
      D => read_addr_next(0),
      Q => \read_addr_reg_n_0_[0]\
    );
\read_addr_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      CLR => rd_reset,
      D => read_addr_next(1),
      Q => p_1_in20_in
    );
\read_addr_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      CLR => rd_reset,
      D => read_addr_next(2),
      Q => p_2_in22_in
    );
\read_addr_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      CLR => rd_reset,
      D => read_addr_next(3),
      Q => \read_addr_reg_n_0_[3]\
    );
\read_addrgray_reg[0]\: unisim.vcomponents.FDPE
     port map (
      C => S1_AXI_ACLK,
      CE => E(0),
      D => read_nextgray(0),
      PRE => rd_reset,
      Q => \read_addrgray_reg_n_0_[0]\
    );
\read_addrgray_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => S1_AXI_ACLK,
      CE => E(0),
      CLR => rd_reset,
      D => read_nextgray(1),
      Q => p_2_in52_in
    );
\read_addrgray_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => S1_AXI_ACLK,
      CE => E(0),
      CLR => rd_reset,
      D => read_nextgray(2),
      Q => p_2_in57_in
    );
\read_addrgray_reg[3]\: unisim.vcomponents.FDPE
     port map (
      C => S1_AXI_ACLK,
      CE => E(0),
      D => read_nextgray(3),
      PRE => rd_reset,
      Q => p_2_in62_in
    );
\read_lastgray_reg[0]\: unisim.vcomponents.FDPE
     port map (
      C => S1_AXI_ACLK,
      CE => E(0),
      D => \read_addrgray_reg_n_0_[0]\,
      PRE => rd_reset,
      Q => read_lastgray(0)
    );
\read_lastgray_reg[1]\: unisim.vcomponents.FDPE
     port map (
      C => S1_AXI_ACLK,
      CE => E(0),
      D => p_2_in52_in,
      PRE => rd_reset,
      Q => read_lastgray(1)
    );
\read_lastgray_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => S1_AXI_ACLK,
      CE => E(0),
      CLR => rd_reset,
      D => p_2_in57_in,
      Q => read_lastgray(2)
    );
\read_lastgray_reg[3]\: unisim.vcomponents.FDPE
     port map (
      C => S1_AXI_ACLK,
      CE => E(0),
      D => p_2_in62_in,
      PRE => rd_reset,
      Q => read_lastgray(3)
    );
\read_nextgray[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_1_in20_in,
      I1 => \read_addr_reg_n_0_[0]\,
      O => Bin2Gray(0)
    );
\read_nextgray[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_2_in22_in,
      I1 => p_1_in20_in,
      O => Bin2Gray(1)
    );
\read_nextgray[2]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_addr_reg_n_0_[3]\,
      I1 => p_2_in22_in,
      O => Bin2Gray(2)
    );
\read_nextgray_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => S1_AXI_ACLK,
      CE => E(0),
      CLR => rd_reset,
      D => Bin2Gray(0),
      Q => read_nextgray(0)
    );
\read_nextgray_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => S1_AXI_ACLK,
      CE => E(0),
      CLR => rd_reset,
      D => Bin2Gray(1),
      Q => read_nextgray(1)
    );
\read_nextgray_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => S1_AXI_ACLK,
      CE => E(0),
      CLR => rd_reset,
      D => Bin2Gray(2),
      Q => read_nextgray(2)
    );
rit_detect_d1_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => rit_detect_d1_i_2_n_0,
      I1 => \rit_register_reg[0]\(3),
      I2 => Rd_Length(3),
      O => rit_detect_d0
    );
rit_detect_d1_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2F02FFFF00002F02"
    )
        port map (
      I0 => Rd_Length(0),
      I1 => \rit_register_reg[0]\(0),
      I2 => \rit_register_reg[0]\(1),
      I3 => Rd_Length(1),
      I4 => \rit_register_reg[0]\(2),
      I5 => Rd_Length(2),
      O => rit_detect_d1_i_2_n_0
    );
\sit_detect_d1_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \sit_detect_d1_i_2__0_n_0\,
      I1 => Wr_Length(3),
      I2 => Q(3),
      O => sit_detect_d0
    );
\sit_detect_d1_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DF0DFFFF0000DF0D"
    )
        port map (
      I0 => Wr_Length(0),
      I1 => Q(0),
      I2 => Wr_Length(1),
      I3 => Q(1),
      I4 => Wr_Length(2),
      I5 => Q(2),
      O => \sit_detect_d1_i_2__0_n_0\
    );
\wr_read_lastgray_dx_reg[0][0]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => read_lastgray(0),
      Q => \wr_read_lastgray_dx[0]_6\(0),
      R => '0'
    );
\wr_read_lastgray_dx_reg[0][1]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => read_lastgray(1),
      Q => \wr_read_lastgray_dx[0]_6\(1),
      R => '0'
    );
\wr_read_lastgray_dx_reg[0][2]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => read_lastgray(2),
      Q => \wr_read_lastgray_dx[0]_6\(2),
      R => '0'
    );
\wr_read_lastgray_dx_reg[0][3]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => read_lastgray(3),
      Q => \wr_read_lastgray_dx[0]_6\(3),
      R => '0'
    );
\wr_read_lastgray_dx_reg[1][0]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \wr_read_lastgray_dx[0]_6\(0),
      Q => \wr_read_lastgray_dx[1]_7\(0),
      R => '0'
    );
\wr_read_lastgray_dx_reg[1][1]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \wr_read_lastgray_dx[0]_6\(1),
      Q => \wr_read_lastgray_dx[1]_7\(1),
      R => '0'
    );
\wr_read_lastgray_dx_reg[1][2]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \wr_read_lastgray_dx[0]_6\(2),
      Q => \wr_read_lastgray_dx[1]_7\(2),
      R => '0'
    );
\wr_read_lastgray_dx_reg[1][3]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \wr_read_lastgray_dx[0]_6\(3),
      Q => \wr_read_lastgray_dx[1]_7\(3),
      R => '0'
    );
\wr_read_next[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \wr_read_nextgray_dx[1]_1\(2),
      I1 => \wr_read_nextgray_dx[1]_1\(3),
      I2 => \wr_read_nextgray_dx[1]_1\(0),
      I3 => \wr_read_nextgray_dx[1]_1\(1),
      O => \wr_read_next[0]_i_1_n_0\
    );
\wr_read_next[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \wr_read_nextgray_dx[1]_1\(3),
      I1 => \wr_read_nextgray_dx[1]_1\(1),
      I2 => \wr_read_nextgray_dx[1]_1\(2),
      O => \wr_read_next[1]_i_1_n_0\
    );
\wr_read_next[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \wr_read_nextgray_dx[1]_1\(3),
      I1 => \wr_read_nextgray_dx[1]_1\(2),
      O => \wr_read_next[2]_i_1_n_0\
    );
\wr_read_next_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \wr_read_next[0]_i_1_n_0\,
      Q => wr_read_next(0),
      R => '0'
    );
\wr_read_next_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \wr_read_next[1]_i_1_n_0\,
      Q => wr_read_next(1),
      R => '0'
    );
\wr_read_next_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \wr_read_next[2]_i_1_n_0\,
      Q => wr_read_next(2),
      R => '0'
    );
\wr_read_next_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \wr_read_nextgray_dx[1]_1\(3),
      Q => wr_read_next(3),
      R => '0'
    );
\wr_read_nextgray_dx_reg[0][0]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => read_nextgray(0),
      Q => \wr_read_nextgray_dx[0]_0\(0),
      R => '0'
    );
\wr_read_nextgray_dx_reg[0][1]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => read_nextgray(1),
      Q => \wr_read_nextgray_dx[0]_0\(1),
      R => '0'
    );
\wr_read_nextgray_dx_reg[0][2]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => read_nextgray(2),
      Q => \wr_read_nextgray_dx[0]_0\(2),
      R => '0'
    );
\wr_read_nextgray_dx_reg[0][3]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => read_nextgray(3),
      Q => \wr_read_nextgray_dx[0]_0\(3),
      R => '0'
    );
\wr_read_nextgray_dx_reg[1][0]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \wr_read_nextgray_dx[0]_0\(0),
      Q => \wr_read_nextgray_dx[1]_1\(0),
      R => '0'
    );
\wr_read_nextgray_dx_reg[1][1]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \wr_read_nextgray_dx[0]_0\(1),
      Q => \wr_read_nextgray_dx[1]_1\(1),
      R => '0'
    );
\wr_read_nextgray_dx_reg[1][2]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \wr_read_nextgray_dx[0]_0\(2),
      Q => \wr_read_nextgray_dx[1]_1\(2),
      R => '0'
    );
\wr_read_nextgray_dx_reg[1][3]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \wr_read_nextgray_dx[0]_0\(3),
      Q => \wr_read_nextgray_dx[1]_1\(3),
      R => '0'
    );
wr_rst_meta_inst: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => '0',
      PRE => \^rd_rst_meta_inst_0\,
      Q => wr_meta_reset
    );
wr_rst_sync_inst: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => wr_meta_reset,
      PRE => \^rd_rst_meta_inst_0\,
      Q => wr_reset
    );
\write_addr[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \write_addr_reg_n_0_[0]\,
      O => plusOp(0)
    );
\write_addr[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \write_addr_reg_n_0_[0]\,
      I1 => p_1_in16_in,
      O => plusOp(1)
    );
\write_addr[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \write_addr_reg_n_0_[0]\,
      I1 => p_1_in16_in,
      I2 => p_2_in17_in,
      O => plusOp(2)
    );
\write_addr[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => p_1_in16_in,
      I1 => \write_addr_reg_n_0_[0]\,
      I2 => p_2_in17_in,
      I3 => \write_addr_reg_n_0_[3]\,
      O => plusOp(3)
    );
\write_addr_d1_reg[0]\: unisim.vcomponents.FDPE
     port map (
      C => S0_AXI_ACLK,
      CE => \write_nextgray[2]_i_1__0_n_0\,
      D => \write_addr_reg_n_0_[0]\,
      PRE => wr_reset,
      Q => write_addr_d1(0)
    );
\write_addr_d1_reg[1]\: unisim.vcomponents.FDPE
     port map (
      C => S0_AXI_ACLK,
      CE => \write_nextgray[2]_i_1__0_n_0\,
      D => p_1_in16_in,
      PRE => wr_reset,
      Q => write_addr_d1(1)
    );
\write_addr_d1_reg[2]\: unisim.vcomponents.FDPE
     port map (
      C => S0_AXI_ACLK,
      CE => \write_nextgray[2]_i_1__0_n_0\,
      D => p_2_in17_in,
      PRE => wr_reset,
      Q => write_addr_d1(2)
    );
\write_addr_d1_reg[3]\: unisim.vcomponents.FDPE
     port map (
      C => S0_AXI_ACLK,
      CE => \write_nextgray[2]_i_1__0_n_0\,
      D => \write_addr_reg_n_0_[3]\,
      PRE => wr_reset,
      Q => write_nextgray(3)
    );
\write_addr_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => S0_AXI_ACLK,
      CE => \write_nextgray[2]_i_1__0_n_0\,
      CLR => wr_reset,
      D => plusOp(0),
      Q => \write_addr_reg_n_0_[0]\
    );
\write_addr_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => S0_AXI_ACLK,
      CE => \write_nextgray[2]_i_1__0_n_0\,
      CLR => wr_reset,
      D => plusOp(1),
      Q => p_1_in16_in
    );
\write_addr_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => S0_AXI_ACLK,
      CE => \write_nextgray[2]_i_1__0_n_0\,
      CLR => wr_reset,
      D => plusOp(2),
      Q => p_2_in17_in
    );
\write_addr_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => S0_AXI_ACLK,
      CE => \write_nextgray[2]_i_1__0_n_0\,
      CLR => wr_reset,
      D => plusOp(3),
      Q => \write_addr_reg_n_0_[3]\
    );
\write_addrgray_reg[0]\: unisim.vcomponents.FDPE
     port map (
      C => S0_AXI_ACLK,
      CE => \write_nextgray[2]_i_1__0_n_0\,
      D => write_nextgray(0),
      PRE => wr_reset,
      Q => write_addrgray(0)
    );
\write_addrgray_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => S0_AXI_ACLK,
      CE => \write_nextgray[2]_i_1__0_n_0\,
      CLR => wr_reset,
      D => write_nextgray(1),
      Q => write_addrgray(1)
    );
\write_addrgray_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => S0_AXI_ACLK,
      CE => \write_nextgray[2]_i_1__0_n_0\,
      CLR => wr_reset,
      D => write_nextgray(2),
      Q => write_addrgray(2)
    );
\write_addrgray_reg[3]\: unisim.vcomponents.FDPE
     port map (
      C => S0_AXI_ACLK,
      CE => \write_nextgray[2]_i_1__0_n_0\,
      D => write_nextgray(3),
      PRE => wr_reset,
      Q => write_addrgray(3)
    );
\write_nextgray[0]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_1_in16_in,
      I1 => \write_addr_reg_n_0_[0]\,
      O => \write_nextgray[0]_i_1__0_n_0\
    );
\write_nextgray[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_2_in17_in,
      I1 => p_1_in16_in,
      O => \write_nextgray[1]_i_1__0_n_0\
    );
\write_nextgray[2]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => \^write_nextgray_reg[2]_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg\,
      I2 => Bus_RNW_reg_0,
      I3 => \^write_nextgray_reg[2]_0\,
      O => \write_nextgray[2]_i_1__0_n_0\
    );
\write_nextgray[2]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \write_addr_reg_n_0_[3]\,
      I1 => p_2_in17_in,
      O => \write_nextgray[2]_i_2__0_n_0\
    );
\write_nextgray_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => S0_AXI_ACLK,
      CE => \write_nextgray[2]_i_1__0_n_0\,
      CLR => wr_reset,
      D => \write_nextgray[0]_i_1__0_n_0\,
      Q => write_nextgray(0)
    );
\write_nextgray_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => S0_AXI_ACLK,
      CE => \write_nextgray[2]_i_1__0_n_0\,
      CLR => wr_reset,
      D => \write_nextgray[1]_i_1__0_n_0\,
      Q => write_nextgray(1)
    );
\write_nextgray_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => S0_AXI_ACLK,
      CE => \write_nextgray[2]_i_1__0_n_0\,
      CLR => wr_reset,
      D => \write_nextgray[2]_i_2__0_n_0\,
      Q => write_nextgray(2)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_mailbox_0_0_pselect_f is
  port (
    \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0]\ : out STD_LOGIC;
    Q : in STD_LOGIC;
    \bus2ip_addr_i_reg[5]\ : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
end design_1_mailbox_0_0_pselect_f;

architecture STRUCTURE of design_1_mailbox_0_0_pselect_f is
begin
CS: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000002"
    )
        port map (
      I0 => Q,
      I1 => \bus2ip_addr_i_reg[5]\(2),
      I2 => \bus2ip_addr_i_reg[5]\(1),
      I3 => \bus2ip_addr_i_reg[5]\(3),
      I4 => \bus2ip_addr_i_reg[5]\(0),
      O => \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_mailbox_0_0_pselect_f_2 is
  port (
    \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0]\ : out STD_LOGIC;
    Q : in STD_LOGIC;
    \bus2ip_addr_i_reg[5]\ : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_mailbox_0_0_pselect_f_2 : entity is "pselect_f";
end design_1_mailbox_0_0_pselect_f_2;

architecture STRUCTURE of design_1_mailbox_0_0_pselect_f_2 is
begin
CS: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000002"
    )
        port map (
      I0 => Q,
      I1 => \bus2ip_addr_i_reg[5]\(2),
      I2 => \bus2ip_addr_i_reg[5]\(1),
      I3 => \bus2ip_addr_i_reg[5]\(3),
      I4 => \bus2ip_addr_i_reg[5]\(0),
      O => \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_mailbox_0_0_pselect_f__parameterized1\ is
  port (
    ce_expnd_i_8 : out STD_LOGIC;
    \bus2ip_addr_i_reg[5]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Q : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_mailbox_0_0_pselect_f__parameterized1\ : entity is "pselect_f";
end \design_1_mailbox_0_0_pselect_f__parameterized1\;

architecture STRUCTURE of \design_1_mailbox_0_0_pselect_f__parameterized1\ is
begin
CS: unisim.vcomponents.LUT5
    generic map(
      INIT => X"01000000"
    )
        port map (
      I0 => \bus2ip_addr_i_reg[5]\(3),
      I1 => \bus2ip_addr_i_reg[5]\(2),
      I2 => \bus2ip_addr_i_reg[5]\(0),
      I3 => \bus2ip_addr_i_reg[5]\(1),
      I4 => Q,
      O => ce_expnd_i_8
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_mailbox_0_0_pselect_f__parameterized11\ is
  port (
    ce_expnd_i_8 : out STD_LOGIC;
    \bus2ip_addr_i_reg[5]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Q : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_mailbox_0_0_pselect_f__parameterized11\ : entity is "pselect_f";
end \design_1_mailbox_0_0_pselect_f__parameterized11\;

architecture STRUCTURE of \design_1_mailbox_0_0_pselect_f__parameterized11\ is
begin
CS: unisim.vcomponents.LUT5
    generic map(
      INIT => X"01000000"
    )
        port map (
      I0 => \bus2ip_addr_i_reg[5]\(3),
      I1 => \bus2ip_addr_i_reg[5]\(2),
      I2 => \bus2ip_addr_i_reg[5]\(0),
      I3 => \bus2ip_addr_i_reg[5]\(1),
      I4 => Q,
      O => ce_expnd_i_8
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_mailbox_0_0_pselect_f__parameterized13\ is
  port (
    ce_expnd_i_6 : out STD_LOGIC;
    \bus2ip_addr_i_reg[5]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Q : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_mailbox_0_0_pselect_f__parameterized13\ : entity is "pselect_f";
end \design_1_mailbox_0_0_pselect_f__parameterized13\;

architecture STRUCTURE of \design_1_mailbox_0_0_pselect_f__parameterized13\ is
begin
CS: unisim.vcomponents.LUT5
    generic map(
      INIT => X"01000000"
    )
        port map (
      I0 => \bus2ip_addr_i_reg[5]\(3),
      I1 => \bus2ip_addr_i_reg[5]\(1),
      I2 => \bus2ip_addr_i_reg[5]\(0),
      I3 => \bus2ip_addr_i_reg[5]\(2),
      I4 => Q,
      O => ce_expnd_i_6
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_mailbox_0_0_pselect_f__parameterized14\ is
  port (
    ce_expnd_i_5 : out STD_LOGIC;
    \bus2ip_addr_i_reg[5]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Q : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_mailbox_0_0_pselect_f__parameterized14\ : entity is "pselect_f";
end \design_1_mailbox_0_0_pselect_f__parameterized14\;

architecture STRUCTURE of \design_1_mailbox_0_0_pselect_f__parameterized14\ is
begin
CS: unisim.vcomponents.LUT5
    generic map(
      INIT => X"10000000"
    )
        port map (
      I0 => \bus2ip_addr_i_reg[5]\(3),
      I1 => \bus2ip_addr_i_reg[5]\(1),
      I2 => Q,
      I3 => \bus2ip_addr_i_reg[5]\(0),
      I4 => \bus2ip_addr_i_reg[5]\(2),
      O => ce_expnd_i_5
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_mailbox_0_0_pselect_f__parameterized16\ is
  port (
    ce_expnd_i_3 : out STD_LOGIC;
    \bus2ip_addr_i_reg[5]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Q : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_mailbox_0_0_pselect_f__parameterized16\ : entity is "pselect_f";
end \design_1_mailbox_0_0_pselect_f__parameterized16\;

architecture STRUCTURE of \design_1_mailbox_0_0_pselect_f__parameterized16\ is
begin
CS: unisim.vcomponents.LUT5
    generic map(
      INIT => X"40000000"
    )
        port map (
      I0 => \bus2ip_addr_i_reg[5]\(3),
      I1 => Q,
      I2 => \bus2ip_addr_i_reg[5]\(2),
      I3 => \bus2ip_addr_i_reg[5]\(0),
      I4 => \bus2ip_addr_i_reg[5]\(1),
      O => ce_expnd_i_3
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_mailbox_0_0_pselect_f__parameterized19\ is
  port (
    ce_expnd_i_0 : out STD_LOGIC;
    \bus2ip_addr_i_reg[5]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Q : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_mailbox_0_0_pselect_f__parameterized19\ : entity is "pselect_f";
end \design_1_mailbox_0_0_pselect_f__parameterized19\;

architecture STRUCTURE of \design_1_mailbox_0_0_pselect_f__parameterized19\ is
begin
CS: unisim.vcomponents.LUT5
    generic map(
      INIT => X"10000000"
    )
        port map (
      I0 => \bus2ip_addr_i_reg[5]\(2),
      I1 => \bus2ip_addr_i_reg[5]\(0),
      I2 => \bus2ip_addr_i_reg[5]\(3),
      I3 => \bus2ip_addr_i_reg[5]\(1),
      I4 => Q,
      O => ce_expnd_i_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_mailbox_0_0_pselect_f__parameterized3\ is
  port (
    ce_expnd_i_6 : out STD_LOGIC;
    \bus2ip_addr_i_reg[5]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Q : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_mailbox_0_0_pselect_f__parameterized3\ : entity is "pselect_f";
end \design_1_mailbox_0_0_pselect_f__parameterized3\;

architecture STRUCTURE of \design_1_mailbox_0_0_pselect_f__parameterized3\ is
begin
CS: unisim.vcomponents.LUT5
    generic map(
      INIT => X"01000000"
    )
        port map (
      I0 => \bus2ip_addr_i_reg[5]\(3),
      I1 => \bus2ip_addr_i_reg[5]\(1),
      I2 => \bus2ip_addr_i_reg[5]\(0),
      I3 => \bus2ip_addr_i_reg[5]\(2),
      I4 => Q,
      O => ce_expnd_i_6
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_mailbox_0_0_pselect_f__parameterized4\ is
  port (
    ce_expnd_i_5 : out STD_LOGIC;
    \bus2ip_addr_i_reg[5]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Q : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_mailbox_0_0_pselect_f__parameterized4\ : entity is "pselect_f";
end \design_1_mailbox_0_0_pselect_f__parameterized4\;

architecture STRUCTURE of \design_1_mailbox_0_0_pselect_f__parameterized4\ is
begin
CS: unisim.vcomponents.LUT5
    generic map(
      INIT => X"10000000"
    )
        port map (
      I0 => \bus2ip_addr_i_reg[5]\(3),
      I1 => \bus2ip_addr_i_reg[5]\(1),
      I2 => Q,
      I3 => \bus2ip_addr_i_reg[5]\(0),
      I4 => \bus2ip_addr_i_reg[5]\(2),
      O => ce_expnd_i_5
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_mailbox_0_0_pselect_f__parameterized6\ is
  port (
    ce_expnd_i_3 : out STD_LOGIC;
    \bus2ip_addr_i_reg[5]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Q : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_mailbox_0_0_pselect_f__parameterized6\ : entity is "pselect_f";
end \design_1_mailbox_0_0_pselect_f__parameterized6\;

architecture STRUCTURE of \design_1_mailbox_0_0_pselect_f__parameterized6\ is
begin
CS: unisim.vcomponents.LUT5
    generic map(
      INIT => X"40000000"
    )
        port map (
      I0 => \bus2ip_addr_i_reg[5]\(3),
      I1 => Q,
      I2 => \bus2ip_addr_i_reg[5]\(2),
      I3 => \bus2ip_addr_i_reg[5]\(0),
      I4 => \bus2ip_addr_i_reg[5]\(1),
      O => ce_expnd_i_3
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_mailbox_0_0_pselect_f__parameterized9\ is
  port (
    ce_expnd_i_0 : out STD_LOGIC;
    \bus2ip_addr_i_reg[5]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Q : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_mailbox_0_0_pselect_f__parameterized9\ : entity is "pselect_f";
end \design_1_mailbox_0_0_pselect_f__parameterized9\;

architecture STRUCTURE of \design_1_mailbox_0_0_pselect_f__parameterized9\ is
begin
CS: unisim.vcomponents.LUT5
    generic map(
      INIT => X"10000000"
    )
        port map (
      I0 => \bus2ip_addr_i_reg[5]\(2),
      I1 => \bus2ip_addr_i_reg[5]\(0),
      I2 => \bus2ip_addr_i_reg[5]\(3),
      I3 => \bus2ip_addr_i_reg[5]\(1),
      I4 => Q,
      O => ce_expnd_i_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_mailbox_0_0_address_decoder is
  port (
    \s_axi_rdata_i_reg[31]\ : out STD_LOGIC;
    \is_register_reg[2]\ : out STD_LOGIC;
    \read_nextgray_reg[2]\ : out STD_LOGIC;
    write_fsl_error_d1_reg : out STD_LOGIC;
    Bus_RNW_reg_reg_0 : out STD_LOGIC;
    empty_allow : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S0_AXI_ARREADY : out STD_LOGIC;
    error_detect : out STD_LOGIC;
    read_fsl_error : out STD_LOGIC;
    write_fsl_error : out STD_LOGIC;
    \s_axi_rdata_i_reg[31]_0\ : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S0_AXI_WREADY : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \sit_register_reg[3]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_rvalid_i_reg : out STD_LOGIC;
    s_axi_bvalid_i_reg : out STD_LOGIC;
    full_error_reg : out STD_LOGIC;
    empty_error_reg : out STD_LOGIC;
    \read_nextgray_reg[2]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC;
    S0_AXI_ACLK : in STD_LOGIC;
    \out\ : in STD_LOGIC;
    \state1__2\ : in STD_LOGIC;
    S0_AXI_ARVALID_0 : in STD_LOGIC;
    \state_reg[1]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    write_fsl_error_d1 : in STD_LOGIC;
    full_i_reg : in STD_LOGIC;
    read_fsl_error_d1 : in STD_LOGIC;
    rit_detect_d0 : in STD_LOGIC;
    sit_detect_d0 : in STD_LOGIC;
    full_error : in STD_LOGIC;
    empty_error : in STD_LOGIC;
    is_read : in STD_LOGIC;
    S0_AXI_ARVALID : in STD_LOGIC;
    is_write_reg : in STD_LOGIC;
    \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4]\ : in STD_LOGIC_VECTOR ( 4 downto 0 );
    \sit_register_reg[0]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    p_6_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \rit_register_reg[0]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S0_AXI_RREADY : in STD_LOGIC;
    s_axi_rvalid_i_reg_0 : in STD_LOGIC;
    S0_AXI_BREADY : in STD_LOGIC;
    s_axi_bvalid_i_reg_0 : in STD_LOGIC;
    Rst : in STD_LOGIC;
    \DataOut_reg[31]\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ie_register : in STD_LOGIC_VECTOR ( 0 to 2 );
    \bus2ip_addr_i_reg[5]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    bus2ip_rnw_i : in STD_LOGIC
  );
end design_1_mailbox_0_0_address_decoder;

architecture STRUCTURE of design_1_mailbox_0_0_address_decoder is
  signal Bus_RNW_reg_i_1_n_0 : STD_LOGIC;
  signal \^bus_rnw_reg_reg_0\ : STD_LOGIC;
  signal \GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1__0_n_0\ : STD_LOGIC;
  signal \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\ : STD_LOGIC;
  signal \GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg\ : STD_LOGIC;
  signal \GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg\ : STD_LOGIC;
  signal \GEN_BKEND_CE_REGISTERS[6].ce_out_i[6]_i_1_n_0\ : STD_LOGIC;
  signal \GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg\ : STD_LOGIC;
  signal \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg\ : STD_LOGIC;
  signal \GEN_BKEND_CE_REGISTERS[8].ce_out_i[8]_i_1_n_0\ : STD_LOGIC;
  signal \GEN_BKEND_CE_REGISTERS[9].ce_out_i[9]_i_1_n_0\ : STD_LOGIC;
  signal \MEM_DECODE_GEN[0].PER_CE_GEN[0].MULTIPLE_CES_THIS_CS_GEN.CE_I_n_0\ : STD_LOGIC;
  signal \^s0_axi_arready\ : STD_LOGIC;
  signal S0_AXI_ARREADY_INST_0_i_1_n_0 : STD_LOGIC;
  signal S0_AXI_ARREADY_INST_0_i_2_n_0 : STD_LOGIC;
  signal S0_AXI_ARREADY_INST_0_i_3_n_0 : STD_LOGIC;
  signal S0_AXI_ARREADY_INST_0_i_4_n_0 : STD_LOGIC;
  signal \^s0_axi_wready\ : STD_LOGIC;
  signal ce_expnd_i_0 : STD_LOGIC;
  signal ce_expnd_i_3 : STD_LOGIC;
  signal ce_expnd_i_5 : STD_LOGIC;
  signal ce_expnd_i_6 : STD_LOGIC;
  signal ce_expnd_i_8 : STD_LOGIC;
  signal \eqOp__3\ : STD_LOGIC;
  signal \ip2bus_wrack__3\ : STD_LOGIC;
  signal \^is_register_reg[2]\ : STD_LOGIC;
  signal \^read_fsl_error\ : STD_LOGIC;
  signal \^read_nextgray_reg[2]\ : STD_LOGIC;
  signal \s_axi_rdata_i[0]_i_2__0_n_0\ : STD_LOGIC;
  signal \s_axi_rdata_i[0]_i_3__0_n_0\ : STD_LOGIC;
  signal \s_axi_rdata_i[1]_i_2__0_n_0\ : STD_LOGIC;
  signal \s_axi_rdata_i[1]_i_3__0_n_0\ : STD_LOGIC;
  signal \s_axi_rdata_i[1]_i_4__0_n_0\ : STD_LOGIC;
  signal \s_axi_rdata_i[1]_i_5__0_n_0\ : STD_LOGIC;
  signal \s_axi_rdata_i[2]_i_2__0_n_0\ : STD_LOGIC;
  signal \s_axi_rdata_i[2]_i_3__0_n_0\ : STD_LOGIC;
  signal \s_axi_rdata_i[2]_i_4__0_n_0\ : STD_LOGIC;
  signal \s_axi_rdata_i[2]_i_5__0_n_0\ : STD_LOGIC;
  signal \s_axi_rdata_i[2]_i_6__0_n_0\ : STD_LOGIC;
  signal \s_axi_rdata_i[31]_i_3__0_n_0\ : STD_LOGIC;
  signal \s_axi_rdata_i[3]_i_2__0_n_0\ : STD_LOGIC;
  signal \s_axi_rdata_i[3]_i_3__0_n_0\ : STD_LOGIC;
  signal \^s_axi_rdata_i_reg[31]\ : STD_LOGIC;
  signal \^write_fsl_error_d1_reg\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of Bus_RNW_reg_i_1 : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \GEN_BKEND_CE_REGISTERS[8].ce_out_i[8]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \GEN_BKEND_CE_REGISTERS[9].ce_out_i[9]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of S0_AXI_ARREADY_INST_0_i_1 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of S0_AXI_ARREADY_INST_0_i_2 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of S0_AXI_ARREADY_INST_0_i_3 : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of S0_AXI_ARREADY_INST_0_i_4 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \empty_i_1__0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \read_fsl_error_d1_i_1__0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \read_nextgray[2]_i_1__0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \rit_register[0]_i_1__0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \s_axi_rdata_i[1]_i_4__0\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \s_axi_rdata_i[1]_i_5__0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \s_axi_rdata_i[2]_i_4__0\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \s_axi_rdata_i[2]_i_5__0\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \s_axi_rdata_i[2]_i_6__0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \s_axi_rdata_i[3]_i_2__0\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \s_axi_rdata_i[3]_i_3__0\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \sit_register[0]_i_1__0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \write_fsl_error_d1_i_1__0\ : label is "soft_lutpair8";
begin
  Bus_RNW_reg_reg_0 <= \^bus_rnw_reg_reg_0\;
  S0_AXI_ARREADY <= \^s0_axi_arready\;
  S0_AXI_WREADY <= \^s0_axi_wready\;
  \is_register_reg[2]\ <= \^is_register_reg[2]\;
  read_fsl_error <= \^read_fsl_error\;
  \read_nextgray_reg[2]\ <= \^read_nextgray_reg[2]\;
  \s_axi_rdata_i_reg[31]\ <= \^s_axi_rdata_i_reg[31]\;
  write_fsl_error_d1_reg <= \^write_fsl_error_d1_reg\;
Bus_RNW_reg_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => bus2ip_rnw_i,
      I1 => Q,
      I2 => \^bus_rnw_reg_reg_0\,
      O => Bus_RNW_reg_i_1_n_0
    );
Bus_RNW_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => Bus_RNW_reg_i_1_n_0,
      Q => \^bus_rnw_reg_reg_0\,
      R => '0'
    );
\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => Q,
      D => \MEM_DECODE_GEN[0].PER_CE_GEN[0].MULTIPLE_CES_THIS_CS_GEN.CE_I_n_0\,
      Q => \^write_fsl_error_d1_reg\,
      R => \GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1__0_n_0\
    );
\GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \^s0_axi_wready\,
      I1 => \^s0_axi_arready\,
      I2 => Rst,
      O => \GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1__0_n_0\
    );
\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => Q,
      D => ce_expnd_i_0,
      Q => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      R => \GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1__0_n_0\
    );
\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => Q,
      D => ce_expnd_i_8,
      Q => \^read_nextgray_reg[2]\,
      R => \GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1__0_n_0\
    );
\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => Q,
      D => ce_expnd_i_6,
      Q => \GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg\,
      R => \GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1__0_n_0\
    );
\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => Q,
      D => ce_expnd_i_5,
      Q => \GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg\,
      R => \GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1__0_n_0\
    );
\GEN_BKEND_CE_REGISTERS[6].ce_out_i[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"10000000"
    )
        port map (
      I0 => \bus2ip_addr_i_reg[5]\(0),
      I1 => \bus2ip_addr_i_reg[5]\(3),
      I2 => Q,
      I3 => \bus2ip_addr_i_reg[5]\(1),
      I4 => \bus2ip_addr_i_reg[5]\(2),
      O => \GEN_BKEND_CE_REGISTERS[6].ce_out_i[6]_i_1_n_0\
    );
\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => Q,
      D => \GEN_BKEND_CE_REGISTERS[6].ce_out_i[6]_i_1_n_0\,
      Q => \GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg\,
      R => \GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1__0_n_0\
    );
\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => Q,
      D => ce_expnd_i_3,
      Q => \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg\,
      R => \GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1__0_n_0\
    );
\GEN_BKEND_CE_REGISTERS[8].ce_out_i[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"01000000"
    )
        port map (
      I0 => \bus2ip_addr_i_reg[5]\(1),
      I1 => \bus2ip_addr_i_reg[5]\(2),
      I2 => \bus2ip_addr_i_reg[5]\(0),
      I3 => Q,
      I4 => \bus2ip_addr_i_reg[5]\(3),
      O => \GEN_BKEND_CE_REGISTERS[8].ce_out_i[8]_i_1_n_0\
    );
\GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => Q,
      D => \GEN_BKEND_CE_REGISTERS[8].ce_out_i[8]_i_1_n_0\,
      Q => \^is_register_reg[2]\,
      R => \GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1__0_n_0\
    );
\GEN_BKEND_CE_REGISTERS[9].ce_out_i[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"10000000"
    )
        port map (
      I0 => \bus2ip_addr_i_reg[5]\(1),
      I1 => \bus2ip_addr_i_reg[5]\(2),
      I2 => \bus2ip_addr_i_reg[5]\(3),
      I3 => \bus2ip_addr_i_reg[5]\(0),
      I4 => Q,
      O => \GEN_BKEND_CE_REGISTERS[9].ce_out_i[9]_i_1_n_0\
    );
\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => Q,
      D => \GEN_BKEND_CE_REGISTERS[9].ce_out_i[9]_i_1_n_0\,
      Q => \^s_axi_rdata_i_reg[31]\,
      R => \GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1__0_n_0\
    );
\MEM_DECODE_GEN[0].PER_CE_GEN[0].MULTIPLE_CES_THIS_CS_GEN.CE_I\: entity work.design_1_mailbox_0_0_pselect_f_2
     port map (
      \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0]\ => \MEM_DECODE_GEN[0].PER_CE_GEN[0].MULTIPLE_CES_THIS_CS_GEN.CE_I_n_0\,
      Q => Q,
      \bus2ip_addr_i_reg[5]\(3 downto 0) => \bus2ip_addr_i_reg[5]\(3 downto 0)
    );
\MEM_DECODE_GEN[0].PER_CE_GEN[10].MULTIPLE_CES_THIS_CS_GEN.CE_I\: entity work.\design_1_mailbox_0_0_pselect_f__parameterized9\
     port map (
      Q => Q,
      \bus2ip_addr_i_reg[5]\(3 downto 0) => \bus2ip_addr_i_reg[5]\(3 downto 0),
      ce_expnd_i_0 => ce_expnd_i_0
    );
\MEM_DECODE_GEN[0].PER_CE_GEN[2].MULTIPLE_CES_THIS_CS_GEN.CE_I\: entity work.\design_1_mailbox_0_0_pselect_f__parameterized1\
     port map (
      Q => Q,
      \bus2ip_addr_i_reg[5]\(3 downto 0) => \bus2ip_addr_i_reg[5]\(3 downto 0),
      ce_expnd_i_8 => ce_expnd_i_8
    );
\MEM_DECODE_GEN[0].PER_CE_GEN[4].MULTIPLE_CES_THIS_CS_GEN.CE_I\: entity work.\design_1_mailbox_0_0_pselect_f__parameterized3\
     port map (
      Q => Q,
      \bus2ip_addr_i_reg[5]\(3 downto 0) => \bus2ip_addr_i_reg[5]\(3 downto 0),
      ce_expnd_i_6 => ce_expnd_i_6
    );
\MEM_DECODE_GEN[0].PER_CE_GEN[5].MULTIPLE_CES_THIS_CS_GEN.CE_I\: entity work.\design_1_mailbox_0_0_pselect_f__parameterized4\
     port map (
      Q => Q,
      \bus2ip_addr_i_reg[5]\(3 downto 0) => \bus2ip_addr_i_reg[5]\(3 downto 0),
      ce_expnd_i_5 => ce_expnd_i_5
    );
\MEM_DECODE_GEN[0].PER_CE_GEN[7].MULTIPLE_CES_THIS_CS_GEN.CE_I\: entity work.\design_1_mailbox_0_0_pselect_f__parameterized6\
     port map (
      Q => Q,
      \bus2ip_addr_i_reg[5]\(3 downto 0) => \bus2ip_addr_i_reg[5]\(3 downto 0),
      ce_expnd_i_3 => ce_expnd_i_3
    );
S0_AXI_ARREADY_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFEFFFE"
    )
        port map (
      I0 => S0_AXI_ARREADY_INST_0_i_1_n_0,
      I1 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      I2 => S0_AXI_ARREADY_INST_0_i_3_n_0,
      I3 => S0_AXI_ARREADY_INST_0_i_4_n_0,
      I4 => is_read,
      I5 => \eqOp__3\,
      O => \^s0_axi_arready\
    );
S0_AXI_ARREADY_INST_0_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00FE00"
    )
        port map (
      I0 => \^s_axi_rdata_i_reg[31]\,
      I1 => \^is_register_reg[2]\,
      I2 => \^read_nextgray_reg[2]\,
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      O => S0_AXI_ARREADY_INST_0_i_1_n_0
    );
S0_AXI_ARREADY_INST_0_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"C8"
    )
        port map (
      I0 => \GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg\,
      I1 => \^bus_rnw_reg_reg_0\,
      I2 => \GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg\,
      O => S0_AXI_ARREADY_INST_0_i_2_n_0
    );
S0_AXI_ARREADY_INST_0_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg\,
      I1 => \^bus_rnw_reg_reg_0\,
      O => S0_AXI_ARREADY_INST_0_i_3_n_0
    );
S0_AXI_ARREADY_INST_0_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg\,
      I1 => \^bus_rnw_reg_reg_0\,
      O => S0_AXI_ARREADY_INST_0_i_4_n_0
    );
S0_AXI_WREADY_INST_0: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => \ip2bus_wrack__3\,
      I1 => is_write_reg,
      I2 => \eqOp__3\,
      O => \^s0_axi_wready\
    );
S0_AXI_WREADY_INST_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F0F0F0F0F0F0F0E"
    )
        port map (
      I0 => \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg\,
      I1 => \^s_axi_rdata_i_reg[31]\,
      I2 => \^bus_rnw_reg_reg_0\,
      I3 => \^write_fsl_error_d1_reg\,
      I4 => \GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg\,
      I5 => \^is_register_reg[2]\,
      O => \ip2bus_wrack__3\
    );
S0_AXI_WREADY_INST_0_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000010"
    )
        port map (
      I0 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4]\(3),
      I1 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4]\(2),
      I2 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4]\(4),
      I3 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4]\(0),
      I4 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4]\(1),
      O => \eqOp__3\
    );
\empty_error_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000EAAAAA"
    )
        port map (
      I0 => empty_error,
      I1 => \out\,
      I2 => \^read_nextgray_reg[2]\,
      I3 => \GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg\,
      I4 => \^bus_rnw_reg_reg_0\,
      I5 => Rst,
      O => empty_error_reg
    );
\empty_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => \out\,
      I1 => \^read_nextgray_reg[2]\,
      I2 => \^bus_rnw_reg_reg_0\,
      O => empty_allow
    );
\full_error_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000AAEAEA"
    )
        port map (
      I0 => full_error,
      I1 => \^write_fsl_error_d1_reg\,
      I2 => full_i_reg,
      I3 => \GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg\,
      I4 => \^bus_rnw_reg_reg_0\,
      I5 => Rst,
      O => full_error_reg
    );
\is_register[0]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1000FFFF10001000"
    )
        port map (
      I0 => write_fsl_error_d1,
      I1 => \^bus_rnw_reg_reg_0\,
      I2 => \^write_fsl_error_d1_reg\,
      I3 => full_i_reg,
      I4 => read_fsl_error_d1,
      I5 => \^read_fsl_error\,
      O => error_detect
    );
\read_fsl_error_d1_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \out\,
      I1 => \^bus_rnw_reg_reg_0\,
      I2 => \^read_nextgray_reg[2]\,
      O => \^read_fsl_error\
    );
\read_nextgray[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^read_nextgray_reg[2]\,
      I1 => \^bus_rnw_reg_reg_0\,
      I2 => \out\,
      O => \read_nextgray_reg[2]_0\(0)
    );
\rit_register[0]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg\,
      I1 => \^bus_rnw_reg_reg_0\,
      O => E(0)
    );
s_axi_bvalid_i_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08FF0808"
    )
        port map (
      I0 => \^s0_axi_wready\,
      I1 => \state_reg[1]\(1),
      I2 => \state_reg[1]\(0),
      I3 => S0_AXI_BREADY,
      I4 => s_axi_bvalid_i_reg_0,
      O => s_axi_bvalid_i_reg
    );
\s_axi_rdata_i[0]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFF0E00FE000E"
    )
        port map (
      I0 => \s_axi_rdata_i[0]_i_2__0_n_0\,
      I1 => \s_axi_rdata_i[0]_i_3__0_n_0\,
      I2 => \s_axi_rdata_i[1]_i_4__0_n_0\,
      I3 => \s_axi_rdata_i[1]_i_5__0_n_0\,
      I4 => empty_error,
      I5 => \out\,
      O => \s_axi_rdata_i_reg[31]_0\(0)
    );
\s_axi_rdata_i[0]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAF0AAF0AACCAA00"
    )
        port map (
      I0 => \sit_register_reg[0]\(0),
      I1 => p_6_in(0),
      I2 => \rit_register_reg[0]\(0),
      I3 => S0_AXI_ARREADY_INST_0_i_4_n_0,
      I4 => \s_axi_rdata_i[2]_i_4__0_n_0\,
      I5 => S0_AXI_ARREADY_INST_0_i_3_n_0,
      O => \s_axi_rdata_i[0]_i_2__0_n_0\
    );
\s_axi_rdata_i[0]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AA88B8B8"
    )
        port map (
      I0 => ie_register(2),
      I1 => \s_axi_rdata_i[2]_i_5__0_n_0\,
      I2 => \DataOut_reg[31]\(0),
      I3 => p_6_in(0),
      I4 => \s_axi_rdata_i[2]_i_6__0_n_0\,
      I5 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      O => \s_axi_rdata_i[0]_i_3__0_n_0\
    );
\s_axi_rdata_i[10]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(10),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(10)
    );
\s_axi_rdata_i[11]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(11),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(11)
    );
\s_axi_rdata_i[12]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(12),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(12)
    );
\s_axi_rdata_i[13]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(13),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(13)
    );
\s_axi_rdata_i[14]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(14),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(14)
    );
\s_axi_rdata_i[15]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(15),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(15)
    );
\s_axi_rdata_i[16]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(16),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(16)
    );
\s_axi_rdata_i[17]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(17),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(17)
    );
\s_axi_rdata_i[18]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(18),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(18)
    );
\s_axi_rdata_i[19]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(19),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(19)
    );
\s_axi_rdata_i[1]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFF0E00FE000E"
    )
        port map (
      I0 => \s_axi_rdata_i[1]_i_2__0_n_0\,
      I1 => \s_axi_rdata_i[1]_i_3__0_n_0\,
      I2 => \s_axi_rdata_i[1]_i_4__0_n_0\,
      I3 => \s_axi_rdata_i[1]_i_5__0_n_0\,
      I4 => full_error,
      I5 => full_i_reg,
      O => \s_axi_rdata_i_reg[31]_0\(1)
    );
\s_axi_rdata_i[1]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAF0AAF0AACCAA00"
    )
        port map (
      I0 => \sit_register_reg[0]\(1),
      I1 => p_6_in(1),
      I2 => \rit_register_reg[0]\(1),
      I3 => S0_AXI_ARREADY_INST_0_i_4_n_0,
      I4 => \s_axi_rdata_i[2]_i_4__0_n_0\,
      I5 => S0_AXI_ARREADY_INST_0_i_3_n_0,
      O => \s_axi_rdata_i[1]_i_2__0_n_0\
    );
\s_axi_rdata_i[1]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AA88B8B8"
    )
        port map (
      I0 => ie_register(1),
      I1 => \s_axi_rdata_i[2]_i_5__0_n_0\,
      I2 => \DataOut_reg[31]\(1),
      I3 => p_6_in(1),
      I4 => \s_axi_rdata_i[2]_i_6__0_n_0\,
      I5 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      O => \s_axi_rdata_i[1]_i_3__0_n_0\
    );
\s_axi_rdata_i[1]_i_4__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg\,
      I1 => \^bus_rnw_reg_reg_0\,
      O => \s_axi_rdata_i[1]_i_4__0_n_0\
    );
\s_axi_rdata_i[1]_i_5__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg\,
      I1 => \^bus_rnw_reg_reg_0\,
      O => \s_axi_rdata_i[1]_i_5__0_n_0\
    );
\s_axi_rdata_i[20]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(20),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(20)
    );
\s_axi_rdata_i[21]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(21),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(21)
    );
\s_axi_rdata_i[22]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(22),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(22)
    );
\s_axi_rdata_i[23]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(23),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(23)
    );
\s_axi_rdata_i[24]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(24),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(24)
    );
\s_axi_rdata_i[25]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(25),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(25)
    );
\s_axi_rdata_i[26]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(26),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(26)
    );
\s_axi_rdata_i[27]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(27),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(27)
    );
\s_axi_rdata_i[28]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(28),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(28)
    );
\s_axi_rdata_i[29]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(29),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(29)
    );
\s_axi_rdata_i[2]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEE0EEE00EE0EEE"
    )
        port map (
      I0 => \s_axi_rdata_i[2]_i_2__0_n_0\,
      I1 => \s_axi_rdata_i[2]_i_3__0_n_0\,
      I2 => \GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg\,
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg\,
      I5 => sit_detect_d0,
      O => \s_axi_rdata_i_reg[31]_0\(2)
    );
\s_axi_rdata_i[2]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAF0AAF0AACCAA00"
    )
        port map (
      I0 => \sit_register_reg[0]\(2),
      I1 => p_6_in(2),
      I2 => \rit_register_reg[0]\(2),
      I3 => S0_AXI_ARREADY_INST_0_i_4_n_0,
      I4 => \s_axi_rdata_i[2]_i_4__0_n_0\,
      I5 => S0_AXI_ARREADY_INST_0_i_3_n_0,
      O => \s_axi_rdata_i[2]_i_2__0_n_0\
    );
\s_axi_rdata_i[2]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AA88B8B8"
    )
        port map (
      I0 => ie_register(0),
      I1 => \s_axi_rdata_i[2]_i_5__0_n_0\,
      I2 => \DataOut_reg[31]\(2),
      I3 => p_6_in(2),
      I4 => \s_axi_rdata_i[2]_i_6__0_n_0\,
      I5 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      O => \s_axi_rdata_i[2]_i_3__0_n_0\
    );
\s_axi_rdata_i[2]_i_4__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^is_register_reg[2]\,
      I1 => \^bus_rnw_reg_reg_0\,
      O => \s_axi_rdata_i[2]_i_4__0_n_0\
    );
\s_axi_rdata_i[2]_i_5__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^s_axi_rdata_i_reg[31]\,
      I1 => \^bus_rnw_reg_reg_0\,
      O => \s_axi_rdata_i[2]_i_5__0_n_0\
    );
\s_axi_rdata_i[2]_i_6__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I1 => \^bus_rnw_reg_reg_0\,
      O => \s_axi_rdata_i[2]_i_6__0_n_0\
    );
\s_axi_rdata_i[30]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(30),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(30)
    );
\s_axi_rdata_i[31]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(31),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(31)
    );
\s_axi_rdata_i[31]_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F0E0"
    )
        port map (
      I0 => \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg\,
      I1 => \^is_register_reg[2]\,
      I2 => \^bus_rnw_reg_reg_0\,
      I3 => \GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg\,
      O => \s_axi_rdata_i[31]_i_3__0_n_0\
    );
\s_axi_rdata_i[3]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEE0EEE00EE0EEE"
    )
        port map (
      I0 => \s_axi_rdata_i[3]_i_2__0_n_0\,
      I1 => \s_axi_rdata_i[3]_i_3__0_n_0\,
      I2 => \GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg\,
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg\,
      I5 => rit_detect_d0,
      O => \s_axi_rdata_i_reg[31]_0\(3)
    );
\s_axi_rdata_i[3]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AC00A000"
    )
        port map (
      I0 => \sit_register_reg[0]\(3),
      I1 => \rit_register_reg[0]\(3),
      I2 => \GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg\,
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg\,
      O => \s_axi_rdata_i[3]_i_2__0_n_0\
    );
\s_axi_rdata_i[3]_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00003070"
    )
        port map (
      I0 => \^s_axi_rdata_i_reg[31]\,
      I1 => \^bus_rnw_reg_reg_0\,
      I2 => \DataOut_reg[31]\(3),
      I3 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I4 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      O => \s_axi_rdata_i[3]_i_3__0_n_0\
    );
\s_axi_rdata_i[4]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(4),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(4)
    );
\s_axi_rdata_i[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(5),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(5)
    );
\s_axi_rdata_i[6]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(6),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(6)
    );
\s_axi_rdata_i[7]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(7),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(7)
    );
\s_axi_rdata_i[8]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(8),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(8)
    );
\s_axi_rdata_i[9]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3__0_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(9),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S0_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(9)
    );
s_axi_rvalid_i_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08FF0808"
    )
        port map (
      I0 => \^s0_axi_arready\,
      I1 => \state_reg[1]\(0),
      I2 => \state_reg[1]\(1),
      I3 => S0_AXI_RREADY,
      I4 => s_axi_rvalid_i_reg_0,
      O => s_axi_rvalid_i_reg
    );
\sit_register[0]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg\,
      I1 => \^bus_rnw_reg_reg_0\,
      O => \sit_register_reg[3]\(0)
    );
\state[0]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"77FC44FC"
    )
        port map (
      I0 => \state1__2\,
      I1 => \state_reg[1]\(0),
      I2 => S0_AXI_ARVALID,
      I3 => \state_reg[1]\(1),
      I4 => \^s0_axi_wready\,
      O => D(0)
    );
\state[1]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5FFC50FC"
    )
        port map (
      I0 => \state1__2\,
      I1 => S0_AXI_ARVALID_0,
      I2 => \state_reg[1]\(1),
      I3 => \state_reg[1]\(0),
      I4 => \^s0_axi_arready\,
      O => D(1)
    );
\write_fsl_error_d1_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \^bus_rnw_reg_reg_0\,
      I1 => \^write_fsl_error_d1_reg\,
      I2 => full_i_reg,
      O => write_fsl_error
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_mailbox_0_0_address_decoder__parameterized0\ is
  port (
    \s_axi_rdata_i_reg[31]\ : out STD_LOGIC;
    \is_register_reg[2]\ : out STD_LOGIC;
    \read_nextgray_reg[2]\ : out STD_LOGIC;
    write_fsl_error_d1_reg : out STD_LOGIC;
    Bus_RNW_reg_reg_0 : out STD_LOGIC;
    empty_allow : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S1_AXI_ARREADY : out STD_LOGIC;
    error_detect : out STD_LOGIC;
    read_fsl_error : out STD_LOGIC;
    write_fsl_error : out STD_LOGIC;
    \s_axi_rdata_i_reg[31]_0\ : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S1_AXI_WREADY : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \sit_register_reg[3]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_rvalid_i_reg : out STD_LOGIC;
    s_axi_bvalid_i_reg : out STD_LOGIC;
    full_error_reg : out STD_LOGIC;
    empty_error_reg : out STD_LOGIC;
    \read_nextgray_reg[2]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC;
    S1_AXI_ACLK : in STD_LOGIC;
    \out\ : in STD_LOGIC;
    \state1__2\ : in STD_LOGIC;
    S1_AXI_ARVALID_0 : in STD_LOGIC;
    \state_reg[1]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    write_fsl_error_d1 : in STD_LOGIC;
    full_i_reg : in STD_LOGIC;
    read_fsl_error_d1 : in STD_LOGIC;
    rit_detect_d0 : in STD_LOGIC;
    sit_detect_d0 : in STD_LOGIC;
    full_error_reg_0 : in STD_LOGIC;
    empty_error_reg_0 : in STD_LOGIC;
    is_read : in STD_LOGIC;
    S1_AXI_ARVALID : in STD_LOGIC;
    is_write_reg : in STD_LOGIC;
    \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4]\ : in STD_LOGIC_VECTOR ( 4 downto 0 );
    \sit_register_reg[0]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    p_6_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \rit_register_reg[0]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S1_AXI_RREADY : in STD_LOGIC;
    s_axi_rvalid_i_reg_0 : in STD_LOGIC;
    S1_AXI_BREADY : in STD_LOGIC;
    s_axi_bvalid_i_reg_0 : in STD_LOGIC;
    \Rst_Async.SYS_Rst_If1_reg\ : in STD_LOGIC;
    \DataOut_reg[31]\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ie_register : in STD_LOGIC_VECTOR ( 0 to 2 );
    \bus2ip_addr_i_reg[5]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    bus2ip_rnw_i_reg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_mailbox_0_0_address_decoder__parameterized0\ : entity is "address_decoder";
end \design_1_mailbox_0_0_address_decoder__parameterized0\;

architecture STRUCTURE of \design_1_mailbox_0_0_address_decoder__parameterized0\ is
  signal \Bus_RNW_reg_i_1__0_n_0\ : STD_LOGIC;
  signal \^bus_rnw_reg_reg_0\ : STD_LOGIC;
  signal \GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1_n_0\ : STD_LOGIC;
  signal \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\ : STD_LOGIC;
  signal \GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg\ : STD_LOGIC;
  signal \GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg\ : STD_LOGIC;
  signal \GEN_BKEND_CE_REGISTERS[6].ce_out_i[6]_i_1__0_n_0\ : STD_LOGIC;
  signal \GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg\ : STD_LOGIC;
  signal \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg\ : STD_LOGIC;
  signal \GEN_BKEND_CE_REGISTERS[8].ce_out_i[8]_i_1__0_n_0\ : STD_LOGIC;
  signal \GEN_BKEND_CE_REGISTERS[9].ce_out_i[9]_i_1__0_n_0\ : STD_LOGIC;
  signal \MEM_DECODE_GEN[0].PER_CE_GEN[0].MULTIPLE_CES_THIS_CS_GEN.CE_I_n_0\ : STD_LOGIC;
  signal \^s1_axi_arready\ : STD_LOGIC;
  signal S1_AXI_ARREADY_INST_0_i_1_n_0 : STD_LOGIC;
  signal S1_AXI_ARREADY_INST_0_i_2_n_0 : STD_LOGIC;
  signal S1_AXI_ARREADY_INST_0_i_3_n_0 : STD_LOGIC;
  signal S1_AXI_ARREADY_INST_0_i_4_n_0 : STD_LOGIC;
  signal \^s1_axi_wready\ : STD_LOGIC;
  signal ce_expnd_i_0 : STD_LOGIC;
  signal ce_expnd_i_3 : STD_LOGIC;
  signal ce_expnd_i_5 : STD_LOGIC;
  signal ce_expnd_i_6 : STD_LOGIC;
  signal ce_expnd_i_8 : STD_LOGIC;
  signal \eqOp__3\ : STD_LOGIC;
  signal \ip2bus_wrack__3\ : STD_LOGIC;
  signal \^is_register_reg[2]\ : STD_LOGIC;
  signal \^read_fsl_error\ : STD_LOGIC;
  signal \^read_nextgray_reg[2]\ : STD_LOGIC;
  signal \s_axi_rdata_i[0]_i_2_n_0\ : STD_LOGIC;
  signal \s_axi_rdata_i[0]_i_3_n_0\ : STD_LOGIC;
  signal \s_axi_rdata_i[1]_i_2_n_0\ : STD_LOGIC;
  signal \s_axi_rdata_i[1]_i_3_n_0\ : STD_LOGIC;
  signal \s_axi_rdata_i[1]_i_4_n_0\ : STD_LOGIC;
  signal \s_axi_rdata_i[1]_i_5_n_0\ : STD_LOGIC;
  signal \s_axi_rdata_i[2]_i_2_n_0\ : STD_LOGIC;
  signal \s_axi_rdata_i[2]_i_3_n_0\ : STD_LOGIC;
  signal \s_axi_rdata_i[2]_i_4_n_0\ : STD_LOGIC;
  signal \s_axi_rdata_i[2]_i_5_n_0\ : STD_LOGIC;
  signal \s_axi_rdata_i[2]_i_6_n_0\ : STD_LOGIC;
  signal \s_axi_rdata_i[31]_i_3_n_0\ : STD_LOGIC;
  signal \s_axi_rdata_i[3]_i_2_n_0\ : STD_LOGIC;
  signal \s_axi_rdata_i[3]_i_3_n_0\ : STD_LOGIC;
  signal \^s_axi_rdata_i_reg[31]\ : STD_LOGIC;
  signal \^write_fsl_error_d1_reg\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \Bus_RNW_reg_i_1__0\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \GEN_BKEND_CE_REGISTERS[8].ce_out_i[8]_i_1__0\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \GEN_BKEND_CE_REGISTERS[9].ce_out_i[9]_i_1__0\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of S1_AXI_ARREADY_INST_0_i_1 : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of S1_AXI_ARREADY_INST_0_i_2 : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of S1_AXI_ARREADY_INST_0_i_3 : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of S1_AXI_ARREADY_INST_0_i_4 : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of empty_i_1 : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of read_fsl_error_d1_i_1 : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \read_nextgray[2]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \rit_register[0]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \s_axi_rdata_i[1]_i_4\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \s_axi_rdata_i[1]_i_5\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \s_axi_rdata_i[2]_i_4\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \s_axi_rdata_i[2]_i_5\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \s_axi_rdata_i[2]_i_6\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \s_axi_rdata_i[3]_i_2\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \s_axi_rdata_i[3]_i_3\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \sit_register[0]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of write_fsl_error_d1_i_1 : label is "soft_lutpair22";
begin
  Bus_RNW_reg_reg_0 <= \^bus_rnw_reg_reg_0\;
  S1_AXI_ARREADY <= \^s1_axi_arready\;
  S1_AXI_WREADY <= \^s1_axi_wready\;
  \is_register_reg[2]\ <= \^is_register_reg[2]\;
  read_fsl_error <= \^read_fsl_error\;
  \read_nextgray_reg[2]\ <= \^read_nextgray_reg[2]\;
  \s_axi_rdata_i_reg[31]\ <= \^s_axi_rdata_i_reg[31]\;
  write_fsl_error_d1_reg <= \^write_fsl_error_d1_reg\;
\Bus_RNW_reg_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => bus2ip_rnw_i_reg,
      I1 => Q,
      I2 => \^bus_rnw_reg_reg_0\,
      O => \Bus_RNW_reg_i_1__0_n_0\
    );
Bus_RNW_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \Bus_RNW_reg_i_1__0_n_0\,
      Q => \^bus_rnw_reg_reg_0\,
      R => '0'
    );
\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => Q,
      D => \MEM_DECODE_GEN[0].PER_CE_GEN[0].MULTIPLE_CES_THIS_CS_GEN.CE_I_n_0\,
      Q => \^write_fsl_error_d1_reg\,
      R => \GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1_n_0\
    );
\GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \^s1_axi_wready\,
      I1 => \^s1_axi_arready\,
      I2 => \Rst_Async.SYS_Rst_If1_reg\,
      O => \GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1_n_0\
    );
\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => Q,
      D => ce_expnd_i_0,
      Q => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      R => \GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1_n_0\
    );
\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => Q,
      D => ce_expnd_i_8,
      Q => \^read_nextgray_reg[2]\,
      R => \GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1_n_0\
    );
\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => Q,
      D => ce_expnd_i_6,
      Q => \GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg\,
      R => \GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1_n_0\
    );
\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => Q,
      D => ce_expnd_i_5,
      Q => \GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg\,
      R => \GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1_n_0\
    );
\GEN_BKEND_CE_REGISTERS[6].ce_out_i[6]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"10000000"
    )
        port map (
      I0 => \bus2ip_addr_i_reg[5]\(0),
      I1 => \bus2ip_addr_i_reg[5]\(3),
      I2 => Q,
      I3 => \bus2ip_addr_i_reg[5]\(1),
      I4 => \bus2ip_addr_i_reg[5]\(2),
      O => \GEN_BKEND_CE_REGISTERS[6].ce_out_i[6]_i_1__0_n_0\
    );
\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => Q,
      D => \GEN_BKEND_CE_REGISTERS[6].ce_out_i[6]_i_1__0_n_0\,
      Q => \GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg\,
      R => \GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1_n_0\
    );
\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => Q,
      D => ce_expnd_i_3,
      Q => \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg\,
      R => \GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1_n_0\
    );
\GEN_BKEND_CE_REGISTERS[8].ce_out_i[8]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"01000000"
    )
        port map (
      I0 => \bus2ip_addr_i_reg[5]\(1),
      I1 => \bus2ip_addr_i_reg[5]\(2),
      I2 => \bus2ip_addr_i_reg[5]\(0),
      I3 => Q,
      I4 => \bus2ip_addr_i_reg[5]\(3),
      O => \GEN_BKEND_CE_REGISTERS[8].ce_out_i[8]_i_1__0_n_0\
    );
\GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => Q,
      D => \GEN_BKEND_CE_REGISTERS[8].ce_out_i[8]_i_1__0_n_0\,
      Q => \^is_register_reg[2]\,
      R => \GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1_n_0\
    );
\GEN_BKEND_CE_REGISTERS[9].ce_out_i[9]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"10000000"
    )
        port map (
      I0 => \bus2ip_addr_i_reg[5]\(1),
      I1 => \bus2ip_addr_i_reg[5]\(2),
      I2 => \bus2ip_addr_i_reg[5]\(3),
      I3 => \bus2ip_addr_i_reg[5]\(0),
      I4 => Q,
      O => \GEN_BKEND_CE_REGISTERS[9].ce_out_i[9]_i_1__0_n_0\
    );
\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => Q,
      D => \GEN_BKEND_CE_REGISTERS[9].ce_out_i[9]_i_1__0_n_0\,
      Q => \^s_axi_rdata_i_reg[31]\,
      R => \GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1_n_0\
    );
\MEM_DECODE_GEN[0].PER_CE_GEN[0].MULTIPLE_CES_THIS_CS_GEN.CE_I\: entity work.design_1_mailbox_0_0_pselect_f
     port map (
      \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0]\ => \MEM_DECODE_GEN[0].PER_CE_GEN[0].MULTIPLE_CES_THIS_CS_GEN.CE_I_n_0\,
      Q => Q,
      \bus2ip_addr_i_reg[5]\(3 downto 0) => \bus2ip_addr_i_reg[5]\(3 downto 0)
    );
\MEM_DECODE_GEN[0].PER_CE_GEN[10].MULTIPLE_CES_THIS_CS_GEN.CE_I\: entity work.\design_1_mailbox_0_0_pselect_f__parameterized19\
     port map (
      Q => Q,
      \bus2ip_addr_i_reg[5]\(3 downto 0) => \bus2ip_addr_i_reg[5]\(3 downto 0),
      ce_expnd_i_0 => ce_expnd_i_0
    );
\MEM_DECODE_GEN[0].PER_CE_GEN[2].MULTIPLE_CES_THIS_CS_GEN.CE_I\: entity work.\design_1_mailbox_0_0_pselect_f__parameterized11\
     port map (
      Q => Q,
      \bus2ip_addr_i_reg[5]\(3 downto 0) => \bus2ip_addr_i_reg[5]\(3 downto 0),
      ce_expnd_i_8 => ce_expnd_i_8
    );
\MEM_DECODE_GEN[0].PER_CE_GEN[4].MULTIPLE_CES_THIS_CS_GEN.CE_I\: entity work.\design_1_mailbox_0_0_pselect_f__parameterized13\
     port map (
      Q => Q,
      \bus2ip_addr_i_reg[5]\(3 downto 0) => \bus2ip_addr_i_reg[5]\(3 downto 0),
      ce_expnd_i_6 => ce_expnd_i_6
    );
\MEM_DECODE_GEN[0].PER_CE_GEN[5].MULTIPLE_CES_THIS_CS_GEN.CE_I\: entity work.\design_1_mailbox_0_0_pselect_f__parameterized14\
     port map (
      Q => Q,
      \bus2ip_addr_i_reg[5]\(3 downto 0) => \bus2ip_addr_i_reg[5]\(3 downto 0),
      ce_expnd_i_5 => ce_expnd_i_5
    );
\MEM_DECODE_GEN[0].PER_CE_GEN[7].MULTIPLE_CES_THIS_CS_GEN.CE_I\: entity work.\design_1_mailbox_0_0_pselect_f__parameterized16\
     port map (
      Q => Q,
      \bus2ip_addr_i_reg[5]\(3 downto 0) => \bus2ip_addr_i_reg[5]\(3 downto 0),
      ce_expnd_i_3 => ce_expnd_i_3
    );
S1_AXI_ARREADY_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFEFFFE"
    )
        port map (
      I0 => S1_AXI_ARREADY_INST_0_i_1_n_0,
      I1 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      I2 => S1_AXI_ARREADY_INST_0_i_3_n_0,
      I3 => S1_AXI_ARREADY_INST_0_i_4_n_0,
      I4 => is_read,
      I5 => \eqOp__3\,
      O => \^s1_axi_arready\
    );
S1_AXI_ARREADY_INST_0_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00FE00"
    )
        port map (
      I0 => \^s_axi_rdata_i_reg[31]\,
      I1 => \^is_register_reg[2]\,
      I2 => \^read_nextgray_reg[2]\,
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      O => S1_AXI_ARREADY_INST_0_i_1_n_0
    );
S1_AXI_ARREADY_INST_0_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"C8"
    )
        port map (
      I0 => \GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg\,
      I1 => \^bus_rnw_reg_reg_0\,
      I2 => \GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg\,
      O => S1_AXI_ARREADY_INST_0_i_2_n_0
    );
S1_AXI_ARREADY_INST_0_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg\,
      I1 => \^bus_rnw_reg_reg_0\,
      O => S1_AXI_ARREADY_INST_0_i_3_n_0
    );
S1_AXI_ARREADY_INST_0_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg\,
      I1 => \^bus_rnw_reg_reg_0\,
      O => S1_AXI_ARREADY_INST_0_i_4_n_0
    );
S1_AXI_WREADY_INST_0: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => \ip2bus_wrack__3\,
      I1 => is_write_reg,
      I2 => \eqOp__3\,
      O => \^s1_axi_wready\
    );
S1_AXI_WREADY_INST_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F0F0F0F0F0F0F0E"
    )
        port map (
      I0 => \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg\,
      I1 => \^s_axi_rdata_i_reg[31]\,
      I2 => \^bus_rnw_reg_reg_0\,
      I3 => \^write_fsl_error_d1_reg\,
      I4 => \GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg\,
      I5 => \^is_register_reg[2]\,
      O => \ip2bus_wrack__3\
    );
S1_AXI_WREADY_INST_0_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000010"
    )
        port map (
      I0 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4]\(3),
      I1 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4]\(2),
      I2 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4]\(4),
      I3 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4]\(0),
      I4 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4]\(1),
      O => \eqOp__3\
    );
empty_error_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000EAAAAA"
    )
        port map (
      I0 => empty_error_reg_0,
      I1 => \out\,
      I2 => \^read_nextgray_reg[2]\,
      I3 => \GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg\,
      I4 => \^bus_rnw_reg_reg_0\,
      I5 => \Rst_Async.SYS_Rst_If1_reg\,
      O => empty_error_reg
    );
empty_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => \out\,
      I1 => \^read_nextgray_reg[2]\,
      I2 => \^bus_rnw_reg_reg_0\,
      O => empty_allow
    );
full_error_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000AAEAEA"
    )
        port map (
      I0 => full_error_reg_0,
      I1 => \^write_fsl_error_d1_reg\,
      I2 => full_i_reg,
      I3 => \GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg\,
      I4 => \^bus_rnw_reg_reg_0\,
      I5 => \Rst_Async.SYS_Rst_If1_reg\,
      O => full_error_reg
    );
\is_register[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1000FFFF10001000"
    )
        port map (
      I0 => write_fsl_error_d1,
      I1 => \^bus_rnw_reg_reg_0\,
      I2 => \^write_fsl_error_d1_reg\,
      I3 => full_i_reg,
      I4 => read_fsl_error_d1,
      I5 => \^read_fsl_error\,
      O => error_detect
    );
read_fsl_error_d1_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \out\,
      I1 => \^bus_rnw_reg_reg_0\,
      I2 => \^read_nextgray_reg[2]\,
      O => \^read_fsl_error\
    );
\read_nextgray[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^read_nextgray_reg[2]\,
      I1 => \^bus_rnw_reg_reg_0\,
      I2 => \out\,
      O => \read_nextgray_reg[2]_0\(0)
    );
\rit_register[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg\,
      I1 => \^bus_rnw_reg_reg_0\,
      O => E(0)
    );
\s_axi_bvalid_i_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08FF0808"
    )
        port map (
      I0 => \^s1_axi_wready\,
      I1 => \state_reg[1]\(1),
      I2 => \state_reg[1]\(0),
      I3 => S1_AXI_BREADY,
      I4 => s_axi_bvalid_i_reg_0,
      O => s_axi_bvalid_i_reg
    );
\s_axi_rdata_i[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFF0E00FE000E"
    )
        port map (
      I0 => \s_axi_rdata_i[0]_i_2_n_0\,
      I1 => \s_axi_rdata_i[0]_i_3_n_0\,
      I2 => \s_axi_rdata_i[1]_i_4_n_0\,
      I3 => \s_axi_rdata_i[1]_i_5_n_0\,
      I4 => empty_error_reg_0,
      I5 => \out\,
      O => \s_axi_rdata_i_reg[31]_0\(0)
    );
\s_axi_rdata_i[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAF0AAF0AACCAA00"
    )
        port map (
      I0 => \sit_register_reg[0]\(0),
      I1 => p_6_in(0),
      I2 => \rit_register_reg[0]\(0),
      I3 => S1_AXI_ARREADY_INST_0_i_4_n_0,
      I4 => \s_axi_rdata_i[2]_i_4_n_0\,
      I5 => S1_AXI_ARREADY_INST_0_i_3_n_0,
      O => \s_axi_rdata_i[0]_i_2_n_0\
    );
\s_axi_rdata_i[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AA88B8B8"
    )
        port map (
      I0 => ie_register(2),
      I1 => \s_axi_rdata_i[2]_i_5_n_0\,
      I2 => \DataOut_reg[31]\(0),
      I3 => p_6_in(0),
      I4 => \s_axi_rdata_i[2]_i_6_n_0\,
      I5 => \s_axi_rdata_i[31]_i_3_n_0\,
      O => \s_axi_rdata_i[0]_i_3_n_0\
    );
\s_axi_rdata_i[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(10),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(10)
    );
\s_axi_rdata_i[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(11),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(11)
    );
\s_axi_rdata_i[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(12),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(12)
    );
\s_axi_rdata_i[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(13),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(13)
    );
\s_axi_rdata_i[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(14),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(14)
    );
\s_axi_rdata_i[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(15),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(15)
    );
\s_axi_rdata_i[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(16),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(16)
    );
\s_axi_rdata_i[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(17),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(17)
    );
\s_axi_rdata_i[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(18),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(18)
    );
\s_axi_rdata_i[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(19),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(19)
    );
\s_axi_rdata_i[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFF0E00FE000E"
    )
        port map (
      I0 => \s_axi_rdata_i[1]_i_2_n_0\,
      I1 => \s_axi_rdata_i[1]_i_3_n_0\,
      I2 => \s_axi_rdata_i[1]_i_4_n_0\,
      I3 => \s_axi_rdata_i[1]_i_5_n_0\,
      I4 => full_error_reg_0,
      I5 => full_i_reg,
      O => \s_axi_rdata_i_reg[31]_0\(1)
    );
\s_axi_rdata_i[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAF0AAF0AACCAA00"
    )
        port map (
      I0 => \sit_register_reg[0]\(1),
      I1 => p_6_in(1),
      I2 => \rit_register_reg[0]\(1),
      I3 => S1_AXI_ARREADY_INST_0_i_4_n_0,
      I4 => \s_axi_rdata_i[2]_i_4_n_0\,
      I5 => S1_AXI_ARREADY_INST_0_i_3_n_0,
      O => \s_axi_rdata_i[1]_i_2_n_0\
    );
\s_axi_rdata_i[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AA88B8B8"
    )
        port map (
      I0 => ie_register(1),
      I1 => \s_axi_rdata_i[2]_i_5_n_0\,
      I2 => \DataOut_reg[31]\(1),
      I3 => p_6_in(1),
      I4 => \s_axi_rdata_i[2]_i_6_n_0\,
      I5 => \s_axi_rdata_i[31]_i_3_n_0\,
      O => \s_axi_rdata_i[1]_i_3_n_0\
    );
\s_axi_rdata_i[1]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg\,
      I1 => \^bus_rnw_reg_reg_0\,
      O => \s_axi_rdata_i[1]_i_4_n_0\
    );
\s_axi_rdata_i[1]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg\,
      I1 => \^bus_rnw_reg_reg_0\,
      O => \s_axi_rdata_i[1]_i_5_n_0\
    );
\s_axi_rdata_i[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(20),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(20)
    );
\s_axi_rdata_i[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(21),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(21)
    );
\s_axi_rdata_i[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(22),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(22)
    );
\s_axi_rdata_i[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(23),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(23)
    );
\s_axi_rdata_i[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(24),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(24)
    );
\s_axi_rdata_i[25]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(25),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(25)
    );
\s_axi_rdata_i[26]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(26),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(26)
    );
\s_axi_rdata_i[27]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(27),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(27)
    );
\s_axi_rdata_i[28]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(28),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(28)
    );
\s_axi_rdata_i[29]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(29),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(29)
    );
\s_axi_rdata_i[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEE0EEE00EE0EEE"
    )
        port map (
      I0 => \s_axi_rdata_i[2]_i_2_n_0\,
      I1 => \s_axi_rdata_i[2]_i_3_n_0\,
      I2 => \GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg\,
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg\,
      I5 => sit_detect_d0,
      O => \s_axi_rdata_i_reg[31]_0\(2)
    );
\s_axi_rdata_i[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAF0AAF0AACCAA00"
    )
        port map (
      I0 => \sit_register_reg[0]\(2),
      I1 => p_6_in(2),
      I2 => \rit_register_reg[0]\(2),
      I3 => S1_AXI_ARREADY_INST_0_i_4_n_0,
      I4 => \s_axi_rdata_i[2]_i_4_n_0\,
      I5 => S1_AXI_ARREADY_INST_0_i_3_n_0,
      O => \s_axi_rdata_i[2]_i_2_n_0\
    );
\s_axi_rdata_i[2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AA88B8B8"
    )
        port map (
      I0 => ie_register(0),
      I1 => \s_axi_rdata_i[2]_i_5_n_0\,
      I2 => \DataOut_reg[31]\(2),
      I3 => p_6_in(2),
      I4 => \s_axi_rdata_i[2]_i_6_n_0\,
      I5 => \s_axi_rdata_i[31]_i_3_n_0\,
      O => \s_axi_rdata_i[2]_i_3_n_0\
    );
\s_axi_rdata_i[2]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^is_register_reg[2]\,
      I1 => \^bus_rnw_reg_reg_0\,
      O => \s_axi_rdata_i[2]_i_4_n_0\
    );
\s_axi_rdata_i[2]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^s_axi_rdata_i_reg[31]\,
      I1 => \^bus_rnw_reg_reg_0\,
      O => \s_axi_rdata_i[2]_i_5_n_0\
    );
\s_axi_rdata_i[2]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I1 => \^bus_rnw_reg_reg_0\,
      O => \s_axi_rdata_i[2]_i_6_n_0\
    );
\s_axi_rdata_i[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(30),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(30)
    );
\s_axi_rdata_i[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(31),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(31)
    );
\s_axi_rdata_i[31]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F0E0"
    )
        port map (
      I0 => \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg\,
      I1 => \^is_register_reg[2]\,
      I2 => \^bus_rnw_reg_reg_0\,
      I3 => \GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg\,
      O => \s_axi_rdata_i[31]_i_3_n_0\
    );
\s_axi_rdata_i[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEE0EEE00EE0EEE"
    )
        port map (
      I0 => \s_axi_rdata_i[3]_i_2_n_0\,
      I1 => \s_axi_rdata_i[3]_i_3_n_0\,
      I2 => \GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg\,
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg\,
      I5 => rit_detect_d0,
      O => \s_axi_rdata_i_reg[31]_0\(3)
    );
\s_axi_rdata_i[3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AC00A000"
    )
        port map (
      I0 => \sit_register_reg[0]\(3),
      I1 => \rit_register_reg[0]\(3),
      I2 => \GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg\,
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg\,
      O => \s_axi_rdata_i[3]_i_2_n_0\
    );
\s_axi_rdata_i[3]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00003070"
    )
        port map (
      I0 => \^s_axi_rdata_i_reg[31]\,
      I1 => \^bus_rnw_reg_reg_0\,
      I2 => \DataOut_reg[31]\(3),
      I3 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I4 => \s_axi_rdata_i[31]_i_3_n_0\,
      O => \s_axi_rdata_i[3]_i_3_n_0\
    );
\s_axi_rdata_i[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(4),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(4)
    );
\s_axi_rdata_i[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(5),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(5)
    );
\s_axi_rdata_i[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(6),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(6)
    );
\s_axi_rdata_i[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(7),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(7)
    );
\s_axi_rdata_i[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(8),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(8)
    );
\s_axi_rdata_i[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000501050"
    )
        port map (
      I0 => \s_axi_rdata_i[31]_i_3_n_0\,
      I1 => \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg\,
      I2 => \DataOut_reg[31]\(9),
      I3 => \^bus_rnw_reg_reg_0\,
      I4 => \^s_axi_rdata_i_reg[31]\,
      I5 => S1_AXI_ARREADY_INST_0_i_2_n_0,
      O => \s_axi_rdata_i_reg[31]_0\(9)
    );
\s_axi_rvalid_i_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08FF0808"
    )
        port map (
      I0 => \^s1_axi_arready\,
      I1 => \state_reg[1]\(0),
      I2 => \state_reg[1]\(1),
      I3 => S1_AXI_RREADY,
      I4 => s_axi_rvalid_i_reg_0,
      O => s_axi_rvalid_i_reg
    );
\sit_register[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg\,
      I1 => \^bus_rnw_reg_reg_0\,
      O => \sit_register_reg[3]\(0)
    );
\state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"77FC44FC"
    )
        port map (
      I0 => \state1__2\,
      I1 => \state_reg[1]\(0),
      I2 => S1_AXI_ARVALID,
      I3 => \state_reg[1]\(1),
      I4 => \^s1_axi_wready\,
      O => D(0)
    );
\state[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5FFC50FC"
    )
        port map (
      I0 => \state1__2\,
      I1 => S1_AXI_ARVALID_0,
      I2 => \state_reg[1]\(1),
      I3 => \state_reg[1]\(0),
      I4 => \^s1_axi_arready\,
      O => D(1)
    );
write_fsl_error_d1_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \^bus_rnw_reg_reg_0\,
      I1 => \^write_fsl_error_d1_reg\,
      I2 => full_i_reg,
      O => write_fsl_error
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_mailbox_0_0_fsl_v20 is
  port (
    rd_rst_meta_inst : out STD_LOGIC;
    \out\ : out STD_LOGIC;
    \write_nextgray_reg[2]\ : out STD_LOGIC;
    rit_detect_d0 : out STD_LOGIC;
    sit_detect_d0 : out STD_LOGIC;
    \s_axi_rdata_i_reg[31]\ : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S1_AXI_ACLK : in STD_LOGIC;
    S0_AXI_ACLK : in STD_LOGIC;
    empty_allow : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    Bus_RNW_reg : in STD_LOGIC;
    \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg\ : in STD_LOGIC;
    \rit_register_reg[0]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg\ : in STD_LOGIC;
    Bus_RNW_reg_0 : in STD_LOGIC;
    S0_AXI_ARESETN : in STD_LOGIC;
    SYS_Rst : in STD_LOGIC;
    S1_AXI_ARESETN : in STD_LOGIC;
    S0_AXI_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end design_1_mailbox_0_0_fsl_v20;

architecture STRUCTURE of design_1_mailbox_0_0_fsl_v20 is
begin
\Using_FIFO.Async_FIFO_Gen.Async_FIFO_I1\: entity work.design_1_mailbox_0_0_Async_FIFO_1
     port map (
      Bus_RNW_reg => Bus_RNW_reg,
      Bus_RNW_reg_0 => Bus_RNW_reg_0,
      E(0) => E(0),
      \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg\ => \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg\,
      \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg\ => \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg\,
      Q(3 downto 0) => Q(3 downto 0),
      S0_AXI_ACLK => S0_AXI_ACLK,
      S0_AXI_ARESETN => S0_AXI_ARESETN,
      S0_AXI_WDATA(31 downto 0) => S0_AXI_WDATA(31 downto 0),
      S1_AXI_ACLK => S1_AXI_ACLK,
      S1_AXI_ARESETN => S1_AXI_ARESETN,
      SYS_Rst => SYS_Rst,
      empty_allow => empty_allow,
      \out\ => \out\,
      rd_rst_meta_inst_0 => rd_rst_meta_inst,
      rit_detect_d0 => rit_detect_d0,
      \rit_register_reg[0]\(3 downto 0) => \rit_register_reg[0]\(3 downto 0),
      \s_axi_rdata_i_reg[31]\(31 downto 0) => \s_axi_rdata_i_reg[31]\(31 downto 0),
      sit_detect_d0 => sit_detect_d0,
      \write_nextgray_reg[2]_0\ => \write_nextgray_reg[2]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_mailbox_0_0_fsl_v20_0 is
  port (
    \out\ : out STD_LOGIC;
    \write_addr_reg[0]\ : out STD_LOGIC;
    sit_detect_d0 : out STD_LOGIC;
    rit_detect_d0 : out STD_LOGIC;
    DataOut : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S0_AXI_ACLK : in STD_LOGIC;
    S1_AXI_ARESETN : in STD_LOGIC;
    S1_AXI_ACLK : in STD_LOGIC;
    empty_allow : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Bus_RNW_reg : in STD_LOGIC;
    \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg\ : in STD_LOGIC;
    \rit_register_reg[0]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg\ : in STD_LOGIC;
    Bus_RNW_reg_0 : in STD_LOGIC;
    S1_AXI_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_mailbox_0_0_fsl_v20_0 : entity is "fsl_v20";
end design_1_mailbox_0_0_fsl_v20_0;

architecture STRUCTURE of design_1_mailbox_0_0_fsl_v20_0 is
begin
\Using_FIFO.Async_FIFO_Gen.Async_FIFO_I1\: entity work.design_1_mailbox_0_0_Async_FIFO
     port map (
      Bus_RNW_reg => Bus_RNW_reg,
      Bus_RNW_reg_0 => Bus_RNW_reg_0,
      DataOut(31 downto 0) => DataOut(31 downto 0),
      E(0) => E(0),
      \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg\ => \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg\,
      \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg\ => \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg\,
      Q(3 downto 0) => Q(3 downto 0),
      S0_AXI_ACLK => S0_AXI_ACLK,
      S1_AXI_ACLK => S1_AXI_ACLK,
      S1_AXI_ARESETN => S1_AXI_ARESETN,
      S1_AXI_WDATA(31 downto 0) => S1_AXI_WDATA(31 downto 0),
      empty_allow => empty_allow,
      \out\ => \out\,
      rit_detect_d0 => rit_detect_d0,
      \rit_register_reg[0]\(3 downto 0) => \rit_register_reg[0]\(3 downto 0),
      sit_detect_d0 => sit_detect_d0,
      \write_addr_reg[0]_0\ => \write_addr_reg[0]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_mailbox_0_0_slave_attachment is
  port (
    \s_axi_rdata_i_reg[31]_0\ : out STD_LOGIC;
    \is_register_reg[2]\ : out STD_LOGIC;
    \read_nextgray_reg[2]\ : out STD_LOGIC;
    write_fsl_error_d1_reg : out STD_LOGIC;
    Bus_RNW_reg_reg : out STD_LOGIC;
    S0_AXI_RVALID : out STD_LOGIC;
    S0_AXI_BVALID : out STD_LOGIC;
    empty_allow : out STD_LOGIC;
    S0_AXI_ARREADY : out STD_LOGIC;
    error_detect : out STD_LOGIC;
    read_fsl_error : out STD_LOGIC;
    write_fsl_error : out STD_LOGIC;
    S0_AXI_WREADY : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \sit_register_reg[3]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    full_error_reg : out STD_LOGIC;
    empty_error_reg : out STD_LOGIC;
    \read_nextgray_reg[2]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    S0_AXI_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    Rst : in STD_LOGIC;
    S0_AXI_ACLK : in STD_LOGIC;
    \out\ : in STD_LOGIC;
    S0_AXI_ARVALID : in STD_LOGIC;
    S0_AXI_AWVALID : in STD_LOGIC;
    S0_AXI_WVALID : in STD_LOGIC;
    S0_AXI_ARADDR : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S0_AXI_AWADDR : in STD_LOGIC_VECTOR ( 3 downto 0 );
    write_fsl_error_d1 : in STD_LOGIC;
    full_i_reg : in STD_LOGIC;
    read_fsl_error_d1 : in STD_LOGIC;
    rit_detect_d0 : in STD_LOGIC;
    sit_detect_d0 : in STD_LOGIC;
    full_error : in STD_LOGIC;
    empty_error : in STD_LOGIC;
    S0_AXI_RREADY : in STD_LOGIC;
    S0_AXI_BREADY : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    p_6_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \rit_register_reg[0]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \DataOut_reg[31]\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ie_register : in STD_LOGIC_VECTOR ( 0 to 2 )
  );
end design_1_mailbox_0_0_slave_attachment;

architecture STRUCTURE of design_1_mailbox_0_0_slave_attachment is
  signal \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal I_DECODER_n_12 : STD_LOGIC;
  signal I_DECODER_n_13 : STD_LOGIC;
  signal I_DECODER_n_14 : STD_LOGIC;
  signal I_DECODER_n_15 : STD_LOGIC;
  signal I_DECODER_n_16 : STD_LOGIC;
  signal I_DECODER_n_17 : STD_LOGIC;
  signal I_DECODER_n_18 : STD_LOGIC;
  signal I_DECODER_n_19 : STD_LOGIC;
  signal I_DECODER_n_20 : STD_LOGIC;
  signal I_DECODER_n_21 : STD_LOGIC;
  signal I_DECODER_n_22 : STD_LOGIC;
  signal I_DECODER_n_23 : STD_LOGIC;
  signal I_DECODER_n_24 : STD_LOGIC;
  signal I_DECODER_n_25 : STD_LOGIC;
  signal I_DECODER_n_26 : STD_LOGIC;
  signal I_DECODER_n_27 : STD_LOGIC;
  signal I_DECODER_n_28 : STD_LOGIC;
  signal I_DECODER_n_29 : STD_LOGIC;
  signal I_DECODER_n_30 : STD_LOGIC;
  signal I_DECODER_n_31 : STD_LOGIC;
  signal I_DECODER_n_32 : STD_LOGIC;
  signal I_DECODER_n_33 : STD_LOGIC;
  signal I_DECODER_n_34 : STD_LOGIC;
  signal I_DECODER_n_35 : STD_LOGIC;
  signal I_DECODER_n_36 : STD_LOGIC;
  signal I_DECODER_n_37 : STD_LOGIC;
  signal I_DECODER_n_38 : STD_LOGIC;
  signal I_DECODER_n_39 : STD_LOGIC;
  signal I_DECODER_n_40 : STD_LOGIC;
  signal I_DECODER_n_41 : STD_LOGIC;
  signal I_DECODER_n_42 : STD_LOGIC;
  signal I_DECODER_n_43 : STD_LOGIC;
  signal I_DECODER_n_47 : STD_LOGIC;
  signal I_DECODER_n_48 : STD_LOGIC;
  signal I_DECODER_n_6 : STD_LOGIC;
  signal I_DECODER_n_7 : STD_LOGIC;
  signal \^s0_axi_bvalid\ : STD_LOGIC;
  signal \^s0_axi_rvalid\ : STD_LOGIC;
  signal \bus2ip_addr_i[5]_i_1__0_n_0\ : STD_LOGIC;
  signal \bus2ip_addr_i_reg_n_0_[2]\ : STD_LOGIC;
  signal \bus2ip_addr_i_reg_n_0_[3]\ : STD_LOGIC;
  signal \bus2ip_addr_i_reg_n_0_[4]\ : STD_LOGIC;
  signal \bus2ip_addr_i_reg_n_0_[5]\ : STD_LOGIC;
  signal bus2ip_rnw_i : STD_LOGIC;
  signal bus2ip_rnw_i06_out : STD_LOGIC;
  signal clear : STD_LOGIC;
  signal is_read : STD_LOGIC;
  signal \is_read_i_1__0_n_0\ : STD_LOGIC;
  signal is_write : STD_LOGIC;
  signal \is_write_i_1__0_n_0\ : STD_LOGIC;
  signal is_write_reg_n_0 : STD_LOGIC;
  signal p_1_in : STD_LOGIC_VECTOR ( 5 downto 2 );
  signal plusOp : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \^rst\ : STD_LOGIC;
  signal \s_axi_rdata_i[31]_i_1__0_n_0\ : STD_LOGIC;
  signal start2 : STD_LOGIC;
  signal start2_i_1_n_0 : STD_LOGIC;
  signal state : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \state1__2\ : STD_LOGIC;
  signal \state[1]_i_3__0_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \INCLUDE_DPHASE_TIMER.dpto_cnt[1]_i_1__0\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \INCLUDE_DPHASE_TIMER.dpto_cnt[2]_i_1__0\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \INCLUDE_DPHASE_TIMER.dpto_cnt[3]_i_1__0\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_2__0\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \bus2ip_addr_i[3]_i_1__0\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \bus2ip_rnw_i_i_1__0\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of start2_i_1 : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \state[1]_i_3__0\ : label is "soft_lutpair11";
begin
  S0_AXI_BVALID <= \^s0_axi_bvalid\;
  S0_AXI_RVALID <= \^s0_axi_rvalid\;
\INCLUDE_DPHASE_TIMER.dpto_cnt[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(0),
      O => plusOp(0)
    );
\INCLUDE_DPHASE_TIMER.dpto_cnt[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(0),
      I1 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(1),
      O => plusOp(1)
    );
\INCLUDE_DPHASE_TIMER.dpto_cnt[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(0),
      I1 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(1),
      I2 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(2),
      O => plusOp(2)
    );
\INCLUDE_DPHASE_TIMER.dpto_cnt[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(1),
      I1 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(0),
      I2 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(2),
      I3 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(3),
      O => plusOp(3)
    );
\INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => state(0),
      I1 => state(1),
      O => clear
    );
\INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(2),
      I1 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(0),
      I2 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(1),
      I3 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(3),
      I4 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(4),
      O => plusOp(4)
    );
\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => plusOp(0),
      Q => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(0),
      R => clear
    );
\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => plusOp(1),
      Q => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(1),
      R => clear
    );
\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => plusOp(2),
      Q => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(2),
      R => clear
    );
\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => plusOp(3),
      Q => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(3),
      R => clear
    );
\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => plusOp(4),
      Q => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(4),
      R => clear
    );
I_DECODER: entity work.design_1_mailbox_0_0_address_decoder
     port map (
      Bus_RNW_reg_reg_0 => Bus_RNW_reg_reg,
      D(1) => I_DECODER_n_6,
      D(0) => I_DECODER_n_7,
      \DataOut_reg[31]\(31 downto 0) => \DataOut_reg[31]\(31 downto 0),
      E(0) => E(0),
      \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4]\(4 downto 0) => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(4 downto 0),
      Q => start2,
      Rst => Rst,
      S0_AXI_ACLK => S0_AXI_ACLK,
      S0_AXI_ARREADY => S0_AXI_ARREADY,
      S0_AXI_ARVALID => S0_AXI_ARVALID,
      S0_AXI_ARVALID_0 => \state[1]_i_3__0_n_0\,
      S0_AXI_BREADY => S0_AXI_BREADY,
      S0_AXI_RREADY => S0_AXI_RREADY,
      S0_AXI_WREADY => S0_AXI_WREADY,
      \bus2ip_addr_i_reg[5]\(3) => \bus2ip_addr_i_reg_n_0_[5]\,
      \bus2ip_addr_i_reg[5]\(2) => \bus2ip_addr_i_reg_n_0_[4]\,
      \bus2ip_addr_i_reg[5]\(1) => \bus2ip_addr_i_reg_n_0_[3]\,
      \bus2ip_addr_i_reg[5]\(0) => \bus2ip_addr_i_reg_n_0_[2]\,
      bus2ip_rnw_i => bus2ip_rnw_i,
      empty_allow => empty_allow,
      empty_error => empty_error,
      empty_error_reg => empty_error_reg,
      error_detect => error_detect,
      full_error => full_error,
      full_error_reg => full_error_reg,
      full_i_reg => full_i_reg,
      ie_register(0 to 2) => ie_register(0 to 2),
      is_read => is_read,
      \is_register_reg[2]\ => \is_register_reg[2]\,
      is_write_reg => is_write_reg_n_0,
      \out\ => \out\,
      p_6_in(2 downto 0) => p_6_in(2 downto 0),
      read_fsl_error => read_fsl_error,
      read_fsl_error_d1 => read_fsl_error_d1,
      \read_nextgray_reg[2]\ => \read_nextgray_reg[2]\,
      \read_nextgray_reg[2]_0\(0) => \read_nextgray_reg[2]_0\(0),
      rit_detect_d0 => rit_detect_d0,
      \rit_register_reg[0]\(3 downto 0) => \rit_register_reg[0]\(3 downto 0),
      s_axi_bvalid_i_reg => I_DECODER_n_48,
      s_axi_bvalid_i_reg_0 => \^s0_axi_bvalid\,
      \s_axi_rdata_i_reg[31]\ => \s_axi_rdata_i_reg[31]_0\,
      \s_axi_rdata_i_reg[31]_0\(31) => I_DECODER_n_12,
      \s_axi_rdata_i_reg[31]_0\(30) => I_DECODER_n_13,
      \s_axi_rdata_i_reg[31]_0\(29) => I_DECODER_n_14,
      \s_axi_rdata_i_reg[31]_0\(28) => I_DECODER_n_15,
      \s_axi_rdata_i_reg[31]_0\(27) => I_DECODER_n_16,
      \s_axi_rdata_i_reg[31]_0\(26) => I_DECODER_n_17,
      \s_axi_rdata_i_reg[31]_0\(25) => I_DECODER_n_18,
      \s_axi_rdata_i_reg[31]_0\(24) => I_DECODER_n_19,
      \s_axi_rdata_i_reg[31]_0\(23) => I_DECODER_n_20,
      \s_axi_rdata_i_reg[31]_0\(22) => I_DECODER_n_21,
      \s_axi_rdata_i_reg[31]_0\(21) => I_DECODER_n_22,
      \s_axi_rdata_i_reg[31]_0\(20) => I_DECODER_n_23,
      \s_axi_rdata_i_reg[31]_0\(19) => I_DECODER_n_24,
      \s_axi_rdata_i_reg[31]_0\(18) => I_DECODER_n_25,
      \s_axi_rdata_i_reg[31]_0\(17) => I_DECODER_n_26,
      \s_axi_rdata_i_reg[31]_0\(16) => I_DECODER_n_27,
      \s_axi_rdata_i_reg[31]_0\(15) => I_DECODER_n_28,
      \s_axi_rdata_i_reg[31]_0\(14) => I_DECODER_n_29,
      \s_axi_rdata_i_reg[31]_0\(13) => I_DECODER_n_30,
      \s_axi_rdata_i_reg[31]_0\(12) => I_DECODER_n_31,
      \s_axi_rdata_i_reg[31]_0\(11) => I_DECODER_n_32,
      \s_axi_rdata_i_reg[31]_0\(10) => I_DECODER_n_33,
      \s_axi_rdata_i_reg[31]_0\(9) => I_DECODER_n_34,
      \s_axi_rdata_i_reg[31]_0\(8) => I_DECODER_n_35,
      \s_axi_rdata_i_reg[31]_0\(7) => I_DECODER_n_36,
      \s_axi_rdata_i_reg[31]_0\(6) => I_DECODER_n_37,
      \s_axi_rdata_i_reg[31]_0\(5) => I_DECODER_n_38,
      \s_axi_rdata_i_reg[31]_0\(4) => I_DECODER_n_39,
      \s_axi_rdata_i_reg[31]_0\(3) => I_DECODER_n_40,
      \s_axi_rdata_i_reg[31]_0\(2) => I_DECODER_n_41,
      \s_axi_rdata_i_reg[31]_0\(1) => I_DECODER_n_42,
      \s_axi_rdata_i_reg[31]_0\(0) => I_DECODER_n_43,
      s_axi_rvalid_i_reg => I_DECODER_n_47,
      s_axi_rvalid_i_reg_0 => \^s0_axi_rvalid\,
      sit_detect_d0 => sit_detect_d0,
      \sit_register_reg[0]\(3 downto 0) => Q(3 downto 0),
      \sit_register_reg[3]\(0) => \sit_register_reg[3]\(0),
      \state1__2\ => \state1__2\,
      \state_reg[1]\(1 downto 0) => state(1 downto 0),
      write_fsl_error => write_fsl_error,
      write_fsl_error_d1 => write_fsl_error_d1,
      write_fsl_error_d1_reg => write_fsl_error_d1_reg
    );
\bus2ip_addr_i[2]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCCACCCC"
    )
        port map (
      I0 => S0_AXI_ARADDR(0),
      I1 => S0_AXI_AWADDR(0),
      I2 => state(0),
      I3 => state(1),
      I4 => S0_AXI_ARVALID,
      O => p_1_in(2)
    );
\bus2ip_addr_i[3]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCCACCCC"
    )
        port map (
      I0 => S0_AXI_ARADDR(1),
      I1 => S0_AXI_AWADDR(1),
      I2 => state(0),
      I3 => state(1),
      I4 => S0_AXI_ARVALID,
      O => p_1_in(3)
    );
\bus2ip_addr_i[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCCACCCC"
    )
        port map (
      I0 => S0_AXI_ARADDR(2),
      I1 => S0_AXI_AWADDR(2),
      I2 => state(0),
      I3 => state(1),
      I4 => S0_AXI_ARVALID,
      O => p_1_in(4)
    );
\bus2ip_addr_i[5]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000000EA"
    )
        port map (
      I0 => S0_AXI_ARVALID,
      I1 => S0_AXI_AWVALID,
      I2 => S0_AXI_WVALID,
      I3 => state(1),
      I4 => state(0),
      O => \bus2ip_addr_i[5]_i_1__0_n_0\
    );
\bus2ip_addr_i[5]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCCACCCC"
    )
        port map (
      I0 => S0_AXI_ARADDR(3),
      I1 => S0_AXI_AWADDR(3),
      I2 => state(0),
      I3 => state(1),
      I4 => S0_AXI_ARVALID,
      O => p_1_in(5)
    );
\bus2ip_addr_i_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => \bus2ip_addr_i[5]_i_1__0_n_0\,
      D => p_1_in(2),
      Q => \bus2ip_addr_i_reg_n_0_[2]\,
      R => \^rst\
    );
\bus2ip_addr_i_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => \bus2ip_addr_i[5]_i_1__0_n_0\,
      D => p_1_in(3),
      Q => \bus2ip_addr_i_reg_n_0_[3]\,
      R => \^rst\
    );
\bus2ip_addr_i_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => \bus2ip_addr_i[5]_i_1__0_n_0\,
      D => p_1_in(4),
      Q => \bus2ip_addr_i_reg_n_0_[4]\,
      R => \^rst\
    );
\bus2ip_addr_i_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => \bus2ip_addr_i[5]_i_1__0_n_0\,
      D => p_1_in(5),
      Q => \bus2ip_addr_i_reg_n_0_[5]\,
      R => \^rst\
    );
\bus2ip_rnw_i_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"10"
    )
        port map (
      I0 => state(0),
      I1 => state(1),
      I2 => S0_AXI_ARVALID,
      O => bus2ip_rnw_i06_out
    );
bus2ip_rnw_i_reg: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => \bus2ip_addr_i[5]_i_1__0_n_0\,
      D => bus2ip_rnw_i06_out,
      Q => bus2ip_rnw_i,
      R => \^rst\
    );
\is_read_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"3FFA000A"
    )
        port map (
      I0 => S0_AXI_ARVALID,
      I1 => \state1__2\,
      I2 => state(0),
      I3 => state(1),
      I4 => is_read,
      O => \is_read_i_1__0_n_0\
    );
is_read_reg: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \is_read_i_1__0_n_0\,
      Q => is_read,
      R => \^rst\
    );
\is_write_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0040FFFF00400000"
    )
        port map (
      I0 => S0_AXI_ARVALID,
      I1 => S0_AXI_AWVALID,
      I2 => S0_AXI_WVALID,
      I3 => state(1),
      I4 => is_write,
      I5 => is_write_reg_n_0,
      O => \is_write_i_1__0_n_0\
    );
\is_write_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F88800000000FFFF"
    )
        port map (
      I0 => \^s0_axi_rvalid\,
      I1 => S0_AXI_RREADY,
      I2 => \^s0_axi_bvalid\,
      I3 => S0_AXI_BREADY,
      I4 => state(0),
      I5 => state(1),
      O => is_write
    );
is_write_reg: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \is_write_i_1__0_n_0\,
      Q => is_write_reg_n_0,
      R => \^rst\
    );
rst_reg: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => Rst,
      Q => \^rst\,
      R => '0'
    );
s_axi_bvalid_i_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => I_DECODER_n_48,
      Q => \^s0_axi_bvalid\,
      R => \^rst\
    );
\s_axi_rdata_i[31]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => state(0),
      I1 => state(1),
      O => \s_axi_rdata_i[31]_i_1__0_n_0\
    );
\s_axi_rdata_i_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_43,
      Q => S0_AXI_RDATA(0),
      R => \^rst\
    );
\s_axi_rdata_i_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_33,
      Q => S0_AXI_RDATA(10),
      R => \^rst\
    );
\s_axi_rdata_i_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_32,
      Q => S0_AXI_RDATA(11),
      R => \^rst\
    );
\s_axi_rdata_i_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_31,
      Q => S0_AXI_RDATA(12),
      R => \^rst\
    );
\s_axi_rdata_i_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_30,
      Q => S0_AXI_RDATA(13),
      R => \^rst\
    );
\s_axi_rdata_i_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_29,
      Q => S0_AXI_RDATA(14),
      R => \^rst\
    );
\s_axi_rdata_i_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_28,
      Q => S0_AXI_RDATA(15),
      R => \^rst\
    );
\s_axi_rdata_i_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_27,
      Q => S0_AXI_RDATA(16),
      R => \^rst\
    );
\s_axi_rdata_i_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_26,
      Q => S0_AXI_RDATA(17),
      R => \^rst\
    );
\s_axi_rdata_i_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_25,
      Q => S0_AXI_RDATA(18),
      R => \^rst\
    );
\s_axi_rdata_i_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_24,
      Q => S0_AXI_RDATA(19),
      R => \^rst\
    );
\s_axi_rdata_i_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_42,
      Q => S0_AXI_RDATA(1),
      R => \^rst\
    );
\s_axi_rdata_i_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_23,
      Q => S0_AXI_RDATA(20),
      R => \^rst\
    );
\s_axi_rdata_i_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_22,
      Q => S0_AXI_RDATA(21),
      R => \^rst\
    );
\s_axi_rdata_i_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_21,
      Q => S0_AXI_RDATA(22),
      R => \^rst\
    );
\s_axi_rdata_i_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_20,
      Q => S0_AXI_RDATA(23),
      R => \^rst\
    );
\s_axi_rdata_i_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_19,
      Q => S0_AXI_RDATA(24),
      R => \^rst\
    );
\s_axi_rdata_i_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_18,
      Q => S0_AXI_RDATA(25),
      R => \^rst\
    );
\s_axi_rdata_i_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_17,
      Q => S0_AXI_RDATA(26),
      R => \^rst\
    );
\s_axi_rdata_i_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_16,
      Q => S0_AXI_RDATA(27),
      R => \^rst\
    );
\s_axi_rdata_i_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_15,
      Q => S0_AXI_RDATA(28),
      R => \^rst\
    );
\s_axi_rdata_i_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_14,
      Q => S0_AXI_RDATA(29),
      R => \^rst\
    );
\s_axi_rdata_i_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_41,
      Q => S0_AXI_RDATA(2),
      R => \^rst\
    );
\s_axi_rdata_i_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_13,
      Q => S0_AXI_RDATA(30),
      R => \^rst\
    );
\s_axi_rdata_i_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_12,
      Q => S0_AXI_RDATA(31),
      R => \^rst\
    );
\s_axi_rdata_i_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_40,
      Q => S0_AXI_RDATA(3),
      R => \^rst\
    );
\s_axi_rdata_i_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_39,
      Q => S0_AXI_RDATA(4),
      R => \^rst\
    );
\s_axi_rdata_i_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_38,
      Q => S0_AXI_RDATA(5),
      R => \^rst\
    );
\s_axi_rdata_i_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_37,
      Q => S0_AXI_RDATA(6),
      R => \^rst\
    );
\s_axi_rdata_i_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_36,
      Q => S0_AXI_RDATA(7),
      R => \^rst\
    );
\s_axi_rdata_i_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_35,
      Q => S0_AXI_RDATA(8),
      R => \^rst\
    );
\s_axi_rdata_i_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1__0_n_0\,
      D => I_DECODER_n_34,
      Q => S0_AXI_RDATA(9),
      R => \^rst\
    );
s_axi_rvalid_i_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => I_DECODER_n_47,
      Q => \^s0_axi_rvalid\,
      R => \^rst\
    );
start2_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000000F8"
    )
        port map (
      I0 => S0_AXI_AWVALID,
      I1 => S0_AXI_WVALID,
      I2 => S0_AXI_ARVALID,
      I3 => state(1),
      I4 => state(0),
      O => start2_i_1_n_0
    );
start2_reg: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => start2_i_1_n_0,
      Q => start2,
      R => \^rst\
    );
\state[1]_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => S0_AXI_BREADY,
      I1 => \^s0_axi_bvalid\,
      I2 => S0_AXI_RREADY,
      I3 => \^s0_axi_rvalid\,
      O => \state1__2\
    );
\state[1]_i_3__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => S0_AXI_WVALID,
      I1 => S0_AXI_AWVALID,
      I2 => S0_AXI_ARVALID,
      O => \state[1]_i_3__0_n_0\
    );
\state_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => I_DECODER_n_7,
      Q => state(0),
      R => \^rst\
    );
\state_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => I_DECODER_n_6,
      Q => state(1),
      R => \^rst\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_mailbox_0_0_slave_attachment__parameterized0\ is
  port (
    \s_axi_rdata_i_reg[31]_0\ : out STD_LOGIC;
    \is_register_reg[2]\ : out STD_LOGIC;
    \read_nextgray_reg[2]\ : out STD_LOGIC;
    write_fsl_error_d1_reg : out STD_LOGIC;
    Bus_RNW_reg_reg : out STD_LOGIC;
    S1_AXI_RVALID : out STD_LOGIC;
    S1_AXI_BVALID : out STD_LOGIC;
    empty_allow : out STD_LOGIC;
    S1_AXI_ARREADY : out STD_LOGIC;
    error_detect : out STD_LOGIC;
    read_fsl_error : out STD_LOGIC;
    write_fsl_error : out STD_LOGIC;
    S1_AXI_WREADY : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \sit_register_reg[3]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    full_error_reg : out STD_LOGIC;
    empty_error_reg : out STD_LOGIC;
    \read_nextgray_reg[2]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    S1_AXI_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \Rst_Async.SYS_Rst_If1_reg\ : in STD_LOGIC;
    S1_AXI_ACLK : in STD_LOGIC;
    \out\ : in STD_LOGIC;
    S1_AXI_ARVALID : in STD_LOGIC;
    S1_AXI_AWVALID : in STD_LOGIC;
    S1_AXI_WVALID : in STD_LOGIC;
    S1_AXI_ARADDR : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S1_AXI_AWADDR : in STD_LOGIC_VECTOR ( 3 downto 0 );
    write_fsl_error_d1 : in STD_LOGIC;
    full_i_reg : in STD_LOGIC;
    read_fsl_error_d1 : in STD_LOGIC;
    rit_detect_d0 : in STD_LOGIC;
    sit_detect_d0 : in STD_LOGIC;
    full_error_reg_0 : in STD_LOGIC;
    empty_error_reg_0 : in STD_LOGIC;
    S1_AXI_RREADY : in STD_LOGIC;
    S1_AXI_BREADY : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    p_6_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \rit_register_reg[0]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \DataOut_reg[31]\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ie_register : in STD_LOGIC_VECTOR ( 0 to 2 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_mailbox_0_0_slave_attachment__parameterized0\ : entity is "slave_attachment";
end \design_1_mailbox_0_0_slave_attachment__parameterized0\;

architecture STRUCTURE of \design_1_mailbox_0_0_slave_attachment__parameterized0\ is
  signal \INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1_n_0\ : STD_LOGIC;
  signal \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal I_DECODER_n_12 : STD_LOGIC;
  signal I_DECODER_n_13 : STD_LOGIC;
  signal I_DECODER_n_14 : STD_LOGIC;
  signal I_DECODER_n_15 : STD_LOGIC;
  signal I_DECODER_n_16 : STD_LOGIC;
  signal I_DECODER_n_17 : STD_LOGIC;
  signal I_DECODER_n_18 : STD_LOGIC;
  signal I_DECODER_n_19 : STD_LOGIC;
  signal I_DECODER_n_20 : STD_LOGIC;
  signal I_DECODER_n_21 : STD_LOGIC;
  signal I_DECODER_n_22 : STD_LOGIC;
  signal I_DECODER_n_23 : STD_LOGIC;
  signal I_DECODER_n_24 : STD_LOGIC;
  signal I_DECODER_n_25 : STD_LOGIC;
  signal I_DECODER_n_26 : STD_LOGIC;
  signal I_DECODER_n_27 : STD_LOGIC;
  signal I_DECODER_n_28 : STD_LOGIC;
  signal I_DECODER_n_29 : STD_LOGIC;
  signal I_DECODER_n_30 : STD_LOGIC;
  signal I_DECODER_n_31 : STD_LOGIC;
  signal I_DECODER_n_32 : STD_LOGIC;
  signal I_DECODER_n_33 : STD_LOGIC;
  signal I_DECODER_n_34 : STD_LOGIC;
  signal I_DECODER_n_35 : STD_LOGIC;
  signal I_DECODER_n_36 : STD_LOGIC;
  signal I_DECODER_n_37 : STD_LOGIC;
  signal I_DECODER_n_38 : STD_LOGIC;
  signal I_DECODER_n_39 : STD_LOGIC;
  signal I_DECODER_n_40 : STD_LOGIC;
  signal I_DECODER_n_41 : STD_LOGIC;
  signal I_DECODER_n_42 : STD_LOGIC;
  signal I_DECODER_n_43 : STD_LOGIC;
  signal I_DECODER_n_47 : STD_LOGIC;
  signal I_DECODER_n_48 : STD_LOGIC;
  signal I_DECODER_n_6 : STD_LOGIC;
  signal I_DECODER_n_7 : STD_LOGIC;
  signal \^s1_axi_bvalid\ : STD_LOGIC;
  signal \^s1_axi_rvalid\ : STD_LOGIC;
  signal \bus2ip_addr_i[2]_i_1_n_0\ : STD_LOGIC;
  signal \bus2ip_addr_i[3]_i_1_n_0\ : STD_LOGIC;
  signal \bus2ip_addr_i[4]_i_1_n_0\ : STD_LOGIC;
  signal \bus2ip_addr_i[5]_i_1_n_0\ : STD_LOGIC;
  signal \bus2ip_addr_i[5]_i_2_n_0\ : STD_LOGIC;
  signal \bus2ip_addr_i_reg_n_0_[2]\ : STD_LOGIC;
  signal \bus2ip_addr_i_reg_n_0_[3]\ : STD_LOGIC;
  signal \bus2ip_addr_i_reg_n_0_[4]\ : STD_LOGIC;
  signal \bus2ip_addr_i_reg_n_0_[5]\ : STD_LOGIC;
  signal bus2ip_rnw_i06_out : STD_LOGIC;
  signal bus2ip_rnw_i_reg_n_0 : STD_LOGIC;
  signal is_read : STD_LOGIC;
  signal is_read_i_1_n_0 : STD_LOGIC;
  signal is_write : STD_LOGIC;
  signal is_write_i_1_n_0 : STD_LOGIC;
  signal is_write_reg_n_0 : STD_LOGIC;
  signal plusOp : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal rst : STD_LOGIC;
  signal \s_axi_rdata_i[31]_i_1_n_0\ : STD_LOGIC;
  signal start2 : STD_LOGIC;
  signal start2_i_1_n_0 : STD_LOGIC;
  signal state : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \state1__2\ : STD_LOGIC;
  signal \state[1]_i_3_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \INCLUDE_DPHASE_TIMER.dpto_cnt[1]_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \INCLUDE_DPHASE_TIMER.dpto_cnt[2]_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \INCLUDE_DPHASE_TIMER.dpto_cnt[3]_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_2\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \bus2ip_addr_i[3]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of bus2ip_rnw_i_i_1 : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of start2_i_1 : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \state[1]_i_3\ : label is "soft_lutpair25";
begin
  S1_AXI_BVALID <= \^s1_axi_bvalid\;
  S1_AXI_RVALID <= \^s1_axi_rvalid\;
\INCLUDE_DPHASE_TIMER.dpto_cnt[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(0),
      O => plusOp(0)
    );
\INCLUDE_DPHASE_TIMER.dpto_cnt[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(0),
      I1 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(1),
      O => plusOp(1)
    );
\INCLUDE_DPHASE_TIMER.dpto_cnt[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(0),
      I1 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(1),
      I2 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(2),
      O => plusOp(2)
    );
\INCLUDE_DPHASE_TIMER.dpto_cnt[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(1),
      I1 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(0),
      I2 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(2),
      I3 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(3),
      O => plusOp(3)
    );
\INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => state(0),
      I1 => state(1),
      O => \INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1_n_0\
    );
\INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(2),
      I1 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(0),
      I2 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(1),
      I3 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(3),
      I4 => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(4),
      O => plusOp(4)
    );
\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => plusOp(0),
      Q => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(0),
      R => \INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1_n_0\
    );
\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => plusOp(1),
      Q => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(1),
      R => \INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1_n_0\
    );
\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => plusOp(2),
      Q => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(2),
      R => \INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1_n_0\
    );
\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => plusOp(3),
      Q => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(3),
      R => \INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1_n_0\
    );
\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => plusOp(4),
      Q => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(4),
      R => \INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1_n_0\
    );
I_DECODER: entity work.\design_1_mailbox_0_0_address_decoder__parameterized0\
     port map (
      Bus_RNW_reg_reg_0 => Bus_RNW_reg_reg,
      D(1) => I_DECODER_n_6,
      D(0) => I_DECODER_n_7,
      \DataOut_reg[31]\(31 downto 0) => \DataOut_reg[31]\(31 downto 0),
      E(0) => E(0),
      \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4]\(4 downto 0) => \INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0\(4 downto 0),
      Q => start2,
      \Rst_Async.SYS_Rst_If1_reg\ => \Rst_Async.SYS_Rst_If1_reg\,
      S1_AXI_ACLK => S1_AXI_ACLK,
      S1_AXI_ARREADY => S1_AXI_ARREADY,
      S1_AXI_ARVALID => S1_AXI_ARVALID,
      S1_AXI_ARVALID_0 => \state[1]_i_3_n_0\,
      S1_AXI_BREADY => S1_AXI_BREADY,
      S1_AXI_RREADY => S1_AXI_RREADY,
      S1_AXI_WREADY => S1_AXI_WREADY,
      \bus2ip_addr_i_reg[5]\(3) => \bus2ip_addr_i_reg_n_0_[5]\,
      \bus2ip_addr_i_reg[5]\(2) => \bus2ip_addr_i_reg_n_0_[4]\,
      \bus2ip_addr_i_reg[5]\(1) => \bus2ip_addr_i_reg_n_0_[3]\,
      \bus2ip_addr_i_reg[5]\(0) => \bus2ip_addr_i_reg_n_0_[2]\,
      bus2ip_rnw_i_reg => bus2ip_rnw_i_reg_n_0,
      empty_allow => empty_allow,
      empty_error_reg => empty_error_reg,
      empty_error_reg_0 => empty_error_reg_0,
      error_detect => error_detect,
      full_error_reg => full_error_reg,
      full_error_reg_0 => full_error_reg_0,
      full_i_reg => full_i_reg,
      ie_register(0 to 2) => ie_register(0 to 2),
      is_read => is_read,
      \is_register_reg[2]\ => \is_register_reg[2]\,
      is_write_reg => is_write_reg_n_0,
      \out\ => \out\,
      p_6_in(2 downto 0) => p_6_in(2 downto 0),
      read_fsl_error => read_fsl_error,
      read_fsl_error_d1 => read_fsl_error_d1,
      \read_nextgray_reg[2]\ => \read_nextgray_reg[2]\,
      \read_nextgray_reg[2]_0\(0) => \read_nextgray_reg[2]_0\(0),
      rit_detect_d0 => rit_detect_d0,
      \rit_register_reg[0]\(3 downto 0) => \rit_register_reg[0]\(3 downto 0),
      s_axi_bvalid_i_reg => I_DECODER_n_48,
      s_axi_bvalid_i_reg_0 => \^s1_axi_bvalid\,
      \s_axi_rdata_i_reg[31]\ => \s_axi_rdata_i_reg[31]_0\,
      \s_axi_rdata_i_reg[31]_0\(31) => I_DECODER_n_12,
      \s_axi_rdata_i_reg[31]_0\(30) => I_DECODER_n_13,
      \s_axi_rdata_i_reg[31]_0\(29) => I_DECODER_n_14,
      \s_axi_rdata_i_reg[31]_0\(28) => I_DECODER_n_15,
      \s_axi_rdata_i_reg[31]_0\(27) => I_DECODER_n_16,
      \s_axi_rdata_i_reg[31]_0\(26) => I_DECODER_n_17,
      \s_axi_rdata_i_reg[31]_0\(25) => I_DECODER_n_18,
      \s_axi_rdata_i_reg[31]_0\(24) => I_DECODER_n_19,
      \s_axi_rdata_i_reg[31]_0\(23) => I_DECODER_n_20,
      \s_axi_rdata_i_reg[31]_0\(22) => I_DECODER_n_21,
      \s_axi_rdata_i_reg[31]_0\(21) => I_DECODER_n_22,
      \s_axi_rdata_i_reg[31]_0\(20) => I_DECODER_n_23,
      \s_axi_rdata_i_reg[31]_0\(19) => I_DECODER_n_24,
      \s_axi_rdata_i_reg[31]_0\(18) => I_DECODER_n_25,
      \s_axi_rdata_i_reg[31]_0\(17) => I_DECODER_n_26,
      \s_axi_rdata_i_reg[31]_0\(16) => I_DECODER_n_27,
      \s_axi_rdata_i_reg[31]_0\(15) => I_DECODER_n_28,
      \s_axi_rdata_i_reg[31]_0\(14) => I_DECODER_n_29,
      \s_axi_rdata_i_reg[31]_0\(13) => I_DECODER_n_30,
      \s_axi_rdata_i_reg[31]_0\(12) => I_DECODER_n_31,
      \s_axi_rdata_i_reg[31]_0\(11) => I_DECODER_n_32,
      \s_axi_rdata_i_reg[31]_0\(10) => I_DECODER_n_33,
      \s_axi_rdata_i_reg[31]_0\(9) => I_DECODER_n_34,
      \s_axi_rdata_i_reg[31]_0\(8) => I_DECODER_n_35,
      \s_axi_rdata_i_reg[31]_0\(7) => I_DECODER_n_36,
      \s_axi_rdata_i_reg[31]_0\(6) => I_DECODER_n_37,
      \s_axi_rdata_i_reg[31]_0\(5) => I_DECODER_n_38,
      \s_axi_rdata_i_reg[31]_0\(4) => I_DECODER_n_39,
      \s_axi_rdata_i_reg[31]_0\(3) => I_DECODER_n_40,
      \s_axi_rdata_i_reg[31]_0\(2) => I_DECODER_n_41,
      \s_axi_rdata_i_reg[31]_0\(1) => I_DECODER_n_42,
      \s_axi_rdata_i_reg[31]_0\(0) => I_DECODER_n_43,
      s_axi_rvalid_i_reg => I_DECODER_n_47,
      s_axi_rvalid_i_reg_0 => \^s1_axi_rvalid\,
      sit_detect_d0 => sit_detect_d0,
      \sit_register_reg[0]\(3 downto 0) => Q(3 downto 0),
      \sit_register_reg[3]\(0) => \sit_register_reg[3]\(0),
      \state1__2\ => \state1__2\,
      \state_reg[1]\(1 downto 0) => state(1 downto 0),
      write_fsl_error => write_fsl_error,
      write_fsl_error_d1 => write_fsl_error_d1,
      write_fsl_error_d1_reg => write_fsl_error_d1_reg
    );
\bus2ip_addr_i[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCCACCCC"
    )
        port map (
      I0 => S1_AXI_ARADDR(0),
      I1 => S1_AXI_AWADDR(0),
      I2 => state(0),
      I3 => state(1),
      I4 => S1_AXI_ARVALID,
      O => \bus2ip_addr_i[2]_i_1_n_0\
    );
\bus2ip_addr_i[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCCACCCC"
    )
        port map (
      I0 => S1_AXI_ARADDR(1),
      I1 => S1_AXI_AWADDR(1),
      I2 => state(0),
      I3 => state(1),
      I4 => S1_AXI_ARVALID,
      O => \bus2ip_addr_i[3]_i_1_n_0\
    );
\bus2ip_addr_i[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCCACCCC"
    )
        port map (
      I0 => S1_AXI_ARADDR(2),
      I1 => S1_AXI_AWADDR(2),
      I2 => state(0),
      I3 => state(1),
      I4 => S1_AXI_ARVALID,
      O => \bus2ip_addr_i[4]_i_1_n_0\
    );
\bus2ip_addr_i[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000000EA"
    )
        port map (
      I0 => S1_AXI_ARVALID,
      I1 => S1_AXI_AWVALID,
      I2 => S1_AXI_WVALID,
      I3 => state(1),
      I4 => state(0),
      O => \bus2ip_addr_i[5]_i_1_n_0\
    );
\bus2ip_addr_i[5]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCCACCCC"
    )
        port map (
      I0 => S1_AXI_ARADDR(3),
      I1 => S1_AXI_AWADDR(3),
      I2 => state(0),
      I3 => state(1),
      I4 => S1_AXI_ARVALID,
      O => \bus2ip_addr_i[5]_i_2_n_0\
    );
\bus2ip_addr_i_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => \bus2ip_addr_i[5]_i_1_n_0\,
      D => \bus2ip_addr_i[2]_i_1_n_0\,
      Q => \bus2ip_addr_i_reg_n_0_[2]\,
      R => rst
    );
\bus2ip_addr_i_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => \bus2ip_addr_i[5]_i_1_n_0\,
      D => \bus2ip_addr_i[3]_i_1_n_0\,
      Q => \bus2ip_addr_i_reg_n_0_[3]\,
      R => rst
    );
\bus2ip_addr_i_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => \bus2ip_addr_i[5]_i_1_n_0\,
      D => \bus2ip_addr_i[4]_i_1_n_0\,
      Q => \bus2ip_addr_i_reg_n_0_[4]\,
      R => rst
    );
\bus2ip_addr_i_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => \bus2ip_addr_i[5]_i_1_n_0\,
      D => \bus2ip_addr_i[5]_i_2_n_0\,
      Q => \bus2ip_addr_i_reg_n_0_[5]\,
      R => rst
    );
bus2ip_rnw_i_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"10"
    )
        port map (
      I0 => state(0),
      I1 => state(1),
      I2 => S1_AXI_ARVALID,
      O => bus2ip_rnw_i06_out
    );
bus2ip_rnw_i_reg: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => \bus2ip_addr_i[5]_i_1_n_0\,
      D => bus2ip_rnw_i06_out,
      Q => bus2ip_rnw_i_reg_n_0,
      R => rst
    );
is_read_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"3FFA000A"
    )
        port map (
      I0 => S1_AXI_ARVALID,
      I1 => \state1__2\,
      I2 => state(0),
      I3 => state(1),
      I4 => is_read,
      O => is_read_i_1_n_0
    );
is_read_reg: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => is_read_i_1_n_0,
      Q => is_read,
      R => rst
    );
is_write_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0040FFFF00400000"
    )
        port map (
      I0 => S1_AXI_ARVALID,
      I1 => S1_AXI_AWVALID,
      I2 => S1_AXI_WVALID,
      I3 => state(1),
      I4 => is_write,
      I5 => is_write_reg_n_0,
      O => is_write_i_1_n_0
    );
is_write_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F88800000000FFFF"
    )
        port map (
      I0 => \^s1_axi_rvalid\,
      I1 => S1_AXI_RREADY,
      I2 => \^s1_axi_bvalid\,
      I3 => S1_AXI_BREADY,
      I4 => state(0),
      I5 => state(1),
      O => is_write
    );
is_write_reg: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => is_write_i_1_n_0,
      Q => is_write_reg_n_0,
      R => rst
    );
rst_reg: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \Rst_Async.SYS_Rst_If1_reg\,
      Q => rst,
      R => '0'
    );
s_axi_bvalid_i_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => I_DECODER_n_48,
      Q => \^s1_axi_bvalid\,
      R => rst
    );
\s_axi_rdata_i[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => state(0),
      I1 => state(1),
      O => \s_axi_rdata_i[31]_i_1_n_0\
    );
\s_axi_rdata_i_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_43,
      Q => S1_AXI_RDATA(0),
      R => rst
    );
\s_axi_rdata_i_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_33,
      Q => S1_AXI_RDATA(10),
      R => rst
    );
\s_axi_rdata_i_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_32,
      Q => S1_AXI_RDATA(11),
      R => rst
    );
\s_axi_rdata_i_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_31,
      Q => S1_AXI_RDATA(12),
      R => rst
    );
\s_axi_rdata_i_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_30,
      Q => S1_AXI_RDATA(13),
      R => rst
    );
\s_axi_rdata_i_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_29,
      Q => S1_AXI_RDATA(14),
      R => rst
    );
\s_axi_rdata_i_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_28,
      Q => S1_AXI_RDATA(15),
      R => rst
    );
\s_axi_rdata_i_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_27,
      Q => S1_AXI_RDATA(16),
      R => rst
    );
\s_axi_rdata_i_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_26,
      Q => S1_AXI_RDATA(17),
      R => rst
    );
\s_axi_rdata_i_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_25,
      Q => S1_AXI_RDATA(18),
      R => rst
    );
\s_axi_rdata_i_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_24,
      Q => S1_AXI_RDATA(19),
      R => rst
    );
\s_axi_rdata_i_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_42,
      Q => S1_AXI_RDATA(1),
      R => rst
    );
\s_axi_rdata_i_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_23,
      Q => S1_AXI_RDATA(20),
      R => rst
    );
\s_axi_rdata_i_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_22,
      Q => S1_AXI_RDATA(21),
      R => rst
    );
\s_axi_rdata_i_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_21,
      Q => S1_AXI_RDATA(22),
      R => rst
    );
\s_axi_rdata_i_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_20,
      Q => S1_AXI_RDATA(23),
      R => rst
    );
\s_axi_rdata_i_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_19,
      Q => S1_AXI_RDATA(24),
      R => rst
    );
\s_axi_rdata_i_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_18,
      Q => S1_AXI_RDATA(25),
      R => rst
    );
\s_axi_rdata_i_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_17,
      Q => S1_AXI_RDATA(26),
      R => rst
    );
\s_axi_rdata_i_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_16,
      Q => S1_AXI_RDATA(27),
      R => rst
    );
\s_axi_rdata_i_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_15,
      Q => S1_AXI_RDATA(28),
      R => rst
    );
\s_axi_rdata_i_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_14,
      Q => S1_AXI_RDATA(29),
      R => rst
    );
\s_axi_rdata_i_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_41,
      Q => S1_AXI_RDATA(2),
      R => rst
    );
\s_axi_rdata_i_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_13,
      Q => S1_AXI_RDATA(30),
      R => rst
    );
\s_axi_rdata_i_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_12,
      Q => S1_AXI_RDATA(31),
      R => rst
    );
\s_axi_rdata_i_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_40,
      Q => S1_AXI_RDATA(3),
      R => rst
    );
\s_axi_rdata_i_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_39,
      Q => S1_AXI_RDATA(4),
      R => rst
    );
\s_axi_rdata_i_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_38,
      Q => S1_AXI_RDATA(5),
      R => rst
    );
\s_axi_rdata_i_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_37,
      Q => S1_AXI_RDATA(6),
      R => rst
    );
\s_axi_rdata_i_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_36,
      Q => S1_AXI_RDATA(7),
      R => rst
    );
\s_axi_rdata_i_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_35,
      Q => S1_AXI_RDATA(8),
      R => rst
    );
\s_axi_rdata_i_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => \s_axi_rdata_i[31]_i_1_n_0\,
      D => I_DECODER_n_34,
      Q => S1_AXI_RDATA(9),
      R => rst
    );
s_axi_rvalid_i_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => I_DECODER_n_47,
      Q => \^s1_axi_rvalid\,
      R => rst
    );
start2_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000000F8"
    )
        port map (
      I0 => S1_AXI_AWVALID,
      I1 => S1_AXI_WVALID,
      I2 => S1_AXI_ARVALID,
      I3 => state(1),
      I4 => state(0),
      O => start2_i_1_n_0
    );
start2_reg: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => start2_i_1_n_0,
      Q => start2,
      R => rst
    );
\state[1]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => S1_AXI_BREADY,
      I1 => \^s1_axi_bvalid\,
      I2 => S1_AXI_RREADY,
      I3 => \^s1_axi_rvalid\,
      O => \state1__2\
    );
\state[1]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => S1_AXI_WVALID,
      I1 => S1_AXI_AWVALID,
      I2 => S1_AXI_ARVALID,
      O => \state[1]_i_3_n_0\
    );
\state_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => I_DECODER_n_7,
      Q => state(0),
      R => rst
    );
\state_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => I_DECODER_n_6,
      Q => state(1),
      R => rst
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_mailbox_0_0_axi_lite_ipif is
  port (
    \GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg\ : out STD_LOGIC;
    \GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg\ : out STD_LOGIC;
    \read_nextgray_reg[2]\ : out STD_LOGIC;
    write_fsl_error_d1_reg : out STD_LOGIC;
    Bus_RNW_reg_reg : out STD_LOGIC;
    S0_AXI_RVALID : out STD_LOGIC;
    S0_AXI_BVALID : out STD_LOGIC;
    empty_allow : out STD_LOGIC;
    S0_AXI_ARREADY : out STD_LOGIC;
    error_detect : out STD_LOGIC;
    read_fsl_error : out STD_LOGIC;
    write_fsl_error : out STD_LOGIC;
    S0_AXI_WREADY : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \sit_register_reg[3]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    full_error_reg : out STD_LOGIC;
    empty_error_reg : out STD_LOGIC;
    \read_nextgray_reg[2]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    S0_AXI_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    Rst : in STD_LOGIC;
    S0_AXI_ACLK : in STD_LOGIC;
    \out\ : in STD_LOGIC;
    S0_AXI_ARVALID : in STD_LOGIC;
    S0_AXI_AWVALID : in STD_LOGIC;
    S0_AXI_WVALID : in STD_LOGIC;
    S0_AXI_ARADDR : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S0_AXI_AWADDR : in STD_LOGIC_VECTOR ( 3 downto 0 );
    write_fsl_error_d1 : in STD_LOGIC;
    full_i_reg : in STD_LOGIC;
    read_fsl_error_d1 : in STD_LOGIC;
    rit_detect_d0 : in STD_LOGIC;
    sit_detect_d0 : in STD_LOGIC;
    full_error : in STD_LOGIC;
    empty_error : in STD_LOGIC;
    S0_AXI_RREADY : in STD_LOGIC;
    S0_AXI_BREADY : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    p_6_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \rit_register_reg[0]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \DataOut_reg[31]\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ie_register : in STD_LOGIC_VECTOR ( 0 to 2 )
  );
end design_1_mailbox_0_0_axi_lite_ipif;

architecture STRUCTURE of design_1_mailbox_0_0_axi_lite_ipif is
begin
I_SLAVE_ATTACHMENT: entity work.design_1_mailbox_0_0_slave_attachment
     port map (
      Bus_RNW_reg_reg => Bus_RNW_reg_reg,
      \DataOut_reg[31]\(31 downto 0) => \DataOut_reg[31]\(31 downto 0),
      E(0) => E(0),
      Q(3 downto 0) => Q(3 downto 0),
      Rst => Rst,
      S0_AXI_ACLK => S0_AXI_ACLK,
      S0_AXI_ARADDR(3 downto 0) => S0_AXI_ARADDR(3 downto 0),
      S0_AXI_ARREADY => S0_AXI_ARREADY,
      S0_AXI_ARVALID => S0_AXI_ARVALID,
      S0_AXI_AWADDR(3 downto 0) => S0_AXI_AWADDR(3 downto 0),
      S0_AXI_AWVALID => S0_AXI_AWVALID,
      S0_AXI_BREADY => S0_AXI_BREADY,
      S0_AXI_BVALID => S0_AXI_BVALID,
      S0_AXI_RDATA(31 downto 0) => S0_AXI_RDATA(31 downto 0),
      S0_AXI_RREADY => S0_AXI_RREADY,
      S0_AXI_RVALID => S0_AXI_RVALID,
      S0_AXI_WREADY => S0_AXI_WREADY,
      S0_AXI_WVALID => S0_AXI_WVALID,
      empty_allow => empty_allow,
      empty_error => empty_error,
      empty_error_reg => empty_error_reg,
      error_detect => error_detect,
      full_error => full_error,
      full_error_reg => full_error_reg,
      full_i_reg => full_i_reg,
      ie_register(0 to 2) => ie_register(0 to 2),
      \is_register_reg[2]\ => \GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg\,
      \out\ => \out\,
      p_6_in(2 downto 0) => p_6_in(2 downto 0),
      read_fsl_error => read_fsl_error,
      read_fsl_error_d1 => read_fsl_error_d1,
      \read_nextgray_reg[2]\ => \read_nextgray_reg[2]\,
      \read_nextgray_reg[2]_0\(0) => \read_nextgray_reg[2]_0\(0),
      rit_detect_d0 => rit_detect_d0,
      \rit_register_reg[0]\(3 downto 0) => \rit_register_reg[0]\(3 downto 0),
      \s_axi_rdata_i_reg[31]_0\ => \GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg\,
      sit_detect_d0 => sit_detect_d0,
      \sit_register_reg[3]\(0) => \sit_register_reg[3]\(0),
      write_fsl_error => write_fsl_error,
      write_fsl_error_d1 => write_fsl_error_d1,
      write_fsl_error_d1_reg => write_fsl_error_d1_reg
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_mailbox_0_0_axi_lite_ipif__parameterized1\ is
  port (
    \GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg\ : out STD_LOGIC;
    \GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg\ : out STD_LOGIC;
    \read_nextgray_reg[2]\ : out STD_LOGIC;
    write_fsl_error_d1_reg : out STD_LOGIC;
    Bus_RNW_reg_reg : out STD_LOGIC;
    S1_AXI_RVALID : out STD_LOGIC;
    S1_AXI_BVALID : out STD_LOGIC;
    empty_allow : out STD_LOGIC;
    S1_AXI_ARREADY : out STD_LOGIC;
    error_detect : out STD_LOGIC;
    read_fsl_error : out STD_LOGIC;
    write_fsl_error : out STD_LOGIC;
    S1_AXI_WREADY : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \sit_register_reg[3]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    full_error_reg : out STD_LOGIC;
    empty_error_reg : out STD_LOGIC;
    \read_nextgray_reg[2]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    S1_AXI_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \Rst_Async.SYS_Rst_If1_reg\ : in STD_LOGIC;
    S1_AXI_ACLK : in STD_LOGIC;
    \out\ : in STD_LOGIC;
    S1_AXI_ARVALID : in STD_LOGIC;
    S1_AXI_AWVALID : in STD_LOGIC;
    S1_AXI_WVALID : in STD_LOGIC;
    S1_AXI_ARADDR : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S1_AXI_AWADDR : in STD_LOGIC_VECTOR ( 3 downto 0 );
    write_fsl_error_d1 : in STD_LOGIC;
    full_i_reg : in STD_LOGIC;
    read_fsl_error_d1 : in STD_LOGIC;
    rit_detect_d0 : in STD_LOGIC;
    sit_detect_d0 : in STD_LOGIC;
    full_error_reg_0 : in STD_LOGIC;
    empty_error_reg_0 : in STD_LOGIC;
    S1_AXI_RREADY : in STD_LOGIC;
    S1_AXI_BREADY : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    p_6_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \rit_register_reg[0]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \DataOut_reg[31]\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ie_register : in STD_LOGIC_VECTOR ( 0 to 2 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_mailbox_0_0_axi_lite_ipif__parameterized1\ : entity is "axi_lite_ipif";
end \design_1_mailbox_0_0_axi_lite_ipif__parameterized1\;

architecture STRUCTURE of \design_1_mailbox_0_0_axi_lite_ipif__parameterized1\ is
begin
I_SLAVE_ATTACHMENT: entity work.\design_1_mailbox_0_0_slave_attachment__parameterized0\
     port map (
      Bus_RNW_reg_reg => Bus_RNW_reg_reg,
      \DataOut_reg[31]\(31 downto 0) => \DataOut_reg[31]\(31 downto 0),
      E(0) => E(0),
      Q(3 downto 0) => Q(3 downto 0),
      \Rst_Async.SYS_Rst_If1_reg\ => \Rst_Async.SYS_Rst_If1_reg\,
      S1_AXI_ACLK => S1_AXI_ACLK,
      S1_AXI_ARADDR(3 downto 0) => S1_AXI_ARADDR(3 downto 0),
      S1_AXI_ARREADY => S1_AXI_ARREADY,
      S1_AXI_ARVALID => S1_AXI_ARVALID,
      S1_AXI_AWADDR(3 downto 0) => S1_AXI_AWADDR(3 downto 0),
      S1_AXI_AWVALID => S1_AXI_AWVALID,
      S1_AXI_BREADY => S1_AXI_BREADY,
      S1_AXI_BVALID => S1_AXI_BVALID,
      S1_AXI_RDATA(31 downto 0) => S1_AXI_RDATA(31 downto 0),
      S1_AXI_RREADY => S1_AXI_RREADY,
      S1_AXI_RVALID => S1_AXI_RVALID,
      S1_AXI_WREADY => S1_AXI_WREADY,
      S1_AXI_WVALID => S1_AXI_WVALID,
      empty_allow => empty_allow,
      empty_error_reg => empty_error_reg,
      empty_error_reg_0 => empty_error_reg_0,
      error_detect => error_detect,
      full_error_reg => full_error_reg,
      full_error_reg_0 => full_error_reg_0,
      full_i_reg => full_i_reg,
      ie_register(0 to 2) => ie_register(0 to 2),
      \is_register_reg[2]\ => \GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg\,
      \out\ => \out\,
      p_6_in(2 downto 0) => p_6_in(2 downto 0),
      read_fsl_error => read_fsl_error,
      read_fsl_error_d1 => read_fsl_error_d1,
      \read_nextgray_reg[2]\ => \read_nextgray_reg[2]\,
      \read_nextgray_reg[2]_0\(0) => \read_nextgray_reg[2]_0\(0),
      rit_detect_d0 => rit_detect_d0,
      \rit_register_reg[0]\(3 downto 0) => \rit_register_reg[0]\(3 downto 0),
      \s_axi_rdata_i_reg[31]_0\ => \GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg\,
      sit_detect_d0 => sit_detect_d0,
      \sit_register_reg[3]\(0) => \sit_register_reg[3]\(0),
      write_fsl_error => write_fsl_error,
      write_fsl_error_d1 => write_fsl_error_d1,
      write_fsl_error_d1_reg => write_fsl_error_d1_reg
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_mailbox_0_0_if_decode is
  port (
    \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg\ : out STD_LOGIC;
    \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg\ : out STD_LOGIC;
    Bus_RNW_reg : out STD_LOGIC;
    S0_AXI_RVALID : out STD_LOGIC;
    S0_AXI_BVALID : out STD_LOGIC;
    empty_allow : out STD_LOGIC;
    S0_AXI_ARREADY : out STD_LOGIC;
    S0_AXI_WREADY : out STD_LOGIC;
    Interrupt_0 : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 3 downto 0 );
    rit_detect_d1_reg_0 : out STD_LOGIC_VECTOR ( 3 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    S0_AXI_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    Rst : in STD_LOGIC;
    S0_AXI_ACLK : in STD_LOGIC;
    sit_detect_d0 : in STD_LOGIC;
    rit_detect_d0 : in STD_LOGIC;
    \out\ : in STD_LOGIC;
    S0_AXI_ARVALID : in STD_LOGIC;
    S0_AXI_AWVALID : in STD_LOGIC;
    S0_AXI_WVALID : in STD_LOGIC;
    S0_AXI_ARADDR : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S0_AXI_AWADDR : in STD_LOGIC_VECTOR ( 3 downto 0 );
    full_i_reg : in STD_LOGIC;
    S0_AXI_RREADY : in STD_LOGIC;
    S0_AXI_BREADY : in STD_LOGIC;
    \DataOut_reg[31]\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S0_AXI_WDATA : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
end design_1_mailbox_0_0_if_decode;

architecture STRUCTURE of design_1_mailbox_0_0_if_decode is
  signal \^bus_rnw_reg\ : STD_LOGIC;
  signal \I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg\ : STD_LOGIC;
  signal \I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg\ : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \Using_AXI.AXI4_If_n_15\ : STD_LOGIC;
  signal \Using_AXI.AXI4_If_n_16\ : STD_LOGIC;
  signal empty_error : STD_LOGIC;
  signal error_detect : STD_LOGIC;
  signal full_error : STD_LOGIC;
  signal ie_register : STD_LOGIC_VECTOR ( 0 to 2 );
  signal \ie_register[0]_i_1_n_0\ : STD_LOGIC;
  signal \ie_register[1]_i_1_n_0\ : STD_LOGIC;
  signal \ie_register[2]_i_1_n_0\ : STD_LOGIC;
  signal \is_register[0]_i_1_n_0\ : STD_LOGIC;
  signal \is_register[1]_i_1_n_0\ : STD_LOGIC;
  signal \is_register[2]_i_1_n_0\ : STD_LOGIC;
  signal p_0_in1_in : STD_LOGIC;
  signal p_1_in2_in : STD_LOGIC;
  signal p_6_in : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal read_fsl_error : STD_LOGIC;
  signal read_fsl_error_d1 : STD_LOGIC;
  signal rit_detect_d1 : STD_LOGIC;
  signal \^rit_detect_d1_reg_0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal sit_detect_d1 : STD_LOGIC;
  signal write_fsl_error : STD_LOGIC;
  signal write_fsl_error_d1 : STD_LOGIC;
begin
  Bus_RNW_reg <= \^bus_rnw_reg\;
  Q(3 downto 0) <= \^q\(3 downto 0);
  rit_detect_d1_reg_0(3 downto 0) <= \^rit_detect_d1_reg_0\(3 downto 0);
Interrupt_0_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => ie_register(1),
      I1 => p_6_in(1),
      I2 => p_6_in(2),
      I3 => ie_register(0),
      I4 => p_6_in(0),
      I5 => ie_register(2),
      O => Interrupt_0
    );
\Using_AXI.AXI4_If\: entity work.design_1_mailbox_0_0_axi_lite_ipif
     port map (
      Bus_RNW_reg_reg => \^bus_rnw_reg\,
      \DataOut_reg[31]\(31 downto 0) => \DataOut_reg[31]\(31 downto 0),
      E(0) => p_1_in2_in,
      \GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg\ => \I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg\,
      \GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg\ => \I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg\,
      Q(3 downto 0) => \^q\(3 downto 0),
      Rst => Rst,
      S0_AXI_ACLK => S0_AXI_ACLK,
      S0_AXI_ARADDR(3 downto 0) => S0_AXI_ARADDR(3 downto 0),
      S0_AXI_ARREADY => S0_AXI_ARREADY,
      S0_AXI_ARVALID => S0_AXI_ARVALID,
      S0_AXI_AWADDR(3 downto 0) => S0_AXI_AWADDR(3 downto 0),
      S0_AXI_AWVALID => S0_AXI_AWVALID,
      S0_AXI_BREADY => S0_AXI_BREADY,
      S0_AXI_BVALID => S0_AXI_BVALID,
      S0_AXI_RDATA(31 downto 0) => S0_AXI_RDATA(31 downto 0),
      S0_AXI_RREADY => S0_AXI_RREADY,
      S0_AXI_RVALID => S0_AXI_RVALID,
      S0_AXI_WREADY => S0_AXI_WREADY,
      S0_AXI_WVALID => S0_AXI_WVALID,
      empty_allow => empty_allow,
      empty_error => empty_error,
      empty_error_reg => \Using_AXI.AXI4_If_n_16\,
      error_detect => error_detect,
      full_error => full_error,
      full_error_reg => \Using_AXI.AXI4_If_n_15\,
      full_i_reg => full_i_reg,
      ie_register(0 to 2) => ie_register(0 to 2),
      \out\ => \out\,
      p_6_in(2 downto 0) => p_6_in(2 downto 0),
      read_fsl_error => read_fsl_error,
      read_fsl_error_d1 => read_fsl_error_d1,
      \read_nextgray_reg[2]\ => \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg\,
      \read_nextgray_reg[2]_0\(0) => E(0),
      rit_detect_d0 => rit_detect_d0,
      \rit_register_reg[0]\(3 downto 0) => \^rit_detect_d1_reg_0\(3 downto 0),
      sit_detect_d0 => sit_detect_d0,
      \sit_register_reg[3]\(0) => p_0_in1_in,
      write_fsl_error => write_fsl_error,
      write_fsl_error_d1 => write_fsl_error_d1,
      write_fsl_error_d1_reg => \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg\
    );
empty_error_reg: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \Using_AXI.AXI4_If_n_16\,
      Q => empty_error,
      R => '0'
    );
full_error_reg: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \Using_AXI.AXI4_If_n_15\,
      Q => full_error,
      R => '0'
    );
\ie_register[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => S0_AXI_WDATA(2),
      I1 => \I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg\,
      I2 => \^bus_rnw_reg\,
      I3 => ie_register(0),
      O => \ie_register[0]_i_1_n_0\
    );
\ie_register[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => S0_AXI_WDATA(1),
      I1 => \I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg\,
      I2 => \^bus_rnw_reg\,
      I3 => ie_register(1),
      O => \ie_register[1]_i_1_n_0\
    );
\ie_register[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => S0_AXI_WDATA(0),
      I1 => \I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg\,
      I2 => \^bus_rnw_reg\,
      I3 => ie_register(2),
      O => \ie_register[2]_i_1_n_0\
    );
\ie_register_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \ie_register[0]_i_1_n_0\,
      Q => ie_register(0),
      R => Rst
    );
\ie_register_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \ie_register[1]_i_1_n_0\,
      Q => ie_register(1),
      R => Rst
    );
\ie_register_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \ie_register[2]_i_1_n_0\,
      Q => ie_register(2),
      R => Rst
    );
\is_register[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFBFAAAA"
    )
        port map (
      I0 => error_detect,
      I1 => S0_AXI_WDATA(2),
      I2 => \I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg\,
      I3 => \^bus_rnw_reg\,
      I4 => p_6_in(2),
      O => \is_register[0]_i_1_n_0\
    );
\is_register[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF4FFF44444444"
    )
        port map (
      I0 => rit_detect_d1,
      I1 => rit_detect_d0,
      I2 => S0_AXI_WDATA(1),
      I3 => \I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg\,
      I4 => \^bus_rnw_reg\,
      I5 => p_6_in(1),
      O => \is_register[1]_i_1_n_0\
    );
\is_register[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF4FFF44444444"
    )
        port map (
      I0 => sit_detect_d1,
      I1 => sit_detect_d0,
      I2 => S0_AXI_WDATA(0),
      I3 => \I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg\,
      I4 => \^bus_rnw_reg\,
      I5 => p_6_in(0),
      O => \is_register[2]_i_1_n_0\
    );
\is_register_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \is_register[0]_i_1_n_0\,
      Q => p_6_in(2),
      R => Rst
    );
\is_register_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \is_register[1]_i_1_n_0\,
      Q => p_6_in(1),
      R => Rst
    );
\is_register_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => \is_register[2]_i_1_n_0\,
      Q => p_6_in(0),
      R => Rst
    );
read_fsl_error_d1_reg: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => read_fsl_error,
      Q => read_fsl_error_d1,
      R => Rst
    );
rit_detect_d1_reg: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => rit_detect_d0,
      Q => rit_detect_d1,
      R => '0'
    );
\rit_register_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => p_1_in2_in,
      D => S0_AXI_WDATA(3),
      Q => \^rit_detect_d1_reg_0\(3),
      R => Rst
    );
\rit_register_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => p_1_in2_in,
      D => S0_AXI_WDATA(2),
      Q => \^rit_detect_d1_reg_0\(2),
      R => Rst
    );
\rit_register_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => p_1_in2_in,
      D => S0_AXI_WDATA(1),
      Q => \^rit_detect_d1_reg_0\(1),
      R => Rst
    );
\rit_register_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => p_1_in2_in,
      D => S0_AXI_WDATA(0),
      Q => \^rit_detect_d1_reg_0\(0),
      R => Rst
    );
sit_detect_d1_reg: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => sit_detect_d0,
      Q => sit_detect_d1,
      R => '0'
    );
\sit_register_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => p_0_in1_in,
      D => S0_AXI_WDATA(3),
      Q => \^q\(3),
      R => Rst
    );
\sit_register_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => p_0_in1_in,
      D => S0_AXI_WDATA(2),
      Q => \^q\(2),
      R => Rst
    );
\sit_register_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => p_0_in1_in,
      D => S0_AXI_WDATA(1),
      Q => \^q\(1),
      R => Rst
    );
\sit_register_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => p_0_in1_in,
      D => S0_AXI_WDATA(0),
      Q => \^q\(0),
      R => Rst
    );
write_fsl_error_d1_reg: unisim.vcomponents.FDRE
     port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => write_fsl_error,
      Q => write_fsl_error_d1,
      R => Rst
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \design_1_mailbox_0_0_if_decode__parameterized1\ is
  port (
    \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg\ : out STD_LOGIC;
    \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg\ : out STD_LOGIC;
    Bus_RNW_reg : out STD_LOGIC;
    S1_AXI_RVALID : out STD_LOGIC;
    S1_AXI_BVALID : out STD_LOGIC;
    empty_allow : out STD_LOGIC;
    S1_AXI_ARREADY : out STD_LOGIC;
    S1_AXI_WREADY : out STD_LOGIC;
    Interrupt_1 : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 3 downto 0 );
    rit_detect_d1_reg_0 : out STD_LOGIC_VECTOR ( 3 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    S1_AXI_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \Rst_Async.SYS_Rst_If1_reg\ : in STD_LOGIC;
    S1_AXI_ACLK : in STD_LOGIC;
    sit_detect_d0 : in STD_LOGIC;
    rit_detect_d0 : in STD_LOGIC;
    \out\ : in STD_LOGIC;
    S1_AXI_ARVALID : in STD_LOGIC;
    S1_AXI_AWVALID : in STD_LOGIC;
    S1_AXI_WVALID : in STD_LOGIC;
    S1_AXI_ARADDR : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S1_AXI_AWADDR : in STD_LOGIC_VECTOR ( 3 downto 0 );
    full_i_reg : in STD_LOGIC;
    S1_AXI_RREADY : in STD_LOGIC;
    S1_AXI_BREADY : in STD_LOGIC;
    \DataOut_reg[31]\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S1_AXI_WDATA : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \design_1_mailbox_0_0_if_decode__parameterized1\ : entity is "if_decode";
end \design_1_mailbox_0_0_if_decode__parameterized1\;

architecture STRUCTURE of \design_1_mailbox_0_0_if_decode__parameterized1\ is
  signal \^bus_rnw_reg\ : STD_LOGIC;
  signal \I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg\ : STD_LOGIC;
  signal \I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg\ : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \Using_AXI.AXI4_If_n_15\ : STD_LOGIC;
  signal \Using_AXI.AXI4_If_n_16\ : STD_LOGIC;
  signal empty_error_reg_n_0 : STD_LOGIC;
  signal error_detect : STD_LOGIC;
  signal full_error_reg_n_0 : STD_LOGIC;
  signal ie_register : STD_LOGIC_VECTOR ( 0 to 2 );
  signal \ie_register[0]_i_1_n_0\ : STD_LOGIC;
  signal \ie_register[1]_i_1_n_0\ : STD_LOGIC;
  signal \ie_register[2]_i_1_n_0\ : STD_LOGIC;
  signal \is_register[0]_i_1_n_0\ : STD_LOGIC;
  signal \is_register[1]_i_1_n_0\ : STD_LOGIC;
  signal \is_register[2]_i_1_n_0\ : STD_LOGIC;
  signal p_0_in1_in : STD_LOGIC;
  signal p_1_in2_in : STD_LOGIC;
  signal p_6_in : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal read_fsl_error : STD_LOGIC;
  signal read_fsl_error_d1 : STD_LOGIC;
  signal rit_detect_d1 : STD_LOGIC;
  signal \^rit_detect_d1_reg_0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal sit_detect_d1 : STD_LOGIC;
  signal write_fsl_error : STD_LOGIC;
  signal write_fsl_error_d1 : STD_LOGIC;
begin
  Bus_RNW_reg <= \^bus_rnw_reg\;
  Q(3 downto 0) <= \^q\(3 downto 0);
  rit_detect_d1_reg_0(3 downto 0) <= \^rit_detect_d1_reg_0\(3 downto 0);
Interrupt_1_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => ie_register(1),
      I1 => p_6_in(1),
      I2 => p_6_in(2),
      I3 => ie_register(0),
      I4 => p_6_in(0),
      I5 => ie_register(2),
      O => Interrupt_1
    );
\Using_AXI.AXI4_If\: entity work.\design_1_mailbox_0_0_axi_lite_ipif__parameterized1\
     port map (
      Bus_RNW_reg_reg => \^bus_rnw_reg\,
      \DataOut_reg[31]\(31 downto 0) => \DataOut_reg[31]\(31 downto 0),
      E(0) => p_1_in2_in,
      \GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg\ => \I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg\,
      \GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg\ => \I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg\,
      Q(3 downto 0) => \^q\(3 downto 0),
      \Rst_Async.SYS_Rst_If1_reg\ => \Rst_Async.SYS_Rst_If1_reg\,
      S1_AXI_ACLK => S1_AXI_ACLK,
      S1_AXI_ARADDR(3 downto 0) => S1_AXI_ARADDR(3 downto 0),
      S1_AXI_ARREADY => S1_AXI_ARREADY,
      S1_AXI_ARVALID => S1_AXI_ARVALID,
      S1_AXI_AWADDR(3 downto 0) => S1_AXI_AWADDR(3 downto 0),
      S1_AXI_AWVALID => S1_AXI_AWVALID,
      S1_AXI_BREADY => S1_AXI_BREADY,
      S1_AXI_BVALID => S1_AXI_BVALID,
      S1_AXI_RDATA(31 downto 0) => S1_AXI_RDATA(31 downto 0),
      S1_AXI_RREADY => S1_AXI_RREADY,
      S1_AXI_RVALID => S1_AXI_RVALID,
      S1_AXI_WREADY => S1_AXI_WREADY,
      S1_AXI_WVALID => S1_AXI_WVALID,
      empty_allow => empty_allow,
      empty_error_reg => \Using_AXI.AXI4_If_n_16\,
      empty_error_reg_0 => empty_error_reg_n_0,
      error_detect => error_detect,
      full_error_reg => \Using_AXI.AXI4_If_n_15\,
      full_error_reg_0 => full_error_reg_n_0,
      full_i_reg => full_i_reg,
      ie_register(0 to 2) => ie_register(0 to 2),
      \out\ => \out\,
      p_6_in(2 downto 0) => p_6_in(2 downto 0),
      read_fsl_error => read_fsl_error,
      read_fsl_error_d1 => read_fsl_error_d1,
      \read_nextgray_reg[2]\ => \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg\,
      \read_nextgray_reg[2]_0\(0) => E(0),
      rit_detect_d0 => rit_detect_d0,
      \rit_register_reg[0]\(3 downto 0) => \^rit_detect_d1_reg_0\(3 downto 0),
      sit_detect_d0 => sit_detect_d0,
      \sit_register_reg[3]\(0) => p_0_in1_in,
      write_fsl_error => write_fsl_error,
      write_fsl_error_d1 => write_fsl_error_d1,
      write_fsl_error_d1_reg => \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg\
    );
empty_error_reg: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \Using_AXI.AXI4_If_n_16\,
      Q => empty_error_reg_n_0,
      R => '0'
    );
full_error_reg: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \Using_AXI.AXI4_If_n_15\,
      Q => full_error_reg_n_0,
      R => '0'
    );
\ie_register[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => S1_AXI_WDATA(2),
      I1 => \I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg\,
      I2 => \^bus_rnw_reg\,
      I3 => ie_register(0),
      O => \ie_register[0]_i_1_n_0\
    );
\ie_register[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => S1_AXI_WDATA(1),
      I1 => \I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg\,
      I2 => \^bus_rnw_reg\,
      I3 => ie_register(1),
      O => \ie_register[1]_i_1_n_0\
    );
\ie_register[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => S1_AXI_WDATA(0),
      I1 => \I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg\,
      I2 => \^bus_rnw_reg\,
      I3 => ie_register(2),
      O => \ie_register[2]_i_1_n_0\
    );
\ie_register_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \ie_register[0]_i_1_n_0\,
      Q => ie_register(0),
      R => \Rst_Async.SYS_Rst_If1_reg\
    );
\ie_register_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \ie_register[1]_i_1_n_0\,
      Q => ie_register(1),
      R => \Rst_Async.SYS_Rst_If1_reg\
    );
\ie_register_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \ie_register[2]_i_1_n_0\,
      Q => ie_register(2),
      R => \Rst_Async.SYS_Rst_If1_reg\
    );
\is_register[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFBFAAAA"
    )
        port map (
      I0 => error_detect,
      I1 => S1_AXI_WDATA(2),
      I2 => \I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg\,
      I3 => \^bus_rnw_reg\,
      I4 => p_6_in(2),
      O => \is_register[0]_i_1_n_0\
    );
\is_register[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF4FFF44444444"
    )
        port map (
      I0 => rit_detect_d1,
      I1 => rit_detect_d0,
      I2 => S1_AXI_WDATA(1),
      I3 => \I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg\,
      I4 => \^bus_rnw_reg\,
      I5 => p_6_in(1),
      O => \is_register[1]_i_1_n_0\
    );
\is_register[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF4FFF44444444"
    )
        port map (
      I0 => sit_detect_d1,
      I1 => sit_detect_d0,
      I2 => S1_AXI_WDATA(0),
      I3 => \I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg\,
      I4 => \^bus_rnw_reg\,
      I5 => p_6_in(0),
      O => \is_register[2]_i_1_n_0\
    );
\is_register_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \is_register[0]_i_1_n_0\,
      Q => p_6_in(2),
      R => \Rst_Async.SYS_Rst_If1_reg\
    );
\is_register_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \is_register[1]_i_1_n_0\,
      Q => p_6_in(1),
      R => \Rst_Async.SYS_Rst_If1_reg\
    );
\is_register_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => \is_register[2]_i_1_n_0\,
      Q => p_6_in(0),
      R => \Rst_Async.SYS_Rst_If1_reg\
    );
read_fsl_error_d1_reg: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => read_fsl_error,
      Q => read_fsl_error_d1,
      R => \Rst_Async.SYS_Rst_If1_reg\
    );
rit_detect_d1_reg: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => rit_detect_d0,
      Q => rit_detect_d1,
      R => '0'
    );
\rit_register_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => p_1_in2_in,
      D => S1_AXI_WDATA(3),
      Q => \^rit_detect_d1_reg_0\(3),
      R => \Rst_Async.SYS_Rst_If1_reg\
    );
\rit_register_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => p_1_in2_in,
      D => S1_AXI_WDATA(2),
      Q => \^rit_detect_d1_reg_0\(2),
      R => \Rst_Async.SYS_Rst_If1_reg\
    );
\rit_register_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => p_1_in2_in,
      D => S1_AXI_WDATA(1),
      Q => \^rit_detect_d1_reg_0\(1),
      R => \Rst_Async.SYS_Rst_If1_reg\
    );
\rit_register_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => p_1_in2_in,
      D => S1_AXI_WDATA(0),
      Q => \^rit_detect_d1_reg_0\(0),
      R => \Rst_Async.SYS_Rst_If1_reg\
    );
sit_detect_d1_reg: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => sit_detect_d0,
      Q => sit_detect_d1,
      R => '0'
    );
\sit_register_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => p_0_in1_in,
      D => S1_AXI_WDATA(3),
      Q => \^q\(3),
      R => \Rst_Async.SYS_Rst_If1_reg\
    );
\sit_register_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => p_0_in1_in,
      D => S1_AXI_WDATA(2),
      Q => \^q\(2),
      R => \Rst_Async.SYS_Rst_If1_reg\
    );
\sit_register_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => p_0_in1_in,
      D => S1_AXI_WDATA(1),
      Q => \^q\(1),
      R => \Rst_Async.SYS_Rst_If1_reg\
    );
\sit_register_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => p_0_in1_in,
      D => S1_AXI_WDATA(0),
      Q => \^q\(0),
      R => \Rst_Async.SYS_Rst_If1_reg\
    );
write_fsl_error_d1_reg: unisim.vcomponents.FDRE
     port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => write_fsl_error,
      Q => write_fsl_error_d1,
      R => \Rst_Async.SYS_Rst_If1_reg\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_mailbox_0_0_mailbox is
  port (
    SYS_Rst : in STD_LOGIC;
    S0_AXI_ACLK : in STD_LOGIC;
    S0_AXI_ARESETN : in STD_LOGIC;
    S0_AXI_AWADDR : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S0_AXI_AWVALID : in STD_LOGIC;
    S0_AXI_AWREADY : out STD_LOGIC;
    S0_AXI_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S0_AXI_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S0_AXI_WVALID : in STD_LOGIC;
    S0_AXI_WREADY : out STD_LOGIC;
    S0_AXI_BRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S0_AXI_BVALID : out STD_LOGIC;
    S0_AXI_BREADY : in STD_LOGIC;
    S0_AXI_ARADDR : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S0_AXI_ARVALID : in STD_LOGIC;
    S0_AXI_ARREADY : out STD_LOGIC;
    S0_AXI_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S0_AXI_RRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S0_AXI_RVALID : out STD_LOGIC;
    S0_AXI_RREADY : in STD_LOGIC;
    S1_AXI_ACLK : in STD_LOGIC;
    S1_AXI_ARESETN : in STD_LOGIC;
    S1_AXI_AWADDR : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S1_AXI_AWVALID : in STD_LOGIC;
    S1_AXI_AWREADY : out STD_LOGIC;
    S1_AXI_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S1_AXI_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S1_AXI_WVALID : in STD_LOGIC;
    S1_AXI_WREADY : out STD_LOGIC;
    S1_AXI_BRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S1_AXI_BVALID : out STD_LOGIC;
    S1_AXI_BREADY : in STD_LOGIC;
    S1_AXI_ARADDR : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S1_AXI_ARVALID : in STD_LOGIC;
    S1_AXI_ARREADY : out STD_LOGIC;
    S1_AXI_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S1_AXI_RRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S1_AXI_RVALID : out STD_LOGIC;
    S1_AXI_RREADY : in STD_LOGIC;
    M0_AXIS_ACLK : in STD_LOGIC;
    M0_AXIS_TLAST : out STD_LOGIC;
    M0_AXIS_TDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M0_AXIS_TVALID : out STD_LOGIC;
    M0_AXIS_TREADY : in STD_LOGIC;
    S0_AXIS_ACLK : in STD_LOGIC;
    S0_AXIS_TLAST : in STD_LOGIC;
    S0_AXIS_TDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S0_AXIS_TVALID : in STD_LOGIC;
    S0_AXIS_TREADY : out STD_LOGIC;
    M1_AXIS_ACLK : in STD_LOGIC;
    M1_AXIS_TLAST : out STD_LOGIC;
    M1_AXIS_TDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M1_AXIS_TVALID : out STD_LOGIC;
    M1_AXIS_TREADY : in STD_LOGIC;
    S1_AXIS_ACLK : in STD_LOGIC;
    S1_AXIS_TLAST : in STD_LOGIC;
    S1_AXIS_TDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S1_AXIS_TVALID : in STD_LOGIC;
    S1_AXIS_TREADY : out STD_LOGIC;
    Interrupt_0 : out STD_LOGIC;
    Interrupt_1 : out STD_LOGIC
  );
  attribute C_ASYNC_CLKS : integer;
  attribute C_ASYNC_CLKS of design_1_mailbox_0_0_mailbox : entity is 1;
  attribute C_ENABLE_BUS_ERROR : integer;
  attribute C_ENABLE_BUS_ERROR of design_1_mailbox_0_0_mailbox : entity is 0;
  attribute C_EXT_RESET_HIGH : integer;
  attribute C_EXT_RESET_HIGH of design_1_mailbox_0_0_mailbox : entity is 1;
  attribute C_FAMILY : string;
  attribute C_FAMILY of design_1_mailbox_0_0_mailbox : entity is "zynq";
  attribute C_IMPL_STYLE : integer;
  attribute C_IMPL_STYLE of design_1_mailbox_0_0_mailbox : entity is 0;
  attribute C_INTERCONNECT_PORT_0 : integer;
  attribute C_INTERCONNECT_PORT_0 of design_1_mailbox_0_0_mailbox : entity is 2;
  attribute C_INTERCONNECT_PORT_1 : integer;
  attribute C_INTERCONNECT_PORT_1 of design_1_mailbox_0_0_mailbox : entity is 2;
  attribute C_M0_AXIS_DATA_WIDTH : integer;
  attribute C_M0_AXIS_DATA_WIDTH of design_1_mailbox_0_0_mailbox : entity is 32;
  attribute C_M1_AXIS_DATA_WIDTH : integer;
  attribute C_M1_AXIS_DATA_WIDTH of design_1_mailbox_0_0_mailbox : entity is 32;
  attribute C_MAILBOX_DEPTH : integer;
  attribute C_MAILBOX_DEPTH of design_1_mailbox_0_0_mailbox : entity is 16;
  attribute C_NUM_SYNC_FF : integer;
  attribute C_NUM_SYNC_FF of design_1_mailbox_0_0_mailbox : entity is 2;
  attribute C_S0_AXIS_DATA_WIDTH : integer;
  attribute C_S0_AXIS_DATA_WIDTH of design_1_mailbox_0_0_mailbox : entity is 32;
  attribute C_S0_AXI_ADDR_WIDTH : integer;
  attribute C_S0_AXI_ADDR_WIDTH of design_1_mailbox_0_0_mailbox : entity is 32;
  attribute C_S0_AXI_BASEADDR : integer;
  attribute C_S0_AXI_BASEADDR of design_1_mailbox_0_0_mailbox : entity is 1132462080;
  attribute C_S0_AXI_DATA_WIDTH : integer;
  attribute C_S0_AXI_DATA_WIDTH of design_1_mailbox_0_0_mailbox : entity is 32;
  attribute C_S0_AXI_HIGHADDR : integer;
  attribute C_S0_AXI_HIGHADDR of design_1_mailbox_0_0_mailbox : entity is 1132527615;
  attribute C_S1_AXIS_DATA_WIDTH : integer;
  attribute C_S1_AXIS_DATA_WIDTH of design_1_mailbox_0_0_mailbox : entity is 32;
  attribute C_S1_AXI_ADDR_WIDTH : integer;
  attribute C_S1_AXI_ADDR_WIDTH of design_1_mailbox_0_0_mailbox : entity is 32;
  attribute C_S1_AXI_BASEADDR : integer;
  attribute C_S1_AXI_BASEADDR of design_1_mailbox_0_0_mailbox : entity is 1130364928;
  attribute C_S1_AXI_DATA_WIDTH : integer;
  attribute C_S1_AXI_DATA_WIDTH of design_1_mailbox_0_0_mailbox : entity is 32;
  attribute C_S1_AXI_HIGHADDR : integer;
  attribute C_S1_AXI_HIGHADDR of design_1_mailbox_0_0_mailbox : entity is 1130430463;
end design_1_mailbox_0_0_mailbox;

architecture STRUCTURE of design_1_mailbox_0_0_mailbox is
  signal \<const0>\ : STD_LOGIC;
  signal Bus0_Rst_d1 : STD_LOGIC;
  signal Bus0_Rst_d2 : STD_LOGIC;
  signal Bus1_Rst_d1 : STD_LOGIC;
  signal Bus1_Rst_d2 : STD_LOGIC;
  signal DataOut : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal FSL0_M_Full_I : STD_LOGIC;
  signal FSL1_M_Full_I : STD_LOGIC;
  signal Rst : STD_LOGIC;
  signal \Rst_Async.SYS_Rst_If1_reg_n_0\ : STD_LOGIC;
  signal \^s0_axi_wready\ : STD_LOGIC;
  signal \^s1_axi_wready\ : STD_LOGIC;
  signal SYS_Rst_Input0_d1 : STD_LOGIC;
  signal SYS_Rst_Input0_d2 : STD_LOGIC;
  signal SYS_Rst_Input1_d1 : STD_LOGIC;
  signal SYS_Rst_Input1_d2 : STD_LOGIC;
  signal \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/Bus_RNW_reg\ : STD_LOGIC;
  signal \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/Bus_RNW_reg_1\ : STD_LOGIC;
  signal \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg\ : STD_LOGIC;
  signal \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg_2\ : STD_LOGIC;
  signal \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg\ : STD_LOGIC;
  signal \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg_3\ : STD_LOGIC;
  signal \Using_Bus_0.Bus0_If_n_17\ : STD_LOGIC;
  signal \Using_Bus_1.Bus1_If_n_10\ : STD_LOGIC;
  signal \Using_Bus_1.Bus1_If_n_11\ : STD_LOGIC;
  signal \Using_Bus_1.Bus1_If_n_12\ : STD_LOGIC;
  signal \Using_Bus_1.Bus1_If_n_13\ : STD_LOGIC;
  signal \Using_Bus_1.Bus1_If_n_14\ : STD_LOGIC;
  signal \Using_Bus_1.Bus1_If_n_15\ : STD_LOGIC;
  signal \Using_Bus_1.Bus1_If_n_16\ : STD_LOGIC;
  signal \Using_Bus_1.Bus1_If_n_17\ : STD_LOGIC;
  signal \Using_Bus_1.Bus1_If_n_9\ : STD_LOGIC;
  signal \Using_FIFO.Async_FIFO_Gen.Async_FIFO_I1/empty_allow\ : STD_LOGIC;
  signal \Using_FIFO.Async_FIFO_Gen.Async_FIFO_I1/empty_allow_0\ : STD_LOGIC;
  signal \Using_FIFO.Async_FIFO_Gen.Async_FIFO_I1/p_3_in\ : STD_LOGIC;
  signal \Using_FIFO.Async_FIFO_Gen.Async_FIFO_I1/p_3_in_6\ : STD_LOGIC;
  signal fsl_0_to_1_n_0 : STD_LOGIC;
  signal fsl_1_to_0_n_10 : STD_LOGIC;
  signal fsl_1_to_0_n_11 : STD_LOGIC;
  signal fsl_1_to_0_n_12 : STD_LOGIC;
  signal fsl_1_to_0_n_13 : STD_LOGIC;
  signal fsl_1_to_0_n_14 : STD_LOGIC;
  signal fsl_1_to_0_n_15 : STD_LOGIC;
  signal fsl_1_to_0_n_16 : STD_LOGIC;
  signal fsl_1_to_0_n_17 : STD_LOGIC;
  signal fsl_1_to_0_n_18 : STD_LOGIC;
  signal fsl_1_to_0_n_19 : STD_LOGIC;
  signal fsl_1_to_0_n_20 : STD_LOGIC;
  signal fsl_1_to_0_n_21 : STD_LOGIC;
  signal fsl_1_to_0_n_22 : STD_LOGIC;
  signal fsl_1_to_0_n_23 : STD_LOGIC;
  signal fsl_1_to_0_n_24 : STD_LOGIC;
  signal fsl_1_to_0_n_25 : STD_LOGIC;
  signal fsl_1_to_0_n_26 : STD_LOGIC;
  signal fsl_1_to_0_n_27 : STD_LOGIC;
  signal fsl_1_to_0_n_28 : STD_LOGIC;
  signal fsl_1_to_0_n_29 : STD_LOGIC;
  signal fsl_1_to_0_n_30 : STD_LOGIC;
  signal fsl_1_to_0_n_31 : STD_LOGIC;
  signal fsl_1_to_0_n_32 : STD_LOGIC;
  signal fsl_1_to_0_n_33 : STD_LOGIC;
  signal fsl_1_to_0_n_34 : STD_LOGIC;
  signal fsl_1_to_0_n_35 : STD_LOGIC;
  signal fsl_1_to_0_n_4 : STD_LOGIC;
  signal fsl_1_to_0_n_5 : STD_LOGIC;
  signal fsl_1_to_0_n_6 : STD_LOGIC;
  signal fsl_1_to_0_n_7 : STD_LOGIC;
  signal fsl_1_to_0_n_8 : STD_LOGIC;
  signal fsl_1_to_0_n_9 : STD_LOGIC;
  signal if0_rst : STD_LOGIC;
  signal if1_rst : STD_LOGIC;
  signal p_0_out : STD_LOGIC;
  signal p_2_out : STD_LOGIC;
  signal rit_detect_d0 : STD_LOGIC;
  signal rit_detect_d0_4 : STD_LOGIC;
  signal rit_register : STD_LOGIC_VECTOR ( 0 to 3 );
  signal sit_detect_d0 : STD_LOGIC;
  signal sit_detect_d0_5 : STD_LOGIC;
  signal sit_register : STD_LOGIC_VECTOR ( 0 to 3 );
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of \Rst_Async.Bus_RST_FF_I0_1\ : label is std.standard.true;
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \Rst_Async.Bus_RST_FF_I0_1\ : label is "FD";
  attribute box_type : string;
  attribute box_type of \Rst_Async.Bus_RST_FF_I0_1\ : label is "PRIMITIVE";
  attribute ASYNC_REG of \Rst_Async.Bus_RST_FF_I0_2\ : label is std.standard.true;
  attribute XILINX_LEGACY_PRIM of \Rst_Async.Bus_RST_FF_I0_2\ : label is "FD";
  attribute box_type of \Rst_Async.Bus_RST_FF_I0_2\ : label is "PRIMITIVE";
  attribute ASYNC_REG of \Rst_Async.Bus_RST_FF_I1_1\ : label is std.standard.true;
  attribute XILINX_LEGACY_PRIM of \Rst_Async.Bus_RST_FF_I1_1\ : label is "FD";
  attribute box_type of \Rst_Async.Bus_RST_FF_I1_1\ : label is "PRIMITIVE";
  attribute ASYNC_REG of \Rst_Async.Bus_RST_FF_I1_2\ : label is std.standard.true;
  attribute XILINX_LEGACY_PRIM of \Rst_Async.Bus_RST_FF_I1_2\ : label is "FD";
  attribute box_type of \Rst_Async.Bus_RST_FF_I1_2\ : label is "PRIMITIVE";
  attribute ASYNC_REG of \Rst_Async.SYS_RST_FF_I0_1\ : label is std.standard.true;
  attribute XILINX_LEGACY_PRIM of \Rst_Async.SYS_RST_FF_I0_1\ : label is "FD";
  attribute box_type of \Rst_Async.SYS_RST_FF_I0_1\ : label is "PRIMITIVE";
  attribute ASYNC_REG of \Rst_Async.SYS_RST_FF_I0_2\ : label is std.standard.true;
  attribute XILINX_LEGACY_PRIM of \Rst_Async.SYS_RST_FF_I0_2\ : label is "FD";
  attribute box_type of \Rst_Async.SYS_RST_FF_I0_2\ : label is "PRIMITIVE";
  attribute ASYNC_REG of \Rst_Async.SYS_RST_FF_I1_1\ : label is std.standard.true;
  attribute XILINX_LEGACY_PRIM of \Rst_Async.SYS_RST_FF_I1_1\ : label is "FD";
  attribute box_type of \Rst_Async.SYS_RST_FF_I1_1\ : label is "PRIMITIVE";
  attribute ASYNC_REG of \Rst_Async.SYS_RST_FF_I1_2\ : label is std.standard.true;
  attribute XILINX_LEGACY_PRIM of \Rst_Async.SYS_RST_FF_I1_2\ : label is "FD";
  attribute box_type of \Rst_Async.SYS_RST_FF_I1_2\ : label is "PRIMITIVE";
begin
  M0_AXIS_TDATA(31) <= \<const0>\;
  M0_AXIS_TDATA(30) <= \<const0>\;
  M0_AXIS_TDATA(29) <= \<const0>\;
  M0_AXIS_TDATA(28) <= \<const0>\;
  M0_AXIS_TDATA(27) <= \<const0>\;
  M0_AXIS_TDATA(26) <= \<const0>\;
  M0_AXIS_TDATA(25) <= \<const0>\;
  M0_AXIS_TDATA(24) <= \<const0>\;
  M0_AXIS_TDATA(23) <= \<const0>\;
  M0_AXIS_TDATA(22) <= \<const0>\;
  M0_AXIS_TDATA(21) <= \<const0>\;
  M0_AXIS_TDATA(20) <= \<const0>\;
  M0_AXIS_TDATA(19) <= \<const0>\;
  M0_AXIS_TDATA(18) <= \<const0>\;
  M0_AXIS_TDATA(17) <= \<const0>\;
  M0_AXIS_TDATA(16) <= \<const0>\;
  M0_AXIS_TDATA(15) <= \<const0>\;
  M0_AXIS_TDATA(14) <= \<const0>\;
  M0_AXIS_TDATA(13) <= \<const0>\;
  M0_AXIS_TDATA(12) <= \<const0>\;
  M0_AXIS_TDATA(11) <= \<const0>\;
  M0_AXIS_TDATA(10) <= \<const0>\;
  M0_AXIS_TDATA(9) <= \<const0>\;
  M0_AXIS_TDATA(8) <= \<const0>\;
  M0_AXIS_TDATA(7) <= \<const0>\;
  M0_AXIS_TDATA(6) <= \<const0>\;
  M0_AXIS_TDATA(5) <= \<const0>\;
  M0_AXIS_TDATA(4) <= \<const0>\;
  M0_AXIS_TDATA(3) <= \<const0>\;
  M0_AXIS_TDATA(2) <= \<const0>\;
  M0_AXIS_TDATA(1) <= \<const0>\;
  M0_AXIS_TDATA(0) <= \<const0>\;
  M0_AXIS_TLAST <= \<const0>\;
  M0_AXIS_TVALID <= \<const0>\;
  M1_AXIS_TDATA(31) <= \<const0>\;
  M1_AXIS_TDATA(30) <= \<const0>\;
  M1_AXIS_TDATA(29) <= \<const0>\;
  M1_AXIS_TDATA(28) <= \<const0>\;
  M1_AXIS_TDATA(27) <= \<const0>\;
  M1_AXIS_TDATA(26) <= \<const0>\;
  M1_AXIS_TDATA(25) <= \<const0>\;
  M1_AXIS_TDATA(24) <= \<const0>\;
  M1_AXIS_TDATA(23) <= \<const0>\;
  M1_AXIS_TDATA(22) <= \<const0>\;
  M1_AXIS_TDATA(21) <= \<const0>\;
  M1_AXIS_TDATA(20) <= \<const0>\;
  M1_AXIS_TDATA(19) <= \<const0>\;
  M1_AXIS_TDATA(18) <= \<const0>\;
  M1_AXIS_TDATA(17) <= \<const0>\;
  M1_AXIS_TDATA(16) <= \<const0>\;
  M1_AXIS_TDATA(15) <= \<const0>\;
  M1_AXIS_TDATA(14) <= \<const0>\;
  M1_AXIS_TDATA(13) <= \<const0>\;
  M1_AXIS_TDATA(12) <= \<const0>\;
  M1_AXIS_TDATA(11) <= \<const0>\;
  M1_AXIS_TDATA(10) <= \<const0>\;
  M1_AXIS_TDATA(9) <= \<const0>\;
  M1_AXIS_TDATA(8) <= \<const0>\;
  M1_AXIS_TDATA(7) <= \<const0>\;
  M1_AXIS_TDATA(6) <= \<const0>\;
  M1_AXIS_TDATA(5) <= \<const0>\;
  M1_AXIS_TDATA(4) <= \<const0>\;
  M1_AXIS_TDATA(3) <= \<const0>\;
  M1_AXIS_TDATA(2) <= \<const0>\;
  M1_AXIS_TDATA(1) <= \<const0>\;
  M1_AXIS_TDATA(0) <= \<const0>\;
  M1_AXIS_TLAST <= \<const0>\;
  M1_AXIS_TVALID <= \<const0>\;
  S0_AXIS_TREADY <= \<const0>\;
  S0_AXI_AWREADY <= \^s0_axi_wready\;
  S0_AXI_BRESP(1) <= \<const0>\;
  S0_AXI_BRESP(0) <= \<const0>\;
  S0_AXI_RRESP(1) <= \<const0>\;
  S0_AXI_RRESP(0) <= \<const0>\;
  S0_AXI_WREADY <= \^s0_axi_wready\;
  S1_AXIS_TREADY <= \<const0>\;
  S1_AXI_AWREADY <= \^s1_axi_wready\;
  S1_AXI_BRESP(1) <= \<const0>\;
  S1_AXI_BRESP(0) <= \<const0>\;
  S1_AXI_RRESP(1) <= \<const0>\;
  S1_AXI_RRESP(0) <= \<const0>\;
  S1_AXI_WREADY <= \^s1_axi_wready\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
\Rst_Async.Bus_RST_FF_I0_1\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => if1_rst,
      Q => Bus1_Rst_d1,
      R => '0'
    );
\Rst_Async.Bus_RST_FF_I0_1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => S1_AXI_ARESETN,
      O => if1_rst
    );
\Rst_Async.Bus_RST_FF_I0_2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => Bus1_Rst_d1,
      Q => Bus1_Rst_d2,
      R => '0'
    );
\Rst_Async.Bus_RST_FF_I1_1\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => if0_rst,
      Q => Bus0_Rst_d1,
      R => '0'
    );
\Rst_Async.Bus_RST_FF_I1_1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => S0_AXI_ARESETN,
      O => if0_rst
    );
\Rst_Async.Bus_RST_FF_I1_2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => Bus0_Rst_d1,
      Q => Bus0_Rst_d2,
      R => '0'
    );
\Rst_Async.SYS_RST_FF_I0_1\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => SYS_Rst,
      Q => SYS_Rst_Input0_d1,
      R => '0'
    );
\Rst_Async.SYS_RST_FF_I0_2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => SYS_Rst_Input0_d1,
      Q => SYS_Rst_Input0_d2,
      R => '0'
    );
\Rst_Async.SYS_RST_FF_I1_1\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => SYS_Rst,
      Q => SYS_Rst_Input1_d1,
      R => '0'
    );
\Rst_Async.SYS_RST_FF_I1_2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => SYS_Rst_Input1_d1,
      Q => SYS_Rst_Input1_d2,
      R => '0'
    );
\Rst_Async.SYS_Rst_If0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => SYS_Rst_Input0_d2,
      I1 => S0_AXI_ARESETN,
      I2 => Bus1_Rst_d2,
      O => p_2_out
    );
\Rst_Async.SYS_Rst_If0_reg\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S0_AXI_ACLK,
      CE => '1',
      D => p_2_out,
      Q => Rst,
      R => '0'
    );
\Rst_Async.SYS_Rst_If1_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => SYS_Rst_Input1_d2,
      I1 => S1_AXI_ARESETN,
      I2 => Bus0_Rst_d2,
      O => p_0_out
    );
\Rst_Async.SYS_Rst_If1_reg\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => S1_AXI_ACLK,
      CE => '1',
      D => p_0_out,
      Q => \Rst_Async.SYS_Rst_If1_reg_n_0\,
      R => '0'
    );
\Using_Bus_0.Bus0_If\: entity work.design_1_mailbox_0_0_if_decode
     port map (
      Bus_RNW_reg => \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/Bus_RNW_reg\,
      \DataOut_reg[31]\(31) => fsl_1_to_0_n_4,
      \DataOut_reg[31]\(30) => fsl_1_to_0_n_5,
      \DataOut_reg[31]\(29) => fsl_1_to_0_n_6,
      \DataOut_reg[31]\(28) => fsl_1_to_0_n_7,
      \DataOut_reg[31]\(27) => fsl_1_to_0_n_8,
      \DataOut_reg[31]\(26) => fsl_1_to_0_n_9,
      \DataOut_reg[31]\(25) => fsl_1_to_0_n_10,
      \DataOut_reg[31]\(24) => fsl_1_to_0_n_11,
      \DataOut_reg[31]\(23) => fsl_1_to_0_n_12,
      \DataOut_reg[31]\(22) => fsl_1_to_0_n_13,
      \DataOut_reg[31]\(21) => fsl_1_to_0_n_14,
      \DataOut_reg[31]\(20) => fsl_1_to_0_n_15,
      \DataOut_reg[31]\(19) => fsl_1_to_0_n_16,
      \DataOut_reg[31]\(18) => fsl_1_to_0_n_17,
      \DataOut_reg[31]\(17) => fsl_1_to_0_n_18,
      \DataOut_reg[31]\(16) => fsl_1_to_0_n_19,
      \DataOut_reg[31]\(15) => fsl_1_to_0_n_20,
      \DataOut_reg[31]\(14) => fsl_1_to_0_n_21,
      \DataOut_reg[31]\(13) => fsl_1_to_0_n_22,
      \DataOut_reg[31]\(12) => fsl_1_to_0_n_23,
      \DataOut_reg[31]\(11) => fsl_1_to_0_n_24,
      \DataOut_reg[31]\(10) => fsl_1_to_0_n_25,
      \DataOut_reg[31]\(9) => fsl_1_to_0_n_26,
      \DataOut_reg[31]\(8) => fsl_1_to_0_n_27,
      \DataOut_reg[31]\(7) => fsl_1_to_0_n_28,
      \DataOut_reg[31]\(6) => fsl_1_to_0_n_29,
      \DataOut_reg[31]\(5) => fsl_1_to_0_n_30,
      \DataOut_reg[31]\(4) => fsl_1_to_0_n_31,
      \DataOut_reg[31]\(3) => fsl_1_to_0_n_32,
      \DataOut_reg[31]\(2) => fsl_1_to_0_n_33,
      \DataOut_reg[31]\(1) => fsl_1_to_0_n_34,
      \DataOut_reg[31]\(0) => fsl_1_to_0_n_35,
      E(0) => \Using_Bus_0.Bus0_If_n_17\,
      \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg\ => \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg\,
      \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg\ => \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg\,
      Interrupt_0 => Interrupt_0,
      Q(3) => sit_register(0),
      Q(2) => sit_register(1),
      Q(1) => sit_register(2),
      Q(0) => sit_register(3),
      Rst => Rst,
      S0_AXI_ACLK => S0_AXI_ACLK,
      S0_AXI_ARADDR(3 downto 0) => S0_AXI_ARADDR(5 downto 2),
      S0_AXI_ARREADY => S0_AXI_ARREADY,
      S0_AXI_ARVALID => S0_AXI_ARVALID,
      S0_AXI_AWADDR(3 downto 0) => S0_AXI_AWADDR(5 downto 2),
      S0_AXI_AWVALID => S0_AXI_AWVALID,
      S0_AXI_BREADY => S0_AXI_BREADY,
      S0_AXI_BVALID => S0_AXI_BVALID,
      S0_AXI_RDATA(31 downto 0) => S0_AXI_RDATA(31 downto 0),
      S0_AXI_RREADY => S0_AXI_RREADY,
      S0_AXI_RVALID => S0_AXI_RVALID,
      S0_AXI_WDATA(3 downto 0) => S0_AXI_WDATA(3 downto 0),
      S0_AXI_WREADY => \^s0_axi_wready\,
      S0_AXI_WVALID => S0_AXI_WVALID,
      empty_allow => \Using_FIFO.Async_FIFO_Gen.Async_FIFO_I1/empty_allow\,
      full_i_reg => FSL0_M_Full_I,
      \out\ => \Using_FIFO.Async_FIFO_Gen.Async_FIFO_I1/p_3_in_6\,
      rit_detect_d0 => rit_detect_d0_4,
      rit_detect_d1_reg_0(3) => rit_register(0),
      rit_detect_d1_reg_0(2) => rit_register(1),
      rit_detect_d1_reg_0(1) => rit_register(2),
      rit_detect_d1_reg_0(0) => rit_register(3),
      sit_detect_d0 => sit_detect_d0
    );
\Using_Bus_1.Bus1_If\: entity work.\design_1_mailbox_0_0_if_decode__parameterized1\
     port map (
      Bus_RNW_reg => \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/Bus_RNW_reg_1\,
      \DataOut_reg[31]\(31 downto 0) => DataOut(31 downto 0),
      E(0) => \Using_Bus_1.Bus1_If_n_17\,
      \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg\ => \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg_2\,
      \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg\ => \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg_3\,
      Interrupt_1 => Interrupt_1,
      Q(3) => \Using_Bus_1.Bus1_If_n_9\,
      Q(2) => \Using_Bus_1.Bus1_If_n_10\,
      Q(1) => \Using_Bus_1.Bus1_If_n_11\,
      Q(0) => \Using_Bus_1.Bus1_If_n_12\,
      \Rst_Async.SYS_Rst_If1_reg\ => \Rst_Async.SYS_Rst_If1_reg_n_0\,
      S1_AXI_ACLK => S1_AXI_ACLK,
      S1_AXI_ARADDR(3 downto 0) => S1_AXI_ARADDR(5 downto 2),
      S1_AXI_ARREADY => S1_AXI_ARREADY,
      S1_AXI_ARVALID => S1_AXI_ARVALID,
      S1_AXI_AWADDR(3 downto 0) => S1_AXI_AWADDR(5 downto 2),
      S1_AXI_AWVALID => S1_AXI_AWVALID,
      S1_AXI_BREADY => S1_AXI_BREADY,
      S1_AXI_BVALID => S1_AXI_BVALID,
      S1_AXI_RDATA(31 downto 0) => S1_AXI_RDATA(31 downto 0),
      S1_AXI_RREADY => S1_AXI_RREADY,
      S1_AXI_RVALID => S1_AXI_RVALID,
      S1_AXI_WDATA(3 downto 0) => S1_AXI_WDATA(3 downto 0),
      S1_AXI_WREADY => \^s1_axi_wready\,
      S1_AXI_WVALID => S1_AXI_WVALID,
      empty_allow => \Using_FIFO.Async_FIFO_Gen.Async_FIFO_I1/empty_allow_0\,
      full_i_reg => FSL1_M_Full_I,
      \out\ => \Using_FIFO.Async_FIFO_Gen.Async_FIFO_I1/p_3_in\,
      rit_detect_d0 => rit_detect_d0,
      rit_detect_d1_reg_0(3) => \Using_Bus_1.Bus1_If_n_13\,
      rit_detect_d1_reg_0(2) => \Using_Bus_1.Bus1_If_n_14\,
      rit_detect_d1_reg_0(1) => \Using_Bus_1.Bus1_If_n_15\,
      rit_detect_d1_reg_0(0) => \Using_Bus_1.Bus1_If_n_16\,
      sit_detect_d0 => sit_detect_d0_5
    );
fsl_0_to_1: entity work.design_1_mailbox_0_0_fsl_v20
     port map (
      Bus_RNW_reg => \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/Bus_RNW_reg_1\,
      Bus_RNW_reg_0 => \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/Bus_RNW_reg\,
      E(0) => \Using_Bus_1.Bus1_If_n_17\,
      \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg\ => \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg\,
      \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg\ => \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg_3\,
      Q(3) => sit_register(0),
      Q(2) => sit_register(1),
      Q(1) => sit_register(2),
      Q(0) => sit_register(3),
      S0_AXI_ACLK => S0_AXI_ACLK,
      S0_AXI_ARESETN => S0_AXI_ARESETN,
      S0_AXI_WDATA(31 downto 0) => S0_AXI_WDATA(31 downto 0),
      S1_AXI_ACLK => S1_AXI_ACLK,
      S1_AXI_ARESETN => S1_AXI_ARESETN,
      SYS_Rst => SYS_Rst,
      empty_allow => \Using_FIFO.Async_FIFO_Gen.Async_FIFO_I1/empty_allow_0\,
      \out\ => \Using_FIFO.Async_FIFO_Gen.Async_FIFO_I1/p_3_in\,
      rd_rst_meta_inst => fsl_0_to_1_n_0,
      rit_detect_d0 => rit_detect_d0,
      \rit_register_reg[0]\(3) => \Using_Bus_1.Bus1_If_n_13\,
      \rit_register_reg[0]\(2) => \Using_Bus_1.Bus1_If_n_14\,
      \rit_register_reg[0]\(1) => \Using_Bus_1.Bus1_If_n_15\,
      \rit_register_reg[0]\(0) => \Using_Bus_1.Bus1_If_n_16\,
      \s_axi_rdata_i_reg[31]\(31 downto 0) => DataOut(31 downto 0),
      sit_detect_d0 => sit_detect_d0,
      \write_nextgray_reg[2]\ => FSL0_M_Full_I
    );
fsl_1_to_0: entity work.design_1_mailbox_0_0_fsl_v20_0
     port map (
      Bus_RNW_reg => \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/Bus_RNW_reg\,
      Bus_RNW_reg_0 => \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/Bus_RNW_reg_1\,
      DataOut(31) => fsl_1_to_0_n_4,
      DataOut(30) => fsl_1_to_0_n_5,
      DataOut(29) => fsl_1_to_0_n_6,
      DataOut(28) => fsl_1_to_0_n_7,
      DataOut(27) => fsl_1_to_0_n_8,
      DataOut(26) => fsl_1_to_0_n_9,
      DataOut(25) => fsl_1_to_0_n_10,
      DataOut(24) => fsl_1_to_0_n_11,
      DataOut(23) => fsl_1_to_0_n_12,
      DataOut(22) => fsl_1_to_0_n_13,
      DataOut(21) => fsl_1_to_0_n_14,
      DataOut(20) => fsl_1_to_0_n_15,
      DataOut(19) => fsl_1_to_0_n_16,
      DataOut(18) => fsl_1_to_0_n_17,
      DataOut(17) => fsl_1_to_0_n_18,
      DataOut(16) => fsl_1_to_0_n_19,
      DataOut(15) => fsl_1_to_0_n_20,
      DataOut(14) => fsl_1_to_0_n_21,
      DataOut(13) => fsl_1_to_0_n_22,
      DataOut(12) => fsl_1_to_0_n_23,
      DataOut(11) => fsl_1_to_0_n_24,
      DataOut(10) => fsl_1_to_0_n_25,
      DataOut(9) => fsl_1_to_0_n_26,
      DataOut(8) => fsl_1_to_0_n_27,
      DataOut(7) => fsl_1_to_0_n_28,
      DataOut(6) => fsl_1_to_0_n_29,
      DataOut(5) => fsl_1_to_0_n_30,
      DataOut(4) => fsl_1_to_0_n_31,
      DataOut(3) => fsl_1_to_0_n_32,
      DataOut(2) => fsl_1_to_0_n_33,
      DataOut(1) => fsl_1_to_0_n_34,
      DataOut(0) => fsl_1_to_0_n_35,
      E(0) => \Using_Bus_0.Bus0_If_n_17\,
      \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg\ => \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg_2\,
      \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg\ => \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg\,
      Q(3) => \Using_Bus_1.Bus1_If_n_9\,
      Q(2) => \Using_Bus_1.Bus1_If_n_10\,
      Q(1) => \Using_Bus_1.Bus1_If_n_11\,
      Q(0) => \Using_Bus_1.Bus1_If_n_12\,
      S0_AXI_ACLK => S0_AXI_ACLK,
      S1_AXI_ACLK => S1_AXI_ACLK,
      S1_AXI_ARESETN => fsl_0_to_1_n_0,
      S1_AXI_WDATA(31 downto 0) => S1_AXI_WDATA(31 downto 0),
      empty_allow => \Using_FIFO.Async_FIFO_Gen.Async_FIFO_I1/empty_allow\,
      \out\ => \Using_FIFO.Async_FIFO_Gen.Async_FIFO_I1/p_3_in_6\,
      rit_detect_d0 => rit_detect_d0_4,
      \rit_register_reg[0]\(3) => rit_register(0),
      \rit_register_reg[0]\(2) => rit_register(1),
      \rit_register_reg[0]\(1) => rit_register(2),
      \rit_register_reg[0]\(0) => rit_register(3),
      sit_detect_d0 => sit_detect_d0_5,
      \write_addr_reg[0]\ => FSL1_M_Full_I
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_mailbox_0_0 is
  port (
    S0_AXI_ACLK : in STD_LOGIC;
    S0_AXI_ARESETN : in STD_LOGIC;
    S0_AXI_AWADDR : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S0_AXI_AWVALID : in STD_LOGIC;
    S0_AXI_AWREADY : out STD_LOGIC;
    S0_AXI_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S0_AXI_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S0_AXI_WVALID : in STD_LOGIC;
    S0_AXI_WREADY : out STD_LOGIC;
    S0_AXI_BRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S0_AXI_BVALID : out STD_LOGIC;
    S0_AXI_BREADY : in STD_LOGIC;
    S0_AXI_ARADDR : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S0_AXI_ARVALID : in STD_LOGIC;
    S0_AXI_ARREADY : out STD_LOGIC;
    S0_AXI_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S0_AXI_RRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S0_AXI_RVALID : out STD_LOGIC;
    S0_AXI_RREADY : in STD_LOGIC;
    S1_AXI_ACLK : in STD_LOGIC;
    S1_AXI_ARESETN : in STD_LOGIC;
    S1_AXI_AWADDR : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S1_AXI_AWVALID : in STD_LOGIC;
    S1_AXI_AWREADY : out STD_LOGIC;
    S1_AXI_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S1_AXI_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S1_AXI_WVALID : in STD_LOGIC;
    S1_AXI_WREADY : out STD_LOGIC;
    S1_AXI_BRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S1_AXI_BVALID : out STD_LOGIC;
    S1_AXI_BREADY : in STD_LOGIC;
    S1_AXI_ARADDR : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S1_AXI_ARVALID : in STD_LOGIC;
    S1_AXI_ARREADY : out STD_LOGIC;
    S1_AXI_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S1_AXI_RRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S1_AXI_RVALID : out STD_LOGIC;
    S1_AXI_RREADY : in STD_LOGIC;
    Interrupt_0 : out STD_LOGIC;
    Interrupt_1 : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_mailbox_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_mailbox_0_0 : entity is "design_1_mailbox_0_0,mailbox,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_1_mailbox_0_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_1_mailbox_0_0 : entity is "mailbox,Vivado 2016.4";
end design_1_mailbox_0_0;

architecture STRUCTURE of design_1_mailbox_0_0 is
  signal NLW_U0_M0_AXIS_TLAST_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_M0_AXIS_TVALID_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_M1_AXIS_TLAST_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_M1_AXIS_TVALID_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_S0_AXIS_TREADY_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_S1_AXIS_TREADY_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_M0_AXIS_TDATA_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_U0_M1_AXIS_TDATA_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  attribute C_ASYNC_CLKS : integer;
  attribute C_ASYNC_CLKS of U0 : label is 1;
  attribute C_ENABLE_BUS_ERROR : integer;
  attribute C_ENABLE_BUS_ERROR of U0 : label is 0;
  attribute C_EXT_RESET_HIGH : integer;
  attribute C_EXT_RESET_HIGH of U0 : label is 1;
  attribute C_FAMILY : string;
  attribute C_FAMILY of U0 : label is "zynq";
  attribute C_IMPL_STYLE : integer;
  attribute C_IMPL_STYLE of U0 : label is 0;
  attribute C_INTERCONNECT_PORT_0 : integer;
  attribute C_INTERCONNECT_PORT_0 of U0 : label is 2;
  attribute C_INTERCONNECT_PORT_1 : integer;
  attribute C_INTERCONNECT_PORT_1 of U0 : label is 2;
  attribute C_M0_AXIS_DATA_WIDTH : integer;
  attribute C_M0_AXIS_DATA_WIDTH of U0 : label is 32;
  attribute C_M1_AXIS_DATA_WIDTH : integer;
  attribute C_M1_AXIS_DATA_WIDTH of U0 : label is 32;
  attribute C_MAILBOX_DEPTH : integer;
  attribute C_MAILBOX_DEPTH of U0 : label is 16;
  attribute C_NUM_SYNC_FF : integer;
  attribute C_NUM_SYNC_FF of U0 : label is 2;
  attribute C_S0_AXIS_DATA_WIDTH : integer;
  attribute C_S0_AXIS_DATA_WIDTH of U0 : label is 32;
  attribute C_S0_AXI_ADDR_WIDTH : integer;
  attribute C_S0_AXI_ADDR_WIDTH of U0 : label is 32;
  attribute C_S0_AXI_BASEADDR : integer;
  attribute C_S0_AXI_BASEADDR of U0 : label is 1132462080;
  attribute C_S0_AXI_DATA_WIDTH : integer;
  attribute C_S0_AXI_DATA_WIDTH of U0 : label is 32;
  attribute C_S0_AXI_HIGHADDR : integer;
  attribute C_S0_AXI_HIGHADDR of U0 : label is 1132527615;
  attribute C_S1_AXIS_DATA_WIDTH : integer;
  attribute C_S1_AXIS_DATA_WIDTH of U0 : label is 32;
  attribute C_S1_AXI_ADDR_WIDTH : integer;
  attribute C_S1_AXI_ADDR_WIDTH of U0 : label is 32;
  attribute C_S1_AXI_BASEADDR : integer;
  attribute C_S1_AXI_BASEADDR of U0 : label is 1130364928;
  attribute C_S1_AXI_DATA_WIDTH : integer;
  attribute C_S1_AXI_DATA_WIDTH of U0 : label is 32;
  attribute C_S1_AXI_HIGHADDR : integer;
  attribute C_S1_AXI_HIGHADDR of U0 : label is 1130430463;
begin
U0: entity work.design_1_mailbox_0_0_mailbox
     port map (
      Interrupt_0 => Interrupt_0,
      Interrupt_1 => Interrupt_1,
      M0_AXIS_ACLK => '0',
      M0_AXIS_TDATA(31 downto 0) => NLW_U0_M0_AXIS_TDATA_UNCONNECTED(31 downto 0),
      M0_AXIS_TLAST => NLW_U0_M0_AXIS_TLAST_UNCONNECTED,
      M0_AXIS_TREADY => '0',
      M0_AXIS_TVALID => NLW_U0_M0_AXIS_TVALID_UNCONNECTED,
      M1_AXIS_ACLK => '0',
      M1_AXIS_TDATA(31 downto 0) => NLW_U0_M1_AXIS_TDATA_UNCONNECTED(31 downto 0),
      M1_AXIS_TLAST => NLW_U0_M1_AXIS_TLAST_UNCONNECTED,
      M1_AXIS_TREADY => '0',
      M1_AXIS_TVALID => NLW_U0_M1_AXIS_TVALID_UNCONNECTED,
      S0_AXIS_ACLK => '0',
      S0_AXIS_TDATA(31 downto 0) => B"00000000000000000000000000000000",
      S0_AXIS_TLAST => '0',
      S0_AXIS_TREADY => NLW_U0_S0_AXIS_TREADY_UNCONNECTED,
      S0_AXIS_TVALID => '0',
      S0_AXI_ACLK => S0_AXI_ACLK,
      S0_AXI_ARADDR(31 downto 0) => S0_AXI_ARADDR(31 downto 0),
      S0_AXI_ARESETN => S0_AXI_ARESETN,
      S0_AXI_ARREADY => S0_AXI_ARREADY,
      S0_AXI_ARVALID => S0_AXI_ARVALID,
      S0_AXI_AWADDR(31 downto 0) => S0_AXI_AWADDR(31 downto 0),
      S0_AXI_AWREADY => S0_AXI_AWREADY,
      S0_AXI_AWVALID => S0_AXI_AWVALID,
      S0_AXI_BREADY => S0_AXI_BREADY,
      S0_AXI_BRESP(1 downto 0) => S0_AXI_BRESP(1 downto 0),
      S0_AXI_BVALID => S0_AXI_BVALID,
      S0_AXI_RDATA(31 downto 0) => S0_AXI_RDATA(31 downto 0),
      S0_AXI_RREADY => S0_AXI_RREADY,
      S0_AXI_RRESP(1 downto 0) => S0_AXI_RRESP(1 downto 0),
      S0_AXI_RVALID => S0_AXI_RVALID,
      S0_AXI_WDATA(31 downto 0) => S0_AXI_WDATA(31 downto 0),
      S0_AXI_WREADY => S0_AXI_WREADY,
      S0_AXI_WSTRB(3 downto 0) => S0_AXI_WSTRB(3 downto 0),
      S0_AXI_WVALID => S0_AXI_WVALID,
      S1_AXIS_ACLK => '0',
      S1_AXIS_TDATA(31 downto 0) => B"00000000000000000000000000000000",
      S1_AXIS_TLAST => '0',
      S1_AXIS_TREADY => NLW_U0_S1_AXIS_TREADY_UNCONNECTED,
      S1_AXIS_TVALID => '0',
      S1_AXI_ACLK => S1_AXI_ACLK,
      S1_AXI_ARADDR(31 downto 0) => S1_AXI_ARADDR(31 downto 0),
      S1_AXI_ARESETN => S1_AXI_ARESETN,
      S1_AXI_ARREADY => S1_AXI_ARREADY,
      S1_AXI_ARVALID => S1_AXI_ARVALID,
      S1_AXI_AWADDR(31 downto 0) => S1_AXI_AWADDR(31 downto 0),
      S1_AXI_AWREADY => S1_AXI_AWREADY,
      S1_AXI_AWVALID => S1_AXI_AWVALID,
      S1_AXI_BREADY => S1_AXI_BREADY,
      S1_AXI_BRESP(1 downto 0) => S1_AXI_BRESP(1 downto 0),
      S1_AXI_BVALID => S1_AXI_BVALID,
      S1_AXI_RDATA(31 downto 0) => S1_AXI_RDATA(31 downto 0),
      S1_AXI_RREADY => S1_AXI_RREADY,
      S1_AXI_RRESP(1 downto 0) => S1_AXI_RRESP(1 downto 0),
      S1_AXI_RVALID => S1_AXI_RVALID,
      S1_AXI_WDATA(31 downto 0) => S1_AXI_WDATA(31 downto 0),
      S1_AXI_WREADY => S1_AXI_WREADY,
      S1_AXI_WSTRB(3 downto 0) => S1_AXI_WSTRB(3 downto 0),
      S1_AXI_WVALID => S1_AXI_WVALID,
      SYS_Rst => '0'
    );
end STRUCTURE;
