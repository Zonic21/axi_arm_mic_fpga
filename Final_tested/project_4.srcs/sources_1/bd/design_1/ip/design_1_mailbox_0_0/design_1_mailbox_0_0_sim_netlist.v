// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.4 (win64) Build 1756540 Mon Jan 23 19:11:23 MST 2017
// Date        : Thu May 18 14:25:23 2017
// Host        : DESKTOP-CLNFD4A running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top design_1_mailbox_0_0 -prefix
//               design_1_mailbox_0_0_ design_1_mailbox_0_0_sim_netlist.v
// Design      : design_1_mailbox_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_mailbox_0_0_Async_FIFO
   (out,
    \write_addr_reg[0]_0 ,
    sit_detect_d0,
    rit_detect_d0,
    DataOut,
    S0_AXI_ACLK,
    S1_AXI_ARESETN,
    S1_AXI_ACLK,
    empty_allow,
    E,
    Q,
    Bus_RNW_reg,
    \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ,
    \rit_register_reg[0] ,
    \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ,
    Bus_RNW_reg_0,
    S1_AXI_WDATA);
  output out;
  output \write_addr_reg[0]_0 ;
  output sit_detect_d0;
  output rit_detect_d0;
  output [31:0]DataOut;
  input S0_AXI_ACLK;
  input S1_AXI_ARESETN;
  input S1_AXI_ACLK;
  input empty_allow;
  input [0:0]E;
  input [3:0]Q;
  input Bus_RNW_reg;
  input \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  input [3:0]\rit_register_reg[0] ;
  input \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ;
  input Bus_RNW_reg_0;
  input [31:0]S1_AXI_WDATA;

  wire [2:0]Bin2Gray;
  wire Bus_RNW_reg;
  wire Bus_RNW_reg_0;
  wire CI;
  wire [31:0]DataOut;
  wire [0:0]E;
  wire \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  wire [2:0]Gray2Bin;
  wire [3:0]Q;
  wire \Rd_Length_i[0]_i_1_n_0 ;
  wire \Rd_Length_i[1]_i_1_n_0 ;
  wire \Rd_Length_i[2]_i_1_n_0 ;
  wire \Rd_Length_i[3]_i_1_n_0 ;
  wire \Rd_Length_i[3]_i_2_n_0 ;
  wire \Rd_Length_i_reg_n_0_[0] ;
  wire \Rd_Length_i_reg_n_0_[1] ;
  wire \Rd_Length_i_reg_n_0_[2] ;
  wire \Rd_Length_i_reg_n_0_[3] ;
  wire S;
  wire S0;
  wire S00_out;
  wire S01_out;
  wire S02_out;
  wire S04_out;
  wire S05_out;
  wire S06_out;
  wire S0_AXI_ACLK;
  wire S1_AXI_ACLK;
  wire S1_AXI_ARESETN;
  wire [31:0]S1_AXI_WDATA;
  wire \Wr_Length_i[0]_i_1__0_n_0 ;
  wire \Wr_Length_i[1]_i_1__0_n_0 ;
  wire \Wr_Length_i[2]_i_1__0_n_0 ;
  wire \Wr_Length_i[3]_i_1__0_n_0 ;
  wire \Wr_Length_i[3]_i_2__0_n_0 ;
  wire \Wr_Length_i_reg_n_0_[0] ;
  wire \Wr_Length_i_reg_n_0_[1] ;
  wire \Wr_Length_i_reg_n_0_[2] ;
  wire \Wr_Length_i_reg_n_0_[3] ;
  (* async_reg = "true" *) wire empty;
  wire empty_allow;
  wire emptyg;
  wire emuxcyo_0;
  wire emuxcyo_1;
  wire fmuxcyo_0;
  wire fmuxcyo_1;
  wire fmuxcyo_2;
  wire full_i_i_1__0_n_0;
  wire fullg;
  wire p_1_in16_in;
  wire p_1_in20_in;
  wire p_2_in17_in;
  wire p_2_in22_in;
  wire p_2_in52_in;
  wire p_2_in57_in;
  wire p_2_in62_in;
  wire [3:0]plusOp;
  wire ram_mem_reg_0_15_0_5_i_1__0_n_0;
  wire ram_mem_reg_0_15_0_5_i_2__0_n_0;
  wire ram_mem_reg_0_15_0_5_i_3__0_n_0;
  wire ram_mem_reg_0_15_0_5_i_4__0_n_0;
  wire ram_mem_reg_0_15_0_5_n_0;
  wire ram_mem_reg_0_15_0_5_n_1;
  wire ram_mem_reg_0_15_0_5_n_2;
  wire ram_mem_reg_0_15_0_5_n_3;
  wire ram_mem_reg_0_15_0_5_n_4;
  wire ram_mem_reg_0_15_0_5_n_5;
  wire ram_mem_reg_0_15_12_17_n_0;
  wire ram_mem_reg_0_15_12_17_n_1;
  wire ram_mem_reg_0_15_12_17_n_2;
  wire ram_mem_reg_0_15_12_17_n_3;
  wire ram_mem_reg_0_15_12_17_n_4;
  wire ram_mem_reg_0_15_12_17_n_5;
  wire ram_mem_reg_0_15_18_23_n_0;
  wire ram_mem_reg_0_15_18_23_n_1;
  wire ram_mem_reg_0_15_18_23_n_2;
  wire ram_mem_reg_0_15_18_23_n_3;
  wire ram_mem_reg_0_15_18_23_n_4;
  wire ram_mem_reg_0_15_18_23_n_5;
  wire ram_mem_reg_0_15_24_29_n_0;
  wire ram_mem_reg_0_15_24_29_n_1;
  wire ram_mem_reg_0_15_24_29_n_2;
  wire ram_mem_reg_0_15_24_29_n_3;
  wire ram_mem_reg_0_15_24_29_n_4;
  wire ram_mem_reg_0_15_24_29_n_5;
  wire ram_mem_reg_0_15_30_31_n_0;
  wire ram_mem_reg_0_15_30_31_n_1;
  wire ram_mem_reg_0_15_6_11_n_0;
  wire ram_mem_reg_0_15_6_11_n_1;
  wire ram_mem_reg_0_15_6_11_n_2;
  wire ram_mem_reg_0_15_6_11_n_3;
  wire ram_mem_reg_0_15_6_11_n_4;
  wire ram_mem_reg_0_15_6_11_n_5;
  wire rd_meta_reset;
  wire rd_reset;
  (* async_reg = "true" *) wire [3:0]\rd_write_addrgray_dx[0]_4 ;
  (* async_reg = "true" *) wire [3:0]\rd_write_addrgray_dx[1]_5 ;
  (* async_reg = "true" *) wire [3:0]\rd_write_addrgray_dx[2]_8 ;
  wire \rd_write_next_reg_n_0_[0] ;
  wire \rd_write_next_reg_n_0_[1] ;
  wire \rd_write_next_reg_n_0_[2] ;
  wire \rd_write_next_reg_n_0_[3] ;
  (* async_reg = "true" *) wire [3:0]\rd_write_nextgray_dx[0]_2 ;
  (* async_reg = "true" *) wire [3:0]\rd_write_nextgray_dx[1]_3 ;
  (* async_reg = "true" *) wire [3:0]\rd_write_nextgray_dx[2]_9 ;
  wire \read_addr_d1_reg_n_0_[0] ;
  wire \read_addr_d1_reg_n_0_[1] ;
  wire \read_addr_d1_reg_n_0_[2] ;
  wire \read_addr_reg_n_0_[0] ;
  wire \read_addr_reg_n_0_[3] ;
  wire \read_addrgray_reg_n_0_[0] ;
  wire [3:0]read_lastgray;
  wire [3:0]read_nextgray;
  wire rit_detect_d0;
  wire rit_detect_d1_i_2__0_n_0;
  wire [3:0]\rit_register_reg[0] ;
  wire sit_detect_d0;
  wire sit_detect_d1_i_2_n_0;
  wire wr_meta_reset;
  (* async_reg = "true" *) wire [3:0]\wr_read_lastgray_dx[0]_6 ;
  (* async_reg = "true" *) wire [3:0]\wr_read_lastgray_dx[1]_7 ;
  (* async_reg = "true" *) wire [3:0]\wr_read_lastgray_dx[2]_10 ;
  wire \wr_read_next[0]_i_1_n_0 ;
  wire \wr_read_next[1]_i_1_n_0 ;
  wire \wr_read_next[2]_i_1_n_0 ;
  wire \wr_read_next_reg_n_0_[0] ;
  wire \wr_read_next_reg_n_0_[1] ;
  wire \wr_read_next_reg_n_0_[2] ;
  wire \wr_read_next_reg_n_0_[3] ;
  (* async_reg = "true" *) wire [3:0]\wr_read_nextgray_dx[0]_0 ;
  (* async_reg = "true" *) wire [3:0]\wr_read_nextgray_dx[1]_1 ;
  (* async_reg = "true" *) wire [3:0]\wr_read_nextgray_dx[2]_11 ;
  wire wr_reset;
  wire \write_addr_d1_reg_n_0_[0] ;
  wire \write_addr_d1_reg_n_0_[1] ;
  wire \write_addr_d1_reg_n_0_[2] ;
  (* async_reg = "true" *) wire \write_addr_reg[0]_0 ;
  wire \write_addr_reg_n_0_[0] ;
  wire \write_addr_reg_n_0_[3] ;
  wire [3:0]write_addrgray;
  wire [3:0]write_nextgray;
  wire \write_nextgray[0]_i_1_n_0 ;
  wire \write_nextgray[1]_i_1_n_0 ;
  wire \write_nextgray[2]_i_1_n_0 ;
  wire \write_nextgray[2]_i_2_n_0 ;
  wire [3:0]NLW_emuxcylow_CARRY4_O_UNCONNECTED;
  wire [3:0]NLW_fmuxcylow_CARRY4_O_UNCONNECTED;
  wire [1:0]NLW_ram_mem_reg_0_15_0_5_DOD_UNCONNECTED;
  wire [1:0]NLW_ram_mem_reg_0_15_12_17_DOD_UNCONNECTED;
  wire [1:0]NLW_ram_mem_reg_0_15_18_23_DOD_UNCONNECTED;
  wire [1:0]NLW_ram_mem_reg_0_15_24_29_DOD_UNCONNECTED;
  wire [1:0]NLW_ram_mem_reg_0_15_30_31_DOB_UNCONNECTED;
  wire [1:0]NLW_ram_mem_reg_0_15_30_31_DOC_UNCONNECTED;
  wire [1:0]NLW_ram_mem_reg_0_15_30_31_DOD_UNCONNECTED;
  wire [1:0]NLW_ram_mem_reg_0_15_6_11_DOD_UNCONNECTED;

  assign out = empty;
  FDRE \DataOut_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_0_5_n_1),
        .Q(DataOut[0]),
        .R(1'b0));
  FDRE \DataOut_reg[10] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_6_11_n_5),
        .Q(DataOut[10]),
        .R(1'b0));
  FDRE \DataOut_reg[11] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_6_11_n_4),
        .Q(DataOut[11]),
        .R(1'b0));
  FDRE \DataOut_reg[12] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_12_17_n_1),
        .Q(DataOut[12]),
        .R(1'b0));
  FDRE \DataOut_reg[13] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_12_17_n_0),
        .Q(DataOut[13]),
        .R(1'b0));
  FDRE \DataOut_reg[14] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_12_17_n_3),
        .Q(DataOut[14]),
        .R(1'b0));
  FDRE \DataOut_reg[15] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_12_17_n_2),
        .Q(DataOut[15]),
        .R(1'b0));
  FDRE \DataOut_reg[16] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_12_17_n_5),
        .Q(DataOut[16]),
        .R(1'b0));
  FDRE \DataOut_reg[17] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_12_17_n_4),
        .Q(DataOut[17]),
        .R(1'b0));
  FDRE \DataOut_reg[18] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_18_23_n_1),
        .Q(DataOut[18]),
        .R(1'b0));
  FDRE \DataOut_reg[19] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_18_23_n_0),
        .Q(DataOut[19]),
        .R(1'b0));
  FDRE \DataOut_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_0_5_n_0),
        .Q(DataOut[1]),
        .R(1'b0));
  FDRE \DataOut_reg[20] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_18_23_n_3),
        .Q(DataOut[20]),
        .R(1'b0));
  FDRE \DataOut_reg[21] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_18_23_n_2),
        .Q(DataOut[21]),
        .R(1'b0));
  FDRE \DataOut_reg[22] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_18_23_n_5),
        .Q(DataOut[22]),
        .R(1'b0));
  FDRE \DataOut_reg[23] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_18_23_n_4),
        .Q(DataOut[23]),
        .R(1'b0));
  FDRE \DataOut_reg[24] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_24_29_n_1),
        .Q(DataOut[24]),
        .R(1'b0));
  FDRE \DataOut_reg[25] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_24_29_n_0),
        .Q(DataOut[25]),
        .R(1'b0));
  FDRE \DataOut_reg[26] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_24_29_n_3),
        .Q(DataOut[26]),
        .R(1'b0));
  FDRE \DataOut_reg[27] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_24_29_n_2),
        .Q(DataOut[27]),
        .R(1'b0));
  FDRE \DataOut_reg[28] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_24_29_n_5),
        .Q(DataOut[28]),
        .R(1'b0));
  FDRE \DataOut_reg[29] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_24_29_n_4),
        .Q(DataOut[29]),
        .R(1'b0));
  FDRE \DataOut_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_0_5_n_3),
        .Q(DataOut[2]),
        .R(1'b0));
  FDRE \DataOut_reg[30] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_30_31_n_1),
        .Q(DataOut[30]),
        .R(1'b0));
  FDRE \DataOut_reg[31] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_30_31_n_0),
        .Q(DataOut[31]),
        .R(1'b0));
  FDRE \DataOut_reg[3] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_0_5_n_2),
        .Q(DataOut[3]),
        .R(1'b0));
  FDRE \DataOut_reg[4] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_0_5_n_5),
        .Q(DataOut[4]),
        .R(1'b0));
  FDRE \DataOut_reg[5] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_0_5_n_4),
        .Q(DataOut[5]),
        .R(1'b0));
  FDRE \DataOut_reg[6] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_6_11_n_1),
        .Q(DataOut[6]),
        .R(1'b0));
  FDRE \DataOut_reg[7] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_6_11_n_0),
        .Q(DataOut[7]),
        .R(1'b0));
  FDRE \DataOut_reg[8] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_6_11_n_3),
        .Q(DataOut[8]),
        .R(1'b0));
  FDRE \DataOut_reg[9] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(ram_mem_reg_0_15_6_11_n_2),
        .Q(DataOut[9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h9A95)) 
    \Gen_emuxcy[1].emuxcy_i_1__0 
       (.I0(\rd_write_addrgray_dx[1]_5 [1]),
        .I1(p_2_in52_in),
        .I2(empty),
        .I3(read_nextgray[1]),
        .O(S04_out));
  LUT4 #(
    .INIT(16'h9A95)) 
    \Gen_emuxcy[2].emuxcy_i_1__0 
       (.I0(\rd_write_addrgray_dx[1]_5 [2]),
        .I1(p_2_in57_in),
        .I2(empty),
        .I3(read_nextgray[2]),
        .O(S05_out));
  LUT4 #(
    .INIT(16'h9A95)) 
    \Gen_fmuxcy[1].fmuxcy_i_1 
       (.I0(\wr_read_lastgray_dx[1]_7 [1]),
        .I1(write_addrgray[1]),
        .I2(\write_addr_reg[0]_0 ),
        .I3(write_nextgray[1]),
        .O(S00_out));
  LUT4 #(
    .INIT(16'h9A95)) 
    \Gen_fmuxcy[2].fmuxcy_i_1 
       (.I0(\wr_read_lastgray_dx[1]_7 [2]),
        .I1(write_addrgray[2]),
        .I2(\write_addr_reg[0]_0 ),
        .I3(write_nextgray[2]),
        .O(S01_out));
  LUT2 #(
    .INIT(4'h6)) 
    \Rd_Length_i[0]_i_1 
       (.I0(\rd_write_next_reg_n_0_[0] ),
        .I1(\read_addr_d1_reg_n_0_[0] ),
        .O(\Rd_Length_i[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT4 #(
    .INIT(16'h2DD2)) 
    \Rd_Length_i[1]_i_1 
       (.I0(\read_addr_d1_reg_n_0_[0] ),
        .I1(\rd_write_next_reg_n_0_[0] ),
        .I2(\read_addr_d1_reg_n_0_[1] ),
        .I3(\rd_write_next_reg_n_0_[1] ),
        .O(\Rd_Length_i[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F04B0FBB0FB4F04)) 
    \Rd_Length_i[2]_i_1 
       (.I0(\rd_write_next_reg_n_0_[0] ),
        .I1(\read_addr_d1_reg_n_0_[0] ),
        .I2(\rd_write_next_reg_n_0_[1] ),
        .I3(\read_addr_d1_reg_n_0_[1] ),
        .I4(\read_addr_d1_reg_n_0_[2] ),
        .I5(\rd_write_next_reg_n_0_[2] ),
        .O(\Rd_Length_i[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h718E8E71)) 
    \Rd_Length_i[3]_i_1 
       (.I0(\Rd_Length_i[3]_i_2_n_0 ),
        .I1(\rd_write_next_reg_n_0_[2] ),
        .I2(\read_addr_d1_reg_n_0_[2] ),
        .I3(read_nextgray[3]),
        .I4(\rd_write_next_reg_n_0_[3] ),
        .O(\Rd_Length_i[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT4 #(
    .INIT(16'hDD4D)) 
    \Rd_Length_i[3]_i_2 
       (.I0(\read_addr_d1_reg_n_0_[1] ),
        .I1(\rd_write_next_reg_n_0_[1] ),
        .I2(\read_addr_d1_reg_n_0_[0] ),
        .I3(\rd_write_next_reg_n_0_[0] ),
        .O(\Rd_Length_i[3]_i_2_n_0 ));
  FDCE \Rd_Length_i_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .CLR(rd_reset),
        .D(\Rd_Length_i[0]_i_1_n_0 ),
        .Q(\Rd_Length_i_reg_n_0_[0] ));
  FDCE \Rd_Length_i_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .CLR(rd_reset),
        .D(\Rd_Length_i[1]_i_1_n_0 ),
        .Q(\Rd_Length_i_reg_n_0_[1] ));
  FDCE \Rd_Length_i_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .CLR(rd_reset),
        .D(\Rd_Length_i[2]_i_1_n_0 ),
        .Q(\Rd_Length_i_reg_n_0_[2] ));
  FDCE \Rd_Length_i_reg[3] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .CLR(rd_reset),
        .D(\Rd_Length_i[3]_i_1_n_0 ),
        .Q(\Rd_Length_i_reg_n_0_[3] ));
  LUT2 #(
    .INIT(4'h6)) 
    \Wr_Length_i[0]_i_1__0 
       (.I0(\write_addr_d1_reg_n_0_[0] ),
        .I1(\wr_read_next_reg_n_0_[0] ),
        .O(\Wr_Length_i[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT4 #(
    .INIT(16'h2DD2)) 
    \Wr_Length_i[1]_i_1__0 
       (.I0(\wr_read_next_reg_n_0_[0] ),
        .I1(\write_addr_d1_reg_n_0_[0] ),
        .I2(\wr_read_next_reg_n_0_[1] ),
        .I3(\write_addr_d1_reg_n_0_[1] ),
        .O(\Wr_Length_i[1]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h4F04B0FBB0FB4F04)) 
    \Wr_Length_i[2]_i_1__0 
       (.I0(\write_addr_d1_reg_n_0_[0] ),
        .I1(\wr_read_next_reg_n_0_[0] ),
        .I2(\write_addr_d1_reg_n_0_[1] ),
        .I3(\wr_read_next_reg_n_0_[1] ),
        .I4(\wr_read_next_reg_n_0_[2] ),
        .I5(\write_addr_d1_reg_n_0_[2] ),
        .O(\Wr_Length_i[2]_i_1__0_n_0 ));
  LUT5 #(
    .INIT(32'h718E8E71)) 
    \Wr_Length_i[3]_i_1__0 
       (.I0(\Wr_Length_i[3]_i_2__0_n_0 ),
        .I1(\write_addr_d1_reg_n_0_[2] ),
        .I2(\wr_read_next_reg_n_0_[2] ),
        .I3(\wr_read_next_reg_n_0_[3] ),
        .I4(write_nextgray[3]),
        .O(\Wr_Length_i[3]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT4 #(
    .INIT(16'hDD4D)) 
    \Wr_Length_i[3]_i_2__0 
       (.I0(\wr_read_next_reg_n_0_[1] ),
        .I1(\write_addr_d1_reg_n_0_[1] ),
        .I2(\wr_read_next_reg_n_0_[0] ),
        .I3(\write_addr_d1_reg_n_0_[0] ),
        .O(\Wr_Length_i[3]_i_2__0_n_0 ));
  FDCE \Wr_Length_i_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .CLR(wr_reset),
        .D(\Wr_Length_i[0]_i_1__0_n_0 ),
        .Q(\Wr_Length_i_reg_n_0_[0] ));
  FDCE \Wr_Length_i_reg[1] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .CLR(wr_reset),
        .D(\Wr_Length_i[1]_i_1__0_n_0 ),
        .Q(\Wr_Length_i_reg_n_0_[1] ));
  FDCE \Wr_Length_i_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .CLR(wr_reset),
        .D(\Wr_Length_i[2]_i_1__0_n_0 ),
        .Q(\Wr_Length_i_reg_n_0_[2] ));
  FDCE \Wr_Length_i_reg[3] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .CLR(wr_reset),
        .D(\Wr_Length_i[3]_i_1__0_n_0 ),
        .Q(\Wr_Length_i_reg_n_0_[3] ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE empty_reg
       (.C(S0_AXI_ACLK),
        .CE(empty_allow),
        .D(emptyg),
        .PRE(rd_reset),
        .Q(empty));
  LUT4 #(
    .INIT(16'h9A95)) 
    emuxcyhigh_i_1__0
       (.I0(\rd_write_addrgray_dx[1]_5 [3]),
        .I1(p_2_in62_in),
        .I2(empty),
        .I3(read_nextgray[3]),
        .O(S06_out));
  (* XILINX_LEGACY_PRIM = "(MUXCY,XORCY)" *) 
  (* XILINX_TRANSFORM_PINMAP = "LO:O" *) 
  (* box_type = "PRIMITIVE" *) 
  CARRY4 emuxcylow_CARRY4
       (.CI(1'b0),
        .CO({emptyg,CI,emuxcyo_1,emuxcyo_0}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_emuxcylow_CARRY4_O_UNCONNECTED[3:0]),
        .S({S06_out,S05_out,S04_out,S}));
  LUT4 #(
    .INIT(16'h9A95)) 
    emuxcylow_i_1__0
       (.I0(\rd_write_addrgray_dx[1]_5 [0]),
        .I1(\read_addrgray_reg_n_0_[0] ),
        .I2(empty),
        .I3(read_nextgray[0]),
        .O(S));
  LUT4 #(
    .INIT(16'h9A95)) 
    fmuxcyhigh_i_1
       (.I0(\wr_read_lastgray_dx[1]_7 [3]),
        .I1(write_addrgray[3]),
        .I2(\write_addr_reg[0]_0 ),
        .I3(write_nextgray[3]),
        .O(S02_out));
  (* XILINX_LEGACY_PRIM = "(MUXCY,XORCY)" *) 
  (* XILINX_TRANSFORM_PINMAP = "LO:O" *) 
  (* box_type = "PRIMITIVE" *) 
  CARRY4 fmuxcylow_CARRY4
       (.CI(1'b0),
        .CO({fullg,fmuxcyo_2,fmuxcyo_1,fmuxcyo_0}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_fmuxcylow_CARRY4_O_UNCONNECTED[3:0]),
        .S({S02_out,S01_out,S00_out,S0}));
  LUT4 #(
    .INIT(16'h9A95)) 
    fmuxcylow_i_1
       (.I0(\wr_read_lastgray_dx[1]_7 [0]),
        .I1(write_addrgray[0]),
        .I2(\write_addr_reg[0]_0 ),
        .I3(write_nextgray[0]),
        .O(S0));
  LUT5 #(
    .INIT(32'h88888A88)) 
    full_i_i_1__0
       (.I0(fullg),
        .I1(\write_addr_reg[0]_0 ),
        .I2(\write_addr_reg[0]_0 ),
        .I3(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I4(Bus_RNW_reg_0),
        .O(full_i_i_1__0_n_0));
  (* KEEP = "yes" *) 
  FDPE full_i_reg
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(full_i_i_1__0_n_0),
        .PRE(wr_reset),
        .Q(\write_addr_reg[0]_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    i_0
       (.I0(1'b0),
        .O(\rd_write_addrgray_dx[2]_8 [3]));
  LUT1 #(
    .INIT(2'h2)) 
    i_1
       (.I0(1'b0),
        .O(\rd_write_addrgray_dx[2]_8 [2]));
  LUT1 #(
    .INIT(2'h2)) 
    i_10
       (.I0(1'b0),
        .O(\wr_read_lastgray_dx[2]_10 [1]));
  LUT1 #(
    .INIT(2'h2)) 
    i_11
       (.I0(1'b0),
        .O(\wr_read_lastgray_dx[2]_10 [0]));
  LUT1 #(
    .INIT(2'h2)) 
    i_12
       (.I0(1'b0),
        .O(\wr_read_nextgray_dx[2]_11 [3]));
  LUT1 #(
    .INIT(2'h2)) 
    i_13
       (.I0(1'b0),
        .O(\wr_read_nextgray_dx[2]_11 [2]));
  LUT1 #(
    .INIT(2'h2)) 
    i_14
       (.I0(1'b0),
        .O(\wr_read_nextgray_dx[2]_11 [1]));
  LUT1 #(
    .INIT(2'h2)) 
    i_15
       (.I0(1'b0),
        .O(\wr_read_nextgray_dx[2]_11 [0]));
  LUT1 #(
    .INIT(2'h2)) 
    i_2
       (.I0(1'b0),
        .O(\rd_write_addrgray_dx[2]_8 [1]));
  LUT1 #(
    .INIT(2'h2)) 
    i_3
       (.I0(1'b0),
        .O(\rd_write_addrgray_dx[2]_8 [0]));
  LUT1 #(
    .INIT(2'h2)) 
    i_4
       (.I0(1'b0),
        .O(\rd_write_nextgray_dx[2]_9 [3]));
  LUT1 #(
    .INIT(2'h2)) 
    i_5
       (.I0(1'b0),
        .O(\rd_write_nextgray_dx[2]_9 [2]));
  LUT1 #(
    .INIT(2'h2)) 
    i_6
       (.I0(1'b0),
        .O(\rd_write_nextgray_dx[2]_9 [1]));
  LUT1 #(
    .INIT(2'h2)) 
    i_7
       (.I0(1'b0),
        .O(\rd_write_nextgray_dx[2]_9 [0]));
  LUT1 #(
    .INIT(2'h2)) 
    i_8
       (.I0(1'b0),
        .O(\wr_read_lastgray_dx[2]_10 [3]));
  LUT1 #(
    .INIT(2'h2)) 
    i_9
       (.I0(1'b0),
        .O(\wr_read_lastgray_dx[2]_10 [2]));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM32M ram_mem_reg_0_15_0_5
       (.ADDRA({1'b0,ram_mem_reg_0_15_0_5_i_1__0_n_0,ram_mem_reg_0_15_0_5_i_2__0_n_0,ram_mem_reg_0_15_0_5_i_3__0_n_0,ram_mem_reg_0_15_0_5_i_4__0_n_0}),
        .ADDRB({1'b0,ram_mem_reg_0_15_0_5_i_1__0_n_0,ram_mem_reg_0_15_0_5_i_2__0_n_0,ram_mem_reg_0_15_0_5_i_3__0_n_0,ram_mem_reg_0_15_0_5_i_4__0_n_0}),
        .ADDRC({1'b0,ram_mem_reg_0_15_0_5_i_1__0_n_0,ram_mem_reg_0_15_0_5_i_2__0_n_0,ram_mem_reg_0_15_0_5_i_3__0_n_0,ram_mem_reg_0_15_0_5_i_4__0_n_0}),
        .ADDRD({1'b0,\write_addr_reg_n_0_[3] ,p_2_in17_in,p_1_in16_in,\write_addr_reg_n_0_[0] }),
        .DIA(S1_AXI_WDATA[1:0]),
        .DIB(S1_AXI_WDATA[3:2]),
        .DIC(S1_AXI_WDATA[5:4]),
        .DID({1'b0,1'b0}),
        .DOA({ram_mem_reg_0_15_0_5_n_0,ram_mem_reg_0_15_0_5_n_1}),
        .DOB({ram_mem_reg_0_15_0_5_n_2,ram_mem_reg_0_15_0_5_n_3}),
        .DOC({ram_mem_reg_0_15_0_5_n_4,ram_mem_reg_0_15_0_5_n_5}),
        .DOD(NLW_ram_mem_reg_0_15_0_5_DOD_UNCONNECTED[1:0]),
        .WCLK(S1_AXI_ACLK),
        .WE(\write_nextgray[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    ram_mem_reg_0_15_0_5_i_1__0
       (.I0(p_1_in20_in),
        .I1(\read_addr_reg_n_0_[0] ),
        .I2(E),
        .I3(p_2_in22_in),
        .I4(\read_addr_reg_n_0_[3] ),
        .O(ram_mem_reg_0_15_0_5_i_1__0_n_0));
  LUT6 #(
    .INIT(64'hBFFFFFFF40000000)) 
    ram_mem_reg_0_15_0_5_i_2__0
       (.I0(empty),
        .I1(Bus_RNW_reg),
        .I2(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .I3(\read_addr_reg_n_0_[0] ),
        .I4(p_1_in20_in),
        .I5(p_2_in22_in),
        .O(ram_mem_reg_0_15_0_5_i_2__0_n_0));
  LUT5 #(
    .INIT(32'hFF7F0080)) 
    ram_mem_reg_0_15_0_5_i_3__0
       (.I0(\read_addr_reg_n_0_[0] ),
        .I1(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(empty),
        .I4(p_1_in20_in),
        .O(ram_mem_reg_0_15_0_5_i_3__0_n_0));
  LUT4 #(
    .INIT(16'h9AAA)) 
    ram_mem_reg_0_15_0_5_i_4__0
       (.I0(\read_addr_reg_n_0_[0] ),
        .I1(empty),
        .I2(Bus_RNW_reg),
        .I3(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .O(ram_mem_reg_0_15_0_5_i_4__0_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM32M ram_mem_reg_0_15_12_17
       (.ADDRA({1'b0,ram_mem_reg_0_15_0_5_i_1__0_n_0,ram_mem_reg_0_15_0_5_i_2__0_n_0,ram_mem_reg_0_15_0_5_i_3__0_n_0,ram_mem_reg_0_15_0_5_i_4__0_n_0}),
        .ADDRB({1'b0,ram_mem_reg_0_15_0_5_i_1__0_n_0,ram_mem_reg_0_15_0_5_i_2__0_n_0,ram_mem_reg_0_15_0_5_i_3__0_n_0,ram_mem_reg_0_15_0_5_i_4__0_n_0}),
        .ADDRC({1'b0,ram_mem_reg_0_15_0_5_i_1__0_n_0,ram_mem_reg_0_15_0_5_i_2__0_n_0,ram_mem_reg_0_15_0_5_i_3__0_n_0,ram_mem_reg_0_15_0_5_i_4__0_n_0}),
        .ADDRD({1'b0,\write_addr_reg_n_0_[3] ,p_2_in17_in,p_1_in16_in,\write_addr_reg_n_0_[0] }),
        .DIA(S1_AXI_WDATA[13:12]),
        .DIB(S1_AXI_WDATA[15:14]),
        .DIC(S1_AXI_WDATA[17:16]),
        .DID({1'b0,1'b0}),
        .DOA({ram_mem_reg_0_15_12_17_n_0,ram_mem_reg_0_15_12_17_n_1}),
        .DOB({ram_mem_reg_0_15_12_17_n_2,ram_mem_reg_0_15_12_17_n_3}),
        .DOC({ram_mem_reg_0_15_12_17_n_4,ram_mem_reg_0_15_12_17_n_5}),
        .DOD(NLW_ram_mem_reg_0_15_12_17_DOD_UNCONNECTED[1:0]),
        .WCLK(S1_AXI_ACLK),
        .WE(\write_nextgray[2]_i_1_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM32M ram_mem_reg_0_15_18_23
       (.ADDRA({1'b0,ram_mem_reg_0_15_0_5_i_1__0_n_0,ram_mem_reg_0_15_0_5_i_2__0_n_0,ram_mem_reg_0_15_0_5_i_3__0_n_0,ram_mem_reg_0_15_0_5_i_4__0_n_0}),
        .ADDRB({1'b0,ram_mem_reg_0_15_0_5_i_1__0_n_0,ram_mem_reg_0_15_0_5_i_2__0_n_0,ram_mem_reg_0_15_0_5_i_3__0_n_0,ram_mem_reg_0_15_0_5_i_4__0_n_0}),
        .ADDRC({1'b0,ram_mem_reg_0_15_0_5_i_1__0_n_0,ram_mem_reg_0_15_0_5_i_2__0_n_0,ram_mem_reg_0_15_0_5_i_3__0_n_0,ram_mem_reg_0_15_0_5_i_4__0_n_0}),
        .ADDRD({1'b0,\write_addr_reg_n_0_[3] ,p_2_in17_in,p_1_in16_in,\write_addr_reg_n_0_[0] }),
        .DIA(S1_AXI_WDATA[19:18]),
        .DIB(S1_AXI_WDATA[21:20]),
        .DIC(S1_AXI_WDATA[23:22]),
        .DID({1'b0,1'b0}),
        .DOA({ram_mem_reg_0_15_18_23_n_0,ram_mem_reg_0_15_18_23_n_1}),
        .DOB({ram_mem_reg_0_15_18_23_n_2,ram_mem_reg_0_15_18_23_n_3}),
        .DOC({ram_mem_reg_0_15_18_23_n_4,ram_mem_reg_0_15_18_23_n_5}),
        .DOD(NLW_ram_mem_reg_0_15_18_23_DOD_UNCONNECTED[1:0]),
        .WCLK(S1_AXI_ACLK),
        .WE(\write_nextgray[2]_i_1_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM32M ram_mem_reg_0_15_24_29
       (.ADDRA({1'b0,ram_mem_reg_0_15_0_5_i_1__0_n_0,ram_mem_reg_0_15_0_5_i_2__0_n_0,ram_mem_reg_0_15_0_5_i_3__0_n_0,ram_mem_reg_0_15_0_5_i_4__0_n_0}),
        .ADDRB({1'b0,ram_mem_reg_0_15_0_5_i_1__0_n_0,ram_mem_reg_0_15_0_5_i_2__0_n_0,ram_mem_reg_0_15_0_5_i_3__0_n_0,ram_mem_reg_0_15_0_5_i_4__0_n_0}),
        .ADDRC({1'b0,ram_mem_reg_0_15_0_5_i_1__0_n_0,ram_mem_reg_0_15_0_5_i_2__0_n_0,ram_mem_reg_0_15_0_5_i_3__0_n_0,ram_mem_reg_0_15_0_5_i_4__0_n_0}),
        .ADDRD({1'b0,\write_addr_reg_n_0_[3] ,p_2_in17_in,p_1_in16_in,\write_addr_reg_n_0_[0] }),
        .DIA(S1_AXI_WDATA[25:24]),
        .DIB(S1_AXI_WDATA[27:26]),
        .DIC(S1_AXI_WDATA[29:28]),
        .DID({1'b0,1'b0}),
        .DOA({ram_mem_reg_0_15_24_29_n_0,ram_mem_reg_0_15_24_29_n_1}),
        .DOB({ram_mem_reg_0_15_24_29_n_2,ram_mem_reg_0_15_24_29_n_3}),
        .DOC({ram_mem_reg_0_15_24_29_n_4,ram_mem_reg_0_15_24_29_n_5}),
        .DOD(NLW_ram_mem_reg_0_15_24_29_DOD_UNCONNECTED[1:0]),
        .WCLK(S1_AXI_ACLK),
        .WE(\write_nextgray[2]_i_1_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM32M ram_mem_reg_0_15_30_31
       (.ADDRA({1'b0,ram_mem_reg_0_15_0_5_i_1__0_n_0,ram_mem_reg_0_15_0_5_i_2__0_n_0,ram_mem_reg_0_15_0_5_i_3__0_n_0,ram_mem_reg_0_15_0_5_i_4__0_n_0}),
        .ADDRB({1'b0,ram_mem_reg_0_15_0_5_i_1__0_n_0,ram_mem_reg_0_15_0_5_i_2__0_n_0,ram_mem_reg_0_15_0_5_i_3__0_n_0,ram_mem_reg_0_15_0_5_i_4__0_n_0}),
        .ADDRC({1'b0,ram_mem_reg_0_15_0_5_i_1__0_n_0,ram_mem_reg_0_15_0_5_i_2__0_n_0,ram_mem_reg_0_15_0_5_i_3__0_n_0,ram_mem_reg_0_15_0_5_i_4__0_n_0}),
        .ADDRD({1'b0,\write_addr_reg_n_0_[3] ,p_2_in17_in,p_1_in16_in,\write_addr_reg_n_0_[0] }),
        .DIA(S1_AXI_WDATA[31:30]),
        .DIB({1'b0,1'b0}),
        .DIC({1'b0,1'b0}),
        .DID({1'b0,1'b0}),
        .DOA({ram_mem_reg_0_15_30_31_n_0,ram_mem_reg_0_15_30_31_n_1}),
        .DOB(NLW_ram_mem_reg_0_15_30_31_DOB_UNCONNECTED[1:0]),
        .DOC(NLW_ram_mem_reg_0_15_30_31_DOC_UNCONNECTED[1:0]),
        .DOD(NLW_ram_mem_reg_0_15_30_31_DOD_UNCONNECTED[1:0]),
        .WCLK(S1_AXI_ACLK),
        .WE(\write_nextgray[2]_i_1_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM32M ram_mem_reg_0_15_6_11
       (.ADDRA({1'b0,ram_mem_reg_0_15_0_5_i_1__0_n_0,ram_mem_reg_0_15_0_5_i_2__0_n_0,ram_mem_reg_0_15_0_5_i_3__0_n_0,ram_mem_reg_0_15_0_5_i_4__0_n_0}),
        .ADDRB({1'b0,ram_mem_reg_0_15_0_5_i_1__0_n_0,ram_mem_reg_0_15_0_5_i_2__0_n_0,ram_mem_reg_0_15_0_5_i_3__0_n_0,ram_mem_reg_0_15_0_5_i_4__0_n_0}),
        .ADDRC({1'b0,ram_mem_reg_0_15_0_5_i_1__0_n_0,ram_mem_reg_0_15_0_5_i_2__0_n_0,ram_mem_reg_0_15_0_5_i_3__0_n_0,ram_mem_reg_0_15_0_5_i_4__0_n_0}),
        .ADDRD({1'b0,\write_addr_reg_n_0_[3] ,p_2_in17_in,p_1_in16_in,\write_addr_reg_n_0_[0] }),
        .DIA(S1_AXI_WDATA[7:6]),
        .DIB(S1_AXI_WDATA[9:8]),
        .DIC(S1_AXI_WDATA[11:10]),
        .DID({1'b0,1'b0}),
        .DOA({ram_mem_reg_0_15_6_11_n_0,ram_mem_reg_0_15_6_11_n_1}),
        .DOB({ram_mem_reg_0_15_6_11_n_2,ram_mem_reg_0_15_6_11_n_3}),
        .DOC({ram_mem_reg_0_15_6_11_n_4,ram_mem_reg_0_15_6_11_n_5}),
        .DOD(NLW_ram_mem_reg_0_15_6_11_DOD_UNCONNECTED[1:0]),
        .WCLK(S1_AXI_ACLK),
        .WE(\write_nextgray[2]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    rd_rst_meta_inst
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(1'b0),
        .PRE(S1_AXI_ARESETN),
        .Q(rd_meta_reset));
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    rd_rst_sync_inst
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(rd_meta_reset),
        .PRE(S1_AXI_ARESETN),
        .Q(rd_reset));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_addrgray_dx_reg[0][0] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(write_addrgray[0]),
        .Q(\rd_write_addrgray_dx[0]_4 [0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_addrgray_dx_reg[0][1] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(write_addrgray[1]),
        .Q(\rd_write_addrgray_dx[0]_4 [1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_addrgray_dx_reg[0][2] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(write_addrgray[2]),
        .Q(\rd_write_addrgray_dx[0]_4 [2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_addrgray_dx_reg[0][3] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(write_addrgray[3]),
        .Q(\rd_write_addrgray_dx[0]_4 [3]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_addrgray_dx_reg[1][0] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\rd_write_addrgray_dx[0]_4 [0]),
        .Q(\rd_write_addrgray_dx[1]_5 [0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_addrgray_dx_reg[1][1] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\rd_write_addrgray_dx[0]_4 [1]),
        .Q(\rd_write_addrgray_dx[1]_5 [1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_addrgray_dx_reg[1][2] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\rd_write_addrgray_dx[0]_4 [2]),
        .Q(\rd_write_addrgray_dx[1]_5 [2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_addrgray_dx_reg[1][3] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\rd_write_addrgray_dx[0]_4 [3]),
        .Q(\rd_write_addrgray_dx[1]_5 [3]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h6996)) 
    \rd_write_next[0]_i_1 
       (.I0(\rd_write_nextgray_dx[1]_3 [2]),
        .I1(\rd_write_nextgray_dx[1]_3 [3]),
        .I2(\rd_write_nextgray_dx[1]_3 [0]),
        .I3(\rd_write_nextgray_dx[1]_3 [1]),
        .O(Gray2Bin[0]));
  LUT3 #(
    .INIT(8'h96)) 
    \rd_write_next[1]_i_1 
       (.I0(\rd_write_nextgray_dx[1]_3 [3]),
        .I1(\rd_write_nextgray_dx[1]_3 [1]),
        .I2(\rd_write_nextgray_dx[1]_3 [2]),
        .O(Gray2Bin[1]));
  LUT2 #(
    .INIT(4'h6)) 
    \rd_write_next[2]_i_1 
       (.I0(\rd_write_nextgray_dx[1]_3 [3]),
        .I1(\rd_write_nextgray_dx[1]_3 [2]),
        .O(Gray2Bin[2]));
  FDRE \rd_write_next_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(Gray2Bin[0]),
        .Q(\rd_write_next_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \rd_write_next_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(Gray2Bin[1]),
        .Q(\rd_write_next_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \rd_write_next_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(Gray2Bin[2]),
        .Q(\rd_write_next_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \rd_write_next_reg[3] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\rd_write_nextgray_dx[1]_3 [3]),
        .Q(\rd_write_next_reg_n_0_[3] ),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_nextgray_dx_reg[0][0] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(write_nextgray[0]),
        .Q(\rd_write_nextgray_dx[0]_2 [0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_nextgray_dx_reg[0][1] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(write_nextgray[1]),
        .Q(\rd_write_nextgray_dx[0]_2 [1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_nextgray_dx_reg[0][2] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(write_nextgray[2]),
        .Q(\rd_write_nextgray_dx[0]_2 [2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_nextgray_dx_reg[0][3] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(write_nextgray[3]),
        .Q(\rd_write_nextgray_dx[0]_2 [3]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_nextgray_dx_reg[1][0] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\rd_write_nextgray_dx[0]_2 [0]),
        .Q(\rd_write_nextgray_dx[1]_3 [0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_nextgray_dx_reg[1][1] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\rd_write_nextgray_dx[0]_2 [1]),
        .Q(\rd_write_nextgray_dx[1]_3 [1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_nextgray_dx_reg[1][2] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\rd_write_nextgray_dx[0]_2 [2]),
        .Q(\rd_write_nextgray_dx[1]_3 [2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_nextgray_dx_reg[1][3] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\rd_write_nextgray_dx[0]_2 [3]),
        .Q(\rd_write_nextgray_dx[1]_3 [3]),
        .R(1'b0));
  FDPE \read_addr_d1_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(E),
        .D(\read_addr_reg_n_0_[0] ),
        .PRE(rd_reset),
        .Q(\read_addr_d1_reg_n_0_[0] ));
  FDPE \read_addr_d1_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(E),
        .D(p_1_in20_in),
        .PRE(rd_reset),
        .Q(\read_addr_d1_reg_n_0_[1] ));
  FDPE \read_addr_d1_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(E),
        .D(p_2_in22_in),
        .PRE(rd_reset),
        .Q(\read_addr_d1_reg_n_0_[2] ));
  FDPE \read_addr_d1_reg[3] 
       (.C(S0_AXI_ACLK),
        .CE(E),
        .D(\read_addr_reg_n_0_[3] ),
        .PRE(rd_reset),
        .Q(read_nextgray[3]));
  FDCE \read_addr_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .CLR(rd_reset),
        .D(ram_mem_reg_0_15_0_5_i_4__0_n_0),
        .Q(\read_addr_reg_n_0_[0] ));
  FDCE \read_addr_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .CLR(rd_reset),
        .D(ram_mem_reg_0_15_0_5_i_3__0_n_0),
        .Q(p_1_in20_in));
  FDCE \read_addr_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .CLR(rd_reset),
        .D(ram_mem_reg_0_15_0_5_i_2__0_n_0),
        .Q(p_2_in22_in));
  FDCE \read_addr_reg[3] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .CLR(rd_reset),
        .D(ram_mem_reg_0_15_0_5_i_1__0_n_0),
        .Q(\read_addr_reg_n_0_[3] ));
  FDPE \read_addrgray_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(E),
        .D(read_nextgray[0]),
        .PRE(rd_reset),
        .Q(\read_addrgray_reg_n_0_[0] ));
  FDCE \read_addrgray_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(E),
        .CLR(rd_reset),
        .D(read_nextgray[1]),
        .Q(p_2_in52_in));
  FDCE \read_addrgray_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(E),
        .CLR(rd_reset),
        .D(read_nextgray[2]),
        .Q(p_2_in57_in));
  FDPE \read_addrgray_reg[3] 
       (.C(S0_AXI_ACLK),
        .CE(E),
        .D(read_nextgray[3]),
        .PRE(rd_reset),
        .Q(p_2_in62_in));
  FDPE \read_lastgray_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(E),
        .D(\read_addrgray_reg_n_0_[0] ),
        .PRE(rd_reset),
        .Q(read_lastgray[0]));
  FDPE \read_lastgray_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(E),
        .D(p_2_in52_in),
        .PRE(rd_reset),
        .Q(read_lastgray[1]));
  FDCE \read_lastgray_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(E),
        .CLR(rd_reset),
        .D(p_2_in57_in),
        .Q(read_lastgray[2]));
  FDPE \read_lastgray_reg[3] 
       (.C(S0_AXI_ACLK),
        .CE(E),
        .D(p_2_in62_in),
        .PRE(rd_reset),
        .Q(read_lastgray[3]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \read_nextgray[0]_i_1__0 
       (.I0(p_1_in20_in),
        .I1(\read_addr_reg_n_0_[0] ),
        .O(Bin2Gray[0]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \read_nextgray[1]_i_1__0 
       (.I0(p_2_in22_in),
        .I1(p_1_in20_in),
        .O(Bin2Gray[1]));
  LUT2 #(
    .INIT(4'h6)) 
    \read_nextgray[2]_i_2__0 
       (.I0(\read_addr_reg_n_0_[3] ),
        .I1(p_2_in22_in),
        .O(Bin2Gray[2]));
  FDCE \read_nextgray_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(E),
        .CLR(rd_reset),
        .D(Bin2Gray[0]),
        .Q(read_nextgray[0]));
  FDCE \read_nextgray_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(E),
        .CLR(rd_reset),
        .D(Bin2Gray[1]),
        .Q(read_nextgray[1]));
  FDCE \read_nextgray_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(E),
        .CLR(rd_reset),
        .D(Bin2Gray[2]),
        .Q(read_nextgray[2]));
  LUT3 #(
    .INIT(8'hB2)) 
    rit_detect_d1_i_1__0
       (.I0(rit_detect_d1_i_2__0_n_0),
        .I1(\rit_register_reg[0] [3]),
        .I2(\Rd_Length_i_reg_n_0_[3] ),
        .O(rit_detect_d0));
  LUT6 #(
    .INIT(64'h2F02FFFF00002F02)) 
    rit_detect_d1_i_2__0
       (.I0(\Rd_Length_i_reg_n_0_[0] ),
        .I1(\rit_register_reg[0] [0]),
        .I2(\rit_register_reg[0] [1]),
        .I3(\Rd_Length_i_reg_n_0_[1] ),
        .I4(\rit_register_reg[0] [2]),
        .I5(\Rd_Length_i_reg_n_0_[2] ),
        .O(rit_detect_d1_i_2__0_n_0));
  LUT3 #(
    .INIT(8'hB2)) 
    sit_detect_d1_i_1
       (.I0(sit_detect_d1_i_2_n_0),
        .I1(\Wr_Length_i_reg_n_0_[3] ),
        .I2(Q[3]),
        .O(sit_detect_d0));
  LUT6 #(
    .INIT(64'hDF0DFFFF0000DF0D)) 
    sit_detect_d1_i_2
       (.I0(\Wr_Length_i_reg_n_0_[0] ),
        .I1(Q[0]),
        .I2(\Wr_Length_i_reg_n_0_[1] ),
        .I3(Q[1]),
        .I4(\Wr_Length_i_reg_n_0_[2] ),
        .I5(Q[2]),
        .O(sit_detect_d1_i_2_n_0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_lastgray_dx_reg[0][0] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(read_lastgray[0]),
        .Q(\wr_read_lastgray_dx[0]_6 [0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_lastgray_dx_reg[0][1] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(read_lastgray[1]),
        .Q(\wr_read_lastgray_dx[0]_6 [1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_lastgray_dx_reg[0][2] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(read_lastgray[2]),
        .Q(\wr_read_lastgray_dx[0]_6 [2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_lastgray_dx_reg[0][3] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(read_lastgray[3]),
        .Q(\wr_read_lastgray_dx[0]_6 [3]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_lastgray_dx_reg[1][0] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\wr_read_lastgray_dx[0]_6 [0]),
        .Q(\wr_read_lastgray_dx[1]_7 [0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_lastgray_dx_reg[1][1] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\wr_read_lastgray_dx[0]_6 [1]),
        .Q(\wr_read_lastgray_dx[1]_7 [1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_lastgray_dx_reg[1][2] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\wr_read_lastgray_dx[0]_6 [2]),
        .Q(\wr_read_lastgray_dx[1]_7 [2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_lastgray_dx_reg[1][3] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\wr_read_lastgray_dx[0]_6 [3]),
        .Q(\wr_read_lastgray_dx[1]_7 [3]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h6996)) 
    \wr_read_next[0]_i_1 
       (.I0(\wr_read_nextgray_dx[1]_1 [2]),
        .I1(\wr_read_nextgray_dx[1]_1 [3]),
        .I2(\wr_read_nextgray_dx[1]_1 [0]),
        .I3(\wr_read_nextgray_dx[1]_1 [1]),
        .O(\wr_read_next[0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h96)) 
    \wr_read_next[1]_i_1 
       (.I0(\wr_read_nextgray_dx[1]_1 [3]),
        .I1(\wr_read_nextgray_dx[1]_1 [1]),
        .I2(\wr_read_nextgray_dx[1]_1 [2]),
        .O(\wr_read_next[1]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \wr_read_next[2]_i_1 
       (.I0(\wr_read_nextgray_dx[1]_1 [3]),
        .I1(\wr_read_nextgray_dx[1]_1 [2]),
        .O(\wr_read_next[2]_i_1_n_0 ));
  FDRE \wr_read_next_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\wr_read_next[0]_i_1_n_0 ),
        .Q(\wr_read_next_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \wr_read_next_reg[1] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\wr_read_next[1]_i_1_n_0 ),
        .Q(\wr_read_next_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \wr_read_next_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\wr_read_next[2]_i_1_n_0 ),
        .Q(\wr_read_next_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \wr_read_next_reg[3] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\wr_read_nextgray_dx[1]_1 [3]),
        .Q(\wr_read_next_reg_n_0_[3] ),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_nextgray_dx_reg[0][0] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(read_nextgray[0]),
        .Q(\wr_read_nextgray_dx[0]_0 [0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_nextgray_dx_reg[0][1] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(read_nextgray[1]),
        .Q(\wr_read_nextgray_dx[0]_0 [1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_nextgray_dx_reg[0][2] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(read_nextgray[2]),
        .Q(\wr_read_nextgray_dx[0]_0 [2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_nextgray_dx_reg[0][3] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(read_nextgray[3]),
        .Q(\wr_read_nextgray_dx[0]_0 [3]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_nextgray_dx_reg[1][0] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\wr_read_nextgray_dx[0]_0 [0]),
        .Q(\wr_read_nextgray_dx[1]_1 [0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_nextgray_dx_reg[1][1] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\wr_read_nextgray_dx[0]_0 [1]),
        .Q(\wr_read_nextgray_dx[1]_1 [1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_nextgray_dx_reg[1][2] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\wr_read_nextgray_dx[0]_0 [2]),
        .Q(\wr_read_nextgray_dx[1]_1 [2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_nextgray_dx_reg[1][3] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\wr_read_nextgray_dx[0]_0 [3]),
        .Q(\wr_read_nextgray_dx[1]_1 [3]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    wr_rst_meta_inst
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(1'b0),
        .PRE(S1_AXI_ARESETN),
        .Q(wr_meta_reset));
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    wr_rst_sync_inst
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(wr_meta_reset),
        .PRE(S1_AXI_ARESETN),
        .Q(wr_reset));
  LUT1 #(
    .INIT(2'h1)) 
    \write_addr[0]_i_1 
       (.I0(\write_addr_reg_n_0_[0] ),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \write_addr[1]_i_1 
       (.I0(\write_addr_reg_n_0_[0] ),
        .I1(p_1_in16_in),
        .O(plusOp[1]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \write_addr[2]_i_1 
       (.I0(\write_addr_reg_n_0_[0] ),
        .I1(p_1_in16_in),
        .I2(p_2_in17_in),
        .O(plusOp[2]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \write_addr[3]_i_1 
       (.I0(p_1_in16_in),
        .I1(\write_addr_reg_n_0_[0] ),
        .I2(p_2_in17_in),
        .I3(\write_addr_reg_n_0_[3] ),
        .O(plusOp[3]));
  FDPE \write_addr_d1_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1_n_0 ),
        .D(\write_addr_reg_n_0_[0] ),
        .PRE(wr_reset),
        .Q(\write_addr_d1_reg_n_0_[0] ));
  FDPE \write_addr_d1_reg[1] 
       (.C(S1_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1_n_0 ),
        .D(p_1_in16_in),
        .PRE(wr_reset),
        .Q(\write_addr_d1_reg_n_0_[1] ));
  FDPE \write_addr_d1_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1_n_0 ),
        .D(p_2_in17_in),
        .PRE(wr_reset),
        .Q(\write_addr_d1_reg_n_0_[2] ));
  FDPE \write_addr_d1_reg[3] 
       (.C(S1_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1_n_0 ),
        .D(\write_addr_reg_n_0_[3] ),
        .PRE(wr_reset),
        .Q(write_nextgray[3]));
  FDCE \write_addr_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1_n_0 ),
        .CLR(wr_reset),
        .D(plusOp[0]),
        .Q(\write_addr_reg_n_0_[0] ));
  FDCE \write_addr_reg[1] 
       (.C(S1_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1_n_0 ),
        .CLR(wr_reset),
        .D(plusOp[1]),
        .Q(p_1_in16_in));
  FDCE \write_addr_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1_n_0 ),
        .CLR(wr_reset),
        .D(plusOp[2]),
        .Q(p_2_in17_in));
  FDCE \write_addr_reg[3] 
       (.C(S1_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1_n_0 ),
        .CLR(wr_reset),
        .D(plusOp[3]),
        .Q(\write_addr_reg_n_0_[3] ));
  FDPE \write_addrgray_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1_n_0 ),
        .D(write_nextgray[0]),
        .PRE(wr_reset),
        .Q(write_addrgray[0]));
  FDCE \write_addrgray_reg[1] 
       (.C(S1_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1_n_0 ),
        .CLR(wr_reset),
        .D(write_nextgray[1]),
        .Q(write_addrgray[1]));
  FDCE \write_addrgray_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1_n_0 ),
        .CLR(wr_reset),
        .D(write_nextgray[2]),
        .Q(write_addrgray[2]));
  FDPE \write_addrgray_reg[3] 
       (.C(S1_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1_n_0 ),
        .D(write_nextgray[3]),
        .PRE(wr_reset),
        .Q(write_addrgray[3]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \write_nextgray[0]_i_1 
       (.I0(p_1_in16_in),
        .I1(\write_addr_reg_n_0_[0] ),
        .O(\write_nextgray[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \write_nextgray[1]_i_1 
       (.I0(p_2_in17_in),
        .I1(p_1_in16_in),
        .O(\write_nextgray[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0004)) 
    \write_nextgray[2]_i_1 
       (.I0(\write_addr_reg[0]_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I2(Bus_RNW_reg_0),
        .I3(\write_addr_reg[0]_0 ),
        .O(\write_nextgray[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \write_nextgray[2]_i_2 
       (.I0(\write_addr_reg_n_0_[3] ),
        .I1(p_2_in17_in),
        .O(\write_nextgray[2]_i_2_n_0 ));
  FDCE \write_nextgray_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1_n_0 ),
        .CLR(wr_reset),
        .D(\write_nextgray[0]_i_1_n_0 ),
        .Q(write_nextgray[0]));
  FDCE \write_nextgray_reg[1] 
       (.C(S1_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1_n_0 ),
        .CLR(wr_reset),
        .D(\write_nextgray[1]_i_1_n_0 ),
        .Q(write_nextgray[1]));
  FDCE \write_nextgray_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1_n_0 ),
        .CLR(wr_reset),
        .D(\write_nextgray[2]_i_2_n_0 ),
        .Q(write_nextgray[2]));
endmodule

(* ORIG_REF_NAME = "Async_FIFO" *) 
module design_1_mailbox_0_0_Async_FIFO_1
   (rd_rst_meta_inst_0,
    out,
    \write_nextgray_reg[2]_0 ,
    rit_detect_d0,
    sit_detect_d0,
    \s_axi_rdata_i_reg[31] ,
    S1_AXI_ACLK,
    S0_AXI_ACLK,
    empty_allow,
    E,
    Bus_RNW_reg,
    \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ,
    \rit_register_reg[0] ,
    Q,
    \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ,
    Bus_RNW_reg_0,
    S0_AXI_ARESETN,
    SYS_Rst,
    S1_AXI_ARESETN,
    S0_AXI_WDATA);
  output rd_rst_meta_inst_0;
  output out;
  output \write_nextgray_reg[2]_0 ;
  output rit_detect_d0;
  output sit_detect_d0;
  output [31:0]\s_axi_rdata_i_reg[31] ;
  input S1_AXI_ACLK;
  input S0_AXI_ACLK;
  input empty_allow;
  input [0:0]E;
  input Bus_RNW_reg;
  input \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  input [3:0]\rit_register_reg[0] ;
  input [3:0]Q;
  input \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ;
  input Bus_RNW_reg_0;
  input S0_AXI_ARESETN;
  input SYS_Rst;
  input S1_AXI_ARESETN;
  input [31:0]S0_AXI_WDATA;

  wire [2:0]Bin2Gray;
  wire Bus_RNW_reg;
  wire Bus_RNW_reg_0;
  wire CI;
  wire [31:0]DataOut0;
  wire [0:0]E;
  wire \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  wire [2:0]Gray2Bin;
  wire [3:0]Q;
  wire [3:0]Rd_Length;
  wire [3:1]Rd_Length_i00_out;
  wire \Rd_Length_i[0]_i_1__0_n_0 ;
  wire \Rd_Length_i[3]_i_2__0_n_0 ;
  wire S;
  wire S0;
  wire S00_out;
  wire S01_out;
  wire S02_out;
  wire S04_out;
  wire S05_out;
  wire S06_out;
  wire S0_AXI_ACLK;
  wire S0_AXI_ARESETN;
  wire [31:0]S0_AXI_WDATA;
  wire S1_AXI_ACLK;
  wire S1_AXI_ARESETN;
  wire SYS_Rst;
  wire [3:0]Wr_Length;
  wire [3:1]Wr_Length_i01_out;
  wire \Wr_Length_i[0]_i_1_n_0 ;
  wire \Wr_Length_i[3]_i_2_n_0 ;
  (* async_reg = "true" *) wire empty;
  wire empty_allow;
  wire emptyg;
  wire emuxcyo_0;
  wire emuxcyo_1;
  wire fmuxcyo_0;
  wire fmuxcyo_1;
  wire fmuxcyo_2;
  wire full_i_i_1_n_0;
  wire fullg;
  wire p_1_in16_in;
  wire p_1_in20_in;
  wire p_2_in17_in;
  wire p_2_in22_in;
  wire p_2_in52_in;
  wire p_2_in57_in;
  wire p_2_in62_in;
  wire [3:0]plusOp;
  wire rd_meta_reset;
  wire rd_reset;
  wire rd_rst_meta_inst_0;
  (* async_reg = "true" *) wire [3:0]\rd_write_addrgray_dx[0]_4 ;
  (* async_reg = "true" *) wire [3:0]\rd_write_addrgray_dx[1]_5 ;
  (* async_reg = "true" *) wire [3:0]\rd_write_addrgray_dx[2]_8 ;
  wire [3:0]rd_write_next;
  (* async_reg = "true" *) wire [3:0]\rd_write_nextgray_dx[0]_2 ;
  (* async_reg = "true" *) wire [3:0]\rd_write_nextgray_dx[1]_3 ;
  (* async_reg = "true" *) wire [3:0]\rd_write_nextgray_dx[2]_9 ;
  wire [2:0]read_addr_d1;
  wire [3:0]read_addr_next;
  wire \read_addr_reg_n_0_[0] ;
  wire \read_addr_reg_n_0_[3] ;
  wire \read_addrgray_reg_n_0_[0] ;
  wire [3:0]read_lastgray;
  wire [3:0]read_nextgray;
  wire rit_detect_d0;
  wire rit_detect_d1_i_2_n_0;
  wire [3:0]\rit_register_reg[0] ;
  wire [31:0]\s_axi_rdata_i_reg[31] ;
  wire sit_detect_d0;
  wire sit_detect_d1_i_2__0_n_0;
  wire wr_meta_reset;
  (* async_reg = "true" *) wire [3:0]\wr_read_lastgray_dx[0]_6 ;
  (* async_reg = "true" *) wire [3:0]\wr_read_lastgray_dx[1]_7 ;
  (* async_reg = "true" *) wire [3:0]\wr_read_lastgray_dx[2]_10 ;
  wire [3:0]wr_read_next;
  wire \wr_read_next[0]_i_1_n_0 ;
  wire \wr_read_next[1]_i_1_n_0 ;
  wire \wr_read_next[2]_i_1_n_0 ;
  (* async_reg = "true" *) wire [3:0]\wr_read_nextgray_dx[0]_0 ;
  (* async_reg = "true" *) wire [3:0]\wr_read_nextgray_dx[1]_1 ;
  (* async_reg = "true" *) wire [3:0]\wr_read_nextgray_dx[2]_11 ;
  wire wr_reset;
  wire [2:0]write_addr_d1;
  wire \write_addr_reg_n_0_[0] ;
  wire \write_addr_reg_n_0_[3] ;
  wire [3:0]write_addrgray;
  wire [3:0]write_nextgray;
  wire \write_nextgray[0]_i_1__0_n_0 ;
  wire \write_nextgray[1]_i_1__0_n_0 ;
  wire \write_nextgray[2]_i_1__0_n_0 ;
  wire \write_nextgray[2]_i_2__0_n_0 ;
  (* async_reg = "true" *) wire \write_nextgray_reg[2]_0 ;
  wire [3:0]NLW_emuxcylow_CARRY4_O_UNCONNECTED;
  wire [3:0]NLW_fmuxcylow_CARRY4_O_UNCONNECTED;
  wire [1:0]NLW_ram_mem_reg_0_15_0_5_DOD_UNCONNECTED;
  wire [1:0]NLW_ram_mem_reg_0_15_12_17_DOD_UNCONNECTED;
  wire [1:0]NLW_ram_mem_reg_0_15_18_23_DOD_UNCONNECTED;
  wire [1:0]NLW_ram_mem_reg_0_15_24_29_DOD_UNCONNECTED;
  wire [1:0]NLW_ram_mem_reg_0_15_30_31_DOB_UNCONNECTED;
  wire [1:0]NLW_ram_mem_reg_0_15_30_31_DOC_UNCONNECTED;
  wire [1:0]NLW_ram_mem_reg_0_15_30_31_DOD_UNCONNECTED;
  wire [1:0]NLW_ram_mem_reg_0_15_6_11_DOD_UNCONNECTED;

  assign out = empty;
  FDRE \DataOut_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[0]),
        .Q(\s_axi_rdata_i_reg[31] [0]),
        .R(1'b0));
  FDRE \DataOut_reg[10] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[10]),
        .Q(\s_axi_rdata_i_reg[31] [10]),
        .R(1'b0));
  FDRE \DataOut_reg[11] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[11]),
        .Q(\s_axi_rdata_i_reg[31] [11]),
        .R(1'b0));
  FDRE \DataOut_reg[12] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[12]),
        .Q(\s_axi_rdata_i_reg[31] [12]),
        .R(1'b0));
  FDRE \DataOut_reg[13] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[13]),
        .Q(\s_axi_rdata_i_reg[31] [13]),
        .R(1'b0));
  FDRE \DataOut_reg[14] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[14]),
        .Q(\s_axi_rdata_i_reg[31] [14]),
        .R(1'b0));
  FDRE \DataOut_reg[15] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[15]),
        .Q(\s_axi_rdata_i_reg[31] [15]),
        .R(1'b0));
  FDRE \DataOut_reg[16] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[16]),
        .Q(\s_axi_rdata_i_reg[31] [16]),
        .R(1'b0));
  FDRE \DataOut_reg[17] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[17]),
        .Q(\s_axi_rdata_i_reg[31] [17]),
        .R(1'b0));
  FDRE \DataOut_reg[18] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[18]),
        .Q(\s_axi_rdata_i_reg[31] [18]),
        .R(1'b0));
  FDRE \DataOut_reg[19] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[19]),
        .Q(\s_axi_rdata_i_reg[31] [19]),
        .R(1'b0));
  FDRE \DataOut_reg[1] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[1]),
        .Q(\s_axi_rdata_i_reg[31] [1]),
        .R(1'b0));
  FDRE \DataOut_reg[20] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[20]),
        .Q(\s_axi_rdata_i_reg[31] [20]),
        .R(1'b0));
  FDRE \DataOut_reg[21] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[21]),
        .Q(\s_axi_rdata_i_reg[31] [21]),
        .R(1'b0));
  FDRE \DataOut_reg[22] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[22]),
        .Q(\s_axi_rdata_i_reg[31] [22]),
        .R(1'b0));
  FDRE \DataOut_reg[23] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[23]),
        .Q(\s_axi_rdata_i_reg[31] [23]),
        .R(1'b0));
  FDRE \DataOut_reg[24] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[24]),
        .Q(\s_axi_rdata_i_reg[31] [24]),
        .R(1'b0));
  FDRE \DataOut_reg[25] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[25]),
        .Q(\s_axi_rdata_i_reg[31] [25]),
        .R(1'b0));
  FDRE \DataOut_reg[26] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[26]),
        .Q(\s_axi_rdata_i_reg[31] [26]),
        .R(1'b0));
  FDRE \DataOut_reg[27] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[27]),
        .Q(\s_axi_rdata_i_reg[31] [27]),
        .R(1'b0));
  FDRE \DataOut_reg[28] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[28]),
        .Q(\s_axi_rdata_i_reg[31] [28]),
        .R(1'b0));
  FDRE \DataOut_reg[29] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[29]),
        .Q(\s_axi_rdata_i_reg[31] [29]),
        .R(1'b0));
  FDRE \DataOut_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[2]),
        .Q(\s_axi_rdata_i_reg[31] [2]),
        .R(1'b0));
  FDRE \DataOut_reg[30] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[30]),
        .Q(\s_axi_rdata_i_reg[31] [30]),
        .R(1'b0));
  FDRE \DataOut_reg[31] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[31]),
        .Q(\s_axi_rdata_i_reg[31] [31]),
        .R(1'b0));
  FDRE \DataOut_reg[3] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[3]),
        .Q(\s_axi_rdata_i_reg[31] [3]),
        .R(1'b0));
  FDRE \DataOut_reg[4] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[4]),
        .Q(\s_axi_rdata_i_reg[31] [4]),
        .R(1'b0));
  FDRE \DataOut_reg[5] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[5]),
        .Q(\s_axi_rdata_i_reg[31] [5]),
        .R(1'b0));
  FDRE \DataOut_reg[6] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[6]),
        .Q(\s_axi_rdata_i_reg[31] [6]),
        .R(1'b0));
  FDRE \DataOut_reg[7] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[7]),
        .Q(\s_axi_rdata_i_reg[31] [7]),
        .R(1'b0));
  FDRE \DataOut_reg[8] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[8]),
        .Q(\s_axi_rdata_i_reg[31] [8]),
        .R(1'b0));
  FDRE \DataOut_reg[9] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(DataOut0[9]),
        .Q(\s_axi_rdata_i_reg[31] [9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h9A95)) 
    \Gen_emuxcy[1].emuxcy_i_1 
       (.I0(\rd_write_addrgray_dx[1]_5 [1]),
        .I1(p_2_in52_in),
        .I2(empty),
        .I3(read_nextgray[1]),
        .O(S04_out));
  LUT4 #(
    .INIT(16'h9A95)) 
    \Gen_emuxcy[2].emuxcy_i_1 
       (.I0(\rd_write_addrgray_dx[1]_5 [2]),
        .I1(p_2_in57_in),
        .I2(empty),
        .I3(read_nextgray[2]),
        .O(S05_out));
  LUT4 #(
    .INIT(16'h9A95)) 
    \Gen_fmuxcy[1].fmuxcy_i_1__0 
       (.I0(\wr_read_lastgray_dx[1]_7 [1]),
        .I1(write_addrgray[1]),
        .I2(\write_nextgray_reg[2]_0 ),
        .I3(write_nextgray[1]),
        .O(S00_out));
  LUT4 #(
    .INIT(16'h9A95)) 
    \Gen_fmuxcy[2].fmuxcy_i_1__0 
       (.I0(\wr_read_lastgray_dx[1]_7 [2]),
        .I1(write_addrgray[2]),
        .I2(\write_nextgray_reg[2]_0 ),
        .I3(write_nextgray[2]),
        .O(S01_out));
  LUT2 #(
    .INIT(4'h6)) 
    \Rd_Length_i[0]_i_1__0 
       (.I0(rd_write_next[0]),
        .I1(read_addr_d1[0]),
        .O(\Rd_Length_i[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT4 #(
    .INIT(16'h2DD2)) 
    \Rd_Length_i[1]_i_1__0 
       (.I0(read_addr_d1[0]),
        .I1(rd_write_next[0]),
        .I2(read_addr_d1[1]),
        .I3(rd_write_next[1]),
        .O(Rd_Length_i00_out[1]));
  LUT6 #(
    .INIT(64'h4F04B0FBB0FB4F04)) 
    \Rd_Length_i[2]_i_1__0 
       (.I0(rd_write_next[0]),
        .I1(read_addr_d1[0]),
        .I2(rd_write_next[1]),
        .I3(read_addr_d1[1]),
        .I4(read_addr_d1[2]),
        .I5(rd_write_next[2]),
        .O(Rd_Length_i00_out[2]));
  LUT5 #(
    .INIT(32'h718E8E71)) 
    \Rd_Length_i[3]_i_1__0 
       (.I0(\Rd_Length_i[3]_i_2__0_n_0 ),
        .I1(rd_write_next[2]),
        .I2(read_addr_d1[2]),
        .I3(read_nextgray[3]),
        .I4(rd_write_next[3]),
        .O(Rd_Length_i00_out[3]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT4 #(
    .INIT(16'hDD4D)) 
    \Rd_Length_i[3]_i_2__0 
       (.I0(read_addr_d1[1]),
        .I1(rd_write_next[1]),
        .I2(read_addr_d1[0]),
        .I3(rd_write_next[0]),
        .O(\Rd_Length_i[3]_i_2__0_n_0 ));
  FDCE \Rd_Length_i_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .CLR(rd_reset),
        .D(\Rd_Length_i[0]_i_1__0_n_0 ),
        .Q(Rd_Length[0]));
  FDCE \Rd_Length_i_reg[1] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .CLR(rd_reset),
        .D(Rd_Length_i00_out[1]),
        .Q(Rd_Length[1]));
  FDCE \Rd_Length_i_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .CLR(rd_reset),
        .D(Rd_Length_i00_out[2]),
        .Q(Rd_Length[2]));
  FDCE \Rd_Length_i_reg[3] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .CLR(rd_reset),
        .D(Rd_Length_i00_out[3]),
        .Q(Rd_Length[3]));
  LUT2 #(
    .INIT(4'h6)) 
    \Wr_Length_i[0]_i_1 
       (.I0(write_addr_d1[0]),
        .I1(wr_read_next[0]),
        .O(\Wr_Length_i[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT4 #(
    .INIT(16'h2DD2)) 
    \Wr_Length_i[1]_i_1 
       (.I0(wr_read_next[0]),
        .I1(write_addr_d1[0]),
        .I2(wr_read_next[1]),
        .I3(write_addr_d1[1]),
        .O(Wr_Length_i01_out[1]));
  LUT6 #(
    .INIT(64'h4F04B0FBB0FB4F04)) 
    \Wr_Length_i[2]_i_1 
       (.I0(write_addr_d1[0]),
        .I1(wr_read_next[0]),
        .I2(write_addr_d1[1]),
        .I3(wr_read_next[1]),
        .I4(wr_read_next[2]),
        .I5(write_addr_d1[2]),
        .O(Wr_Length_i01_out[2]));
  LUT5 #(
    .INIT(32'h718E8E71)) 
    \Wr_Length_i[3]_i_1 
       (.I0(\Wr_Length_i[3]_i_2_n_0 ),
        .I1(write_addr_d1[2]),
        .I2(wr_read_next[2]),
        .I3(wr_read_next[3]),
        .I4(write_nextgray[3]),
        .O(Wr_Length_i01_out[3]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT4 #(
    .INIT(16'hDD4D)) 
    \Wr_Length_i[3]_i_2 
       (.I0(wr_read_next[1]),
        .I1(write_addr_d1[1]),
        .I2(wr_read_next[0]),
        .I3(write_addr_d1[0]),
        .O(\Wr_Length_i[3]_i_2_n_0 ));
  FDCE \Wr_Length_i_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .CLR(wr_reset),
        .D(\Wr_Length_i[0]_i_1_n_0 ),
        .Q(Wr_Length[0]));
  FDCE \Wr_Length_i_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .CLR(wr_reset),
        .D(Wr_Length_i01_out[1]),
        .Q(Wr_Length[1]));
  FDCE \Wr_Length_i_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .CLR(wr_reset),
        .D(Wr_Length_i01_out[2]),
        .Q(Wr_Length[2]));
  FDCE \Wr_Length_i_reg[3] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .CLR(wr_reset),
        .D(Wr_Length_i01_out[3]),
        .Q(Wr_Length[3]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE empty_reg
       (.C(S1_AXI_ACLK),
        .CE(empty_allow),
        .D(emptyg),
        .PRE(rd_reset),
        .Q(empty));
  LUT4 #(
    .INIT(16'h9A95)) 
    emuxcyhigh_i_1
       (.I0(\rd_write_addrgray_dx[1]_5 [3]),
        .I1(p_2_in62_in),
        .I2(empty),
        .I3(read_nextgray[3]),
        .O(S06_out));
  (* XILINX_LEGACY_PRIM = "(MUXCY,XORCY)" *) 
  (* XILINX_TRANSFORM_PINMAP = "LO:O" *) 
  (* box_type = "PRIMITIVE" *) 
  CARRY4 emuxcylow_CARRY4
       (.CI(1'b0),
        .CO({emptyg,CI,emuxcyo_1,emuxcyo_0}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_emuxcylow_CARRY4_O_UNCONNECTED[3:0]),
        .S({S06_out,S05_out,S04_out,S}));
  LUT4 #(
    .INIT(16'h9A95)) 
    emuxcylow_i_1
       (.I0(\rd_write_addrgray_dx[1]_5 [0]),
        .I1(\read_addrgray_reg_n_0_[0] ),
        .I2(empty),
        .I3(read_nextgray[0]),
        .O(S));
  LUT4 #(
    .INIT(16'h9A95)) 
    fmuxcyhigh_i_1__0
       (.I0(\wr_read_lastgray_dx[1]_7 [3]),
        .I1(write_addrgray[3]),
        .I2(\write_nextgray_reg[2]_0 ),
        .I3(write_nextgray[3]),
        .O(S02_out));
  (* XILINX_LEGACY_PRIM = "(MUXCY,XORCY)" *) 
  (* XILINX_TRANSFORM_PINMAP = "LO:O" *) 
  (* box_type = "PRIMITIVE" *) 
  CARRY4 fmuxcylow_CARRY4
       (.CI(1'b0),
        .CO({fullg,fmuxcyo_2,fmuxcyo_1,fmuxcyo_0}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_fmuxcylow_CARRY4_O_UNCONNECTED[3:0]),
        .S({S02_out,S01_out,S00_out,S0}));
  LUT4 #(
    .INIT(16'h9A95)) 
    fmuxcylow_i_1__0
       (.I0(\wr_read_lastgray_dx[1]_7 [0]),
        .I1(write_addrgray[0]),
        .I2(\write_nextgray_reg[2]_0 ),
        .I3(write_nextgray[0]),
        .O(S0));
  LUT5 #(
    .INIT(32'h88888A88)) 
    full_i_i_1
       (.I0(fullg),
        .I1(\write_nextgray_reg[2]_0 ),
        .I2(\write_nextgray_reg[2]_0 ),
        .I3(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I4(Bus_RNW_reg_0),
        .O(full_i_i_1_n_0));
  (* KEEP = "yes" *) 
  FDPE full_i_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(full_i_i_1_n_0),
        .PRE(wr_reset),
        .Q(\write_nextgray_reg[2]_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    i_0
       (.I0(1'b0),
        .O(\rd_write_addrgray_dx[2]_8 [3]));
  LUT1 #(
    .INIT(2'h2)) 
    i_1
       (.I0(1'b0),
        .O(\rd_write_addrgray_dx[2]_8 [2]));
  LUT1 #(
    .INIT(2'h2)) 
    i_10
       (.I0(1'b0),
        .O(\wr_read_lastgray_dx[2]_10 [1]));
  LUT1 #(
    .INIT(2'h2)) 
    i_11
       (.I0(1'b0),
        .O(\wr_read_lastgray_dx[2]_10 [0]));
  LUT1 #(
    .INIT(2'h2)) 
    i_12
       (.I0(1'b0),
        .O(\wr_read_nextgray_dx[2]_11 [3]));
  LUT1 #(
    .INIT(2'h2)) 
    i_13
       (.I0(1'b0),
        .O(\wr_read_nextgray_dx[2]_11 [2]));
  LUT1 #(
    .INIT(2'h2)) 
    i_14
       (.I0(1'b0),
        .O(\wr_read_nextgray_dx[2]_11 [1]));
  LUT1 #(
    .INIT(2'h2)) 
    i_15
       (.I0(1'b0),
        .O(\wr_read_nextgray_dx[2]_11 [0]));
  LUT1 #(
    .INIT(2'h2)) 
    i_2
       (.I0(1'b0),
        .O(\rd_write_addrgray_dx[2]_8 [1]));
  LUT1 #(
    .INIT(2'h2)) 
    i_3
       (.I0(1'b0),
        .O(\rd_write_addrgray_dx[2]_8 [0]));
  LUT1 #(
    .INIT(2'h2)) 
    i_4
       (.I0(1'b0),
        .O(\rd_write_nextgray_dx[2]_9 [3]));
  LUT1 #(
    .INIT(2'h2)) 
    i_5
       (.I0(1'b0),
        .O(\rd_write_nextgray_dx[2]_9 [2]));
  LUT1 #(
    .INIT(2'h2)) 
    i_6
       (.I0(1'b0),
        .O(\rd_write_nextgray_dx[2]_9 [1]));
  LUT1 #(
    .INIT(2'h2)) 
    i_7
       (.I0(1'b0),
        .O(\rd_write_nextgray_dx[2]_9 [0]));
  LUT1 #(
    .INIT(2'h2)) 
    i_8
       (.I0(1'b0),
        .O(\wr_read_lastgray_dx[2]_10 [3]));
  LUT1 #(
    .INIT(2'h2)) 
    i_9
       (.I0(1'b0),
        .O(\wr_read_lastgray_dx[2]_10 [2]));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM32M ram_mem_reg_0_15_0_5
       (.ADDRA({1'b0,read_addr_next}),
        .ADDRB({1'b0,read_addr_next}),
        .ADDRC({1'b0,read_addr_next}),
        .ADDRD({1'b0,\write_addr_reg_n_0_[3] ,p_2_in17_in,p_1_in16_in,\write_addr_reg_n_0_[0] }),
        .DIA(S0_AXI_WDATA[1:0]),
        .DIB(S0_AXI_WDATA[3:2]),
        .DIC(S0_AXI_WDATA[5:4]),
        .DID({1'b0,1'b0}),
        .DOA(DataOut0[1:0]),
        .DOB(DataOut0[3:2]),
        .DOC(DataOut0[5:4]),
        .DOD(NLW_ram_mem_reg_0_15_0_5_DOD_UNCONNECTED[1:0]),
        .WCLK(S0_AXI_ACLK),
        .WE(\write_nextgray[2]_i_1__0_n_0 ));
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    ram_mem_reg_0_15_0_5_i_1
       (.I0(p_1_in20_in),
        .I1(\read_addr_reg_n_0_[0] ),
        .I2(E),
        .I3(p_2_in22_in),
        .I4(\read_addr_reg_n_0_[3] ),
        .O(read_addr_next[3]));
  LUT6 #(
    .INIT(64'hBFFFFFFF40000000)) 
    ram_mem_reg_0_15_0_5_i_2
       (.I0(empty),
        .I1(Bus_RNW_reg),
        .I2(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .I3(\read_addr_reg_n_0_[0] ),
        .I4(p_1_in20_in),
        .I5(p_2_in22_in),
        .O(read_addr_next[2]));
  LUT5 #(
    .INIT(32'hFF7F0080)) 
    ram_mem_reg_0_15_0_5_i_3
       (.I0(\read_addr_reg_n_0_[0] ),
        .I1(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(empty),
        .I4(p_1_in20_in),
        .O(read_addr_next[1]));
  LUT4 #(
    .INIT(16'h9AAA)) 
    ram_mem_reg_0_15_0_5_i_4
       (.I0(\read_addr_reg_n_0_[0] ),
        .I1(empty),
        .I2(Bus_RNW_reg),
        .I3(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .O(read_addr_next[0]));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM32M ram_mem_reg_0_15_12_17
       (.ADDRA({1'b0,read_addr_next}),
        .ADDRB({1'b0,read_addr_next}),
        .ADDRC({1'b0,read_addr_next}),
        .ADDRD({1'b0,\write_addr_reg_n_0_[3] ,p_2_in17_in,p_1_in16_in,\write_addr_reg_n_0_[0] }),
        .DIA(S0_AXI_WDATA[13:12]),
        .DIB(S0_AXI_WDATA[15:14]),
        .DIC(S0_AXI_WDATA[17:16]),
        .DID({1'b0,1'b0}),
        .DOA(DataOut0[13:12]),
        .DOB(DataOut0[15:14]),
        .DOC(DataOut0[17:16]),
        .DOD(NLW_ram_mem_reg_0_15_12_17_DOD_UNCONNECTED[1:0]),
        .WCLK(S0_AXI_ACLK),
        .WE(\write_nextgray[2]_i_1__0_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM32M ram_mem_reg_0_15_18_23
       (.ADDRA({1'b0,read_addr_next}),
        .ADDRB({1'b0,read_addr_next}),
        .ADDRC({1'b0,read_addr_next}),
        .ADDRD({1'b0,\write_addr_reg_n_0_[3] ,p_2_in17_in,p_1_in16_in,\write_addr_reg_n_0_[0] }),
        .DIA(S0_AXI_WDATA[19:18]),
        .DIB(S0_AXI_WDATA[21:20]),
        .DIC(S0_AXI_WDATA[23:22]),
        .DID({1'b0,1'b0}),
        .DOA(DataOut0[19:18]),
        .DOB(DataOut0[21:20]),
        .DOC(DataOut0[23:22]),
        .DOD(NLW_ram_mem_reg_0_15_18_23_DOD_UNCONNECTED[1:0]),
        .WCLK(S0_AXI_ACLK),
        .WE(\write_nextgray[2]_i_1__0_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM32M ram_mem_reg_0_15_24_29
       (.ADDRA({1'b0,read_addr_next}),
        .ADDRB({1'b0,read_addr_next}),
        .ADDRC({1'b0,read_addr_next}),
        .ADDRD({1'b0,\write_addr_reg_n_0_[3] ,p_2_in17_in,p_1_in16_in,\write_addr_reg_n_0_[0] }),
        .DIA(S0_AXI_WDATA[25:24]),
        .DIB(S0_AXI_WDATA[27:26]),
        .DIC(S0_AXI_WDATA[29:28]),
        .DID({1'b0,1'b0}),
        .DOA(DataOut0[25:24]),
        .DOB(DataOut0[27:26]),
        .DOC(DataOut0[29:28]),
        .DOD(NLW_ram_mem_reg_0_15_24_29_DOD_UNCONNECTED[1:0]),
        .WCLK(S0_AXI_ACLK),
        .WE(\write_nextgray[2]_i_1__0_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM32M ram_mem_reg_0_15_30_31
       (.ADDRA({1'b0,read_addr_next}),
        .ADDRB({1'b0,read_addr_next}),
        .ADDRC({1'b0,read_addr_next}),
        .ADDRD({1'b0,\write_addr_reg_n_0_[3] ,p_2_in17_in,p_1_in16_in,\write_addr_reg_n_0_[0] }),
        .DIA(S0_AXI_WDATA[31:30]),
        .DIB({1'b0,1'b0}),
        .DIC({1'b0,1'b0}),
        .DID({1'b0,1'b0}),
        .DOA(DataOut0[31:30]),
        .DOB(NLW_ram_mem_reg_0_15_30_31_DOB_UNCONNECTED[1:0]),
        .DOC(NLW_ram_mem_reg_0_15_30_31_DOC_UNCONNECTED[1:0]),
        .DOD(NLW_ram_mem_reg_0_15_30_31_DOD_UNCONNECTED[1:0]),
        .WCLK(S0_AXI_ACLK),
        .WE(\write_nextgray[2]_i_1__0_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM32M ram_mem_reg_0_15_6_11
       (.ADDRA({1'b0,read_addr_next}),
        .ADDRB({1'b0,read_addr_next}),
        .ADDRC({1'b0,read_addr_next}),
        .ADDRD({1'b0,\write_addr_reg_n_0_[3] ,p_2_in17_in,p_1_in16_in,\write_addr_reg_n_0_[0] }),
        .DIA(S0_AXI_WDATA[7:6]),
        .DIB(S0_AXI_WDATA[9:8]),
        .DIC(S0_AXI_WDATA[11:10]),
        .DID({1'b0,1'b0}),
        .DOA(DataOut0[7:6]),
        .DOB(DataOut0[9:8]),
        .DOC(DataOut0[11:10]),
        .DOD(NLW_ram_mem_reg_0_15_6_11_DOD_UNCONNECTED[1:0]),
        .WCLK(S0_AXI_ACLK),
        .WE(\write_nextgray[2]_i_1__0_n_0 ));
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    rd_rst_meta_inst
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(1'b0),
        .PRE(rd_rst_meta_inst_0),
        .Q(rd_meta_reset));
  LUT3 #(
    .INIT(8'hDF)) 
    rd_rst_meta_inst_i_1
       (.I0(S0_AXI_ARESETN),
        .I1(SYS_Rst),
        .I2(S1_AXI_ARESETN),
        .O(rd_rst_meta_inst_0));
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    rd_rst_sync_inst
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(rd_meta_reset),
        .PRE(rd_rst_meta_inst_0),
        .Q(rd_reset));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_addrgray_dx_reg[0][0] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(write_addrgray[0]),
        .Q(\rd_write_addrgray_dx[0]_4 [0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_addrgray_dx_reg[0][1] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(write_addrgray[1]),
        .Q(\rd_write_addrgray_dx[0]_4 [1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_addrgray_dx_reg[0][2] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(write_addrgray[2]),
        .Q(\rd_write_addrgray_dx[0]_4 [2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_addrgray_dx_reg[0][3] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(write_addrgray[3]),
        .Q(\rd_write_addrgray_dx[0]_4 [3]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_addrgray_dx_reg[1][0] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\rd_write_addrgray_dx[0]_4 [0]),
        .Q(\rd_write_addrgray_dx[1]_5 [0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_addrgray_dx_reg[1][1] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\rd_write_addrgray_dx[0]_4 [1]),
        .Q(\rd_write_addrgray_dx[1]_5 [1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_addrgray_dx_reg[1][2] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\rd_write_addrgray_dx[0]_4 [2]),
        .Q(\rd_write_addrgray_dx[1]_5 [2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_addrgray_dx_reg[1][3] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\rd_write_addrgray_dx[0]_4 [3]),
        .Q(\rd_write_addrgray_dx[1]_5 [3]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h6996)) 
    \rd_write_next[0]_i_1 
       (.I0(\rd_write_nextgray_dx[1]_3 [2]),
        .I1(\rd_write_nextgray_dx[1]_3 [3]),
        .I2(\rd_write_nextgray_dx[1]_3 [0]),
        .I3(\rd_write_nextgray_dx[1]_3 [1]),
        .O(Gray2Bin[0]));
  LUT3 #(
    .INIT(8'h96)) 
    \rd_write_next[1]_i_1 
       (.I0(\rd_write_nextgray_dx[1]_3 [3]),
        .I1(\rd_write_nextgray_dx[1]_3 [1]),
        .I2(\rd_write_nextgray_dx[1]_3 [2]),
        .O(Gray2Bin[1]));
  LUT2 #(
    .INIT(4'h6)) 
    \rd_write_next[2]_i_1 
       (.I0(\rd_write_nextgray_dx[1]_3 [3]),
        .I1(\rd_write_nextgray_dx[1]_3 [2]),
        .O(Gray2Bin[2]));
  FDRE \rd_write_next_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(Gray2Bin[0]),
        .Q(rd_write_next[0]),
        .R(1'b0));
  FDRE \rd_write_next_reg[1] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(Gray2Bin[1]),
        .Q(rd_write_next[1]),
        .R(1'b0));
  FDRE \rd_write_next_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(Gray2Bin[2]),
        .Q(rd_write_next[2]),
        .R(1'b0));
  FDRE \rd_write_next_reg[3] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\rd_write_nextgray_dx[1]_3 [3]),
        .Q(rd_write_next[3]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_nextgray_dx_reg[0][0] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(write_nextgray[0]),
        .Q(\rd_write_nextgray_dx[0]_2 [0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_nextgray_dx_reg[0][1] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(write_nextgray[1]),
        .Q(\rd_write_nextgray_dx[0]_2 [1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_nextgray_dx_reg[0][2] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(write_nextgray[2]),
        .Q(\rd_write_nextgray_dx[0]_2 [2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_nextgray_dx_reg[0][3] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(write_nextgray[3]),
        .Q(\rd_write_nextgray_dx[0]_2 [3]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_nextgray_dx_reg[1][0] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\rd_write_nextgray_dx[0]_2 [0]),
        .Q(\rd_write_nextgray_dx[1]_3 [0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_nextgray_dx_reg[1][1] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\rd_write_nextgray_dx[0]_2 [1]),
        .Q(\rd_write_nextgray_dx[1]_3 [1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_nextgray_dx_reg[1][2] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\rd_write_nextgray_dx[0]_2 [2]),
        .Q(\rd_write_nextgray_dx[1]_3 [2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \rd_write_nextgray_dx_reg[1][3] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\rd_write_nextgray_dx[0]_2 [3]),
        .Q(\rd_write_nextgray_dx[1]_3 [3]),
        .R(1'b0));
  FDPE \read_addr_d1_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(E),
        .D(\read_addr_reg_n_0_[0] ),
        .PRE(rd_reset),
        .Q(read_addr_d1[0]));
  FDPE \read_addr_d1_reg[1] 
       (.C(S1_AXI_ACLK),
        .CE(E),
        .D(p_1_in20_in),
        .PRE(rd_reset),
        .Q(read_addr_d1[1]));
  FDPE \read_addr_d1_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(E),
        .D(p_2_in22_in),
        .PRE(rd_reset),
        .Q(read_addr_d1[2]));
  FDPE \read_addr_d1_reg[3] 
       (.C(S1_AXI_ACLK),
        .CE(E),
        .D(\read_addr_reg_n_0_[3] ),
        .PRE(rd_reset),
        .Q(read_nextgray[3]));
  FDCE \read_addr_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .CLR(rd_reset),
        .D(read_addr_next[0]),
        .Q(\read_addr_reg_n_0_[0] ));
  FDCE \read_addr_reg[1] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .CLR(rd_reset),
        .D(read_addr_next[1]),
        .Q(p_1_in20_in));
  FDCE \read_addr_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .CLR(rd_reset),
        .D(read_addr_next[2]),
        .Q(p_2_in22_in));
  FDCE \read_addr_reg[3] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .CLR(rd_reset),
        .D(read_addr_next[3]),
        .Q(\read_addr_reg_n_0_[3] ));
  FDPE \read_addrgray_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(E),
        .D(read_nextgray[0]),
        .PRE(rd_reset),
        .Q(\read_addrgray_reg_n_0_[0] ));
  FDCE \read_addrgray_reg[1] 
       (.C(S1_AXI_ACLK),
        .CE(E),
        .CLR(rd_reset),
        .D(read_nextgray[1]),
        .Q(p_2_in52_in));
  FDCE \read_addrgray_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(E),
        .CLR(rd_reset),
        .D(read_nextgray[2]),
        .Q(p_2_in57_in));
  FDPE \read_addrgray_reg[3] 
       (.C(S1_AXI_ACLK),
        .CE(E),
        .D(read_nextgray[3]),
        .PRE(rd_reset),
        .Q(p_2_in62_in));
  FDPE \read_lastgray_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(E),
        .D(\read_addrgray_reg_n_0_[0] ),
        .PRE(rd_reset),
        .Q(read_lastgray[0]));
  FDPE \read_lastgray_reg[1] 
       (.C(S1_AXI_ACLK),
        .CE(E),
        .D(p_2_in52_in),
        .PRE(rd_reset),
        .Q(read_lastgray[1]));
  FDCE \read_lastgray_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(E),
        .CLR(rd_reset),
        .D(p_2_in57_in),
        .Q(read_lastgray[2]));
  FDPE \read_lastgray_reg[3] 
       (.C(S1_AXI_ACLK),
        .CE(E),
        .D(p_2_in62_in),
        .PRE(rd_reset),
        .Q(read_lastgray[3]));
  LUT2 #(
    .INIT(4'h6)) 
    \read_nextgray[0]_i_1 
       (.I0(p_1_in20_in),
        .I1(\read_addr_reg_n_0_[0] ),
        .O(Bin2Gray[0]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \read_nextgray[1]_i_1 
       (.I0(p_2_in22_in),
        .I1(p_1_in20_in),
        .O(Bin2Gray[1]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \read_nextgray[2]_i_2 
       (.I0(\read_addr_reg_n_0_[3] ),
        .I1(p_2_in22_in),
        .O(Bin2Gray[2]));
  FDCE \read_nextgray_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(E),
        .CLR(rd_reset),
        .D(Bin2Gray[0]),
        .Q(read_nextgray[0]));
  FDCE \read_nextgray_reg[1] 
       (.C(S1_AXI_ACLK),
        .CE(E),
        .CLR(rd_reset),
        .D(Bin2Gray[1]),
        .Q(read_nextgray[1]));
  FDCE \read_nextgray_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(E),
        .CLR(rd_reset),
        .D(Bin2Gray[2]),
        .Q(read_nextgray[2]));
  LUT3 #(
    .INIT(8'hB2)) 
    rit_detect_d1_i_1
       (.I0(rit_detect_d1_i_2_n_0),
        .I1(\rit_register_reg[0] [3]),
        .I2(Rd_Length[3]),
        .O(rit_detect_d0));
  LUT6 #(
    .INIT(64'h2F02FFFF00002F02)) 
    rit_detect_d1_i_2
       (.I0(Rd_Length[0]),
        .I1(\rit_register_reg[0] [0]),
        .I2(\rit_register_reg[0] [1]),
        .I3(Rd_Length[1]),
        .I4(\rit_register_reg[0] [2]),
        .I5(Rd_Length[2]),
        .O(rit_detect_d1_i_2_n_0));
  LUT3 #(
    .INIT(8'hB2)) 
    sit_detect_d1_i_1__0
       (.I0(sit_detect_d1_i_2__0_n_0),
        .I1(Wr_Length[3]),
        .I2(Q[3]),
        .O(sit_detect_d0));
  LUT6 #(
    .INIT(64'hDF0DFFFF0000DF0D)) 
    sit_detect_d1_i_2__0
       (.I0(Wr_Length[0]),
        .I1(Q[0]),
        .I2(Wr_Length[1]),
        .I3(Q[1]),
        .I4(Wr_Length[2]),
        .I5(Q[2]),
        .O(sit_detect_d1_i_2__0_n_0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_lastgray_dx_reg[0][0] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(read_lastgray[0]),
        .Q(\wr_read_lastgray_dx[0]_6 [0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_lastgray_dx_reg[0][1] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(read_lastgray[1]),
        .Q(\wr_read_lastgray_dx[0]_6 [1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_lastgray_dx_reg[0][2] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(read_lastgray[2]),
        .Q(\wr_read_lastgray_dx[0]_6 [2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_lastgray_dx_reg[0][3] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(read_lastgray[3]),
        .Q(\wr_read_lastgray_dx[0]_6 [3]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_lastgray_dx_reg[1][0] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\wr_read_lastgray_dx[0]_6 [0]),
        .Q(\wr_read_lastgray_dx[1]_7 [0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_lastgray_dx_reg[1][1] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\wr_read_lastgray_dx[0]_6 [1]),
        .Q(\wr_read_lastgray_dx[1]_7 [1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_lastgray_dx_reg[1][2] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\wr_read_lastgray_dx[0]_6 [2]),
        .Q(\wr_read_lastgray_dx[1]_7 [2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_lastgray_dx_reg[1][3] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\wr_read_lastgray_dx[0]_6 [3]),
        .Q(\wr_read_lastgray_dx[1]_7 [3]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h6996)) 
    \wr_read_next[0]_i_1 
       (.I0(\wr_read_nextgray_dx[1]_1 [2]),
        .I1(\wr_read_nextgray_dx[1]_1 [3]),
        .I2(\wr_read_nextgray_dx[1]_1 [0]),
        .I3(\wr_read_nextgray_dx[1]_1 [1]),
        .O(\wr_read_next[0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h96)) 
    \wr_read_next[1]_i_1 
       (.I0(\wr_read_nextgray_dx[1]_1 [3]),
        .I1(\wr_read_nextgray_dx[1]_1 [1]),
        .I2(\wr_read_nextgray_dx[1]_1 [2]),
        .O(\wr_read_next[1]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \wr_read_next[2]_i_1 
       (.I0(\wr_read_nextgray_dx[1]_1 [3]),
        .I1(\wr_read_nextgray_dx[1]_1 [2]),
        .O(\wr_read_next[2]_i_1_n_0 ));
  FDRE \wr_read_next_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\wr_read_next[0]_i_1_n_0 ),
        .Q(wr_read_next[0]),
        .R(1'b0));
  FDRE \wr_read_next_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\wr_read_next[1]_i_1_n_0 ),
        .Q(wr_read_next[1]),
        .R(1'b0));
  FDRE \wr_read_next_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\wr_read_next[2]_i_1_n_0 ),
        .Q(wr_read_next[2]),
        .R(1'b0));
  FDRE \wr_read_next_reg[3] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\wr_read_nextgray_dx[1]_1 [3]),
        .Q(wr_read_next[3]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_nextgray_dx_reg[0][0] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(read_nextgray[0]),
        .Q(\wr_read_nextgray_dx[0]_0 [0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_nextgray_dx_reg[0][1] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(read_nextgray[1]),
        .Q(\wr_read_nextgray_dx[0]_0 [1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_nextgray_dx_reg[0][2] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(read_nextgray[2]),
        .Q(\wr_read_nextgray_dx[0]_0 [2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_nextgray_dx_reg[0][3] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(read_nextgray[3]),
        .Q(\wr_read_nextgray_dx[0]_0 [3]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_nextgray_dx_reg[1][0] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\wr_read_nextgray_dx[0]_0 [0]),
        .Q(\wr_read_nextgray_dx[1]_1 [0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_nextgray_dx_reg[1][1] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\wr_read_nextgray_dx[0]_0 [1]),
        .Q(\wr_read_nextgray_dx[1]_1 [1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_nextgray_dx_reg[1][2] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\wr_read_nextgray_dx[0]_0 [2]),
        .Q(\wr_read_nextgray_dx[1]_1 [2]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \wr_read_nextgray_dx_reg[1][3] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\wr_read_nextgray_dx[0]_0 [3]),
        .Q(\wr_read_nextgray_dx[1]_1 [3]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    wr_rst_meta_inst
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(1'b0),
        .PRE(rd_rst_meta_inst_0),
        .Q(wr_meta_reset));
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    wr_rst_sync_inst
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(wr_meta_reset),
        .PRE(rd_rst_meta_inst_0),
        .Q(wr_reset));
  LUT1 #(
    .INIT(2'h1)) 
    \write_addr[0]_i_1__0 
       (.I0(\write_addr_reg_n_0_[0] ),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \write_addr[1]_i_1__0 
       (.I0(\write_addr_reg_n_0_[0] ),
        .I1(p_1_in16_in),
        .O(plusOp[1]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \write_addr[2]_i_1__0 
       (.I0(\write_addr_reg_n_0_[0] ),
        .I1(p_1_in16_in),
        .I2(p_2_in17_in),
        .O(plusOp[2]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \write_addr[3]_i_1__0 
       (.I0(p_1_in16_in),
        .I1(\write_addr_reg_n_0_[0] ),
        .I2(p_2_in17_in),
        .I3(\write_addr_reg_n_0_[3] ),
        .O(plusOp[3]));
  FDPE \write_addr_d1_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1__0_n_0 ),
        .D(\write_addr_reg_n_0_[0] ),
        .PRE(wr_reset),
        .Q(write_addr_d1[0]));
  FDPE \write_addr_d1_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1__0_n_0 ),
        .D(p_1_in16_in),
        .PRE(wr_reset),
        .Q(write_addr_d1[1]));
  FDPE \write_addr_d1_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1__0_n_0 ),
        .D(p_2_in17_in),
        .PRE(wr_reset),
        .Q(write_addr_d1[2]));
  FDPE \write_addr_d1_reg[3] 
       (.C(S0_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1__0_n_0 ),
        .D(\write_addr_reg_n_0_[3] ),
        .PRE(wr_reset),
        .Q(write_nextgray[3]));
  FDCE \write_addr_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1__0_n_0 ),
        .CLR(wr_reset),
        .D(plusOp[0]),
        .Q(\write_addr_reg_n_0_[0] ));
  FDCE \write_addr_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1__0_n_0 ),
        .CLR(wr_reset),
        .D(plusOp[1]),
        .Q(p_1_in16_in));
  FDCE \write_addr_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1__0_n_0 ),
        .CLR(wr_reset),
        .D(plusOp[2]),
        .Q(p_2_in17_in));
  FDCE \write_addr_reg[3] 
       (.C(S0_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1__0_n_0 ),
        .CLR(wr_reset),
        .D(plusOp[3]),
        .Q(\write_addr_reg_n_0_[3] ));
  FDPE \write_addrgray_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1__0_n_0 ),
        .D(write_nextgray[0]),
        .PRE(wr_reset),
        .Q(write_addrgray[0]));
  FDCE \write_addrgray_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1__0_n_0 ),
        .CLR(wr_reset),
        .D(write_nextgray[1]),
        .Q(write_addrgray[1]));
  FDCE \write_addrgray_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1__0_n_0 ),
        .CLR(wr_reset),
        .D(write_nextgray[2]),
        .Q(write_addrgray[2]));
  FDPE \write_addrgray_reg[3] 
       (.C(S0_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1__0_n_0 ),
        .D(write_nextgray[3]),
        .PRE(wr_reset),
        .Q(write_addrgray[3]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \write_nextgray[0]_i_1__0 
       (.I0(p_1_in16_in),
        .I1(\write_addr_reg_n_0_[0] ),
        .O(\write_nextgray[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \write_nextgray[1]_i_1__0 
       (.I0(p_2_in17_in),
        .I1(p_1_in16_in),
        .O(\write_nextgray[1]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'h0004)) 
    \write_nextgray[2]_i_1__0 
       (.I0(\write_nextgray_reg[2]_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I2(Bus_RNW_reg_0),
        .I3(\write_nextgray_reg[2]_0 ),
        .O(\write_nextgray[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \write_nextgray[2]_i_2__0 
       (.I0(\write_addr_reg_n_0_[3] ),
        .I1(p_2_in17_in),
        .O(\write_nextgray[2]_i_2__0_n_0 ));
  FDCE \write_nextgray_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1__0_n_0 ),
        .CLR(wr_reset),
        .D(\write_nextgray[0]_i_1__0_n_0 ),
        .Q(write_nextgray[0]));
  FDCE \write_nextgray_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1__0_n_0 ),
        .CLR(wr_reset),
        .D(\write_nextgray[1]_i_1__0_n_0 ),
        .Q(write_nextgray[1]));
  FDCE \write_nextgray_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(\write_nextgray[2]_i_1__0_n_0 ),
        .CLR(wr_reset),
        .D(\write_nextgray[2]_i_2__0_n_0 ),
        .Q(write_nextgray[2]));
endmodule

module design_1_mailbox_0_0_address_decoder
   (\s_axi_rdata_i_reg[31] ,
    \is_register_reg[2] ,
    \read_nextgray_reg[2] ,
    write_fsl_error_d1_reg,
    Bus_RNW_reg_reg_0,
    empty_allow,
    D,
    S0_AXI_ARREADY,
    error_detect,
    read_fsl_error,
    write_fsl_error,
    \s_axi_rdata_i_reg[31]_0 ,
    S0_AXI_WREADY,
    E,
    \sit_register_reg[3] ,
    s_axi_rvalid_i_reg,
    s_axi_bvalid_i_reg,
    full_error_reg,
    empty_error_reg,
    \read_nextgray_reg[2]_0 ,
    Q,
    S0_AXI_ACLK,
    out,
    state1__2,
    S0_AXI_ARVALID_0,
    \state_reg[1] ,
    write_fsl_error_d1,
    full_i_reg,
    read_fsl_error_d1,
    rit_detect_d0,
    sit_detect_d0,
    full_error,
    empty_error,
    is_read,
    S0_AXI_ARVALID,
    is_write_reg,
    \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] ,
    \sit_register_reg[0] ,
    p_6_in,
    \rit_register_reg[0] ,
    S0_AXI_RREADY,
    s_axi_rvalid_i_reg_0,
    S0_AXI_BREADY,
    s_axi_bvalid_i_reg_0,
    Rst,
    \DataOut_reg[31] ,
    ie_register,
    \bus2ip_addr_i_reg[5] ,
    bus2ip_rnw_i);
  output \s_axi_rdata_i_reg[31] ;
  output \is_register_reg[2] ;
  output \read_nextgray_reg[2] ;
  output write_fsl_error_d1_reg;
  output Bus_RNW_reg_reg_0;
  output empty_allow;
  output [1:0]D;
  output S0_AXI_ARREADY;
  output error_detect;
  output read_fsl_error;
  output write_fsl_error;
  output [31:0]\s_axi_rdata_i_reg[31]_0 ;
  output S0_AXI_WREADY;
  output [0:0]E;
  output [0:0]\sit_register_reg[3] ;
  output s_axi_rvalid_i_reg;
  output s_axi_bvalid_i_reg;
  output full_error_reg;
  output empty_error_reg;
  output [0:0]\read_nextgray_reg[2]_0 ;
  input Q;
  input S0_AXI_ACLK;
  input out;
  input state1__2;
  input S0_AXI_ARVALID_0;
  input [1:0]\state_reg[1] ;
  input write_fsl_error_d1;
  input full_i_reg;
  input read_fsl_error_d1;
  input rit_detect_d0;
  input sit_detect_d0;
  input full_error;
  input empty_error;
  input is_read;
  input S0_AXI_ARVALID;
  input is_write_reg;
  input [4:0]\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] ;
  input [3:0]\sit_register_reg[0] ;
  input [2:0]p_6_in;
  input [3:0]\rit_register_reg[0] ;
  input S0_AXI_RREADY;
  input s_axi_rvalid_i_reg_0;
  input S0_AXI_BREADY;
  input s_axi_bvalid_i_reg_0;
  input Rst;
  input [31:0]\DataOut_reg[31] ;
  input [0:2]ie_register;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input bus2ip_rnw_i;

  wire Bus_RNW_reg_i_1_n_0;
  wire Bus_RNW_reg_reg_0;
  wire [1:0]D;
  wire [31:0]\DataOut_reg[31] ;
  wire [0:0]E;
  wire \GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1__0_n_0 ;
  wire \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[6].ce_out_i[6]_i_1_n_0 ;
  wire \GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[8].ce_out_i[8]_i_1_n_0 ;
  wire \GEN_BKEND_CE_REGISTERS[9].ce_out_i[9]_i_1_n_0 ;
  wire [4:0]\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] ;
  wire \MEM_DECODE_GEN[0].PER_CE_GEN[0].MULTIPLE_CES_THIS_CS_GEN.CE_I_n_0 ;
  wire Q;
  wire Rst;
  wire S0_AXI_ACLK;
  wire S0_AXI_ARREADY;
  wire S0_AXI_ARREADY_INST_0_i_1_n_0;
  wire S0_AXI_ARREADY_INST_0_i_2_n_0;
  wire S0_AXI_ARREADY_INST_0_i_3_n_0;
  wire S0_AXI_ARREADY_INST_0_i_4_n_0;
  wire S0_AXI_ARVALID;
  wire S0_AXI_ARVALID_0;
  wire S0_AXI_BREADY;
  wire S0_AXI_RREADY;
  wire S0_AXI_WREADY;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire bus2ip_rnw_i;
  wire ce_expnd_i_0;
  wire ce_expnd_i_3;
  wire ce_expnd_i_5;
  wire ce_expnd_i_6;
  wire ce_expnd_i_8;
  wire empty_allow;
  wire empty_error;
  wire empty_error_reg;
  wire eqOp__3;
  wire error_detect;
  wire full_error;
  wire full_error_reg;
  wire full_i_reg;
  wire [0:2]ie_register;
  wire ip2bus_wrack__3;
  wire is_read;
  wire \is_register_reg[2] ;
  wire is_write_reg;
  wire out;
  wire [2:0]p_6_in;
  wire read_fsl_error;
  wire read_fsl_error_d1;
  wire \read_nextgray_reg[2] ;
  wire [0:0]\read_nextgray_reg[2]_0 ;
  wire rit_detect_d0;
  wire [3:0]\rit_register_reg[0] ;
  wire s_axi_bvalid_i_reg;
  wire s_axi_bvalid_i_reg_0;
  wire \s_axi_rdata_i[0]_i_2__0_n_0 ;
  wire \s_axi_rdata_i[0]_i_3__0_n_0 ;
  wire \s_axi_rdata_i[1]_i_2__0_n_0 ;
  wire \s_axi_rdata_i[1]_i_3__0_n_0 ;
  wire \s_axi_rdata_i[1]_i_4__0_n_0 ;
  wire \s_axi_rdata_i[1]_i_5__0_n_0 ;
  wire \s_axi_rdata_i[2]_i_2__0_n_0 ;
  wire \s_axi_rdata_i[2]_i_3__0_n_0 ;
  wire \s_axi_rdata_i[2]_i_4__0_n_0 ;
  wire \s_axi_rdata_i[2]_i_5__0_n_0 ;
  wire \s_axi_rdata_i[2]_i_6__0_n_0 ;
  wire \s_axi_rdata_i[31]_i_3__0_n_0 ;
  wire \s_axi_rdata_i[3]_i_2__0_n_0 ;
  wire \s_axi_rdata_i[3]_i_3__0_n_0 ;
  wire \s_axi_rdata_i_reg[31] ;
  wire [31:0]\s_axi_rdata_i_reg[31]_0 ;
  wire s_axi_rvalid_i_reg;
  wire s_axi_rvalid_i_reg_0;
  wire sit_detect_d0;
  wire [3:0]\sit_register_reg[0] ;
  wire [0:0]\sit_register_reg[3] ;
  wire state1__2;
  wire [1:0]\state_reg[1] ;
  wire write_fsl_error;
  wire write_fsl_error_d1;
  wire write_fsl_error_d1_reg;

  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    Bus_RNW_reg_i_1
       (.I0(bus2ip_rnw_i),
        .I1(Q),
        .I2(Bus_RNW_reg_reg_0),
        .O(Bus_RNW_reg_i_1_n_0));
  FDRE Bus_RNW_reg_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(Bus_RNW_reg_i_1_n_0),
        .Q(Bus_RNW_reg_reg_0),
        .R(1'b0));
  FDRE \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(Q),
        .D(\MEM_DECODE_GEN[0].PER_CE_GEN[0].MULTIPLE_CES_THIS_CS_GEN.CE_I_n_0 ),
        .Q(write_fsl_error_d1_reg),
        .R(\GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'hFE)) 
    \GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1__0 
       (.I0(S0_AXI_WREADY),
        .I1(S0_AXI_ARREADY),
        .I2(Rst),
        .O(\GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1__0_n_0 ));
  FDRE \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg[10] 
       (.C(S0_AXI_ACLK),
        .CE(Q),
        .D(ce_expnd_i_0),
        .Q(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .R(\GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1__0_n_0 ));
  FDRE \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(Q),
        .D(ce_expnd_i_8),
        .Q(\read_nextgray_reg[2] ),
        .R(\GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1__0_n_0 ));
  FDRE \GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg[4] 
       (.C(S0_AXI_ACLK),
        .CE(Q),
        .D(ce_expnd_i_6),
        .Q(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .R(\GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1__0_n_0 ));
  FDRE \GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg[5] 
       (.C(S0_AXI_ACLK),
        .CE(Q),
        .D(ce_expnd_i_5),
        .Q(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .R(\GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1__0_n_0 ));
  LUT5 #(
    .INIT(32'h10000000)) 
    \GEN_BKEND_CE_REGISTERS[6].ce_out_i[6]_i_1 
       (.I0(\bus2ip_addr_i_reg[5] [0]),
        .I1(\bus2ip_addr_i_reg[5] [3]),
        .I2(Q),
        .I3(\bus2ip_addr_i_reg[5] [1]),
        .I4(\bus2ip_addr_i_reg[5] [2]),
        .O(\GEN_BKEND_CE_REGISTERS[6].ce_out_i[6]_i_1_n_0 ));
  FDRE \GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg[6] 
       (.C(S0_AXI_ACLK),
        .CE(Q),
        .D(\GEN_BKEND_CE_REGISTERS[6].ce_out_i[6]_i_1_n_0 ),
        .Q(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .R(\GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1__0_n_0 ));
  FDRE \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg[7] 
       (.C(S0_AXI_ACLK),
        .CE(Q),
        .D(ce_expnd_i_3),
        .Q(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .R(\GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h01000000)) 
    \GEN_BKEND_CE_REGISTERS[8].ce_out_i[8]_i_1 
       (.I0(\bus2ip_addr_i_reg[5] [1]),
        .I1(\bus2ip_addr_i_reg[5] [2]),
        .I2(\bus2ip_addr_i_reg[5] [0]),
        .I3(Q),
        .I4(\bus2ip_addr_i_reg[5] [3]),
        .O(\GEN_BKEND_CE_REGISTERS[8].ce_out_i[8]_i_1_n_0 ));
  FDRE \GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg[8] 
       (.C(S0_AXI_ACLK),
        .CE(Q),
        .D(\GEN_BKEND_CE_REGISTERS[8].ce_out_i[8]_i_1_n_0 ),
        .Q(\is_register_reg[2] ),
        .R(\GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h10000000)) 
    \GEN_BKEND_CE_REGISTERS[9].ce_out_i[9]_i_1 
       (.I0(\bus2ip_addr_i_reg[5] [1]),
        .I1(\bus2ip_addr_i_reg[5] [2]),
        .I2(\bus2ip_addr_i_reg[5] [3]),
        .I3(\bus2ip_addr_i_reg[5] [0]),
        .I4(Q),
        .O(\GEN_BKEND_CE_REGISTERS[9].ce_out_i[9]_i_1_n_0 ));
  FDRE \GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg[9] 
       (.C(S0_AXI_ACLK),
        .CE(Q),
        .D(\GEN_BKEND_CE_REGISTERS[9].ce_out_i[9]_i_1_n_0 ),
        .Q(\s_axi_rdata_i_reg[31] ),
        .R(\GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1__0_n_0 ));
  design_1_mailbox_0_0_pselect_f_2 \MEM_DECODE_GEN[0].PER_CE_GEN[0].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] (\MEM_DECODE_GEN[0].PER_CE_GEN[0].MULTIPLE_CES_THIS_CS_GEN.CE_I_n_0 ),
        .Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ));
  design_1_mailbox_0_0_pselect_f__parameterized9 \MEM_DECODE_GEN[0].PER_CE_GEN[10].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_0(ce_expnd_i_0));
  design_1_mailbox_0_0_pselect_f__parameterized1 \MEM_DECODE_GEN[0].PER_CE_GEN[2].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_8(ce_expnd_i_8));
  design_1_mailbox_0_0_pselect_f__parameterized3 \MEM_DECODE_GEN[0].PER_CE_GEN[4].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_6(ce_expnd_i_6));
  design_1_mailbox_0_0_pselect_f__parameterized4 \MEM_DECODE_GEN[0].PER_CE_GEN[5].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_5(ce_expnd_i_5));
  design_1_mailbox_0_0_pselect_f__parameterized6 \MEM_DECODE_GEN[0].PER_CE_GEN[7].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_3(ce_expnd_i_3));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFEFFFE)) 
    S0_AXI_ARREADY_INST_0
       (.I0(S0_AXI_ARREADY_INST_0_i_1_n_0),
        .I1(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .I2(S0_AXI_ARREADY_INST_0_i_3_n_0),
        .I3(S0_AXI_ARREADY_INST_0_i_4_n_0),
        .I4(is_read),
        .I5(eqOp__3),
        .O(S0_AXI_ARREADY));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hFF00FE00)) 
    S0_AXI_ARREADY_INST_0_i_1
       (.I0(\s_axi_rdata_i_reg[31] ),
        .I1(\is_register_reg[2] ),
        .I2(\read_nextgray_reg[2] ),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .O(S0_AXI_ARREADY_INST_0_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hC8)) 
    S0_AXI_ARREADY_INST_0_i_2
       (.I0(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I1(Bus_RNW_reg_reg_0),
        .I2(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .O(S0_AXI_ARREADY_INST_0_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h8)) 
    S0_AXI_ARREADY_INST_0_i_3
       (.I0(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I1(Bus_RNW_reg_reg_0),
        .O(S0_AXI_ARREADY_INST_0_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h8)) 
    S0_AXI_ARREADY_INST_0_i_4
       (.I0(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I1(Bus_RNW_reg_reg_0),
        .O(S0_AXI_ARREADY_INST_0_i_4_n_0));
  LUT3 #(
    .INIT(8'hEA)) 
    S0_AXI_WREADY_INST_0
       (.I0(ip2bus_wrack__3),
        .I1(is_write_reg),
        .I2(eqOp__3),
        .O(S0_AXI_WREADY));
  LUT6 #(
    .INIT(64'h0F0F0F0F0F0F0F0E)) 
    S0_AXI_WREADY_INST_0_i_1
       (.I0(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I1(\s_axi_rdata_i_reg[31] ),
        .I2(Bus_RNW_reg_reg_0),
        .I3(write_fsl_error_d1_reg),
        .I4(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I5(\is_register_reg[2] ),
        .O(ip2bus_wrack__3));
  LUT5 #(
    .INIT(32'h00000010)) 
    S0_AXI_WREADY_INST_0_i_2
       (.I0(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] [3]),
        .I1(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] [2]),
        .I2(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] [4]),
        .I3(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] [0]),
        .I4(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] [1]),
        .O(eqOp__3));
  LUT6 #(
    .INIT(64'h0000000000EAAAAA)) 
    empty_error_i_1__0
       (.I0(empty_error),
        .I1(out),
        .I2(\read_nextgray_reg[2] ),
        .I3(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I4(Bus_RNW_reg_reg_0),
        .I5(Rst),
        .O(empty_error_reg));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hEA)) 
    empty_i_1__0
       (.I0(out),
        .I1(\read_nextgray_reg[2] ),
        .I2(Bus_RNW_reg_reg_0),
        .O(empty_allow));
  LUT6 #(
    .INIT(64'h0000000000AAEAEA)) 
    full_error_i_1__0
       (.I0(full_error),
        .I1(write_fsl_error_d1_reg),
        .I2(full_i_reg),
        .I3(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I4(Bus_RNW_reg_reg_0),
        .I5(Rst),
        .O(full_error_reg));
  LUT6 #(
    .INIT(64'h1000FFFF10001000)) 
    \is_register[0]_i_2__0 
       (.I0(write_fsl_error_d1),
        .I1(Bus_RNW_reg_reg_0),
        .I2(write_fsl_error_d1_reg),
        .I3(full_i_reg),
        .I4(read_fsl_error_d1),
        .I5(read_fsl_error),
        .O(error_detect));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h80)) 
    read_fsl_error_d1_i_1__0
       (.I0(out),
        .I1(Bus_RNW_reg_reg_0),
        .I2(\read_nextgray_reg[2] ),
        .O(read_fsl_error));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \read_nextgray[2]_i_1__0 
       (.I0(\read_nextgray_reg[2] ),
        .I1(Bus_RNW_reg_reg_0),
        .I2(out),
        .O(\read_nextgray_reg[2]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \rit_register[0]_i_1__0 
       (.I0(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I1(Bus_RNW_reg_reg_0),
        .O(E));
  LUT5 #(
    .INIT(32'h08FF0808)) 
    s_axi_bvalid_i_i_1
       (.I0(S0_AXI_WREADY),
        .I1(\state_reg[1] [1]),
        .I2(\state_reg[1] [0]),
        .I3(S0_AXI_BREADY),
        .I4(s_axi_bvalid_i_reg_0),
        .O(s_axi_bvalid_i_reg));
  LUT6 #(
    .INIT(64'hFFFEFF0E00FE000E)) 
    \s_axi_rdata_i[0]_i_1__0 
       (.I0(\s_axi_rdata_i[0]_i_2__0_n_0 ),
        .I1(\s_axi_rdata_i[0]_i_3__0_n_0 ),
        .I2(\s_axi_rdata_i[1]_i_4__0_n_0 ),
        .I3(\s_axi_rdata_i[1]_i_5__0_n_0 ),
        .I4(empty_error),
        .I5(out),
        .O(\s_axi_rdata_i_reg[31]_0 [0]));
  LUT6 #(
    .INIT(64'hAAF0AAF0AACCAA00)) 
    \s_axi_rdata_i[0]_i_2__0 
       (.I0(\sit_register_reg[0] [0]),
        .I1(p_6_in[0]),
        .I2(\rit_register_reg[0] [0]),
        .I3(S0_AXI_ARREADY_INST_0_i_4_n_0),
        .I4(\s_axi_rdata_i[2]_i_4__0_n_0 ),
        .I5(S0_AXI_ARREADY_INST_0_i_3_n_0),
        .O(\s_axi_rdata_i[0]_i_2__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000AA88B8B8)) 
    \s_axi_rdata_i[0]_i_3__0 
       (.I0(ie_register[2]),
        .I1(\s_axi_rdata_i[2]_i_5__0_n_0 ),
        .I2(\DataOut_reg[31] [0]),
        .I3(p_6_in[0]),
        .I4(\s_axi_rdata_i[2]_i_6__0_n_0 ),
        .I5(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .O(\s_axi_rdata_i[0]_i_3__0_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[10]_i_1__0 
       (.I0(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [10]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [10]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[11]_i_1__0 
       (.I0(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [11]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [11]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[12]_i_1__0 
       (.I0(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [12]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [12]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[13]_i_1__0 
       (.I0(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [13]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [13]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[14]_i_1__0 
       (.I0(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [14]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [14]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[15]_i_1__0 
       (.I0(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [15]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [15]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[16]_i_1__0 
       (.I0(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [16]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [16]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[17]_i_1__0 
       (.I0(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [17]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [17]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[18]_i_1__0 
       (.I0(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [18]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [18]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[19]_i_1__0 
       (.I0(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [19]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [19]));
  LUT6 #(
    .INIT(64'hFFFEFF0E00FE000E)) 
    \s_axi_rdata_i[1]_i_1__0 
       (.I0(\s_axi_rdata_i[1]_i_2__0_n_0 ),
        .I1(\s_axi_rdata_i[1]_i_3__0_n_0 ),
        .I2(\s_axi_rdata_i[1]_i_4__0_n_0 ),
        .I3(\s_axi_rdata_i[1]_i_5__0_n_0 ),
        .I4(full_error),
        .I5(full_i_reg),
        .O(\s_axi_rdata_i_reg[31]_0 [1]));
  LUT6 #(
    .INIT(64'hAAF0AAF0AACCAA00)) 
    \s_axi_rdata_i[1]_i_2__0 
       (.I0(\sit_register_reg[0] [1]),
        .I1(p_6_in[1]),
        .I2(\rit_register_reg[0] [1]),
        .I3(S0_AXI_ARREADY_INST_0_i_4_n_0),
        .I4(\s_axi_rdata_i[2]_i_4__0_n_0 ),
        .I5(S0_AXI_ARREADY_INST_0_i_3_n_0),
        .O(\s_axi_rdata_i[1]_i_2__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000AA88B8B8)) 
    \s_axi_rdata_i[1]_i_3__0 
       (.I0(ie_register[1]),
        .I1(\s_axi_rdata_i[2]_i_5__0_n_0 ),
        .I2(\DataOut_reg[31] [1]),
        .I3(p_6_in[1]),
        .I4(\s_axi_rdata_i[2]_i_6__0_n_0 ),
        .I5(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .O(\s_axi_rdata_i[1]_i_3__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s_axi_rdata_i[1]_i_4__0 
       (.I0(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I1(Bus_RNW_reg_reg_0),
        .O(\s_axi_rdata_i[1]_i_4__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s_axi_rdata_i[1]_i_5__0 
       (.I0(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .I1(Bus_RNW_reg_reg_0),
        .O(\s_axi_rdata_i[1]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[20]_i_1__0 
       (.I0(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [20]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [20]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[21]_i_1__0 
       (.I0(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [21]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [21]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[22]_i_1__0 
       (.I0(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [22]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [22]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[23]_i_1__0 
       (.I0(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [23]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [23]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[24]_i_1__0 
       (.I0(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [24]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [24]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[25]_i_1__0 
       (.I0(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [25]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [25]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[26]_i_1__0 
       (.I0(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [26]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [26]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[27]_i_1__0 
       (.I0(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [27]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [27]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[28]_i_1__0 
       (.I0(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [28]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [28]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[29]_i_1__0 
       (.I0(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [29]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [29]));
  LUT6 #(
    .INIT(64'hFFEE0EEE00EE0EEE)) 
    \s_axi_rdata_i[2]_i_1__0 
       (.I0(\s_axi_rdata_i[2]_i_2__0_n_0 ),
        .I1(\s_axi_rdata_i[2]_i_3__0_n_0 ),
        .I2(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .I5(sit_detect_d0),
        .O(\s_axi_rdata_i_reg[31]_0 [2]));
  LUT6 #(
    .INIT(64'hAAF0AAF0AACCAA00)) 
    \s_axi_rdata_i[2]_i_2__0 
       (.I0(\sit_register_reg[0] [2]),
        .I1(p_6_in[2]),
        .I2(\rit_register_reg[0] [2]),
        .I3(S0_AXI_ARREADY_INST_0_i_4_n_0),
        .I4(\s_axi_rdata_i[2]_i_4__0_n_0 ),
        .I5(S0_AXI_ARREADY_INST_0_i_3_n_0),
        .O(\s_axi_rdata_i[2]_i_2__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000AA88B8B8)) 
    \s_axi_rdata_i[2]_i_3__0 
       (.I0(ie_register[0]),
        .I1(\s_axi_rdata_i[2]_i_5__0_n_0 ),
        .I2(\DataOut_reg[31] [2]),
        .I3(p_6_in[2]),
        .I4(\s_axi_rdata_i[2]_i_6__0_n_0 ),
        .I5(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .O(\s_axi_rdata_i[2]_i_3__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s_axi_rdata_i[2]_i_4__0 
       (.I0(\is_register_reg[2] ),
        .I1(Bus_RNW_reg_reg_0),
        .O(\s_axi_rdata_i[2]_i_4__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s_axi_rdata_i[2]_i_5__0 
       (.I0(\s_axi_rdata_i_reg[31] ),
        .I1(Bus_RNW_reg_reg_0),
        .O(\s_axi_rdata_i[2]_i_5__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s_axi_rdata_i[2]_i_6__0 
       (.I0(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I1(Bus_RNW_reg_reg_0),
        .O(\s_axi_rdata_i[2]_i_6__0_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[30]_i_1__0 
       (.I0(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [30]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [30]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[31]_i_2__0 
       (.I0(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [31]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [31]));
  LUT4 #(
    .INIT(16'hF0E0)) 
    \s_axi_rdata_i[31]_i_3__0 
       (.I0(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I1(\is_register_reg[2] ),
        .I2(Bus_RNW_reg_reg_0),
        .I3(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .O(\s_axi_rdata_i[31]_i_3__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFEE0EEE00EE0EEE)) 
    \s_axi_rdata_i[3]_i_1__0 
       (.I0(\s_axi_rdata_i[3]_i_2__0_n_0 ),
        .I1(\s_axi_rdata_i[3]_i_3__0_n_0 ),
        .I2(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .I5(rit_detect_d0),
        .O(\s_axi_rdata_i_reg[31]_0 [3]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'hAC00A000)) 
    \s_axi_rdata_i[3]_i_2__0 
       (.I0(\sit_register_reg[0] [3]),
        .I1(\rit_register_reg[0] [3]),
        .I2(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .O(\s_axi_rdata_i[3]_i_2__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h00003070)) 
    \s_axi_rdata_i[3]_i_3__0 
       (.I0(\s_axi_rdata_i_reg[31] ),
        .I1(Bus_RNW_reg_reg_0),
        .I2(\DataOut_reg[31] [3]),
        .I3(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I4(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .O(\s_axi_rdata_i[3]_i_3__0_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[4]_i_1__0 
       (.I0(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [4]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [4]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[5]_i_1__0 
       (.I0(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [5]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [5]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[6]_i_1__0 
       (.I0(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [6]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [6]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[7]_i_1__0 
       (.I0(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [7]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [7]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[8]_i_1__0 
       (.I0(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [8]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [8]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[9]_i_1__0 
       (.I0(\s_axi_rdata_i[31]_i_3__0_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [9]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S0_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [9]));
  LUT5 #(
    .INIT(32'h08FF0808)) 
    s_axi_rvalid_i_i_1
       (.I0(S0_AXI_ARREADY),
        .I1(\state_reg[1] [0]),
        .I2(\state_reg[1] [1]),
        .I3(S0_AXI_RREADY),
        .I4(s_axi_rvalid_i_reg_0),
        .O(s_axi_rvalid_i_reg));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \sit_register[0]_i_1__0 
       (.I0(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I1(Bus_RNW_reg_reg_0),
        .O(\sit_register_reg[3] ));
  LUT5 #(
    .INIT(32'h77FC44FC)) 
    \state[0]_i_1__0 
       (.I0(state1__2),
        .I1(\state_reg[1] [0]),
        .I2(S0_AXI_ARVALID),
        .I3(\state_reg[1] [1]),
        .I4(S0_AXI_WREADY),
        .O(D[0]));
  LUT5 #(
    .INIT(32'h5FFC50FC)) 
    \state[1]_i_1__0 
       (.I0(state1__2),
        .I1(S0_AXI_ARVALID_0),
        .I2(\state_reg[1] [1]),
        .I3(\state_reg[1] [0]),
        .I4(S0_AXI_ARREADY),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h40)) 
    write_fsl_error_d1_i_1__0
       (.I0(Bus_RNW_reg_reg_0),
        .I1(write_fsl_error_d1_reg),
        .I2(full_i_reg),
        .O(write_fsl_error));
endmodule

(* ORIG_REF_NAME = "address_decoder" *) 
module design_1_mailbox_0_0_address_decoder__parameterized0
   (\s_axi_rdata_i_reg[31] ,
    \is_register_reg[2] ,
    \read_nextgray_reg[2] ,
    write_fsl_error_d1_reg,
    Bus_RNW_reg_reg_0,
    empty_allow,
    D,
    S1_AXI_ARREADY,
    error_detect,
    read_fsl_error,
    write_fsl_error,
    \s_axi_rdata_i_reg[31]_0 ,
    S1_AXI_WREADY,
    E,
    \sit_register_reg[3] ,
    s_axi_rvalid_i_reg,
    s_axi_bvalid_i_reg,
    full_error_reg,
    empty_error_reg,
    \read_nextgray_reg[2]_0 ,
    Q,
    S1_AXI_ACLK,
    out,
    state1__2,
    S1_AXI_ARVALID_0,
    \state_reg[1] ,
    write_fsl_error_d1,
    full_i_reg,
    read_fsl_error_d1,
    rit_detect_d0,
    sit_detect_d0,
    full_error_reg_0,
    empty_error_reg_0,
    is_read,
    S1_AXI_ARVALID,
    is_write_reg,
    \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] ,
    \sit_register_reg[0] ,
    p_6_in,
    \rit_register_reg[0] ,
    S1_AXI_RREADY,
    s_axi_rvalid_i_reg_0,
    S1_AXI_BREADY,
    s_axi_bvalid_i_reg_0,
    \Rst_Async.SYS_Rst_If1_reg ,
    \DataOut_reg[31] ,
    ie_register,
    \bus2ip_addr_i_reg[5] ,
    bus2ip_rnw_i_reg);
  output \s_axi_rdata_i_reg[31] ;
  output \is_register_reg[2] ;
  output \read_nextgray_reg[2] ;
  output write_fsl_error_d1_reg;
  output Bus_RNW_reg_reg_0;
  output empty_allow;
  output [1:0]D;
  output S1_AXI_ARREADY;
  output error_detect;
  output read_fsl_error;
  output write_fsl_error;
  output [31:0]\s_axi_rdata_i_reg[31]_0 ;
  output S1_AXI_WREADY;
  output [0:0]E;
  output [0:0]\sit_register_reg[3] ;
  output s_axi_rvalid_i_reg;
  output s_axi_bvalid_i_reg;
  output full_error_reg;
  output empty_error_reg;
  output [0:0]\read_nextgray_reg[2]_0 ;
  input Q;
  input S1_AXI_ACLK;
  input out;
  input state1__2;
  input S1_AXI_ARVALID_0;
  input [1:0]\state_reg[1] ;
  input write_fsl_error_d1;
  input full_i_reg;
  input read_fsl_error_d1;
  input rit_detect_d0;
  input sit_detect_d0;
  input full_error_reg_0;
  input empty_error_reg_0;
  input is_read;
  input S1_AXI_ARVALID;
  input is_write_reg;
  input [4:0]\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] ;
  input [3:0]\sit_register_reg[0] ;
  input [2:0]p_6_in;
  input [3:0]\rit_register_reg[0] ;
  input S1_AXI_RREADY;
  input s_axi_rvalid_i_reg_0;
  input S1_AXI_BREADY;
  input s_axi_bvalid_i_reg_0;
  input \Rst_Async.SYS_Rst_If1_reg ;
  input [31:0]\DataOut_reg[31] ;
  input [0:2]ie_register;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input bus2ip_rnw_i_reg;

  wire Bus_RNW_reg_i_1__0_n_0;
  wire Bus_RNW_reg_reg_0;
  wire [1:0]D;
  wire [31:0]\DataOut_reg[31] ;
  wire [0:0]E;
  wire \GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1_n_0 ;
  wire \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[6].ce_out_i[6]_i_1__0_n_0 ;
  wire \GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[8].ce_out_i[8]_i_1__0_n_0 ;
  wire \GEN_BKEND_CE_REGISTERS[9].ce_out_i[9]_i_1__0_n_0 ;
  wire [4:0]\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] ;
  wire \MEM_DECODE_GEN[0].PER_CE_GEN[0].MULTIPLE_CES_THIS_CS_GEN.CE_I_n_0 ;
  wire Q;
  wire \Rst_Async.SYS_Rst_If1_reg ;
  wire S1_AXI_ACLK;
  wire S1_AXI_ARREADY;
  wire S1_AXI_ARREADY_INST_0_i_1_n_0;
  wire S1_AXI_ARREADY_INST_0_i_2_n_0;
  wire S1_AXI_ARREADY_INST_0_i_3_n_0;
  wire S1_AXI_ARREADY_INST_0_i_4_n_0;
  wire S1_AXI_ARVALID;
  wire S1_AXI_ARVALID_0;
  wire S1_AXI_BREADY;
  wire S1_AXI_RREADY;
  wire S1_AXI_WREADY;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire bus2ip_rnw_i_reg;
  wire ce_expnd_i_0;
  wire ce_expnd_i_3;
  wire ce_expnd_i_5;
  wire ce_expnd_i_6;
  wire ce_expnd_i_8;
  wire empty_allow;
  wire empty_error_reg;
  wire empty_error_reg_0;
  wire eqOp__3;
  wire error_detect;
  wire full_error_reg;
  wire full_error_reg_0;
  wire full_i_reg;
  wire [0:2]ie_register;
  wire ip2bus_wrack__3;
  wire is_read;
  wire \is_register_reg[2] ;
  wire is_write_reg;
  wire out;
  wire [2:0]p_6_in;
  wire read_fsl_error;
  wire read_fsl_error_d1;
  wire \read_nextgray_reg[2] ;
  wire [0:0]\read_nextgray_reg[2]_0 ;
  wire rit_detect_d0;
  wire [3:0]\rit_register_reg[0] ;
  wire s_axi_bvalid_i_reg;
  wire s_axi_bvalid_i_reg_0;
  wire \s_axi_rdata_i[0]_i_2_n_0 ;
  wire \s_axi_rdata_i[0]_i_3_n_0 ;
  wire \s_axi_rdata_i[1]_i_2_n_0 ;
  wire \s_axi_rdata_i[1]_i_3_n_0 ;
  wire \s_axi_rdata_i[1]_i_4_n_0 ;
  wire \s_axi_rdata_i[1]_i_5_n_0 ;
  wire \s_axi_rdata_i[2]_i_2_n_0 ;
  wire \s_axi_rdata_i[2]_i_3_n_0 ;
  wire \s_axi_rdata_i[2]_i_4_n_0 ;
  wire \s_axi_rdata_i[2]_i_5_n_0 ;
  wire \s_axi_rdata_i[2]_i_6_n_0 ;
  wire \s_axi_rdata_i[31]_i_3_n_0 ;
  wire \s_axi_rdata_i[3]_i_2_n_0 ;
  wire \s_axi_rdata_i[3]_i_3_n_0 ;
  wire \s_axi_rdata_i_reg[31] ;
  wire [31:0]\s_axi_rdata_i_reg[31]_0 ;
  wire s_axi_rvalid_i_reg;
  wire s_axi_rvalid_i_reg_0;
  wire sit_detect_d0;
  wire [3:0]\sit_register_reg[0] ;
  wire [0:0]\sit_register_reg[3] ;
  wire state1__2;
  wire [1:0]\state_reg[1] ;
  wire write_fsl_error;
  wire write_fsl_error_d1;
  wire write_fsl_error_d1_reg;

  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    Bus_RNW_reg_i_1__0
       (.I0(bus2ip_rnw_i_reg),
        .I1(Q),
        .I2(Bus_RNW_reg_reg_0),
        .O(Bus_RNW_reg_i_1__0_n_0));
  FDRE Bus_RNW_reg_reg
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(Bus_RNW_reg_i_1__0_n_0),
        .Q(Bus_RNW_reg_reg_0),
        .R(1'b0));
  FDRE \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(Q),
        .D(\MEM_DECODE_GEN[0].PER_CE_GEN[0].MULTIPLE_CES_THIS_CS_GEN.CE_I_n_0 ),
        .Q(write_fsl_error_d1_reg),
        .R(\GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hFE)) 
    \GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1 
       (.I0(S1_AXI_WREADY),
        .I1(S1_AXI_ARREADY),
        .I2(\Rst_Async.SYS_Rst_If1_reg ),
        .O(\GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1_n_0 ));
  FDRE \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg[10] 
       (.C(S1_AXI_ACLK),
        .CE(Q),
        .D(ce_expnd_i_0),
        .Q(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .R(\GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1_n_0 ));
  FDRE \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(Q),
        .D(ce_expnd_i_8),
        .Q(\read_nextgray_reg[2] ),
        .R(\GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1_n_0 ));
  FDRE \GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg[4] 
       (.C(S1_AXI_ACLK),
        .CE(Q),
        .D(ce_expnd_i_6),
        .Q(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .R(\GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1_n_0 ));
  FDRE \GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg[5] 
       (.C(S1_AXI_ACLK),
        .CE(Q),
        .D(ce_expnd_i_5),
        .Q(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .R(\GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h10000000)) 
    \GEN_BKEND_CE_REGISTERS[6].ce_out_i[6]_i_1__0 
       (.I0(\bus2ip_addr_i_reg[5] [0]),
        .I1(\bus2ip_addr_i_reg[5] [3]),
        .I2(Q),
        .I3(\bus2ip_addr_i_reg[5] [1]),
        .I4(\bus2ip_addr_i_reg[5] [2]),
        .O(\GEN_BKEND_CE_REGISTERS[6].ce_out_i[6]_i_1__0_n_0 ));
  FDRE \GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg[6] 
       (.C(S1_AXI_ACLK),
        .CE(Q),
        .D(\GEN_BKEND_CE_REGISTERS[6].ce_out_i[6]_i_1__0_n_0 ),
        .Q(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .R(\GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1_n_0 ));
  FDRE \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg[7] 
       (.C(S1_AXI_ACLK),
        .CE(Q),
        .D(ce_expnd_i_3),
        .Q(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .R(\GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'h01000000)) 
    \GEN_BKEND_CE_REGISTERS[8].ce_out_i[8]_i_1__0 
       (.I0(\bus2ip_addr_i_reg[5] [1]),
        .I1(\bus2ip_addr_i_reg[5] [2]),
        .I2(\bus2ip_addr_i_reg[5] [0]),
        .I3(Q),
        .I4(\bus2ip_addr_i_reg[5] [3]),
        .O(\GEN_BKEND_CE_REGISTERS[8].ce_out_i[8]_i_1__0_n_0 ));
  FDRE \GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg[8] 
       (.C(S1_AXI_ACLK),
        .CE(Q),
        .D(\GEN_BKEND_CE_REGISTERS[8].ce_out_i[8]_i_1__0_n_0 ),
        .Q(\is_register_reg[2] ),
        .R(\GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'h10000000)) 
    \GEN_BKEND_CE_REGISTERS[9].ce_out_i[9]_i_1__0 
       (.I0(\bus2ip_addr_i_reg[5] [1]),
        .I1(\bus2ip_addr_i_reg[5] [2]),
        .I2(\bus2ip_addr_i_reg[5] [3]),
        .I3(\bus2ip_addr_i_reg[5] [0]),
        .I4(Q),
        .O(\GEN_BKEND_CE_REGISTERS[9].ce_out_i[9]_i_1__0_n_0 ));
  FDRE \GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg[9] 
       (.C(S1_AXI_ACLK),
        .CE(Q),
        .D(\GEN_BKEND_CE_REGISTERS[9].ce_out_i[9]_i_1__0_n_0 ),
        .Q(\s_axi_rdata_i_reg[31] ),
        .R(\GEN_BKEND_CE_REGISTERS[10].ce_out_i[10]_i_1_n_0 ));
  design_1_mailbox_0_0_pselect_f \MEM_DECODE_GEN[0].PER_CE_GEN[0].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] (\MEM_DECODE_GEN[0].PER_CE_GEN[0].MULTIPLE_CES_THIS_CS_GEN.CE_I_n_0 ),
        .Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ));
  design_1_mailbox_0_0_pselect_f__parameterized19 \MEM_DECODE_GEN[0].PER_CE_GEN[10].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_0(ce_expnd_i_0));
  design_1_mailbox_0_0_pselect_f__parameterized11 \MEM_DECODE_GEN[0].PER_CE_GEN[2].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_8(ce_expnd_i_8));
  design_1_mailbox_0_0_pselect_f__parameterized13 \MEM_DECODE_GEN[0].PER_CE_GEN[4].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_6(ce_expnd_i_6));
  design_1_mailbox_0_0_pselect_f__parameterized14 \MEM_DECODE_GEN[0].PER_CE_GEN[5].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_5(ce_expnd_i_5));
  design_1_mailbox_0_0_pselect_f__parameterized16 \MEM_DECODE_GEN[0].PER_CE_GEN[7].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_3(ce_expnd_i_3));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFEFFFE)) 
    S1_AXI_ARREADY_INST_0
       (.I0(S1_AXI_ARREADY_INST_0_i_1_n_0),
        .I1(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .I2(S1_AXI_ARREADY_INST_0_i_3_n_0),
        .I3(S1_AXI_ARREADY_INST_0_i_4_n_0),
        .I4(is_read),
        .I5(eqOp__3),
        .O(S1_AXI_ARREADY));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT5 #(
    .INIT(32'hFF00FE00)) 
    S1_AXI_ARREADY_INST_0_i_1
       (.I0(\s_axi_rdata_i_reg[31] ),
        .I1(\is_register_reg[2] ),
        .I2(\read_nextgray_reg[2] ),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .O(S1_AXI_ARREADY_INST_0_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hC8)) 
    S1_AXI_ARREADY_INST_0_i_2
       (.I0(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I1(Bus_RNW_reg_reg_0),
        .I2(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .O(S1_AXI_ARREADY_INST_0_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'h8)) 
    S1_AXI_ARREADY_INST_0_i_3
       (.I0(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I1(Bus_RNW_reg_reg_0),
        .O(S1_AXI_ARREADY_INST_0_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h8)) 
    S1_AXI_ARREADY_INST_0_i_4
       (.I0(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I1(Bus_RNW_reg_reg_0),
        .O(S1_AXI_ARREADY_INST_0_i_4_n_0));
  LUT3 #(
    .INIT(8'hEA)) 
    S1_AXI_WREADY_INST_0
       (.I0(ip2bus_wrack__3),
        .I1(is_write_reg),
        .I2(eqOp__3),
        .O(S1_AXI_WREADY));
  LUT6 #(
    .INIT(64'h0F0F0F0F0F0F0F0E)) 
    S1_AXI_WREADY_INST_0_i_1
       (.I0(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I1(\s_axi_rdata_i_reg[31] ),
        .I2(Bus_RNW_reg_reg_0),
        .I3(write_fsl_error_d1_reg),
        .I4(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I5(\is_register_reg[2] ),
        .O(ip2bus_wrack__3));
  LUT5 #(
    .INIT(32'h00000010)) 
    S1_AXI_WREADY_INST_0_i_2
       (.I0(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] [3]),
        .I1(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] [2]),
        .I2(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] [4]),
        .I3(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] [0]),
        .I4(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] [1]),
        .O(eqOp__3));
  LUT6 #(
    .INIT(64'h0000000000EAAAAA)) 
    empty_error_i_1
       (.I0(empty_error_reg_0),
        .I1(out),
        .I2(\read_nextgray_reg[2] ),
        .I3(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I4(Bus_RNW_reg_reg_0),
        .I5(\Rst_Async.SYS_Rst_If1_reg ),
        .O(empty_error_reg));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hEA)) 
    empty_i_1
       (.I0(out),
        .I1(\read_nextgray_reg[2] ),
        .I2(Bus_RNW_reg_reg_0),
        .O(empty_allow));
  LUT6 #(
    .INIT(64'h0000000000AAEAEA)) 
    full_error_i_1
       (.I0(full_error_reg_0),
        .I1(write_fsl_error_d1_reg),
        .I2(full_i_reg),
        .I3(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I4(Bus_RNW_reg_reg_0),
        .I5(\Rst_Async.SYS_Rst_If1_reg ),
        .O(full_error_reg));
  LUT6 #(
    .INIT(64'h1000FFFF10001000)) 
    \is_register[0]_i_2 
       (.I0(write_fsl_error_d1),
        .I1(Bus_RNW_reg_reg_0),
        .I2(write_fsl_error_d1_reg),
        .I3(full_i_reg),
        .I4(read_fsl_error_d1),
        .I5(read_fsl_error),
        .O(error_detect));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'h80)) 
    read_fsl_error_d1_i_1
       (.I0(out),
        .I1(Bus_RNW_reg_reg_0),
        .I2(\read_nextgray_reg[2] ),
        .O(read_fsl_error));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \read_nextgray[2]_i_1 
       (.I0(\read_nextgray_reg[2] ),
        .I1(Bus_RNW_reg_reg_0),
        .I2(out),
        .O(\read_nextgray_reg[2]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \rit_register[0]_i_1 
       (.I0(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I1(Bus_RNW_reg_reg_0),
        .O(E));
  LUT5 #(
    .INIT(32'h08FF0808)) 
    s_axi_bvalid_i_i_1__0
       (.I0(S1_AXI_WREADY),
        .I1(\state_reg[1] [1]),
        .I2(\state_reg[1] [0]),
        .I3(S1_AXI_BREADY),
        .I4(s_axi_bvalid_i_reg_0),
        .O(s_axi_bvalid_i_reg));
  LUT6 #(
    .INIT(64'hFFFEFF0E00FE000E)) 
    \s_axi_rdata_i[0]_i_1 
       (.I0(\s_axi_rdata_i[0]_i_2_n_0 ),
        .I1(\s_axi_rdata_i[0]_i_3_n_0 ),
        .I2(\s_axi_rdata_i[1]_i_4_n_0 ),
        .I3(\s_axi_rdata_i[1]_i_5_n_0 ),
        .I4(empty_error_reg_0),
        .I5(out),
        .O(\s_axi_rdata_i_reg[31]_0 [0]));
  LUT6 #(
    .INIT(64'hAAF0AAF0AACCAA00)) 
    \s_axi_rdata_i[0]_i_2 
       (.I0(\sit_register_reg[0] [0]),
        .I1(p_6_in[0]),
        .I2(\rit_register_reg[0] [0]),
        .I3(S1_AXI_ARREADY_INST_0_i_4_n_0),
        .I4(\s_axi_rdata_i[2]_i_4_n_0 ),
        .I5(S1_AXI_ARREADY_INST_0_i_3_n_0),
        .O(\s_axi_rdata_i[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00000000AA88B8B8)) 
    \s_axi_rdata_i[0]_i_3 
       (.I0(ie_register[2]),
        .I1(\s_axi_rdata_i[2]_i_5_n_0 ),
        .I2(\DataOut_reg[31] [0]),
        .I3(p_6_in[0]),
        .I4(\s_axi_rdata_i[2]_i_6_n_0 ),
        .I5(\s_axi_rdata_i[31]_i_3_n_0 ),
        .O(\s_axi_rdata_i[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[10]_i_1 
       (.I0(\s_axi_rdata_i[31]_i_3_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [10]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [10]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[11]_i_1 
       (.I0(\s_axi_rdata_i[31]_i_3_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [11]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [11]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[12]_i_1 
       (.I0(\s_axi_rdata_i[31]_i_3_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [12]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [12]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[13]_i_1 
       (.I0(\s_axi_rdata_i[31]_i_3_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [13]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [13]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[14]_i_1 
       (.I0(\s_axi_rdata_i[31]_i_3_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [14]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [14]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[15]_i_1 
       (.I0(\s_axi_rdata_i[31]_i_3_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [15]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [15]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[16]_i_1 
       (.I0(\s_axi_rdata_i[31]_i_3_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [16]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [16]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[17]_i_1 
       (.I0(\s_axi_rdata_i[31]_i_3_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [17]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [17]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[18]_i_1 
       (.I0(\s_axi_rdata_i[31]_i_3_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [18]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [18]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[19]_i_1 
       (.I0(\s_axi_rdata_i[31]_i_3_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [19]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [19]));
  LUT6 #(
    .INIT(64'hFFFEFF0E00FE000E)) 
    \s_axi_rdata_i[1]_i_1 
       (.I0(\s_axi_rdata_i[1]_i_2_n_0 ),
        .I1(\s_axi_rdata_i[1]_i_3_n_0 ),
        .I2(\s_axi_rdata_i[1]_i_4_n_0 ),
        .I3(\s_axi_rdata_i[1]_i_5_n_0 ),
        .I4(full_error_reg_0),
        .I5(full_i_reg),
        .O(\s_axi_rdata_i_reg[31]_0 [1]));
  LUT6 #(
    .INIT(64'hAAF0AAF0AACCAA00)) 
    \s_axi_rdata_i[1]_i_2 
       (.I0(\sit_register_reg[0] [1]),
        .I1(p_6_in[1]),
        .I2(\rit_register_reg[0] [1]),
        .I3(S1_AXI_ARREADY_INST_0_i_4_n_0),
        .I4(\s_axi_rdata_i[2]_i_4_n_0 ),
        .I5(S1_AXI_ARREADY_INST_0_i_3_n_0),
        .O(\s_axi_rdata_i[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00000000AA88B8B8)) 
    \s_axi_rdata_i[1]_i_3 
       (.I0(ie_register[1]),
        .I1(\s_axi_rdata_i[2]_i_5_n_0 ),
        .I2(\DataOut_reg[31] [1]),
        .I3(p_6_in[1]),
        .I4(\s_axi_rdata_i[2]_i_6_n_0 ),
        .I5(\s_axi_rdata_i[31]_i_3_n_0 ),
        .O(\s_axi_rdata_i[1]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s_axi_rdata_i[1]_i_4 
       (.I0(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I1(Bus_RNW_reg_reg_0),
        .O(\s_axi_rdata_i[1]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s_axi_rdata_i[1]_i_5 
       (.I0(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .I1(Bus_RNW_reg_reg_0),
        .O(\s_axi_rdata_i[1]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[20]_i_1 
       (.I0(\s_axi_rdata_i[31]_i_3_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [20]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [20]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[21]_i_1 
       (.I0(\s_axi_rdata_i[31]_i_3_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [21]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [21]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[22]_i_1 
       (.I0(\s_axi_rdata_i[31]_i_3_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [22]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [22]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[23]_i_1 
       (.I0(\s_axi_rdata_i[31]_i_3_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [23]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [23]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[24]_i_1 
       (.I0(\s_axi_rdata_i[31]_i_3_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [24]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [24]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[25]_i_1 
       (.I0(\s_axi_rdata_i[31]_i_3_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [25]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [25]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[26]_i_1 
       (.I0(\s_axi_rdata_i[31]_i_3_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [26]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [26]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[27]_i_1 
       (.I0(\s_axi_rdata_i[31]_i_3_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [27]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [27]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[28]_i_1 
       (.I0(\s_axi_rdata_i[31]_i_3_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [28]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [28]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[29]_i_1 
       (.I0(\s_axi_rdata_i[31]_i_3_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [29]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [29]));
  LUT6 #(
    .INIT(64'hFFEE0EEE00EE0EEE)) 
    \s_axi_rdata_i[2]_i_1 
       (.I0(\s_axi_rdata_i[2]_i_2_n_0 ),
        .I1(\s_axi_rdata_i[2]_i_3_n_0 ),
        .I2(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .I5(sit_detect_d0),
        .O(\s_axi_rdata_i_reg[31]_0 [2]));
  LUT6 #(
    .INIT(64'hAAF0AAF0AACCAA00)) 
    \s_axi_rdata_i[2]_i_2 
       (.I0(\sit_register_reg[0] [2]),
        .I1(p_6_in[2]),
        .I2(\rit_register_reg[0] [2]),
        .I3(S1_AXI_ARREADY_INST_0_i_4_n_0),
        .I4(\s_axi_rdata_i[2]_i_4_n_0 ),
        .I5(S1_AXI_ARREADY_INST_0_i_3_n_0),
        .O(\s_axi_rdata_i[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00000000AA88B8B8)) 
    \s_axi_rdata_i[2]_i_3 
       (.I0(ie_register[0]),
        .I1(\s_axi_rdata_i[2]_i_5_n_0 ),
        .I2(\DataOut_reg[31] [2]),
        .I3(p_6_in[2]),
        .I4(\s_axi_rdata_i[2]_i_6_n_0 ),
        .I5(\s_axi_rdata_i[31]_i_3_n_0 ),
        .O(\s_axi_rdata_i[2]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s_axi_rdata_i[2]_i_4 
       (.I0(\is_register_reg[2] ),
        .I1(Bus_RNW_reg_reg_0),
        .O(\s_axi_rdata_i[2]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s_axi_rdata_i[2]_i_5 
       (.I0(\s_axi_rdata_i_reg[31] ),
        .I1(Bus_RNW_reg_reg_0),
        .O(\s_axi_rdata_i[2]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s_axi_rdata_i[2]_i_6 
       (.I0(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I1(Bus_RNW_reg_reg_0),
        .O(\s_axi_rdata_i[2]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[30]_i_1 
       (.I0(\s_axi_rdata_i[31]_i_3_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [30]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [30]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[31]_i_2 
       (.I0(\s_axi_rdata_i[31]_i_3_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [31]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [31]));
  LUT4 #(
    .INIT(16'hF0E0)) 
    \s_axi_rdata_i[31]_i_3 
       (.I0(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I1(\is_register_reg[2] ),
        .I2(Bus_RNW_reg_reg_0),
        .I3(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .O(\s_axi_rdata_i[31]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFEE0EEE00EE0EEE)) 
    \s_axi_rdata_i[3]_i_1 
       (.I0(\s_axi_rdata_i[3]_i_2_n_0 ),
        .I1(\s_axi_rdata_i[3]_i_3_n_0 ),
        .I2(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .I5(rit_detect_d0),
        .O(\s_axi_rdata_i_reg[31]_0 [3]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT5 #(
    .INIT(32'hAC00A000)) 
    \s_axi_rdata_i[3]_i_2 
       (.I0(\sit_register_reg[0] [3]),
        .I1(\rit_register_reg[0] [3]),
        .I2(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .O(\s_axi_rdata_i[3]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'h00003070)) 
    \s_axi_rdata_i[3]_i_3 
       (.I0(\s_axi_rdata_i_reg[31] ),
        .I1(Bus_RNW_reg_reg_0),
        .I2(\DataOut_reg[31] [3]),
        .I3(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I4(\s_axi_rdata_i[31]_i_3_n_0 ),
        .O(\s_axi_rdata_i[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[4]_i_1 
       (.I0(\s_axi_rdata_i[31]_i_3_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [4]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [4]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[5]_i_1 
       (.I0(\s_axi_rdata_i[31]_i_3_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [5]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [5]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[6]_i_1 
       (.I0(\s_axi_rdata_i[31]_i_3_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [6]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [6]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[7]_i_1 
       (.I0(\s_axi_rdata_i[31]_i_3_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [7]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [7]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[8]_i_1 
       (.I0(\s_axi_rdata_i[31]_i_3_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [8]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [8]));
  LUT6 #(
    .INIT(64'h0000000000501050)) 
    \s_axi_rdata_i[9]_i_1 
       (.I0(\s_axi_rdata_i[31]_i_3_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\DataOut_reg[31] [9]),
        .I3(Bus_RNW_reg_reg_0),
        .I4(\s_axi_rdata_i_reg[31] ),
        .I5(S1_AXI_ARREADY_INST_0_i_2_n_0),
        .O(\s_axi_rdata_i_reg[31]_0 [9]));
  LUT5 #(
    .INIT(32'h08FF0808)) 
    s_axi_rvalid_i_i_1__0
       (.I0(S1_AXI_ARREADY),
        .I1(\state_reg[1] [0]),
        .I2(\state_reg[1] [1]),
        .I3(S1_AXI_RREADY),
        .I4(s_axi_rvalid_i_reg_0),
        .O(s_axi_rvalid_i_reg));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \sit_register[0]_i_1 
       (.I0(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I1(Bus_RNW_reg_reg_0),
        .O(\sit_register_reg[3] ));
  LUT5 #(
    .INIT(32'h77FC44FC)) 
    \state[0]_i_1 
       (.I0(state1__2),
        .I1(\state_reg[1] [0]),
        .I2(S1_AXI_ARVALID),
        .I3(\state_reg[1] [1]),
        .I4(S1_AXI_WREADY),
        .O(D[0]));
  LUT5 #(
    .INIT(32'h5FFC50FC)) 
    \state[1]_i_1 
       (.I0(state1__2),
        .I1(S1_AXI_ARVALID_0),
        .I2(\state_reg[1] [1]),
        .I3(\state_reg[1] [0]),
        .I4(S1_AXI_ARREADY),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'h40)) 
    write_fsl_error_d1_i_1
       (.I0(Bus_RNW_reg_reg_0),
        .I1(write_fsl_error_d1_reg),
        .I2(full_i_reg),
        .O(write_fsl_error));
endmodule

module design_1_mailbox_0_0_axi_lite_ipif
   (\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ,
    \GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ,
    \read_nextgray_reg[2] ,
    write_fsl_error_d1_reg,
    Bus_RNW_reg_reg,
    S0_AXI_RVALID,
    S0_AXI_BVALID,
    empty_allow,
    S0_AXI_ARREADY,
    error_detect,
    read_fsl_error,
    write_fsl_error,
    S0_AXI_WREADY,
    E,
    \sit_register_reg[3] ,
    full_error_reg,
    empty_error_reg,
    \read_nextgray_reg[2]_0 ,
    S0_AXI_RDATA,
    Rst,
    S0_AXI_ACLK,
    out,
    S0_AXI_ARVALID,
    S0_AXI_AWVALID,
    S0_AXI_WVALID,
    S0_AXI_ARADDR,
    S0_AXI_AWADDR,
    write_fsl_error_d1,
    full_i_reg,
    read_fsl_error_d1,
    rit_detect_d0,
    sit_detect_d0,
    full_error,
    empty_error,
    S0_AXI_RREADY,
    S0_AXI_BREADY,
    Q,
    p_6_in,
    \rit_register_reg[0] ,
    \DataOut_reg[31] ,
    ie_register);
  output \GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ;
  output \GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ;
  output \read_nextgray_reg[2] ;
  output write_fsl_error_d1_reg;
  output Bus_RNW_reg_reg;
  output S0_AXI_RVALID;
  output S0_AXI_BVALID;
  output empty_allow;
  output S0_AXI_ARREADY;
  output error_detect;
  output read_fsl_error;
  output write_fsl_error;
  output S0_AXI_WREADY;
  output [0:0]E;
  output [0:0]\sit_register_reg[3] ;
  output full_error_reg;
  output empty_error_reg;
  output [0:0]\read_nextgray_reg[2]_0 ;
  output [31:0]S0_AXI_RDATA;
  input Rst;
  input S0_AXI_ACLK;
  input out;
  input S0_AXI_ARVALID;
  input S0_AXI_AWVALID;
  input S0_AXI_WVALID;
  input [3:0]S0_AXI_ARADDR;
  input [3:0]S0_AXI_AWADDR;
  input write_fsl_error_d1;
  input full_i_reg;
  input read_fsl_error_d1;
  input rit_detect_d0;
  input sit_detect_d0;
  input full_error;
  input empty_error;
  input S0_AXI_RREADY;
  input S0_AXI_BREADY;
  input [3:0]Q;
  input [2:0]p_6_in;
  input [3:0]\rit_register_reg[0] ;
  input [31:0]\DataOut_reg[31] ;
  input [0:2]ie_register;

  wire Bus_RNW_reg_reg;
  wire [31:0]\DataOut_reg[31] ;
  wire [0:0]E;
  wire \GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ;
  wire [3:0]Q;
  wire Rst;
  wire S0_AXI_ACLK;
  wire [3:0]S0_AXI_ARADDR;
  wire S0_AXI_ARREADY;
  wire S0_AXI_ARVALID;
  wire [3:0]S0_AXI_AWADDR;
  wire S0_AXI_AWVALID;
  wire S0_AXI_BREADY;
  wire S0_AXI_BVALID;
  wire [31:0]S0_AXI_RDATA;
  wire S0_AXI_RREADY;
  wire S0_AXI_RVALID;
  wire S0_AXI_WREADY;
  wire S0_AXI_WVALID;
  wire empty_allow;
  wire empty_error;
  wire empty_error_reg;
  wire error_detect;
  wire full_error;
  wire full_error_reg;
  wire full_i_reg;
  wire [0:2]ie_register;
  wire out;
  wire [2:0]p_6_in;
  wire read_fsl_error;
  wire read_fsl_error_d1;
  wire \read_nextgray_reg[2] ;
  wire [0:0]\read_nextgray_reg[2]_0 ;
  wire rit_detect_d0;
  wire [3:0]\rit_register_reg[0] ;
  wire sit_detect_d0;
  wire [0:0]\sit_register_reg[3] ;
  wire write_fsl_error;
  wire write_fsl_error_d1;
  wire write_fsl_error_d1_reg;

  design_1_mailbox_0_0_slave_attachment I_SLAVE_ATTACHMENT
       (.Bus_RNW_reg_reg(Bus_RNW_reg_reg),
        .\DataOut_reg[31] (\DataOut_reg[31] ),
        .E(E),
        .Q(Q),
        .Rst(Rst),
        .S0_AXI_ACLK(S0_AXI_ACLK),
        .S0_AXI_ARADDR(S0_AXI_ARADDR),
        .S0_AXI_ARREADY(S0_AXI_ARREADY),
        .S0_AXI_ARVALID(S0_AXI_ARVALID),
        .S0_AXI_AWADDR(S0_AXI_AWADDR),
        .S0_AXI_AWVALID(S0_AXI_AWVALID),
        .S0_AXI_BREADY(S0_AXI_BREADY),
        .S0_AXI_BVALID(S0_AXI_BVALID),
        .S0_AXI_RDATA(S0_AXI_RDATA),
        .S0_AXI_RREADY(S0_AXI_RREADY),
        .S0_AXI_RVALID(S0_AXI_RVALID),
        .S0_AXI_WREADY(S0_AXI_WREADY),
        .S0_AXI_WVALID(S0_AXI_WVALID),
        .empty_allow(empty_allow),
        .empty_error(empty_error),
        .empty_error_reg(empty_error_reg),
        .error_detect(error_detect),
        .full_error(full_error),
        .full_error_reg(full_error_reg),
        .full_i_reg(full_i_reg),
        .ie_register(ie_register),
        .\is_register_reg[2] (\GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .out(out),
        .p_6_in(p_6_in),
        .read_fsl_error(read_fsl_error),
        .read_fsl_error_d1(read_fsl_error_d1),
        .\read_nextgray_reg[2] (\read_nextgray_reg[2] ),
        .\read_nextgray_reg[2]_0 (\read_nextgray_reg[2]_0 ),
        .rit_detect_d0(rit_detect_d0),
        .\rit_register_reg[0] (\rit_register_reg[0] ),
        .\s_axi_rdata_i_reg[31]_0 (\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .sit_detect_d0(sit_detect_d0),
        .\sit_register_reg[3] (\sit_register_reg[3] ),
        .write_fsl_error(write_fsl_error),
        .write_fsl_error_d1(write_fsl_error_d1),
        .write_fsl_error_d1_reg(write_fsl_error_d1_reg));
endmodule

(* ORIG_REF_NAME = "axi_lite_ipif" *) 
module design_1_mailbox_0_0_axi_lite_ipif__parameterized1
   (\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ,
    \GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ,
    \read_nextgray_reg[2] ,
    write_fsl_error_d1_reg,
    Bus_RNW_reg_reg,
    S1_AXI_RVALID,
    S1_AXI_BVALID,
    empty_allow,
    S1_AXI_ARREADY,
    error_detect,
    read_fsl_error,
    write_fsl_error,
    S1_AXI_WREADY,
    E,
    \sit_register_reg[3] ,
    full_error_reg,
    empty_error_reg,
    \read_nextgray_reg[2]_0 ,
    S1_AXI_RDATA,
    \Rst_Async.SYS_Rst_If1_reg ,
    S1_AXI_ACLK,
    out,
    S1_AXI_ARVALID,
    S1_AXI_AWVALID,
    S1_AXI_WVALID,
    S1_AXI_ARADDR,
    S1_AXI_AWADDR,
    write_fsl_error_d1,
    full_i_reg,
    read_fsl_error_d1,
    rit_detect_d0,
    sit_detect_d0,
    full_error_reg_0,
    empty_error_reg_0,
    S1_AXI_RREADY,
    S1_AXI_BREADY,
    Q,
    p_6_in,
    \rit_register_reg[0] ,
    \DataOut_reg[31] ,
    ie_register);
  output \GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ;
  output \GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ;
  output \read_nextgray_reg[2] ;
  output write_fsl_error_d1_reg;
  output Bus_RNW_reg_reg;
  output S1_AXI_RVALID;
  output S1_AXI_BVALID;
  output empty_allow;
  output S1_AXI_ARREADY;
  output error_detect;
  output read_fsl_error;
  output write_fsl_error;
  output S1_AXI_WREADY;
  output [0:0]E;
  output [0:0]\sit_register_reg[3] ;
  output full_error_reg;
  output empty_error_reg;
  output [0:0]\read_nextgray_reg[2]_0 ;
  output [31:0]S1_AXI_RDATA;
  input \Rst_Async.SYS_Rst_If1_reg ;
  input S1_AXI_ACLK;
  input out;
  input S1_AXI_ARVALID;
  input S1_AXI_AWVALID;
  input S1_AXI_WVALID;
  input [3:0]S1_AXI_ARADDR;
  input [3:0]S1_AXI_AWADDR;
  input write_fsl_error_d1;
  input full_i_reg;
  input read_fsl_error_d1;
  input rit_detect_d0;
  input sit_detect_d0;
  input full_error_reg_0;
  input empty_error_reg_0;
  input S1_AXI_RREADY;
  input S1_AXI_BREADY;
  input [3:0]Q;
  input [2:0]p_6_in;
  input [3:0]\rit_register_reg[0] ;
  input [31:0]\DataOut_reg[31] ;
  input [0:2]ie_register;

  wire Bus_RNW_reg_reg;
  wire [31:0]\DataOut_reg[31] ;
  wire [0:0]E;
  wire \GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ;
  wire [3:0]Q;
  wire \Rst_Async.SYS_Rst_If1_reg ;
  wire S1_AXI_ACLK;
  wire [3:0]S1_AXI_ARADDR;
  wire S1_AXI_ARREADY;
  wire S1_AXI_ARVALID;
  wire [3:0]S1_AXI_AWADDR;
  wire S1_AXI_AWVALID;
  wire S1_AXI_BREADY;
  wire S1_AXI_BVALID;
  wire [31:0]S1_AXI_RDATA;
  wire S1_AXI_RREADY;
  wire S1_AXI_RVALID;
  wire S1_AXI_WREADY;
  wire S1_AXI_WVALID;
  wire empty_allow;
  wire empty_error_reg;
  wire empty_error_reg_0;
  wire error_detect;
  wire full_error_reg;
  wire full_error_reg_0;
  wire full_i_reg;
  wire [0:2]ie_register;
  wire out;
  wire [2:0]p_6_in;
  wire read_fsl_error;
  wire read_fsl_error_d1;
  wire \read_nextgray_reg[2] ;
  wire [0:0]\read_nextgray_reg[2]_0 ;
  wire rit_detect_d0;
  wire [3:0]\rit_register_reg[0] ;
  wire sit_detect_d0;
  wire [0:0]\sit_register_reg[3] ;
  wire write_fsl_error;
  wire write_fsl_error_d1;
  wire write_fsl_error_d1_reg;

  design_1_mailbox_0_0_slave_attachment__parameterized0 I_SLAVE_ATTACHMENT
       (.Bus_RNW_reg_reg(Bus_RNW_reg_reg),
        .\DataOut_reg[31] (\DataOut_reg[31] ),
        .E(E),
        .Q(Q),
        .\Rst_Async.SYS_Rst_If1_reg (\Rst_Async.SYS_Rst_If1_reg ),
        .S1_AXI_ACLK(S1_AXI_ACLK),
        .S1_AXI_ARADDR(S1_AXI_ARADDR),
        .S1_AXI_ARREADY(S1_AXI_ARREADY),
        .S1_AXI_ARVALID(S1_AXI_ARVALID),
        .S1_AXI_AWADDR(S1_AXI_AWADDR),
        .S1_AXI_AWVALID(S1_AXI_AWVALID),
        .S1_AXI_BREADY(S1_AXI_BREADY),
        .S1_AXI_BVALID(S1_AXI_BVALID),
        .S1_AXI_RDATA(S1_AXI_RDATA),
        .S1_AXI_RREADY(S1_AXI_RREADY),
        .S1_AXI_RVALID(S1_AXI_RVALID),
        .S1_AXI_WREADY(S1_AXI_WREADY),
        .S1_AXI_WVALID(S1_AXI_WVALID),
        .empty_allow(empty_allow),
        .empty_error_reg(empty_error_reg),
        .empty_error_reg_0(empty_error_reg_0),
        .error_detect(error_detect),
        .full_error_reg(full_error_reg),
        .full_error_reg_0(full_error_reg_0),
        .full_i_reg(full_i_reg),
        .ie_register(ie_register),
        .\is_register_reg[2] (\GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .out(out),
        .p_6_in(p_6_in),
        .read_fsl_error(read_fsl_error),
        .read_fsl_error_d1(read_fsl_error_d1),
        .\read_nextgray_reg[2] (\read_nextgray_reg[2] ),
        .\read_nextgray_reg[2]_0 (\read_nextgray_reg[2]_0 ),
        .rit_detect_d0(rit_detect_d0),
        .\rit_register_reg[0] (\rit_register_reg[0] ),
        .\s_axi_rdata_i_reg[31]_0 (\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .sit_detect_d0(sit_detect_d0),
        .\sit_register_reg[3] (\sit_register_reg[3] ),
        .write_fsl_error(write_fsl_error),
        .write_fsl_error_d1(write_fsl_error_d1),
        .write_fsl_error_d1_reg(write_fsl_error_d1_reg));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_mailbox_0_0,mailbox,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "mailbox,Vivado 2016.4" *) 
(* NotValidForBitStream *)
module design_1_mailbox_0_0
   (S0_AXI_ACLK,
    S0_AXI_ARESETN,
    S0_AXI_AWADDR,
    S0_AXI_AWVALID,
    S0_AXI_AWREADY,
    S0_AXI_WDATA,
    S0_AXI_WSTRB,
    S0_AXI_WVALID,
    S0_AXI_WREADY,
    S0_AXI_BRESP,
    S0_AXI_BVALID,
    S0_AXI_BREADY,
    S0_AXI_ARADDR,
    S0_AXI_ARVALID,
    S0_AXI_ARREADY,
    S0_AXI_RDATA,
    S0_AXI_RRESP,
    S0_AXI_RVALID,
    S0_AXI_RREADY,
    S1_AXI_ACLK,
    S1_AXI_ARESETN,
    S1_AXI_AWADDR,
    S1_AXI_AWVALID,
    S1_AXI_AWREADY,
    S1_AXI_WDATA,
    S1_AXI_WSTRB,
    S1_AXI_WVALID,
    S1_AXI_WREADY,
    S1_AXI_BRESP,
    S1_AXI_BVALID,
    S1_AXI_BREADY,
    S1_AXI_ARADDR,
    S1_AXI_ARVALID,
    S1_AXI_ARREADY,
    S1_AXI_RDATA,
    S1_AXI_RRESP,
    S1_AXI_RVALID,
    S1_AXI_RREADY,
    Interrupt_0,
    Interrupt_1);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 CLK.S0_AXI_ACLK CLK" *) input S0_AXI_ACLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 RST.S0_AXI_ARESETN RST" *) input S0_AXI_ARESETN;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI AWADDR" *) input [31:0]S0_AXI_AWADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI AWVALID" *) input S0_AXI_AWVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI AWREADY" *) output S0_AXI_AWREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI WDATA" *) input [31:0]S0_AXI_WDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI WSTRB" *) input [3:0]S0_AXI_WSTRB;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI WVALID" *) input S0_AXI_WVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI WREADY" *) output S0_AXI_WREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI BRESP" *) output [1:0]S0_AXI_BRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI BVALID" *) output S0_AXI_BVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI BREADY" *) input S0_AXI_BREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI ARADDR" *) input [31:0]S0_AXI_ARADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI ARVALID" *) input S0_AXI_ARVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI ARREADY" *) output S0_AXI_ARREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI RDATA" *) output [31:0]S0_AXI_RDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI RRESP" *) output [1:0]S0_AXI_RRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI RVALID" *) output S0_AXI_RVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S0_AXI RREADY" *) input S0_AXI_RREADY;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 CLK.S1_AXI_ACLK CLK" *) input S1_AXI_ACLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 RST.S1_AXI_ARESETN RST" *) input S1_AXI_ARESETN;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI AWADDR" *) input [31:0]S1_AXI_AWADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI AWVALID" *) input S1_AXI_AWVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI AWREADY" *) output S1_AXI_AWREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI WDATA" *) input [31:0]S1_AXI_WDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI WSTRB" *) input [3:0]S1_AXI_WSTRB;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI WVALID" *) input S1_AXI_WVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI WREADY" *) output S1_AXI_WREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI BRESP" *) output [1:0]S1_AXI_BRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI BVALID" *) output S1_AXI_BVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI BREADY" *) input S1_AXI_BREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI ARADDR" *) input [31:0]S1_AXI_ARADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI ARVALID" *) input S1_AXI_ARVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI ARREADY" *) output S1_AXI_ARREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI RDATA" *) output [31:0]S1_AXI_RDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI RRESP" *) output [1:0]S1_AXI_RRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI RVALID" *) output S1_AXI_RVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S1_AXI RREADY" *) input S1_AXI_RREADY;
  (* x_interface_info = "xilinx.com:signal:interrupt:1.0 INTERRUPT.Interrupt_0 INTERRUPT" *) output Interrupt_0;
  (* x_interface_info = "xilinx.com:signal:interrupt:1.0 INTERRUPT.Interrupt_1 INTERRUPT" *) output Interrupt_1;

  wire Interrupt_0;
  wire Interrupt_1;
  wire S0_AXI_ACLK;
  wire [31:0]S0_AXI_ARADDR;
  wire S0_AXI_ARESETN;
  wire S0_AXI_ARREADY;
  wire S0_AXI_ARVALID;
  wire [31:0]S0_AXI_AWADDR;
  wire S0_AXI_AWREADY;
  wire S0_AXI_AWVALID;
  wire S0_AXI_BREADY;
  wire [1:0]S0_AXI_BRESP;
  wire S0_AXI_BVALID;
  wire [31:0]S0_AXI_RDATA;
  wire S0_AXI_RREADY;
  wire [1:0]S0_AXI_RRESP;
  wire S0_AXI_RVALID;
  wire [31:0]S0_AXI_WDATA;
  wire S0_AXI_WREADY;
  wire [3:0]S0_AXI_WSTRB;
  wire S0_AXI_WVALID;
  wire S1_AXI_ACLK;
  wire [31:0]S1_AXI_ARADDR;
  wire S1_AXI_ARESETN;
  wire S1_AXI_ARREADY;
  wire S1_AXI_ARVALID;
  wire [31:0]S1_AXI_AWADDR;
  wire S1_AXI_AWREADY;
  wire S1_AXI_AWVALID;
  wire S1_AXI_BREADY;
  wire [1:0]S1_AXI_BRESP;
  wire S1_AXI_BVALID;
  wire [31:0]S1_AXI_RDATA;
  wire S1_AXI_RREADY;
  wire [1:0]S1_AXI_RRESP;
  wire S1_AXI_RVALID;
  wire [31:0]S1_AXI_WDATA;
  wire S1_AXI_WREADY;
  wire [3:0]S1_AXI_WSTRB;
  wire S1_AXI_WVALID;
  wire NLW_U0_M0_AXIS_TLAST_UNCONNECTED;
  wire NLW_U0_M0_AXIS_TVALID_UNCONNECTED;
  wire NLW_U0_M1_AXIS_TLAST_UNCONNECTED;
  wire NLW_U0_M1_AXIS_TVALID_UNCONNECTED;
  wire NLW_U0_S0_AXIS_TREADY_UNCONNECTED;
  wire NLW_U0_S1_AXIS_TREADY_UNCONNECTED;
  wire [31:0]NLW_U0_M0_AXIS_TDATA_UNCONNECTED;
  wire [31:0]NLW_U0_M1_AXIS_TDATA_UNCONNECTED;

  (* C_ASYNC_CLKS = "1" *) 
  (* C_ENABLE_BUS_ERROR = "0" *) 
  (* C_EXT_RESET_HIGH = "1" *) 
  (* C_FAMILY = "zynq" *) 
  (* C_IMPL_STYLE = "0" *) 
  (* C_INTERCONNECT_PORT_0 = "2" *) 
  (* C_INTERCONNECT_PORT_1 = "2" *) 
  (* C_M0_AXIS_DATA_WIDTH = "32" *) 
  (* C_M1_AXIS_DATA_WIDTH = "32" *) 
  (* C_MAILBOX_DEPTH = "16" *) 
  (* C_NUM_SYNC_FF = "2" *) 
  (* C_S0_AXIS_DATA_WIDTH = "32" *) 
  (* C_S0_AXI_ADDR_WIDTH = "32" *) 
  (* C_S0_AXI_BASEADDR = "1132462080" *) 
  (* C_S0_AXI_DATA_WIDTH = "32" *) 
  (* C_S0_AXI_HIGHADDR = "1132527615" *) 
  (* C_S1_AXIS_DATA_WIDTH = "32" *) 
  (* C_S1_AXI_ADDR_WIDTH = "32" *) 
  (* C_S1_AXI_BASEADDR = "1130364928" *) 
  (* C_S1_AXI_DATA_WIDTH = "32" *) 
  (* C_S1_AXI_HIGHADDR = "1130430463" *) 
  design_1_mailbox_0_0_mailbox U0
       (.Interrupt_0(Interrupt_0),
        .Interrupt_1(Interrupt_1),
        .M0_AXIS_ACLK(1'b0),
        .M0_AXIS_TDATA(NLW_U0_M0_AXIS_TDATA_UNCONNECTED[31:0]),
        .M0_AXIS_TLAST(NLW_U0_M0_AXIS_TLAST_UNCONNECTED),
        .M0_AXIS_TREADY(1'b0),
        .M0_AXIS_TVALID(NLW_U0_M0_AXIS_TVALID_UNCONNECTED),
        .M1_AXIS_ACLK(1'b0),
        .M1_AXIS_TDATA(NLW_U0_M1_AXIS_TDATA_UNCONNECTED[31:0]),
        .M1_AXIS_TLAST(NLW_U0_M1_AXIS_TLAST_UNCONNECTED),
        .M1_AXIS_TREADY(1'b0),
        .M1_AXIS_TVALID(NLW_U0_M1_AXIS_TVALID_UNCONNECTED),
        .S0_AXIS_ACLK(1'b0),
        .S0_AXIS_TDATA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .S0_AXIS_TLAST(1'b0),
        .S0_AXIS_TREADY(NLW_U0_S0_AXIS_TREADY_UNCONNECTED),
        .S0_AXIS_TVALID(1'b0),
        .S0_AXI_ACLK(S0_AXI_ACLK),
        .S0_AXI_ARADDR(S0_AXI_ARADDR),
        .S0_AXI_ARESETN(S0_AXI_ARESETN),
        .S0_AXI_ARREADY(S0_AXI_ARREADY),
        .S0_AXI_ARVALID(S0_AXI_ARVALID),
        .S0_AXI_AWADDR(S0_AXI_AWADDR),
        .S0_AXI_AWREADY(S0_AXI_AWREADY),
        .S0_AXI_AWVALID(S0_AXI_AWVALID),
        .S0_AXI_BREADY(S0_AXI_BREADY),
        .S0_AXI_BRESP(S0_AXI_BRESP),
        .S0_AXI_BVALID(S0_AXI_BVALID),
        .S0_AXI_RDATA(S0_AXI_RDATA),
        .S0_AXI_RREADY(S0_AXI_RREADY),
        .S0_AXI_RRESP(S0_AXI_RRESP),
        .S0_AXI_RVALID(S0_AXI_RVALID),
        .S0_AXI_WDATA(S0_AXI_WDATA),
        .S0_AXI_WREADY(S0_AXI_WREADY),
        .S0_AXI_WSTRB(S0_AXI_WSTRB),
        .S0_AXI_WVALID(S0_AXI_WVALID),
        .S1_AXIS_ACLK(1'b0),
        .S1_AXIS_TDATA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .S1_AXIS_TLAST(1'b0),
        .S1_AXIS_TREADY(NLW_U0_S1_AXIS_TREADY_UNCONNECTED),
        .S1_AXIS_TVALID(1'b0),
        .S1_AXI_ACLK(S1_AXI_ACLK),
        .S1_AXI_ARADDR(S1_AXI_ARADDR),
        .S1_AXI_ARESETN(S1_AXI_ARESETN),
        .S1_AXI_ARREADY(S1_AXI_ARREADY),
        .S1_AXI_ARVALID(S1_AXI_ARVALID),
        .S1_AXI_AWADDR(S1_AXI_AWADDR),
        .S1_AXI_AWREADY(S1_AXI_AWREADY),
        .S1_AXI_AWVALID(S1_AXI_AWVALID),
        .S1_AXI_BREADY(S1_AXI_BREADY),
        .S1_AXI_BRESP(S1_AXI_BRESP),
        .S1_AXI_BVALID(S1_AXI_BVALID),
        .S1_AXI_RDATA(S1_AXI_RDATA),
        .S1_AXI_RREADY(S1_AXI_RREADY),
        .S1_AXI_RRESP(S1_AXI_RRESP),
        .S1_AXI_RVALID(S1_AXI_RVALID),
        .S1_AXI_WDATA(S1_AXI_WDATA),
        .S1_AXI_WREADY(S1_AXI_WREADY),
        .S1_AXI_WSTRB(S1_AXI_WSTRB),
        .S1_AXI_WVALID(S1_AXI_WVALID),
        .SYS_Rst(1'b0));
endmodule

module design_1_mailbox_0_0_fsl_v20
   (rd_rst_meta_inst,
    out,
    \write_nextgray_reg[2] ,
    rit_detect_d0,
    sit_detect_d0,
    \s_axi_rdata_i_reg[31] ,
    S1_AXI_ACLK,
    S0_AXI_ACLK,
    empty_allow,
    E,
    Bus_RNW_reg,
    \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ,
    \rit_register_reg[0] ,
    Q,
    \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ,
    Bus_RNW_reg_0,
    S0_AXI_ARESETN,
    SYS_Rst,
    S1_AXI_ARESETN,
    S0_AXI_WDATA);
  output rd_rst_meta_inst;
  output out;
  output \write_nextgray_reg[2] ;
  output rit_detect_d0;
  output sit_detect_d0;
  output [31:0]\s_axi_rdata_i_reg[31] ;
  input S1_AXI_ACLK;
  input S0_AXI_ACLK;
  input empty_allow;
  input [0:0]E;
  input Bus_RNW_reg;
  input \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  input [3:0]\rit_register_reg[0] ;
  input [3:0]Q;
  input \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ;
  input Bus_RNW_reg_0;
  input S0_AXI_ARESETN;
  input SYS_Rst;
  input S1_AXI_ARESETN;
  input [31:0]S0_AXI_WDATA;

  wire Bus_RNW_reg;
  wire Bus_RNW_reg_0;
  wire [0:0]E;
  wire \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  wire [3:0]Q;
  wire S0_AXI_ACLK;
  wire S0_AXI_ARESETN;
  wire [31:0]S0_AXI_WDATA;
  wire S1_AXI_ACLK;
  wire S1_AXI_ARESETN;
  wire SYS_Rst;
  wire empty_allow;
  wire out;
  wire rd_rst_meta_inst;
  wire rit_detect_d0;
  wire [3:0]\rit_register_reg[0] ;
  wire [31:0]\s_axi_rdata_i_reg[31] ;
  wire sit_detect_d0;
  wire \write_nextgray_reg[2] ;

  design_1_mailbox_0_0_Async_FIFO_1 \Using_FIFO.Async_FIFO_Gen.Async_FIFO_I1 
       (.Bus_RNW_reg(Bus_RNW_reg),
        .Bus_RNW_reg_0(Bus_RNW_reg_0),
        .E(E),
        .\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg (\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg (\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .Q(Q),
        .S0_AXI_ACLK(S0_AXI_ACLK),
        .S0_AXI_ARESETN(S0_AXI_ARESETN),
        .S0_AXI_WDATA(S0_AXI_WDATA),
        .S1_AXI_ACLK(S1_AXI_ACLK),
        .S1_AXI_ARESETN(S1_AXI_ARESETN),
        .SYS_Rst(SYS_Rst),
        .empty_allow(empty_allow),
        .out(out),
        .rd_rst_meta_inst_0(rd_rst_meta_inst),
        .rit_detect_d0(rit_detect_d0),
        .\rit_register_reg[0] (\rit_register_reg[0] ),
        .\s_axi_rdata_i_reg[31] (\s_axi_rdata_i_reg[31] ),
        .sit_detect_d0(sit_detect_d0),
        .\write_nextgray_reg[2]_0 (\write_nextgray_reg[2] ));
endmodule

(* ORIG_REF_NAME = "fsl_v20" *) 
module design_1_mailbox_0_0_fsl_v20_0
   (out,
    \write_addr_reg[0] ,
    sit_detect_d0,
    rit_detect_d0,
    DataOut,
    S0_AXI_ACLK,
    S1_AXI_ARESETN,
    S1_AXI_ACLK,
    empty_allow,
    E,
    Q,
    Bus_RNW_reg,
    \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ,
    \rit_register_reg[0] ,
    \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ,
    Bus_RNW_reg_0,
    S1_AXI_WDATA);
  output out;
  output \write_addr_reg[0] ;
  output sit_detect_d0;
  output rit_detect_d0;
  output [31:0]DataOut;
  input S0_AXI_ACLK;
  input S1_AXI_ARESETN;
  input S1_AXI_ACLK;
  input empty_allow;
  input [0:0]E;
  input [3:0]Q;
  input Bus_RNW_reg;
  input \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  input [3:0]\rit_register_reg[0] ;
  input \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ;
  input Bus_RNW_reg_0;
  input [31:0]S1_AXI_WDATA;

  wire Bus_RNW_reg;
  wire Bus_RNW_reg_0;
  wire [31:0]DataOut;
  wire [0:0]E;
  wire \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  wire [3:0]Q;
  wire S0_AXI_ACLK;
  wire S1_AXI_ACLK;
  wire S1_AXI_ARESETN;
  wire [31:0]S1_AXI_WDATA;
  wire empty_allow;
  wire out;
  wire rit_detect_d0;
  wire [3:0]\rit_register_reg[0] ;
  wire sit_detect_d0;
  wire \write_addr_reg[0] ;

  design_1_mailbox_0_0_Async_FIFO \Using_FIFO.Async_FIFO_Gen.Async_FIFO_I1 
       (.Bus_RNW_reg(Bus_RNW_reg),
        .Bus_RNW_reg_0(Bus_RNW_reg_0),
        .DataOut(DataOut),
        .E(E),
        .\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg (\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg (\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .Q(Q),
        .S0_AXI_ACLK(S0_AXI_ACLK),
        .S1_AXI_ACLK(S1_AXI_ACLK),
        .S1_AXI_ARESETN(S1_AXI_ARESETN),
        .S1_AXI_WDATA(S1_AXI_WDATA),
        .empty_allow(empty_allow),
        .out(out),
        .rit_detect_d0(rit_detect_d0),
        .\rit_register_reg[0] (\rit_register_reg[0] ),
        .sit_detect_d0(sit_detect_d0),
        .\write_addr_reg[0]_0 (\write_addr_reg[0] ));
endmodule

module design_1_mailbox_0_0_if_decode
   (\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ,
    \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ,
    Bus_RNW_reg,
    S0_AXI_RVALID,
    S0_AXI_BVALID,
    empty_allow,
    S0_AXI_ARREADY,
    S0_AXI_WREADY,
    Interrupt_0,
    Q,
    rit_detect_d1_reg_0,
    E,
    S0_AXI_RDATA,
    Rst,
    S0_AXI_ACLK,
    sit_detect_d0,
    rit_detect_d0,
    out,
    S0_AXI_ARVALID,
    S0_AXI_AWVALID,
    S0_AXI_WVALID,
    S0_AXI_ARADDR,
    S0_AXI_AWADDR,
    full_i_reg,
    S0_AXI_RREADY,
    S0_AXI_BREADY,
    \DataOut_reg[31] ,
    S0_AXI_WDATA);
  output \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  output \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ;
  output Bus_RNW_reg;
  output S0_AXI_RVALID;
  output S0_AXI_BVALID;
  output empty_allow;
  output S0_AXI_ARREADY;
  output S0_AXI_WREADY;
  output Interrupt_0;
  output [3:0]Q;
  output [3:0]rit_detect_d1_reg_0;
  output [0:0]E;
  output [31:0]S0_AXI_RDATA;
  input Rst;
  input S0_AXI_ACLK;
  input sit_detect_d0;
  input rit_detect_d0;
  input out;
  input S0_AXI_ARVALID;
  input S0_AXI_AWVALID;
  input S0_AXI_WVALID;
  input [3:0]S0_AXI_ARADDR;
  input [3:0]S0_AXI_AWADDR;
  input full_i_reg;
  input S0_AXI_RREADY;
  input S0_AXI_BREADY;
  input [31:0]\DataOut_reg[31] ;
  input [3:0]S0_AXI_WDATA;

  wire Bus_RNW_reg;
  wire [31:0]\DataOut_reg[31] ;
  wire [0:0]E;
  wire \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  wire \I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ;
  wire \I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ;
  wire Interrupt_0;
  wire [3:0]Q;
  wire Rst;
  wire S0_AXI_ACLK;
  wire [3:0]S0_AXI_ARADDR;
  wire S0_AXI_ARREADY;
  wire S0_AXI_ARVALID;
  wire [3:0]S0_AXI_AWADDR;
  wire S0_AXI_AWVALID;
  wire S0_AXI_BREADY;
  wire S0_AXI_BVALID;
  wire [31:0]S0_AXI_RDATA;
  wire S0_AXI_RREADY;
  wire S0_AXI_RVALID;
  wire [3:0]S0_AXI_WDATA;
  wire S0_AXI_WREADY;
  wire S0_AXI_WVALID;
  wire \Using_AXI.AXI4_If_n_15 ;
  wire \Using_AXI.AXI4_If_n_16 ;
  wire empty_allow;
  wire empty_error;
  wire error_detect;
  wire full_error;
  wire full_i_reg;
  wire [0:2]ie_register;
  wire \ie_register[0]_i_1_n_0 ;
  wire \ie_register[1]_i_1_n_0 ;
  wire \ie_register[2]_i_1_n_0 ;
  wire \is_register[0]_i_1_n_0 ;
  wire \is_register[1]_i_1_n_0 ;
  wire \is_register[2]_i_1_n_0 ;
  wire out;
  wire p_0_in1_in;
  wire p_1_in2_in;
  wire [2:0]p_6_in;
  wire read_fsl_error;
  wire read_fsl_error_d1;
  wire rit_detect_d0;
  wire rit_detect_d1;
  wire [3:0]rit_detect_d1_reg_0;
  wire sit_detect_d0;
  wire sit_detect_d1;
  wire write_fsl_error;
  wire write_fsl_error_d1;

  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    Interrupt_0_INST_0
       (.I0(ie_register[1]),
        .I1(p_6_in[1]),
        .I2(p_6_in[2]),
        .I3(ie_register[0]),
        .I4(p_6_in[0]),
        .I5(ie_register[2]),
        .O(Interrupt_0));
  design_1_mailbox_0_0_axi_lite_ipif \Using_AXI.AXI4_If 
       (.Bus_RNW_reg_reg(Bus_RNW_reg),
        .\DataOut_reg[31] (\DataOut_reg[31] ),
        .E(p_1_in2_in),
        .\GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg (\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg (\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .Q(Q),
        .Rst(Rst),
        .S0_AXI_ACLK(S0_AXI_ACLK),
        .S0_AXI_ARADDR(S0_AXI_ARADDR),
        .S0_AXI_ARREADY(S0_AXI_ARREADY),
        .S0_AXI_ARVALID(S0_AXI_ARVALID),
        .S0_AXI_AWADDR(S0_AXI_AWADDR),
        .S0_AXI_AWVALID(S0_AXI_AWVALID),
        .S0_AXI_BREADY(S0_AXI_BREADY),
        .S0_AXI_BVALID(S0_AXI_BVALID),
        .S0_AXI_RDATA(S0_AXI_RDATA),
        .S0_AXI_RREADY(S0_AXI_RREADY),
        .S0_AXI_RVALID(S0_AXI_RVALID),
        .S0_AXI_WREADY(S0_AXI_WREADY),
        .S0_AXI_WVALID(S0_AXI_WVALID),
        .empty_allow(empty_allow),
        .empty_error(empty_error),
        .empty_error_reg(\Using_AXI.AXI4_If_n_16 ),
        .error_detect(error_detect),
        .full_error(full_error),
        .full_error_reg(\Using_AXI.AXI4_If_n_15 ),
        .full_i_reg(full_i_reg),
        .ie_register(ie_register),
        .out(out),
        .p_6_in(p_6_in),
        .read_fsl_error(read_fsl_error),
        .read_fsl_error_d1(read_fsl_error_d1),
        .\read_nextgray_reg[2] (\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .\read_nextgray_reg[2]_0 (E),
        .rit_detect_d0(rit_detect_d0),
        .\rit_register_reg[0] (rit_detect_d1_reg_0),
        .sit_detect_d0(sit_detect_d0),
        .\sit_register_reg[3] (p_0_in1_in),
        .write_fsl_error(write_fsl_error),
        .write_fsl_error_d1(write_fsl_error_d1),
        .write_fsl_error_d1_reg(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ));
  FDRE empty_error_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\Using_AXI.AXI4_If_n_16 ),
        .Q(empty_error),
        .R(1'b0));
  FDRE full_error_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\Using_AXI.AXI4_If_n_15 ),
        .Q(full_error),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hFB08)) 
    \ie_register[0]_i_1 
       (.I0(S0_AXI_WDATA[2]),
        .I1(\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(ie_register[0]),
        .O(\ie_register[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \ie_register[1]_i_1 
       (.I0(S0_AXI_WDATA[1]),
        .I1(\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(ie_register[1]),
        .O(\ie_register[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \ie_register[2]_i_1 
       (.I0(S0_AXI_WDATA[0]),
        .I1(\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(ie_register[2]),
        .O(\ie_register[2]_i_1_n_0 ));
  FDRE \ie_register_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\ie_register[0]_i_1_n_0 ),
        .Q(ie_register[0]),
        .R(Rst));
  FDRE \ie_register_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\ie_register[1]_i_1_n_0 ),
        .Q(ie_register[1]),
        .R(Rst));
  FDRE \ie_register_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\ie_register[2]_i_1_n_0 ),
        .Q(ie_register[2]),
        .R(Rst));
  LUT5 #(
    .INIT(32'hFFBFAAAA)) 
    \is_register[0]_i_1 
       (.I0(error_detect),
        .I1(S0_AXI_WDATA[2]),
        .I2(\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .I3(Bus_RNW_reg),
        .I4(p_6_in[2]),
        .O(\is_register[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF4FFF44444444)) 
    \is_register[1]_i_1 
       (.I0(rit_detect_d1),
        .I1(rit_detect_d0),
        .I2(S0_AXI_WDATA[1]),
        .I3(\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .I4(Bus_RNW_reg),
        .I5(p_6_in[1]),
        .O(\is_register[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF4FFF44444444)) 
    \is_register[2]_i_1 
       (.I0(sit_detect_d1),
        .I1(sit_detect_d0),
        .I2(S0_AXI_WDATA[0]),
        .I3(\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .I4(Bus_RNW_reg),
        .I5(p_6_in[0]),
        .O(\is_register[2]_i_1_n_0 ));
  FDRE \is_register_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\is_register[0]_i_1_n_0 ),
        .Q(p_6_in[2]),
        .R(Rst));
  FDRE \is_register_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\is_register[1]_i_1_n_0 ),
        .Q(p_6_in[1]),
        .R(Rst));
  FDRE \is_register_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(\is_register[2]_i_1_n_0 ),
        .Q(p_6_in[0]),
        .R(Rst));
  FDRE read_fsl_error_d1_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(read_fsl_error),
        .Q(read_fsl_error_d1),
        .R(Rst));
  FDRE rit_detect_d1_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(rit_detect_d0),
        .Q(rit_detect_d1),
        .R(1'b0));
  FDRE \rit_register_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(p_1_in2_in),
        .D(S0_AXI_WDATA[3]),
        .Q(rit_detect_d1_reg_0[3]),
        .R(Rst));
  FDRE \rit_register_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(p_1_in2_in),
        .D(S0_AXI_WDATA[2]),
        .Q(rit_detect_d1_reg_0[2]),
        .R(Rst));
  FDRE \rit_register_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(p_1_in2_in),
        .D(S0_AXI_WDATA[1]),
        .Q(rit_detect_d1_reg_0[1]),
        .R(Rst));
  FDRE \rit_register_reg[3] 
       (.C(S0_AXI_ACLK),
        .CE(p_1_in2_in),
        .D(S0_AXI_WDATA[0]),
        .Q(rit_detect_d1_reg_0[0]),
        .R(Rst));
  FDRE sit_detect_d1_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(sit_detect_d0),
        .Q(sit_detect_d1),
        .R(1'b0));
  FDRE \sit_register_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(p_0_in1_in),
        .D(S0_AXI_WDATA[3]),
        .Q(Q[3]),
        .R(Rst));
  FDRE \sit_register_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(p_0_in1_in),
        .D(S0_AXI_WDATA[2]),
        .Q(Q[2]),
        .R(Rst));
  FDRE \sit_register_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(p_0_in1_in),
        .D(S0_AXI_WDATA[1]),
        .Q(Q[1]),
        .R(Rst));
  FDRE \sit_register_reg[3] 
       (.C(S0_AXI_ACLK),
        .CE(p_0_in1_in),
        .D(S0_AXI_WDATA[0]),
        .Q(Q[0]),
        .R(Rst));
  FDRE write_fsl_error_d1_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(write_fsl_error),
        .Q(write_fsl_error_d1),
        .R(Rst));
endmodule

(* ORIG_REF_NAME = "if_decode" *) 
module design_1_mailbox_0_0_if_decode__parameterized1
   (\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ,
    \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ,
    Bus_RNW_reg,
    S1_AXI_RVALID,
    S1_AXI_BVALID,
    empty_allow,
    S1_AXI_ARREADY,
    S1_AXI_WREADY,
    Interrupt_1,
    Q,
    rit_detect_d1_reg_0,
    E,
    S1_AXI_RDATA,
    \Rst_Async.SYS_Rst_If1_reg ,
    S1_AXI_ACLK,
    sit_detect_d0,
    rit_detect_d0,
    out,
    S1_AXI_ARVALID,
    S1_AXI_AWVALID,
    S1_AXI_WVALID,
    S1_AXI_ARADDR,
    S1_AXI_AWADDR,
    full_i_reg,
    S1_AXI_RREADY,
    S1_AXI_BREADY,
    \DataOut_reg[31] ,
    S1_AXI_WDATA);
  output \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  output \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ;
  output Bus_RNW_reg;
  output S1_AXI_RVALID;
  output S1_AXI_BVALID;
  output empty_allow;
  output S1_AXI_ARREADY;
  output S1_AXI_WREADY;
  output Interrupt_1;
  output [3:0]Q;
  output [3:0]rit_detect_d1_reg_0;
  output [0:0]E;
  output [31:0]S1_AXI_RDATA;
  input \Rst_Async.SYS_Rst_If1_reg ;
  input S1_AXI_ACLK;
  input sit_detect_d0;
  input rit_detect_d0;
  input out;
  input S1_AXI_ARVALID;
  input S1_AXI_AWVALID;
  input S1_AXI_WVALID;
  input [3:0]S1_AXI_ARADDR;
  input [3:0]S1_AXI_AWADDR;
  input full_i_reg;
  input S1_AXI_RREADY;
  input S1_AXI_BREADY;
  input [31:0]\DataOut_reg[31] ;
  input [3:0]S1_AXI_WDATA;

  wire Bus_RNW_reg;
  wire [31:0]\DataOut_reg[31] ;
  wire [0:0]E;
  wire \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  wire \I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ;
  wire \I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ;
  wire Interrupt_1;
  wire [3:0]Q;
  wire \Rst_Async.SYS_Rst_If1_reg ;
  wire S1_AXI_ACLK;
  wire [3:0]S1_AXI_ARADDR;
  wire S1_AXI_ARREADY;
  wire S1_AXI_ARVALID;
  wire [3:0]S1_AXI_AWADDR;
  wire S1_AXI_AWVALID;
  wire S1_AXI_BREADY;
  wire S1_AXI_BVALID;
  wire [31:0]S1_AXI_RDATA;
  wire S1_AXI_RREADY;
  wire S1_AXI_RVALID;
  wire [3:0]S1_AXI_WDATA;
  wire S1_AXI_WREADY;
  wire S1_AXI_WVALID;
  wire \Using_AXI.AXI4_If_n_15 ;
  wire \Using_AXI.AXI4_If_n_16 ;
  wire empty_allow;
  wire empty_error_reg_n_0;
  wire error_detect;
  wire full_error_reg_n_0;
  wire full_i_reg;
  wire [0:2]ie_register;
  wire \ie_register[0]_i_1_n_0 ;
  wire \ie_register[1]_i_1_n_0 ;
  wire \ie_register[2]_i_1_n_0 ;
  wire \is_register[0]_i_1_n_0 ;
  wire \is_register[1]_i_1_n_0 ;
  wire \is_register[2]_i_1_n_0 ;
  wire out;
  wire p_0_in1_in;
  wire p_1_in2_in;
  wire [2:0]p_6_in;
  wire read_fsl_error;
  wire read_fsl_error_d1;
  wire rit_detect_d0;
  wire rit_detect_d1;
  wire [3:0]rit_detect_d1_reg_0;
  wire sit_detect_d0;
  wire sit_detect_d1;
  wire write_fsl_error;
  wire write_fsl_error_d1;

  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    Interrupt_1_INST_0
       (.I0(ie_register[1]),
        .I1(p_6_in[1]),
        .I2(p_6_in[2]),
        .I3(ie_register[0]),
        .I4(p_6_in[0]),
        .I5(ie_register[2]),
        .O(Interrupt_1));
  design_1_mailbox_0_0_axi_lite_ipif__parameterized1 \Using_AXI.AXI4_If 
       (.Bus_RNW_reg_reg(Bus_RNW_reg),
        .\DataOut_reg[31] (\DataOut_reg[31] ),
        .E(p_1_in2_in),
        .\GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg (\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg (\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .Q(Q),
        .\Rst_Async.SYS_Rst_If1_reg (\Rst_Async.SYS_Rst_If1_reg ),
        .S1_AXI_ACLK(S1_AXI_ACLK),
        .S1_AXI_ARADDR(S1_AXI_ARADDR),
        .S1_AXI_ARREADY(S1_AXI_ARREADY),
        .S1_AXI_ARVALID(S1_AXI_ARVALID),
        .S1_AXI_AWADDR(S1_AXI_AWADDR),
        .S1_AXI_AWVALID(S1_AXI_AWVALID),
        .S1_AXI_BREADY(S1_AXI_BREADY),
        .S1_AXI_BVALID(S1_AXI_BVALID),
        .S1_AXI_RDATA(S1_AXI_RDATA),
        .S1_AXI_RREADY(S1_AXI_RREADY),
        .S1_AXI_RVALID(S1_AXI_RVALID),
        .S1_AXI_WREADY(S1_AXI_WREADY),
        .S1_AXI_WVALID(S1_AXI_WVALID),
        .empty_allow(empty_allow),
        .empty_error_reg(\Using_AXI.AXI4_If_n_16 ),
        .empty_error_reg_0(empty_error_reg_n_0),
        .error_detect(error_detect),
        .full_error_reg(\Using_AXI.AXI4_If_n_15 ),
        .full_error_reg_0(full_error_reg_n_0),
        .full_i_reg(full_i_reg),
        .ie_register(ie_register),
        .out(out),
        .p_6_in(p_6_in),
        .read_fsl_error(read_fsl_error),
        .read_fsl_error_d1(read_fsl_error_d1),
        .\read_nextgray_reg[2] (\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .\read_nextgray_reg[2]_0 (E),
        .rit_detect_d0(rit_detect_d0),
        .\rit_register_reg[0] (rit_detect_d1_reg_0),
        .sit_detect_d0(sit_detect_d0),
        .\sit_register_reg[3] (p_0_in1_in),
        .write_fsl_error(write_fsl_error),
        .write_fsl_error_d1(write_fsl_error_d1),
        .write_fsl_error_d1_reg(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ));
  FDRE empty_error_reg
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\Using_AXI.AXI4_If_n_16 ),
        .Q(empty_error_reg_n_0),
        .R(1'b0));
  FDRE full_error_reg
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\Using_AXI.AXI4_If_n_15 ),
        .Q(full_error_reg_n_0),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hFB08)) 
    \ie_register[0]_i_1 
       (.I0(S1_AXI_WDATA[2]),
        .I1(\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(ie_register[0]),
        .O(\ie_register[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \ie_register[1]_i_1 
       (.I0(S1_AXI_WDATA[1]),
        .I1(\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(ie_register[1]),
        .O(\ie_register[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \ie_register[2]_i_1 
       (.I0(S1_AXI_WDATA[0]),
        .I1(\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(ie_register[2]),
        .O(\ie_register[2]_i_1_n_0 ));
  FDRE \ie_register_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\ie_register[0]_i_1_n_0 ),
        .Q(ie_register[0]),
        .R(\Rst_Async.SYS_Rst_If1_reg ));
  FDRE \ie_register_reg[1] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\ie_register[1]_i_1_n_0 ),
        .Q(ie_register[1]),
        .R(\Rst_Async.SYS_Rst_If1_reg ));
  FDRE \ie_register_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\ie_register[2]_i_1_n_0 ),
        .Q(ie_register[2]),
        .R(\Rst_Async.SYS_Rst_If1_reg ));
  LUT5 #(
    .INIT(32'hFFBFAAAA)) 
    \is_register[0]_i_1 
       (.I0(error_detect),
        .I1(S1_AXI_WDATA[2]),
        .I2(\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .I3(Bus_RNW_reg),
        .I4(p_6_in[2]),
        .O(\is_register[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF4FFF44444444)) 
    \is_register[1]_i_1 
       (.I0(rit_detect_d1),
        .I1(rit_detect_d0),
        .I2(S1_AXI_WDATA[1]),
        .I3(\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .I4(Bus_RNW_reg),
        .I5(p_6_in[1]),
        .O(\is_register[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF4FFF44444444)) 
    \is_register[2]_i_1 
       (.I0(sit_detect_d1),
        .I1(sit_detect_d0),
        .I2(S1_AXI_WDATA[0]),
        .I3(\I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .I4(Bus_RNW_reg),
        .I5(p_6_in[0]),
        .O(\is_register[2]_i_1_n_0 ));
  FDRE \is_register_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\is_register[0]_i_1_n_0 ),
        .Q(p_6_in[2]),
        .R(\Rst_Async.SYS_Rst_If1_reg ));
  FDRE \is_register_reg[1] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\is_register[1]_i_1_n_0 ),
        .Q(p_6_in[1]),
        .R(\Rst_Async.SYS_Rst_If1_reg ));
  FDRE \is_register_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\is_register[2]_i_1_n_0 ),
        .Q(p_6_in[0]),
        .R(\Rst_Async.SYS_Rst_If1_reg ));
  FDRE read_fsl_error_d1_reg
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(read_fsl_error),
        .Q(read_fsl_error_d1),
        .R(\Rst_Async.SYS_Rst_If1_reg ));
  FDRE rit_detect_d1_reg
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(rit_detect_d0),
        .Q(rit_detect_d1),
        .R(1'b0));
  FDRE \rit_register_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(p_1_in2_in),
        .D(S1_AXI_WDATA[3]),
        .Q(rit_detect_d1_reg_0[3]),
        .R(\Rst_Async.SYS_Rst_If1_reg ));
  FDRE \rit_register_reg[1] 
       (.C(S1_AXI_ACLK),
        .CE(p_1_in2_in),
        .D(S1_AXI_WDATA[2]),
        .Q(rit_detect_d1_reg_0[2]),
        .R(\Rst_Async.SYS_Rst_If1_reg ));
  FDRE \rit_register_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(p_1_in2_in),
        .D(S1_AXI_WDATA[1]),
        .Q(rit_detect_d1_reg_0[1]),
        .R(\Rst_Async.SYS_Rst_If1_reg ));
  FDRE \rit_register_reg[3] 
       (.C(S1_AXI_ACLK),
        .CE(p_1_in2_in),
        .D(S1_AXI_WDATA[0]),
        .Q(rit_detect_d1_reg_0[0]),
        .R(\Rst_Async.SYS_Rst_If1_reg ));
  FDRE sit_detect_d1_reg
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(sit_detect_d0),
        .Q(sit_detect_d1),
        .R(1'b0));
  FDRE \sit_register_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(p_0_in1_in),
        .D(S1_AXI_WDATA[3]),
        .Q(Q[3]),
        .R(\Rst_Async.SYS_Rst_If1_reg ));
  FDRE \sit_register_reg[1] 
       (.C(S1_AXI_ACLK),
        .CE(p_0_in1_in),
        .D(S1_AXI_WDATA[2]),
        .Q(Q[2]),
        .R(\Rst_Async.SYS_Rst_If1_reg ));
  FDRE \sit_register_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(p_0_in1_in),
        .D(S1_AXI_WDATA[1]),
        .Q(Q[1]),
        .R(\Rst_Async.SYS_Rst_If1_reg ));
  FDRE \sit_register_reg[3] 
       (.C(S1_AXI_ACLK),
        .CE(p_0_in1_in),
        .D(S1_AXI_WDATA[0]),
        .Q(Q[0]),
        .R(\Rst_Async.SYS_Rst_If1_reg ));
  FDRE write_fsl_error_d1_reg
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(write_fsl_error),
        .Q(write_fsl_error_d1),
        .R(\Rst_Async.SYS_Rst_If1_reg ));
endmodule

(* C_ASYNC_CLKS = "1" *) (* C_ENABLE_BUS_ERROR = "0" *) (* C_EXT_RESET_HIGH = "1" *) 
(* C_FAMILY = "zynq" *) (* C_IMPL_STYLE = "0" *) (* C_INTERCONNECT_PORT_0 = "2" *) 
(* C_INTERCONNECT_PORT_1 = "2" *) (* C_M0_AXIS_DATA_WIDTH = "32" *) (* C_M1_AXIS_DATA_WIDTH = "32" *) 
(* C_MAILBOX_DEPTH = "16" *) (* C_NUM_SYNC_FF = "2" *) (* C_S0_AXIS_DATA_WIDTH = "32" *) 
(* C_S0_AXI_ADDR_WIDTH = "32" *) (* C_S0_AXI_BASEADDR = "1132462080" *) (* C_S0_AXI_DATA_WIDTH = "32" *) 
(* C_S0_AXI_HIGHADDR = "1132527615" *) (* C_S1_AXIS_DATA_WIDTH = "32" *) (* C_S1_AXI_ADDR_WIDTH = "32" *) 
(* C_S1_AXI_BASEADDR = "1130364928" *) (* C_S1_AXI_DATA_WIDTH = "32" *) (* C_S1_AXI_HIGHADDR = "1130430463" *) 
module design_1_mailbox_0_0_mailbox
   (SYS_Rst,
    S0_AXI_ACLK,
    S0_AXI_ARESETN,
    S0_AXI_AWADDR,
    S0_AXI_AWVALID,
    S0_AXI_AWREADY,
    S0_AXI_WDATA,
    S0_AXI_WSTRB,
    S0_AXI_WVALID,
    S0_AXI_WREADY,
    S0_AXI_BRESP,
    S0_AXI_BVALID,
    S0_AXI_BREADY,
    S0_AXI_ARADDR,
    S0_AXI_ARVALID,
    S0_AXI_ARREADY,
    S0_AXI_RDATA,
    S0_AXI_RRESP,
    S0_AXI_RVALID,
    S0_AXI_RREADY,
    S1_AXI_ACLK,
    S1_AXI_ARESETN,
    S1_AXI_AWADDR,
    S1_AXI_AWVALID,
    S1_AXI_AWREADY,
    S1_AXI_WDATA,
    S1_AXI_WSTRB,
    S1_AXI_WVALID,
    S1_AXI_WREADY,
    S1_AXI_BRESP,
    S1_AXI_BVALID,
    S1_AXI_BREADY,
    S1_AXI_ARADDR,
    S1_AXI_ARVALID,
    S1_AXI_ARREADY,
    S1_AXI_RDATA,
    S1_AXI_RRESP,
    S1_AXI_RVALID,
    S1_AXI_RREADY,
    M0_AXIS_ACLK,
    M0_AXIS_TLAST,
    M0_AXIS_TDATA,
    M0_AXIS_TVALID,
    M0_AXIS_TREADY,
    S0_AXIS_ACLK,
    S0_AXIS_TLAST,
    S0_AXIS_TDATA,
    S0_AXIS_TVALID,
    S0_AXIS_TREADY,
    M1_AXIS_ACLK,
    M1_AXIS_TLAST,
    M1_AXIS_TDATA,
    M1_AXIS_TVALID,
    M1_AXIS_TREADY,
    S1_AXIS_ACLK,
    S1_AXIS_TLAST,
    S1_AXIS_TDATA,
    S1_AXIS_TVALID,
    S1_AXIS_TREADY,
    Interrupt_0,
    Interrupt_1);
  input SYS_Rst;
  input S0_AXI_ACLK;
  input S0_AXI_ARESETN;
  input [31:0]S0_AXI_AWADDR;
  input S0_AXI_AWVALID;
  output S0_AXI_AWREADY;
  input [31:0]S0_AXI_WDATA;
  input [3:0]S0_AXI_WSTRB;
  input S0_AXI_WVALID;
  output S0_AXI_WREADY;
  output [1:0]S0_AXI_BRESP;
  output S0_AXI_BVALID;
  input S0_AXI_BREADY;
  input [31:0]S0_AXI_ARADDR;
  input S0_AXI_ARVALID;
  output S0_AXI_ARREADY;
  output [31:0]S0_AXI_RDATA;
  output [1:0]S0_AXI_RRESP;
  output S0_AXI_RVALID;
  input S0_AXI_RREADY;
  input S1_AXI_ACLK;
  input S1_AXI_ARESETN;
  input [31:0]S1_AXI_AWADDR;
  input S1_AXI_AWVALID;
  output S1_AXI_AWREADY;
  input [31:0]S1_AXI_WDATA;
  input [3:0]S1_AXI_WSTRB;
  input S1_AXI_WVALID;
  output S1_AXI_WREADY;
  output [1:0]S1_AXI_BRESP;
  output S1_AXI_BVALID;
  input S1_AXI_BREADY;
  input [31:0]S1_AXI_ARADDR;
  input S1_AXI_ARVALID;
  output S1_AXI_ARREADY;
  output [31:0]S1_AXI_RDATA;
  output [1:0]S1_AXI_RRESP;
  output S1_AXI_RVALID;
  input S1_AXI_RREADY;
  input M0_AXIS_ACLK;
  output M0_AXIS_TLAST;
  output [31:0]M0_AXIS_TDATA;
  output M0_AXIS_TVALID;
  input M0_AXIS_TREADY;
  input S0_AXIS_ACLK;
  input S0_AXIS_TLAST;
  input [31:0]S0_AXIS_TDATA;
  input S0_AXIS_TVALID;
  output S0_AXIS_TREADY;
  input M1_AXIS_ACLK;
  output M1_AXIS_TLAST;
  output [31:0]M1_AXIS_TDATA;
  output M1_AXIS_TVALID;
  input M1_AXIS_TREADY;
  input S1_AXIS_ACLK;
  input S1_AXIS_TLAST;
  input [31:0]S1_AXIS_TDATA;
  input S1_AXIS_TVALID;
  output S1_AXIS_TREADY;
  output Interrupt_0;
  output Interrupt_1;

  wire \<const0> ;
  wire Bus0_Rst_d1;
  wire Bus0_Rst_d2;
  wire Bus1_Rst_d1;
  wire Bus1_Rst_d2;
  wire [31:0]DataOut;
  wire FSL0_M_Full_I;
  wire FSL1_M_Full_I;
  wire Interrupt_0;
  wire Interrupt_1;
  wire Rst;
  wire \Rst_Async.SYS_Rst_If1_reg_n_0 ;
  wire S0_AXI_ACLK;
  wire [31:0]S0_AXI_ARADDR;
  wire S0_AXI_ARESETN;
  wire S0_AXI_ARREADY;
  wire S0_AXI_ARVALID;
  wire [31:0]S0_AXI_AWADDR;
  wire S0_AXI_AWVALID;
  wire S0_AXI_BREADY;
  wire S0_AXI_BVALID;
  wire [31:0]S0_AXI_RDATA;
  wire S0_AXI_RREADY;
  wire S0_AXI_RVALID;
  wire [31:0]S0_AXI_WDATA;
  wire S0_AXI_WREADY;
  wire S0_AXI_WVALID;
  wire S1_AXI_ACLK;
  wire [31:0]S1_AXI_ARADDR;
  wire S1_AXI_ARESETN;
  wire S1_AXI_ARREADY;
  wire S1_AXI_ARVALID;
  wire [31:0]S1_AXI_AWADDR;
  wire S1_AXI_AWVALID;
  wire S1_AXI_BREADY;
  wire S1_AXI_BVALID;
  wire [31:0]S1_AXI_RDATA;
  wire S1_AXI_RREADY;
  wire S1_AXI_RVALID;
  wire [31:0]S1_AXI_WDATA;
  wire S1_AXI_WREADY;
  wire S1_AXI_WVALID;
  wire SYS_Rst;
  wire SYS_Rst_Input0_d1;
  wire SYS_Rst_Input0_d2;
  wire SYS_Rst_Input1_d1;
  wire SYS_Rst_Input1_d2;
  wire \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/Bus_RNW_reg ;
  wire \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/Bus_RNW_reg_1 ;
  wire \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ;
  wire \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg_2 ;
  wire \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  wire \Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg_3 ;
  wire \Using_Bus_0.Bus0_If_n_17 ;
  wire \Using_Bus_1.Bus1_If_n_10 ;
  wire \Using_Bus_1.Bus1_If_n_11 ;
  wire \Using_Bus_1.Bus1_If_n_12 ;
  wire \Using_Bus_1.Bus1_If_n_13 ;
  wire \Using_Bus_1.Bus1_If_n_14 ;
  wire \Using_Bus_1.Bus1_If_n_15 ;
  wire \Using_Bus_1.Bus1_If_n_16 ;
  wire \Using_Bus_1.Bus1_If_n_17 ;
  wire \Using_Bus_1.Bus1_If_n_9 ;
  wire \Using_FIFO.Async_FIFO_Gen.Async_FIFO_I1/empty_allow ;
  wire \Using_FIFO.Async_FIFO_Gen.Async_FIFO_I1/empty_allow_0 ;
  wire \Using_FIFO.Async_FIFO_Gen.Async_FIFO_I1/p_3_in ;
  wire \Using_FIFO.Async_FIFO_Gen.Async_FIFO_I1/p_3_in_6 ;
  wire fsl_0_to_1_n_0;
  wire fsl_1_to_0_n_10;
  wire fsl_1_to_0_n_11;
  wire fsl_1_to_0_n_12;
  wire fsl_1_to_0_n_13;
  wire fsl_1_to_0_n_14;
  wire fsl_1_to_0_n_15;
  wire fsl_1_to_0_n_16;
  wire fsl_1_to_0_n_17;
  wire fsl_1_to_0_n_18;
  wire fsl_1_to_0_n_19;
  wire fsl_1_to_0_n_20;
  wire fsl_1_to_0_n_21;
  wire fsl_1_to_0_n_22;
  wire fsl_1_to_0_n_23;
  wire fsl_1_to_0_n_24;
  wire fsl_1_to_0_n_25;
  wire fsl_1_to_0_n_26;
  wire fsl_1_to_0_n_27;
  wire fsl_1_to_0_n_28;
  wire fsl_1_to_0_n_29;
  wire fsl_1_to_0_n_30;
  wire fsl_1_to_0_n_31;
  wire fsl_1_to_0_n_32;
  wire fsl_1_to_0_n_33;
  wire fsl_1_to_0_n_34;
  wire fsl_1_to_0_n_35;
  wire fsl_1_to_0_n_4;
  wire fsl_1_to_0_n_5;
  wire fsl_1_to_0_n_6;
  wire fsl_1_to_0_n_7;
  wire fsl_1_to_0_n_8;
  wire fsl_1_to_0_n_9;
  wire if0_rst;
  wire if1_rst;
  wire p_0_out;
  wire p_2_out;
  wire rit_detect_d0;
  wire rit_detect_d0_4;
  wire [0:3]rit_register;
  wire sit_detect_d0;
  wire sit_detect_d0_5;
  wire [0:3]sit_register;

  assign M0_AXIS_TDATA[31] = \<const0> ;
  assign M0_AXIS_TDATA[30] = \<const0> ;
  assign M0_AXIS_TDATA[29] = \<const0> ;
  assign M0_AXIS_TDATA[28] = \<const0> ;
  assign M0_AXIS_TDATA[27] = \<const0> ;
  assign M0_AXIS_TDATA[26] = \<const0> ;
  assign M0_AXIS_TDATA[25] = \<const0> ;
  assign M0_AXIS_TDATA[24] = \<const0> ;
  assign M0_AXIS_TDATA[23] = \<const0> ;
  assign M0_AXIS_TDATA[22] = \<const0> ;
  assign M0_AXIS_TDATA[21] = \<const0> ;
  assign M0_AXIS_TDATA[20] = \<const0> ;
  assign M0_AXIS_TDATA[19] = \<const0> ;
  assign M0_AXIS_TDATA[18] = \<const0> ;
  assign M0_AXIS_TDATA[17] = \<const0> ;
  assign M0_AXIS_TDATA[16] = \<const0> ;
  assign M0_AXIS_TDATA[15] = \<const0> ;
  assign M0_AXIS_TDATA[14] = \<const0> ;
  assign M0_AXIS_TDATA[13] = \<const0> ;
  assign M0_AXIS_TDATA[12] = \<const0> ;
  assign M0_AXIS_TDATA[11] = \<const0> ;
  assign M0_AXIS_TDATA[10] = \<const0> ;
  assign M0_AXIS_TDATA[9] = \<const0> ;
  assign M0_AXIS_TDATA[8] = \<const0> ;
  assign M0_AXIS_TDATA[7] = \<const0> ;
  assign M0_AXIS_TDATA[6] = \<const0> ;
  assign M0_AXIS_TDATA[5] = \<const0> ;
  assign M0_AXIS_TDATA[4] = \<const0> ;
  assign M0_AXIS_TDATA[3] = \<const0> ;
  assign M0_AXIS_TDATA[2] = \<const0> ;
  assign M0_AXIS_TDATA[1] = \<const0> ;
  assign M0_AXIS_TDATA[0] = \<const0> ;
  assign M0_AXIS_TLAST = \<const0> ;
  assign M0_AXIS_TVALID = \<const0> ;
  assign M1_AXIS_TDATA[31] = \<const0> ;
  assign M1_AXIS_TDATA[30] = \<const0> ;
  assign M1_AXIS_TDATA[29] = \<const0> ;
  assign M1_AXIS_TDATA[28] = \<const0> ;
  assign M1_AXIS_TDATA[27] = \<const0> ;
  assign M1_AXIS_TDATA[26] = \<const0> ;
  assign M1_AXIS_TDATA[25] = \<const0> ;
  assign M1_AXIS_TDATA[24] = \<const0> ;
  assign M1_AXIS_TDATA[23] = \<const0> ;
  assign M1_AXIS_TDATA[22] = \<const0> ;
  assign M1_AXIS_TDATA[21] = \<const0> ;
  assign M1_AXIS_TDATA[20] = \<const0> ;
  assign M1_AXIS_TDATA[19] = \<const0> ;
  assign M1_AXIS_TDATA[18] = \<const0> ;
  assign M1_AXIS_TDATA[17] = \<const0> ;
  assign M1_AXIS_TDATA[16] = \<const0> ;
  assign M1_AXIS_TDATA[15] = \<const0> ;
  assign M1_AXIS_TDATA[14] = \<const0> ;
  assign M1_AXIS_TDATA[13] = \<const0> ;
  assign M1_AXIS_TDATA[12] = \<const0> ;
  assign M1_AXIS_TDATA[11] = \<const0> ;
  assign M1_AXIS_TDATA[10] = \<const0> ;
  assign M1_AXIS_TDATA[9] = \<const0> ;
  assign M1_AXIS_TDATA[8] = \<const0> ;
  assign M1_AXIS_TDATA[7] = \<const0> ;
  assign M1_AXIS_TDATA[6] = \<const0> ;
  assign M1_AXIS_TDATA[5] = \<const0> ;
  assign M1_AXIS_TDATA[4] = \<const0> ;
  assign M1_AXIS_TDATA[3] = \<const0> ;
  assign M1_AXIS_TDATA[2] = \<const0> ;
  assign M1_AXIS_TDATA[1] = \<const0> ;
  assign M1_AXIS_TDATA[0] = \<const0> ;
  assign M1_AXIS_TLAST = \<const0> ;
  assign M1_AXIS_TVALID = \<const0> ;
  assign S0_AXIS_TREADY = \<const0> ;
  assign S0_AXI_AWREADY = S0_AXI_WREADY;
  assign S0_AXI_BRESP[1] = \<const0> ;
  assign S0_AXI_BRESP[0] = \<const0> ;
  assign S0_AXI_RRESP[1] = \<const0> ;
  assign S0_AXI_RRESP[0] = \<const0> ;
  assign S1_AXIS_TREADY = \<const0> ;
  assign S1_AXI_AWREADY = S1_AXI_WREADY;
  assign S1_AXI_BRESP[1] = \<const0> ;
  assign S1_AXI_BRESP[0] = \<const0> ;
  assign S1_AXI_RRESP[1] = \<const0> ;
  assign S1_AXI_RRESP[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* ASYNC_REG *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Rst_Async.Bus_RST_FF_I0_1 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(if1_rst),
        .Q(Bus1_Rst_d1),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \Rst_Async.Bus_RST_FF_I0_1_i_1 
       (.I0(S1_AXI_ARESETN),
        .O(if1_rst));
  (* ASYNC_REG *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Rst_Async.Bus_RST_FF_I0_2 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(Bus1_Rst_d1),
        .Q(Bus1_Rst_d2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Rst_Async.Bus_RST_FF_I1_1 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(if0_rst),
        .Q(Bus0_Rst_d1),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \Rst_Async.Bus_RST_FF_I1_1_i_1 
       (.I0(S0_AXI_ARESETN),
        .O(if0_rst));
  (* ASYNC_REG *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Rst_Async.Bus_RST_FF_I1_2 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(Bus0_Rst_d1),
        .Q(Bus0_Rst_d2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Rst_Async.SYS_RST_FF_I0_1 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(SYS_Rst),
        .Q(SYS_Rst_Input0_d1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Rst_Async.SYS_RST_FF_I0_2 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(SYS_Rst_Input0_d1),
        .Q(SYS_Rst_Input0_d2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Rst_Async.SYS_RST_FF_I1_1 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(SYS_Rst),
        .Q(SYS_Rst_Input1_d1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Rst_Async.SYS_RST_FF_I1_2 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(SYS_Rst_Input1_d1),
        .Q(SYS_Rst_Input1_d2),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hFB)) 
    \Rst_Async.SYS_Rst_If0_i_1 
       (.I0(SYS_Rst_Input0_d2),
        .I1(S0_AXI_ARESETN),
        .I2(Bus1_Rst_d2),
        .O(p_2_out));
  FDRE #(
    .INIT(1'b0)) 
    \Rst_Async.SYS_Rst_If0_reg 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(p_2_out),
        .Q(Rst),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hFB)) 
    \Rst_Async.SYS_Rst_If1_i_1 
       (.I0(SYS_Rst_Input1_d2),
        .I1(S1_AXI_ARESETN),
        .I2(Bus0_Rst_d2),
        .O(p_0_out));
  FDRE #(
    .INIT(1'b0)) 
    \Rst_Async.SYS_Rst_If1_reg 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(p_0_out),
        .Q(\Rst_Async.SYS_Rst_If1_reg_n_0 ),
        .R(1'b0));
  design_1_mailbox_0_0_if_decode \Using_Bus_0.Bus0_If 
       (.Bus_RNW_reg(\Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/Bus_RNW_reg ),
        .\DataOut_reg[31] ({fsl_1_to_0_n_4,fsl_1_to_0_n_5,fsl_1_to_0_n_6,fsl_1_to_0_n_7,fsl_1_to_0_n_8,fsl_1_to_0_n_9,fsl_1_to_0_n_10,fsl_1_to_0_n_11,fsl_1_to_0_n_12,fsl_1_to_0_n_13,fsl_1_to_0_n_14,fsl_1_to_0_n_15,fsl_1_to_0_n_16,fsl_1_to_0_n_17,fsl_1_to_0_n_18,fsl_1_to_0_n_19,fsl_1_to_0_n_20,fsl_1_to_0_n_21,fsl_1_to_0_n_22,fsl_1_to_0_n_23,fsl_1_to_0_n_24,fsl_1_to_0_n_25,fsl_1_to_0_n_26,fsl_1_to_0_n_27,fsl_1_to_0_n_28,fsl_1_to_0_n_29,fsl_1_to_0_n_30,fsl_1_to_0_n_31,fsl_1_to_0_n_32,fsl_1_to_0_n_33,fsl_1_to_0_n_34,fsl_1_to_0_n_35}),
        .E(\Using_Bus_0.Bus0_If_n_17 ),
        .\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg (\Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg (\Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .Interrupt_0(Interrupt_0),
        .Q({sit_register[0],sit_register[1],sit_register[2],sit_register[3]}),
        .Rst(Rst),
        .S0_AXI_ACLK(S0_AXI_ACLK),
        .S0_AXI_ARADDR(S0_AXI_ARADDR[5:2]),
        .S0_AXI_ARREADY(S0_AXI_ARREADY),
        .S0_AXI_ARVALID(S0_AXI_ARVALID),
        .S0_AXI_AWADDR(S0_AXI_AWADDR[5:2]),
        .S0_AXI_AWVALID(S0_AXI_AWVALID),
        .S0_AXI_BREADY(S0_AXI_BREADY),
        .S0_AXI_BVALID(S0_AXI_BVALID),
        .S0_AXI_RDATA(S0_AXI_RDATA),
        .S0_AXI_RREADY(S0_AXI_RREADY),
        .S0_AXI_RVALID(S0_AXI_RVALID),
        .S0_AXI_WDATA(S0_AXI_WDATA[3:0]),
        .S0_AXI_WREADY(S0_AXI_WREADY),
        .S0_AXI_WVALID(S0_AXI_WVALID),
        .empty_allow(\Using_FIFO.Async_FIFO_Gen.Async_FIFO_I1/empty_allow ),
        .full_i_reg(FSL0_M_Full_I),
        .out(\Using_FIFO.Async_FIFO_Gen.Async_FIFO_I1/p_3_in_6 ),
        .rit_detect_d0(rit_detect_d0_4),
        .rit_detect_d1_reg_0({rit_register[0],rit_register[1],rit_register[2],rit_register[3]}),
        .sit_detect_d0(sit_detect_d0));
  design_1_mailbox_0_0_if_decode__parameterized1 \Using_Bus_1.Bus1_If 
       (.Bus_RNW_reg(\Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/Bus_RNW_reg_1 ),
        .\DataOut_reg[31] (DataOut),
        .E(\Using_Bus_1.Bus1_If_n_17 ),
        .\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg (\Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg_2 ),
        .\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg (\Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg_3 ),
        .Interrupt_1(Interrupt_1),
        .Q({\Using_Bus_1.Bus1_If_n_9 ,\Using_Bus_1.Bus1_If_n_10 ,\Using_Bus_1.Bus1_If_n_11 ,\Using_Bus_1.Bus1_If_n_12 }),
        .\Rst_Async.SYS_Rst_If1_reg (\Rst_Async.SYS_Rst_If1_reg_n_0 ),
        .S1_AXI_ACLK(S1_AXI_ACLK),
        .S1_AXI_ARADDR(S1_AXI_ARADDR[5:2]),
        .S1_AXI_ARREADY(S1_AXI_ARREADY),
        .S1_AXI_ARVALID(S1_AXI_ARVALID),
        .S1_AXI_AWADDR(S1_AXI_AWADDR[5:2]),
        .S1_AXI_AWVALID(S1_AXI_AWVALID),
        .S1_AXI_BREADY(S1_AXI_BREADY),
        .S1_AXI_BVALID(S1_AXI_BVALID),
        .S1_AXI_RDATA(S1_AXI_RDATA),
        .S1_AXI_RREADY(S1_AXI_RREADY),
        .S1_AXI_RVALID(S1_AXI_RVALID),
        .S1_AXI_WDATA(S1_AXI_WDATA[3:0]),
        .S1_AXI_WREADY(S1_AXI_WREADY),
        .S1_AXI_WVALID(S1_AXI_WVALID),
        .empty_allow(\Using_FIFO.Async_FIFO_Gen.Async_FIFO_I1/empty_allow_0 ),
        .full_i_reg(FSL1_M_Full_I),
        .out(\Using_FIFO.Async_FIFO_Gen.Async_FIFO_I1/p_3_in ),
        .rit_detect_d0(rit_detect_d0),
        .rit_detect_d1_reg_0({\Using_Bus_1.Bus1_If_n_13 ,\Using_Bus_1.Bus1_If_n_14 ,\Using_Bus_1.Bus1_If_n_15 ,\Using_Bus_1.Bus1_If_n_16 }),
        .sit_detect_d0(sit_detect_d0_5));
  design_1_mailbox_0_0_fsl_v20 fsl_0_to_1
       (.Bus_RNW_reg(\Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/Bus_RNW_reg_1 ),
        .Bus_RNW_reg_0(\Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/Bus_RNW_reg ),
        .E(\Using_Bus_1.Bus1_If_n_17 ),
        .\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg (\Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg (\Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg_3 ),
        .Q({sit_register[0],sit_register[1],sit_register[2],sit_register[3]}),
        .S0_AXI_ACLK(S0_AXI_ACLK),
        .S0_AXI_ARESETN(S0_AXI_ARESETN),
        .S0_AXI_WDATA(S0_AXI_WDATA),
        .S1_AXI_ACLK(S1_AXI_ACLK),
        .S1_AXI_ARESETN(S1_AXI_ARESETN),
        .SYS_Rst(SYS_Rst),
        .empty_allow(\Using_FIFO.Async_FIFO_Gen.Async_FIFO_I1/empty_allow_0 ),
        .out(\Using_FIFO.Async_FIFO_Gen.Async_FIFO_I1/p_3_in ),
        .rd_rst_meta_inst(fsl_0_to_1_n_0),
        .rit_detect_d0(rit_detect_d0),
        .\rit_register_reg[0] ({\Using_Bus_1.Bus1_If_n_13 ,\Using_Bus_1.Bus1_If_n_14 ,\Using_Bus_1.Bus1_If_n_15 ,\Using_Bus_1.Bus1_If_n_16 }),
        .\s_axi_rdata_i_reg[31] (DataOut),
        .sit_detect_d0(sit_detect_d0),
        .\write_nextgray_reg[2] (FSL0_M_Full_I));
  design_1_mailbox_0_0_fsl_v20_0 fsl_1_to_0
       (.Bus_RNW_reg(\Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/Bus_RNW_reg ),
        .Bus_RNW_reg_0(\Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/Bus_RNW_reg_1 ),
        .DataOut({fsl_1_to_0_n_4,fsl_1_to_0_n_5,fsl_1_to_0_n_6,fsl_1_to_0_n_7,fsl_1_to_0_n_8,fsl_1_to_0_n_9,fsl_1_to_0_n_10,fsl_1_to_0_n_11,fsl_1_to_0_n_12,fsl_1_to_0_n_13,fsl_1_to_0_n_14,fsl_1_to_0_n_15,fsl_1_to_0_n_16,fsl_1_to_0_n_17,fsl_1_to_0_n_18,fsl_1_to_0_n_19,fsl_1_to_0_n_20,fsl_1_to_0_n_21,fsl_1_to_0_n_22,fsl_1_to_0_n_23,fsl_1_to_0_n_24,fsl_1_to_0_n_25,fsl_1_to_0_n_26,fsl_1_to_0_n_27,fsl_1_to_0_n_28,fsl_1_to_0_n_29,fsl_1_to_0_n_30,fsl_1_to_0_n_31,fsl_1_to_0_n_32,fsl_1_to_0_n_33,fsl_1_to_0_n_34,fsl_1_to_0_n_35}),
        .E(\Using_Bus_0.Bus0_If_n_17 ),
        .\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg (\Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg_2 ),
        .\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg (\Using_AXI.AXI4_If/I_SLAVE_ATTACHMENT/I_DECODER/GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .Q({\Using_Bus_1.Bus1_If_n_9 ,\Using_Bus_1.Bus1_If_n_10 ,\Using_Bus_1.Bus1_If_n_11 ,\Using_Bus_1.Bus1_If_n_12 }),
        .S0_AXI_ACLK(S0_AXI_ACLK),
        .S1_AXI_ACLK(S1_AXI_ACLK),
        .S1_AXI_ARESETN(fsl_0_to_1_n_0),
        .S1_AXI_WDATA(S1_AXI_WDATA),
        .empty_allow(\Using_FIFO.Async_FIFO_Gen.Async_FIFO_I1/empty_allow ),
        .out(\Using_FIFO.Async_FIFO_Gen.Async_FIFO_I1/p_3_in_6 ),
        .rit_detect_d0(rit_detect_d0_4),
        .\rit_register_reg[0] ({rit_register[0],rit_register[1],rit_register[2],rit_register[3]}),
        .sit_detect_d0(sit_detect_d0_5),
        .\write_addr_reg[0] (FSL1_M_Full_I));
endmodule

module design_1_mailbox_0_0_pselect_f
   (\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ,
    Q,
    \bus2ip_addr_i_reg[5] );
  output \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ;
  input Q;
  input [3:0]\bus2ip_addr_i_reg[5] ;

  wire \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ;
  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;

  LUT5 #(
    .INIT(32'h00000002)) 
    CS
       (.I0(Q),
        .I1(\bus2ip_addr_i_reg[5] [2]),
        .I2(\bus2ip_addr_i_reg[5] [1]),
        .I3(\bus2ip_addr_i_reg[5] [3]),
        .I4(\bus2ip_addr_i_reg[5] [0]),
        .O(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module design_1_mailbox_0_0_pselect_f_2
   (\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ,
    Q,
    \bus2ip_addr_i_reg[5] );
  output \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ;
  input Q;
  input [3:0]\bus2ip_addr_i_reg[5] ;

  wire \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ;
  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;

  LUT5 #(
    .INIT(32'h00000002)) 
    CS
       (.I0(Q),
        .I1(\bus2ip_addr_i_reg[5] [2]),
        .I2(\bus2ip_addr_i_reg[5] [1]),
        .I3(\bus2ip_addr_i_reg[5] [3]),
        .I4(\bus2ip_addr_i_reg[5] [0]),
        .O(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module design_1_mailbox_0_0_pselect_f__parameterized1
   (ce_expnd_i_8,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_8;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_8;

  LUT5 #(
    .INIT(32'h01000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [3]),
        .I1(\bus2ip_addr_i_reg[5] [2]),
        .I2(\bus2ip_addr_i_reg[5] [0]),
        .I3(\bus2ip_addr_i_reg[5] [1]),
        .I4(Q),
        .O(ce_expnd_i_8));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module design_1_mailbox_0_0_pselect_f__parameterized11
   (ce_expnd_i_8,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_8;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_8;

  LUT5 #(
    .INIT(32'h01000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [3]),
        .I1(\bus2ip_addr_i_reg[5] [2]),
        .I2(\bus2ip_addr_i_reg[5] [0]),
        .I3(\bus2ip_addr_i_reg[5] [1]),
        .I4(Q),
        .O(ce_expnd_i_8));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module design_1_mailbox_0_0_pselect_f__parameterized13
   (ce_expnd_i_6,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_6;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_6;

  LUT5 #(
    .INIT(32'h01000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [3]),
        .I1(\bus2ip_addr_i_reg[5] [1]),
        .I2(\bus2ip_addr_i_reg[5] [0]),
        .I3(\bus2ip_addr_i_reg[5] [2]),
        .I4(Q),
        .O(ce_expnd_i_6));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module design_1_mailbox_0_0_pselect_f__parameterized14
   (ce_expnd_i_5,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_5;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_5;

  LUT5 #(
    .INIT(32'h10000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [3]),
        .I1(\bus2ip_addr_i_reg[5] [1]),
        .I2(Q),
        .I3(\bus2ip_addr_i_reg[5] [0]),
        .I4(\bus2ip_addr_i_reg[5] [2]),
        .O(ce_expnd_i_5));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module design_1_mailbox_0_0_pselect_f__parameterized16
   (ce_expnd_i_3,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_3;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_3;

  LUT5 #(
    .INIT(32'h40000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [3]),
        .I1(Q),
        .I2(\bus2ip_addr_i_reg[5] [2]),
        .I3(\bus2ip_addr_i_reg[5] [0]),
        .I4(\bus2ip_addr_i_reg[5] [1]),
        .O(ce_expnd_i_3));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module design_1_mailbox_0_0_pselect_f__parameterized19
   (ce_expnd_i_0,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_0;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_0;

  LUT5 #(
    .INIT(32'h10000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [2]),
        .I1(\bus2ip_addr_i_reg[5] [0]),
        .I2(\bus2ip_addr_i_reg[5] [3]),
        .I3(\bus2ip_addr_i_reg[5] [1]),
        .I4(Q),
        .O(ce_expnd_i_0));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module design_1_mailbox_0_0_pselect_f__parameterized3
   (ce_expnd_i_6,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_6;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_6;

  LUT5 #(
    .INIT(32'h01000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [3]),
        .I1(\bus2ip_addr_i_reg[5] [1]),
        .I2(\bus2ip_addr_i_reg[5] [0]),
        .I3(\bus2ip_addr_i_reg[5] [2]),
        .I4(Q),
        .O(ce_expnd_i_6));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module design_1_mailbox_0_0_pselect_f__parameterized4
   (ce_expnd_i_5,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_5;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_5;

  LUT5 #(
    .INIT(32'h10000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [3]),
        .I1(\bus2ip_addr_i_reg[5] [1]),
        .I2(Q),
        .I3(\bus2ip_addr_i_reg[5] [0]),
        .I4(\bus2ip_addr_i_reg[5] [2]),
        .O(ce_expnd_i_5));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module design_1_mailbox_0_0_pselect_f__parameterized6
   (ce_expnd_i_3,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_3;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_3;

  LUT5 #(
    .INIT(32'h40000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [3]),
        .I1(Q),
        .I2(\bus2ip_addr_i_reg[5] [2]),
        .I3(\bus2ip_addr_i_reg[5] [0]),
        .I4(\bus2ip_addr_i_reg[5] [1]),
        .O(ce_expnd_i_3));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module design_1_mailbox_0_0_pselect_f__parameterized9
   (ce_expnd_i_0,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_0;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_0;

  LUT5 #(
    .INIT(32'h10000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [2]),
        .I1(\bus2ip_addr_i_reg[5] [0]),
        .I2(\bus2ip_addr_i_reg[5] [3]),
        .I3(\bus2ip_addr_i_reg[5] [1]),
        .I4(Q),
        .O(ce_expnd_i_0));
endmodule

module design_1_mailbox_0_0_slave_attachment
   (\s_axi_rdata_i_reg[31]_0 ,
    \is_register_reg[2] ,
    \read_nextgray_reg[2] ,
    write_fsl_error_d1_reg,
    Bus_RNW_reg_reg,
    S0_AXI_RVALID,
    S0_AXI_BVALID,
    empty_allow,
    S0_AXI_ARREADY,
    error_detect,
    read_fsl_error,
    write_fsl_error,
    S0_AXI_WREADY,
    E,
    \sit_register_reg[3] ,
    full_error_reg,
    empty_error_reg,
    \read_nextgray_reg[2]_0 ,
    S0_AXI_RDATA,
    Rst,
    S0_AXI_ACLK,
    out,
    S0_AXI_ARVALID,
    S0_AXI_AWVALID,
    S0_AXI_WVALID,
    S0_AXI_ARADDR,
    S0_AXI_AWADDR,
    write_fsl_error_d1,
    full_i_reg,
    read_fsl_error_d1,
    rit_detect_d0,
    sit_detect_d0,
    full_error,
    empty_error,
    S0_AXI_RREADY,
    S0_AXI_BREADY,
    Q,
    p_6_in,
    \rit_register_reg[0] ,
    \DataOut_reg[31] ,
    ie_register);
  output \s_axi_rdata_i_reg[31]_0 ;
  output \is_register_reg[2] ;
  output \read_nextgray_reg[2] ;
  output write_fsl_error_d1_reg;
  output Bus_RNW_reg_reg;
  output S0_AXI_RVALID;
  output S0_AXI_BVALID;
  output empty_allow;
  output S0_AXI_ARREADY;
  output error_detect;
  output read_fsl_error;
  output write_fsl_error;
  output S0_AXI_WREADY;
  output [0:0]E;
  output [0:0]\sit_register_reg[3] ;
  output full_error_reg;
  output empty_error_reg;
  output [0:0]\read_nextgray_reg[2]_0 ;
  output [31:0]S0_AXI_RDATA;
  input Rst;
  input S0_AXI_ACLK;
  input out;
  input S0_AXI_ARVALID;
  input S0_AXI_AWVALID;
  input S0_AXI_WVALID;
  input [3:0]S0_AXI_ARADDR;
  input [3:0]S0_AXI_AWADDR;
  input write_fsl_error_d1;
  input full_i_reg;
  input read_fsl_error_d1;
  input rit_detect_d0;
  input sit_detect_d0;
  input full_error;
  input empty_error;
  input S0_AXI_RREADY;
  input S0_AXI_BREADY;
  input [3:0]Q;
  input [2:0]p_6_in;
  input [3:0]\rit_register_reg[0] ;
  input [31:0]\DataOut_reg[31] ;
  input [0:2]ie_register;

  wire Bus_RNW_reg_reg;
  wire [31:0]\DataOut_reg[31] ;
  wire [0:0]E;
  wire [4:0]\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 ;
  wire I_DECODER_n_12;
  wire I_DECODER_n_13;
  wire I_DECODER_n_14;
  wire I_DECODER_n_15;
  wire I_DECODER_n_16;
  wire I_DECODER_n_17;
  wire I_DECODER_n_18;
  wire I_DECODER_n_19;
  wire I_DECODER_n_20;
  wire I_DECODER_n_21;
  wire I_DECODER_n_22;
  wire I_DECODER_n_23;
  wire I_DECODER_n_24;
  wire I_DECODER_n_25;
  wire I_DECODER_n_26;
  wire I_DECODER_n_27;
  wire I_DECODER_n_28;
  wire I_DECODER_n_29;
  wire I_DECODER_n_30;
  wire I_DECODER_n_31;
  wire I_DECODER_n_32;
  wire I_DECODER_n_33;
  wire I_DECODER_n_34;
  wire I_DECODER_n_35;
  wire I_DECODER_n_36;
  wire I_DECODER_n_37;
  wire I_DECODER_n_38;
  wire I_DECODER_n_39;
  wire I_DECODER_n_40;
  wire I_DECODER_n_41;
  wire I_DECODER_n_42;
  wire I_DECODER_n_43;
  wire I_DECODER_n_47;
  wire I_DECODER_n_48;
  wire I_DECODER_n_6;
  wire I_DECODER_n_7;
  wire [3:0]Q;
  wire Rst;
  wire S0_AXI_ACLK;
  wire [3:0]S0_AXI_ARADDR;
  wire S0_AXI_ARREADY;
  wire S0_AXI_ARVALID;
  wire [3:0]S0_AXI_AWADDR;
  wire S0_AXI_AWVALID;
  wire S0_AXI_BREADY;
  wire S0_AXI_BVALID;
  wire [31:0]S0_AXI_RDATA;
  wire S0_AXI_RREADY;
  wire S0_AXI_RVALID;
  wire S0_AXI_WREADY;
  wire S0_AXI_WVALID;
  wire \bus2ip_addr_i[5]_i_1__0_n_0 ;
  wire \bus2ip_addr_i_reg_n_0_[2] ;
  wire \bus2ip_addr_i_reg_n_0_[3] ;
  wire \bus2ip_addr_i_reg_n_0_[4] ;
  wire \bus2ip_addr_i_reg_n_0_[5] ;
  wire bus2ip_rnw_i;
  wire bus2ip_rnw_i06_out;
  wire clear;
  wire empty_allow;
  wire empty_error;
  wire empty_error_reg;
  wire error_detect;
  wire full_error;
  wire full_error_reg;
  wire full_i_reg;
  wire [0:2]ie_register;
  wire is_read;
  wire is_read_i_1__0_n_0;
  wire \is_register_reg[2] ;
  wire is_write;
  wire is_write_i_1__0_n_0;
  wire is_write_reg_n_0;
  wire out;
  wire [5:2]p_1_in;
  wire [2:0]p_6_in;
  wire [4:0]plusOp;
  wire read_fsl_error;
  wire read_fsl_error_d1;
  wire \read_nextgray_reg[2] ;
  wire [0:0]\read_nextgray_reg[2]_0 ;
  wire rit_detect_d0;
  wire [3:0]\rit_register_reg[0] ;
  wire rst;
  wire \s_axi_rdata_i[31]_i_1__0_n_0 ;
  wire \s_axi_rdata_i_reg[31]_0 ;
  wire sit_detect_d0;
  wire [0:0]\sit_register_reg[3] ;
  wire start2;
  wire start2_i_1_n_0;
  wire [1:0]state;
  wire state1__2;
  wire \state[1]_i_3__0_n_0 ;
  wire write_fsl_error;
  wire write_fsl_error_d1;
  wire write_fsl_error_d1_reg;

  LUT1 #(
    .INIT(2'h1)) 
    \INCLUDE_DPHASE_TIMER.dpto_cnt[0]_i_1__0 
       (.I0(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [0]),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \INCLUDE_DPHASE_TIMER.dpto_cnt[1]_i_1__0 
       (.I0(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [0]),
        .I1(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [1]),
        .O(plusOp[1]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \INCLUDE_DPHASE_TIMER.dpto_cnt[2]_i_1__0 
       (.I0(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [0]),
        .I1(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [1]),
        .I2(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [2]),
        .O(plusOp[2]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \INCLUDE_DPHASE_TIMER.dpto_cnt[3]_i_1__0 
       (.I0(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [1]),
        .I1(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [0]),
        .I2(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [2]),
        .I3(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [3]),
        .O(plusOp[3]));
  LUT2 #(
    .INIT(4'h9)) 
    \INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1__0 
       (.I0(state[0]),
        .I1(state[1]),
        .O(clear));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_2__0 
       (.I0(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [2]),
        .I1(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [0]),
        .I2(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [1]),
        .I3(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [3]),
        .I4(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [4]),
        .O(plusOp[4]));
  FDRE \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(plusOp[0]),
        .Q(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [0]),
        .R(clear));
  FDRE \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(plusOp[1]),
        .Q(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [1]),
        .R(clear));
  FDRE \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(plusOp[2]),
        .Q(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [2]),
        .R(clear));
  FDRE \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[3] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(plusOp[3]),
        .Q(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [3]),
        .R(clear));
  FDRE \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(plusOp[4]),
        .Q(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [4]),
        .R(clear));
  design_1_mailbox_0_0_address_decoder I_DECODER
       (.Bus_RNW_reg_reg_0(Bus_RNW_reg_reg),
        .D({I_DECODER_n_6,I_DECODER_n_7}),
        .\DataOut_reg[31] (\DataOut_reg[31] ),
        .E(E),
        .\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] (\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 ),
        .Q(start2),
        .Rst(Rst),
        .S0_AXI_ACLK(S0_AXI_ACLK),
        .S0_AXI_ARREADY(S0_AXI_ARREADY),
        .S0_AXI_ARVALID(S0_AXI_ARVALID),
        .S0_AXI_ARVALID_0(\state[1]_i_3__0_n_0 ),
        .S0_AXI_BREADY(S0_AXI_BREADY),
        .S0_AXI_RREADY(S0_AXI_RREADY),
        .S0_AXI_WREADY(S0_AXI_WREADY),
        .\bus2ip_addr_i_reg[5] ({\bus2ip_addr_i_reg_n_0_[5] ,\bus2ip_addr_i_reg_n_0_[4] ,\bus2ip_addr_i_reg_n_0_[3] ,\bus2ip_addr_i_reg_n_0_[2] }),
        .bus2ip_rnw_i(bus2ip_rnw_i),
        .empty_allow(empty_allow),
        .empty_error(empty_error),
        .empty_error_reg(empty_error_reg),
        .error_detect(error_detect),
        .full_error(full_error),
        .full_error_reg(full_error_reg),
        .full_i_reg(full_i_reg),
        .ie_register(ie_register),
        .is_read(is_read),
        .\is_register_reg[2] (\is_register_reg[2] ),
        .is_write_reg(is_write_reg_n_0),
        .out(out),
        .p_6_in(p_6_in),
        .read_fsl_error(read_fsl_error),
        .read_fsl_error_d1(read_fsl_error_d1),
        .\read_nextgray_reg[2] (\read_nextgray_reg[2] ),
        .\read_nextgray_reg[2]_0 (\read_nextgray_reg[2]_0 ),
        .rit_detect_d0(rit_detect_d0),
        .\rit_register_reg[0] (\rit_register_reg[0] ),
        .s_axi_bvalid_i_reg(I_DECODER_n_48),
        .s_axi_bvalid_i_reg_0(S0_AXI_BVALID),
        .\s_axi_rdata_i_reg[31] (\s_axi_rdata_i_reg[31]_0 ),
        .\s_axi_rdata_i_reg[31]_0 ({I_DECODER_n_12,I_DECODER_n_13,I_DECODER_n_14,I_DECODER_n_15,I_DECODER_n_16,I_DECODER_n_17,I_DECODER_n_18,I_DECODER_n_19,I_DECODER_n_20,I_DECODER_n_21,I_DECODER_n_22,I_DECODER_n_23,I_DECODER_n_24,I_DECODER_n_25,I_DECODER_n_26,I_DECODER_n_27,I_DECODER_n_28,I_DECODER_n_29,I_DECODER_n_30,I_DECODER_n_31,I_DECODER_n_32,I_DECODER_n_33,I_DECODER_n_34,I_DECODER_n_35,I_DECODER_n_36,I_DECODER_n_37,I_DECODER_n_38,I_DECODER_n_39,I_DECODER_n_40,I_DECODER_n_41,I_DECODER_n_42,I_DECODER_n_43}),
        .s_axi_rvalid_i_reg(I_DECODER_n_47),
        .s_axi_rvalid_i_reg_0(S0_AXI_RVALID),
        .sit_detect_d0(sit_detect_d0),
        .\sit_register_reg[0] (Q),
        .\sit_register_reg[3] (\sit_register_reg[3] ),
        .state1__2(state1__2),
        .\state_reg[1] (state),
        .write_fsl_error(write_fsl_error),
        .write_fsl_error_d1(write_fsl_error_d1),
        .write_fsl_error_d1_reg(write_fsl_error_d1_reg));
  LUT5 #(
    .INIT(32'hCCCACCCC)) 
    \bus2ip_addr_i[2]_i_1__0 
       (.I0(S0_AXI_ARADDR[0]),
        .I1(S0_AXI_AWADDR[0]),
        .I2(state[0]),
        .I3(state[1]),
        .I4(S0_AXI_ARVALID),
        .O(p_1_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT5 #(
    .INIT(32'hCCCACCCC)) 
    \bus2ip_addr_i[3]_i_1__0 
       (.I0(S0_AXI_ARADDR[1]),
        .I1(S0_AXI_AWADDR[1]),
        .I2(state[0]),
        .I3(state[1]),
        .I4(S0_AXI_ARVALID),
        .O(p_1_in[3]));
  LUT5 #(
    .INIT(32'hCCCACCCC)) 
    \bus2ip_addr_i[4]_i_1__0 
       (.I0(S0_AXI_ARADDR[2]),
        .I1(S0_AXI_AWADDR[2]),
        .I2(state[0]),
        .I3(state[1]),
        .I4(S0_AXI_ARVALID),
        .O(p_1_in[4]));
  LUT5 #(
    .INIT(32'h000000EA)) 
    \bus2ip_addr_i[5]_i_1__0 
       (.I0(S0_AXI_ARVALID),
        .I1(S0_AXI_AWVALID),
        .I2(S0_AXI_WVALID),
        .I3(state[1]),
        .I4(state[0]),
        .O(\bus2ip_addr_i[5]_i_1__0_n_0 ));
  LUT5 #(
    .INIT(32'hCCCACCCC)) 
    \bus2ip_addr_i[5]_i_2__0 
       (.I0(S0_AXI_ARADDR[3]),
        .I1(S0_AXI_AWADDR[3]),
        .I2(state[0]),
        .I3(state[1]),
        .I4(S0_AXI_ARVALID),
        .O(p_1_in[5]));
  FDRE \bus2ip_addr_i_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(\bus2ip_addr_i[5]_i_1__0_n_0 ),
        .D(p_1_in[2]),
        .Q(\bus2ip_addr_i_reg_n_0_[2] ),
        .R(rst));
  FDRE \bus2ip_addr_i_reg[3] 
       (.C(S0_AXI_ACLK),
        .CE(\bus2ip_addr_i[5]_i_1__0_n_0 ),
        .D(p_1_in[3]),
        .Q(\bus2ip_addr_i_reg_n_0_[3] ),
        .R(rst));
  FDRE \bus2ip_addr_i_reg[4] 
       (.C(S0_AXI_ACLK),
        .CE(\bus2ip_addr_i[5]_i_1__0_n_0 ),
        .D(p_1_in[4]),
        .Q(\bus2ip_addr_i_reg_n_0_[4] ),
        .R(rst));
  FDRE \bus2ip_addr_i_reg[5] 
       (.C(S0_AXI_ACLK),
        .CE(\bus2ip_addr_i[5]_i_1__0_n_0 ),
        .D(p_1_in[5]),
        .Q(\bus2ip_addr_i_reg_n_0_[5] ),
        .R(rst));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h10)) 
    bus2ip_rnw_i_i_1__0
       (.I0(state[0]),
        .I1(state[1]),
        .I2(S0_AXI_ARVALID),
        .O(bus2ip_rnw_i06_out));
  FDRE bus2ip_rnw_i_reg
       (.C(S0_AXI_ACLK),
        .CE(\bus2ip_addr_i[5]_i_1__0_n_0 ),
        .D(bus2ip_rnw_i06_out),
        .Q(bus2ip_rnw_i),
        .R(rst));
  LUT5 #(
    .INIT(32'h3FFA000A)) 
    is_read_i_1__0
       (.I0(S0_AXI_ARVALID),
        .I1(state1__2),
        .I2(state[0]),
        .I3(state[1]),
        .I4(is_read),
        .O(is_read_i_1__0_n_0));
  FDRE is_read_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(is_read_i_1__0_n_0),
        .Q(is_read),
        .R(rst));
  LUT6 #(
    .INIT(64'h0040FFFF00400000)) 
    is_write_i_1__0
       (.I0(S0_AXI_ARVALID),
        .I1(S0_AXI_AWVALID),
        .I2(S0_AXI_WVALID),
        .I3(state[1]),
        .I4(is_write),
        .I5(is_write_reg_n_0),
        .O(is_write_i_1__0_n_0));
  LUT6 #(
    .INIT(64'hF88800000000FFFF)) 
    is_write_i_2__0
       (.I0(S0_AXI_RVALID),
        .I1(S0_AXI_RREADY),
        .I2(S0_AXI_BVALID),
        .I3(S0_AXI_BREADY),
        .I4(state[0]),
        .I5(state[1]),
        .O(is_write));
  FDRE is_write_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(is_write_i_1__0_n_0),
        .Q(is_write_reg_n_0),
        .R(rst));
  FDRE rst_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(Rst),
        .Q(rst),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    s_axi_bvalid_i_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(I_DECODER_n_48),
        .Q(S0_AXI_BVALID),
        .R(rst));
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[31]_i_1__0 
       (.I0(state[0]),
        .I1(state[1]),
        .O(\s_axi_rdata_i[31]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_43),
        .Q(S0_AXI_RDATA[0]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[10] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_33),
        .Q(S0_AXI_RDATA[10]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[11] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_32),
        .Q(S0_AXI_RDATA[11]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[12] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_31),
        .Q(S0_AXI_RDATA[12]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[13] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_30),
        .Q(S0_AXI_RDATA[13]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[14] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_29),
        .Q(S0_AXI_RDATA[14]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[15] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_28),
        .Q(S0_AXI_RDATA[15]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[16] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_27),
        .Q(S0_AXI_RDATA[16]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[17] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_26),
        .Q(S0_AXI_RDATA[17]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[18] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_25),
        .Q(S0_AXI_RDATA[18]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[19] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_24),
        .Q(S0_AXI_RDATA[19]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_42),
        .Q(S0_AXI_RDATA[1]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[20] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_23),
        .Q(S0_AXI_RDATA[20]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[21] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_22),
        .Q(S0_AXI_RDATA[21]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[22] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_21),
        .Q(S0_AXI_RDATA[22]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[23] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_20),
        .Q(S0_AXI_RDATA[23]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[24] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_19),
        .Q(S0_AXI_RDATA[24]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[25] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_18),
        .Q(S0_AXI_RDATA[25]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[26] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_17),
        .Q(S0_AXI_RDATA[26]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[27] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_16),
        .Q(S0_AXI_RDATA[27]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[28] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_15),
        .Q(S0_AXI_RDATA[28]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[29] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_14),
        .Q(S0_AXI_RDATA[29]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[2] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_41),
        .Q(S0_AXI_RDATA[2]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[30] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_13),
        .Q(S0_AXI_RDATA[30]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[31] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_12),
        .Q(S0_AXI_RDATA[31]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[3] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_40),
        .Q(S0_AXI_RDATA[3]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[4] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_39),
        .Q(S0_AXI_RDATA[4]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[5] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_38),
        .Q(S0_AXI_RDATA[5]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[6] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_37),
        .Q(S0_AXI_RDATA[6]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[7] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_36),
        .Q(S0_AXI_RDATA[7]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[8] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_35),
        .Q(S0_AXI_RDATA[8]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[9] 
       (.C(S0_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1__0_n_0 ),
        .D(I_DECODER_n_34),
        .Q(S0_AXI_RDATA[9]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    s_axi_rvalid_i_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(I_DECODER_n_47),
        .Q(S0_AXI_RVALID),
        .R(rst));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'h000000F8)) 
    start2_i_1
       (.I0(S0_AXI_AWVALID),
        .I1(S0_AXI_WVALID),
        .I2(S0_AXI_ARVALID),
        .I3(state[1]),
        .I4(state[0]),
        .O(start2_i_1_n_0));
  FDRE start2_reg
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(start2_i_1_n_0),
        .Q(start2),
        .R(rst));
  LUT4 #(
    .INIT(16'hF888)) 
    \state[1]_i_2__0 
       (.I0(S0_AXI_BREADY),
        .I1(S0_AXI_BVALID),
        .I2(S0_AXI_RREADY),
        .I3(S0_AXI_RVALID),
        .O(state1__2));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \state[1]_i_3__0 
       (.I0(S0_AXI_WVALID),
        .I1(S0_AXI_AWVALID),
        .I2(S0_AXI_ARVALID),
        .O(\state[1]_i_3__0_n_0 ));
  FDRE \state_reg[0] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(I_DECODER_n_7),
        .Q(state[0]),
        .R(rst));
  FDRE \state_reg[1] 
       (.C(S0_AXI_ACLK),
        .CE(1'b1),
        .D(I_DECODER_n_6),
        .Q(state[1]),
        .R(rst));
endmodule

(* ORIG_REF_NAME = "slave_attachment" *) 
module design_1_mailbox_0_0_slave_attachment__parameterized0
   (\s_axi_rdata_i_reg[31]_0 ,
    \is_register_reg[2] ,
    \read_nextgray_reg[2] ,
    write_fsl_error_d1_reg,
    Bus_RNW_reg_reg,
    S1_AXI_RVALID,
    S1_AXI_BVALID,
    empty_allow,
    S1_AXI_ARREADY,
    error_detect,
    read_fsl_error,
    write_fsl_error,
    S1_AXI_WREADY,
    E,
    \sit_register_reg[3] ,
    full_error_reg,
    empty_error_reg,
    \read_nextgray_reg[2]_0 ,
    S1_AXI_RDATA,
    \Rst_Async.SYS_Rst_If1_reg ,
    S1_AXI_ACLK,
    out,
    S1_AXI_ARVALID,
    S1_AXI_AWVALID,
    S1_AXI_WVALID,
    S1_AXI_ARADDR,
    S1_AXI_AWADDR,
    write_fsl_error_d1,
    full_i_reg,
    read_fsl_error_d1,
    rit_detect_d0,
    sit_detect_d0,
    full_error_reg_0,
    empty_error_reg_0,
    S1_AXI_RREADY,
    S1_AXI_BREADY,
    Q,
    p_6_in,
    \rit_register_reg[0] ,
    \DataOut_reg[31] ,
    ie_register);
  output \s_axi_rdata_i_reg[31]_0 ;
  output \is_register_reg[2] ;
  output \read_nextgray_reg[2] ;
  output write_fsl_error_d1_reg;
  output Bus_RNW_reg_reg;
  output S1_AXI_RVALID;
  output S1_AXI_BVALID;
  output empty_allow;
  output S1_AXI_ARREADY;
  output error_detect;
  output read_fsl_error;
  output write_fsl_error;
  output S1_AXI_WREADY;
  output [0:0]E;
  output [0:0]\sit_register_reg[3] ;
  output full_error_reg;
  output empty_error_reg;
  output [0:0]\read_nextgray_reg[2]_0 ;
  output [31:0]S1_AXI_RDATA;
  input \Rst_Async.SYS_Rst_If1_reg ;
  input S1_AXI_ACLK;
  input out;
  input S1_AXI_ARVALID;
  input S1_AXI_AWVALID;
  input S1_AXI_WVALID;
  input [3:0]S1_AXI_ARADDR;
  input [3:0]S1_AXI_AWADDR;
  input write_fsl_error_d1;
  input full_i_reg;
  input read_fsl_error_d1;
  input rit_detect_d0;
  input sit_detect_d0;
  input full_error_reg_0;
  input empty_error_reg_0;
  input S1_AXI_RREADY;
  input S1_AXI_BREADY;
  input [3:0]Q;
  input [2:0]p_6_in;
  input [3:0]\rit_register_reg[0] ;
  input [31:0]\DataOut_reg[31] ;
  input [0:2]ie_register;

  wire Bus_RNW_reg_reg;
  wire [31:0]\DataOut_reg[31] ;
  wire [0:0]E;
  wire \INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1_n_0 ;
  wire [4:0]\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 ;
  wire I_DECODER_n_12;
  wire I_DECODER_n_13;
  wire I_DECODER_n_14;
  wire I_DECODER_n_15;
  wire I_DECODER_n_16;
  wire I_DECODER_n_17;
  wire I_DECODER_n_18;
  wire I_DECODER_n_19;
  wire I_DECODER_n_20;
  wire I_DECODER_n_21;
  wire I_DECODER_n_22;
  wire I_DECODER_n_23;
  wire I_DECODER_n_24;
  wire I_DECODER_n_25;
  wire I_DECODER_n_26;
  wire I_DECODER_n_27;
  wire I_DECODER_n_28;
  wire I_DECODER_n_29;
  wire I_DECODER_n_30;
  wire I_DECODER_n_31;
  wire I_DECODER_n_32;
  wire I_DECODER_n_33;
  wire I_DECODER_n_34;
  wire I_DECODER_n_35;
  wire I_DECODER_n_36;
  wire I_DECODER_n_37;
  wire I_DECODER_n_38;
  wire I_DECODER_n_39;
  wire I_DECODER_n_40;
  wire I_DECODER_n_41;
  wire I_DECODER_n_42;
  wire I_DECODER_n_43;
  wire I_DECODER_n_47;
  wire I_DECODER_n_48;
  wire I_DECODER_n_6;
  wire I_DECODER_n_7;
  wire [3:0]Q;
  wire \Rst_Async.SYS_Rst_If1_reg ;
  wire S1_AXI_ACLK;
  wire [3:0]S1_AXI_ARADDR;
  wire S1_AXI_ARREADY;
  wire S1_AXI_ARVALID;
  wire [3:0]S1_AXI_AWADDR;
  wire S1_AXI_AWVALID;
  wire S1_AXI_BREADY;
  wire S1_AXI_BVALID;
  wire [31:0]S1_AXI_RDATA;
  wire S1_AXI_RREADY;
  wire S1_AXI_RVALID;
  wire S1_AXI_WREADY;
  wire S1_AXI_WVALID;
  wire \bus2ip_addr_i[2]_i_1_n_0 ;
  wire \bus2ip_addr_i[3]_i_1_n_0 ;
  wire \bus2ip_addr_i[4]_i_1_n_0 ;
  wire \bus2ip_addr_i[5]_i_1_n_0 ;
  wire \bus2ip_addr_i[5]_i_2_n_0 ;
  wire \bus2ip_addr_i_reg_n_0_[2] ;
  wire \bus2ip_addr_i_reg_n_0_[3] ;
  wire \bus2ip_addr_i_reg_n_0_[4] ;
  wire \bus2ip_addr_i_reg_n_0_[5] ;
  wire bus2ip_rnw_i06_out;
  wire bus2ip_rnw_i_reg_n_0;
  wire empty_allow;
  wire empty_error_reg;
  wire empty_error_reg_0;
  wire error_detect;
  wire full_error_reg;
  wire full_error_reg_0;
  wire full_i_reg;
  wire [0:2]ie_register;
  wire is_read;
  wire is_read_i_1_n_0;
  wire \is_register_reg[2] ;
  wire is_write;
  wire is_write_i_1_n_0;
  wire is_write_reg_n_0;
  wire out;
  wire [2:0]p_6_in;
  wire [4:0]plusOp;
  wire read_fsl_error;
  wire read_fsl_error_d1;
  wire \read_nextgray_reg[2] ;
  wire [0:0]\read_nextgray_reg[2]_0 ;
  wire rit_detect_d0;
  wire [3:0]\rit_register_reg[0] ;
  wire rst;
  wire \s_axi_rdata_i[31]_i_1_n_0 ;
  wire \s_axi_rdata_i_reg[31]_0 ;
  wire sit_detect_d0;
  wire [0:0]\sit_register_reg[3] ;
  wire start2;
  wire start2_i_1_n_0;
  wire [1:0]state;
  wire state1__2;
  wire \state[1]_i_3_n_0 ;
  wire write_fsl_error;
  wire write_fsl_error_d1;
  wire write_fsl_error_d1_reg;

  LUT1 #(
    .INIT(2'h1)) 
    \INCLUDE_DPHASE_TIMER.dpto_cnt[0]_i_1 
       (.I0(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [0]),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \INCLUDE_DPHASE_TIMER.dpto_cnt[1]_i_1 
       (.I0(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [0]),
        .I1(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [1]),
        .O(plusOp[1]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \INCLUDE_DPHASE_TIMER.dpto_cnt[2]_i_1 
       (.I0(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [0]),
        .I1(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [1]),
        .I2(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [2]),
        .O(plusOp[2]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \INCLUDE_DPHASE_TIMER.dpto_cnt[3]_i_1 
       (.I0(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [1]),
        .I1(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [0]),
        .I2(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [2]),
        .I3(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [3]),
        .O(plusOp[3]));
  LUT2 #(
    .INIT(4'h9)) 
    \INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1 
       (.I0(state[0]),
        .I1(state[1]),
        .O(\INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_2 
       (.I0(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [2]),
        .I1(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [0]),
        .I2(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [1]),
        .I3(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [3]),
        .I4(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [4]),
        .O(plusOp[4]));
  FDRE \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(plusOp[0]),
        .Q(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [0]),
        .R(\INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1_n_0 ));
  FDRE \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[1] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(plusOp[1]),
        .Q(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [1]),
        .R(\INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1_n_0 ));
  FDRE \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(plusOp[2]),
        .Q(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [2]),
        .R(\INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1_n_0 ));
  FDRE \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[3] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(plusOp[3]),
        .Q(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [3]),
        .R(\INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1_n_0 ));
  FDRE \INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(plusOp[4]),
        .Q(\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 [4]),
        .R(\INCLUDE_DPHASE_TIMER.dpto_cnt[4]_i_1_n_0 ));
  design_1_mailbox_0_0_address_decoder__parameterized0 I_DECODER
       (.Bus_RNW_reg_reg_0(Bus_RNW_reg_reg),
        .D({I_DECODER_n_6,I_DECODER_n_7}),
        .\DataOut_reg[31] (\DataOut_reg[31] ),
        .E(E),
        .\INCLUDE_DPHASE_TIMER.dpto_cnt_reg[4] (\INCLUDE_DPHASE_TIMER.dpto_cnt_reg__0 ),
        .Q(start2),
        .\Rst_Async.SYS_Rst_If1_reg (\Rst_Async.SYS_Rst_If1_reg ),
        .S1_AXI_ACLK(S1_AXI_ACLK),
        .S1_AXI_ARREADY(S1_AXI_ARREADY),
        .S1_AXI_ARVALID(S1_AXI_ARVALID),
        .S1_AXI_ARVALID_0(\state[1]_i_3_n_0 ),
        .S1_AXI_BREADY(S1_AXI_BREADY),
        .S1_AXI_RREADY(S1_AXI_RREADY),
        .S1_AXI_WREADY(S1_AXI_WREADY),
        .\bus2ip_addr_i_reg[5] ({\bus2ip_addr_i_reg_n_0_[5] ,\bus2ip_addr_i_reg_n_0_[4] ,\bus2ip_addr_i_reg_n_0_[3] ,\bus2ip_addr_i_reg_n_0_[2] }),
        .bus2ip_rnw_i_reg(bus2ip_rnw_i_reg_n_0),
        .empty_allow(empty_allow),
        .empty_error_reg(empty_error_reg),
        .empty_error_reg_0(empty_error_reg_0),
        .error_detect(error_detect),
        .full_error_reg(full_error_reg),
        .full_error_reg_0(full_error_reg_0),
        .full_i_reg(full_i_reg),
        .ie_register(ie_register),
        .is_read(is_read),
        .\is_register_reg[2] (\is_register_reg[2] ),
        .is_write_reg(is_write_reg_n_0),
        .out(out),
        .p_6_in(p_6_in),
        .read_fsl_error(read_fsl_error),
        .read_fsl_error_d1(read_fsl_error_d1),
        .\read_nextgray_reg[2] (\read_nextgray_reg[2] ),
        .\read_nextgray_reg[2]_0 (\read_nextgray_reg[2]_0 ),
        .rit_detect_d0(rit_detect_d0),
        .\rit_register_reg[0] (\rit_register_reg[0] ),
        .s_axi_bvalid_i_reg(I_DECODER_n_48),
        .s_axi_bvalid_i_reg_0(S1_AXI_BVALID),
        .\s_axi_rdata_i_reg[31] (\s_axi_rdata_i_reg[31]_0 ),
        .\s_axi_rdata_i_reg[31]_0 ({I_DECODER_n_12,I_DECODER_n_13,I_DECODER_n_14,I_DECODER_n_15,I_DECODER_n_16,I_DECODER_n_17,I_DECODER_n_18,I_DECODER_n_19,I_DECODER_n_20,I_DECODER_n_21,I_DECODER_n_22,I_DECODER_n_23,I_DECODER_n_24,I_DECODER_n_25,I_DECODER_n_26,I_DECODER_n_27,I_DECODER_n_28,I_DECODER_n_29,I_DECODER_n_30,I_DECODER_n_31,I_DECODER_n_32,I_DECODER_n_33,I_DECODER_n_34,I_DECODER_n_35,I_DECODER_n_36,I_DECODER_n_37,I_DECODER_n_38,I_DECODER_n_39,I_DECODER_n_40,I_DECODER_n_41,I_DECODER_n_42,I_DECODER_n_43}),
        .s_axi_rvalid_i_reg(I_DECODER_n_47),
        .s_axi_rvalid_i_reg_0(S1_AXI_RVALID),
        .sit_detect_d0(sit_detect_d0),
        .\sit_register_reg[0] (Q),
        .\sit_register_reg[3] (\sit_register_reg[3] ),
        .state1__2(state1__2),
        .\state_reg[1] (state),
        .write_fsl_error(write_fsl_error),
        .write_fsl_error_d1(write_fsl_error_d1),
        .write_fsl_error_d1_reg(write_fsl_error_d1_reg));
  LUT5 #(
    .INIT(32'hCCCACCCC)) 
    \bus2ip_addr_i[2]_i_1 
       (.I0(S1_AXI_ARADDR[0]),
        .I1(S1_AXI_AWADDR[0]),
        .I2(state[0]),
        .I3(state[1]),
        .I4(S1_AXI_ARVALID),
        .O(\bus2ip_addr_i[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT5 #(
    .INIT(32'hCCCACCCC)) 
    \bus2ip_addr_i[3]_i_1 
       (.I0(S1_AXI_ARADDR[1]),
        .I1(S1_AXI_AWADDR[1]),
        .I2(state[0]),
        .I3(state[1]),
        .I4(S1_AXI_ARVALID),
        .O(\bus2ip_addr_i[3]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hCCCACCCC)) 
    \bus2ip_addr_i[4]_i_1 
       (.I0(S1_AXI_ARADDR[2]),
        .I1(S1_AXI_AWADDR[2]),
        .I2(state[0]),
        .I3(state[1]),
        .I4(S1_AXI_ARVALID),
        .O(\bus2ip_addr_i[4]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h000000EA)) 
    \bus2ip_addr_i[5]_i_1 
       (.I0(S1_AXI_ARVALID),
        .I1(S1_AXI_AWVALID),
        .I2(S1_AXI_WVALID),
        .I3(state[1]),
        .I4(state[0]),
        .O(\bus2ip_addr_i[5]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hCCCACCCC)) 
    \bus2ip_addr_i[5]_i_2 
       (.I0(S1_AXI_ARADDR[3]),
        .I1(S1_AXI_AWADDR[3]),
        .I2(state[0]),
        .I3(state[1]),
        .I4(S1_AXI_ARVALID),
        .O(\bus2ip_addr_i[5]_i_2_n_0 ));
  FDRE \bus2ip_addr_i_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(\bus2ip_addr_i[5]_i_1_n_0 ),
        .D(\bus2ip_addr_i[2]_i_1_n_0 ),
        .Q(\bus2ip_addr_i_reg_n_0_[2] ),
        .R(rst));
  FDRE \bus2ip_addr_i_reg[3] 
       (.C(S1_AXI_ACLK),
        .CE(\bus2ip_addr_i[5]_i_1_n_0 ),
        .D(\bus2ip_addr_i[3]_i_1_n_0 ),
        .Q(\bus2ip_addr_i_reg_n_0_[3] ),
        .R(rst));
  FDRE \bus2ip_addr_i_reg[4] 
       (.C(S1_AXI_ACLK),
        .CE(\bus2ip_addr_i[5]_i_1_n_0 ),
        .D(\bus2ip_addr_i[4]_i_1_n_0 ),
        .Q(\bus2ip_addr_i_reg_n_0_[4] ),
        .R(rst));
  FDRE \bus2ip_addr_i_reg[5] 
       (.C(S1_AXI_ACLK),
        .CE(\bus2ip_addr_i[5]_i_1_n_0 ),
        .D(\bus2ip_addr_i[5]_i_2_n_0 ),
        .Q(\bus2ip_addr_i_reg_n_0_[5] ),
        .R(rst));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'h10)) 
    bus2ip_rnw_i_i_1
       (.I0(state[0]),
        .I1(state[1]),
        .I2(S1_AXI_ARVALID),
        .O(bus2ip_rnw_i06_out));
  FDRE bus2ip_rnw_i_reg
       (.C(S1_AXI_ACLK),
        .CE(\bus2ip_addr_i[5]_i_1_n_0 ),
        .D(bus2ip_rnw_i06_out),
        .Q(bus2ip_rnw_i_reg_n_0),
        .R(rst));
  LUT5 #(
    .INIT(32'h3FFA000A)) 
    is_read_i_1
       (.I0(S1_AXI_ARVALID),
        .I1(state1__2),
        .I2(state[0]),
        .I3(state[1]),
        .I4(is_read),
        .O(is_read_i_1_n_0));
  FDRE is_read_reg
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(is_read_i_1_n_0),
        .Q(is_read),
        .R(rst));
  LUT6 #(
    .INIT(64'h0040FFFF00400000)) 
    is_write_i_1
       (.I0(S1_AXI_ARVALID),
        .I1(S1_AXI_AWVALID),
        .I2(S1_AXI_WVALID),
        .I3(state[1]),
        .I4(is_write),
        .I5(is_write_reg_n_0),
        .O(is_write_i_1_n_0));
  LUT6 #(
    .INIT(64'hF88800000000FFFF)) 
    is_write_i_2
       (.I0(S1_AXI_RVALID),
        .I1(S1_AXI_RREADY),
        .I2(S1_AXI_BVALID),
        .I3(S1_AXI_BREADY),
        .I4(state[0]),
        .I5(state[1]),
        .O(is_write));
  FDRE is_write_reg
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(is_write_i_1_n_0),
        .Q(is_write_reg_n_0),
        .R(rst));
  FDRE rst_reg
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(\Rst_Async.SYS_Rst_If1_reg ),
        .Q(rst),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    s_axi_bvalid_i_reg
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(I_DECODER_n_48),
        .Q(S1_AXI_BVALID),
        .R(rst));
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[31]_i_1 
       (.I0(state[0]),
        .I1(state[1]),
        .O(\s_axi_rdata_i[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_43),
        .Q(S1_AXI_RDATA[0]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[10] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_33),
        .Q(S1_AXI_RDATA[10]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[11] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_32),
        .Q(S1_AXI_RDATA[11]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[12] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_31),
        .Q(S1_AXI_RDATA[12]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[13] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_30),
        .Q(S1_AXI_RDATA[13]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[14] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_29),
        .Q(S1_AXI_RDATA[14]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[15] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_28),
        .Q(S1_AXI_RDATA[15]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[16] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_27),
        .Q(S1_AXI_RDATA[16]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[17] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_26),
        .Q(S1_AXI_RDATA[17]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[18] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_25),
        .Q(S1_AXI_RDATA[18]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[19] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_24),
        .Q(S1_AXI_RDATA[19]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[1] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_42),
        .Q(S1_AXI_RDATA[1]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[20] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_23),
        .Q(S1_AXI_RDATA[20]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[21] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_22),
        .Q(S1_AXI_RDATA[21]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[22] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_21),
        .Q(S1_AXI_RDATA[22]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[23] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_20),
        .Q(S1_AXI_RDATA[23]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[24] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_19),
        .Q(S1_AXI_RDATA[24]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[25] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_18),
        .Q(S1_AXI_RDATA[25]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[26] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_17),
        .Q(S1_AXI_RDATA[26]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[27] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_16),
        .Q(S1_AXI_RDATA[27]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[28] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_15),
        .Q(S1_AXI_RDATA[28]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[29] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_14),
        .Q(S1_AXI_RDATA[29]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[2] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_41),
        .Q(S1_AXI_RDATA[2]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[30] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_13),
        .Q(S1_AXI_RDATA[30]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[31] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_12),
        .Q(S1_AXI_RDATA[31]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[3] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_40),
        .Q(S1_AXI_RDATA[3]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[4] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_39),
        .Q(S1_AXI_RDATA[4]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[5] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_38),
        .Q(S1_AXI_RDATA[5]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[6] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_37),
        .Q(S1_AXI_RDATA[6]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[7] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_36),
        .Q(S1_AXI_RDATA[7]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[8] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_35),
        .Q(S1_AXI_RDATA[8]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[9] 
       (.C(S1_AXI_ACLK),
        .CE(\s_axi_rdata_i[31]_i_1_n_0 ),
        .D(I_DECODER_n_34),
        .Q(S1_AXI_RDATA[9]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    s_axi_rvalid_i_reg
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(I_DECODER_n_47),
        .Q(S1_AXI_RVALID),
        .R(rst));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT5 #(
    .INIT(32'h000000F8)) 
    start2_i_1
       (.I0(S1_AXI_AWVALID),
        .I1(S1_AXI_WVALID),
        .I2(S1_AXI_ARVALID),
        .I3(state[1]),
        .I4(state[0]),
        .O(start2_i_1_n_0));
  FDRE start2_reg
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(start2_i_1_n_0),
        .Q(start2),
        .R(rst));
  LUT4 #(
    .INIT(16'hF888)) 
    \state[1]_i_2 
       (.I0(S1_AXI_BREADY),
        .I1(S1_AXI_BVALID),
        .I2(S1_AXI_RREADY),
        .I3(S1_AXI_RVALID),
        .O(state1__2));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \state[1]_i_3 
       (.I0(S1_AXI_WVALID),
        .I1(S1_AXI_AWVALID),
        .I2(S1_AXI_ARVALID),
        .O(\state[1]_i_3_n_0 ));
  FDRE \state_reg[0] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(I_DECODER_n_7),
        .Q(state[0]),
        .R(rst));
  FDRE \state_reg[1] 
       (.C(S1_AXI_ACLK),
        .CE(1'b1),
        .D(I_DECODER_n_6),
        .Q(state[1]),
        .R(rst));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
