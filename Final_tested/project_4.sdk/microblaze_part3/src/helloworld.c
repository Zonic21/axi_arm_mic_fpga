// Simple test application for the MicroBlaze that reads in the current
// state of the switches contained on the Zybo board and sends it to the
// ARM processor with the help of a Mailbox IP Core
//
// File: microblaze.c
// Designer: Christopher Reisner
// Affiliation: https://embsys.technikum-wien.at/
// Date: Dec. 27, 2016

#include <stdio.h>
#include "xil_printf.h"
#include "sleep.h"
#include "xparameters.h" // Defines addresses
#include "xmbox.h" // Mailbox driver
#include "microblaze_sleep.h" // Needed for the "Sleep" function
#include "xil_io.h"
#include "PWM_Led.h"

#define printf xil_printf	/* A smaller footprint printf */

int main()
{
	XMbox Mbox0;
	XMbox Mbox1;
	XMbox_Config *ConfigPtr0;
	XMbox_Config *ConfigPtr1;

	int Status;
	u32 Reg32Data0 = 0; // data from the ethernet interface
	u32 Reg32Data1 = 0; // momentan status of the LED
	u32 Reg0, Reg1, Reg2, Reg3, Reg4;
	u32 momentanvalue = 0; // momentan value of the LEDs

	// Configure Mailbox0
	ConfigPtr0 = XMbox_LookupConfig(XPAR_MBOX_0_DEVICE_ID );
	if (ConfigPtr0 == (XMbox_Config *)NULL) {
		printf ("LookupConfig0 Failed.\r\n");
		return XST_FAILURE;
	}
	// Configure Mailbox1
	ConfigPtr1 = XMbox_LookupConfig(XPAR_MBOX_1_DEVICE_ID );
	if (ConfigPtr1 == (XMbox_Config *)NULL) {
		printf ("LookupConfig1 Failed.\r\n");
		return XST_FAILURE;
	}
	// Initialize Mailbox0
	Status = XMbox_CfgInitialize(&Mbox0, ConfigPtr0, ConfigPtr0->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	// Initialize Mailbox1
	Status = XMbox_CfgInitialize(&Mbox1, ConfigPtr1, ConfigPtr1->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	while(1)
	{
		// Wait for new data received from the Mailbox
		XMbox_ReadBlocking(&Mbox0,(u32*)(&Reg32Data0),4);
		char *payloadpointer = &Reg32Data0;
		//payloadpointer = Reg32Data0;
		char buffer[4]; // 8bit nothing, 8 bit LEDID 1,2,3,4, 8 bit mode 1,0, 8 bit value 0 - 255
		for(int i=0; i<4; i++) {
			buffer[i] = *payloadpointer;
			payloadpointer++;
		}
		// Sleep for 10 us
		usleep(20);

		if(buffer[2] == 0x01){
			if (buffer[1] == 0x00){
				Reg0 = Reg0 & 0xFFFFFFFE;
				if(buffer[0] == 0x01){
					Reg1 = Reg1 | 0x00000100;
				} else if (buffer[0] == 0x00){
					Reg1 = Reg1 | 0xFFFFFEFF;
				}
			} else if (buffer[1] == 0x01){
				Reg0 = Reg0 | 0x00000001;
				Reg1 = Reg1 & 0xFFFFFF00;
				Reg1 = Reg1 | buffer[0];
			}
		} else if (buffer[2] == 0x02) {
			if (buffer[1] == 0x00){
				Reg0 = Reg0 & 0xFFFFFFFD;
				if(buffer[0] == 0x01){
					Reg2 = Reg2 | 0x00000100;
				} else if (buffer[0] == 0x00){
					Reg2 = Reg2 | 0xFFFFFEFF;
				}
			} else if (buffer[1] == 0x01){
				Reg0 = Reg0 | 0x00000002;
				Reg2 = Reg2 & 0xFFFFFF00;
				Reg2 = Reg2 | buffer[0];
			}
		} else if (buffer[2] == 0x03) {
			if (buffer[1] == 0x00){
				Reg0 = Reg0 & 0xFFFFFFFB;
				if(buffer[0] == 0x01){
					Reg3 = Reg3 | 0x00000100;
				} else if (buffer[0] == 0x00){
					Reg3 = Reg3 | 0xFFFFFEFF;
				}
			} else if (buffer[1] == 0x01){
				Reg0 = Reg0 | 0x00000004;
				Reg3 = Reg3 & 0xFFFFFF00;
				Reg3 = Reg3 | buffer[0];
			}
		} else if (buffer[2] == 0x04) {
			if (buffer[1] == 0x00){
				Reg0 = Reg0 & 0xFFFFFFF7;
				if(buffer[0] == 0x01){
					Reg4 = Reg4 | 0x00000100;
				} else if (buffer[0] == 0x00){
					Reg4 = Reg4 | 0xFFFFFEFF;
				}
			} else if (buffer[1] == 0x01){
				Reg0 = Reg0 | 0x00000008;
				Reg4 = Reg4 & 0xFFFFFF00;
				Reg4 = Reg4 | buffer[0];
			}
		}
		PWM_LED_mWriteReg(XPAR_PWM_LED_0_S00_AXI_BASEADDR, PWM_LED_S00_AXI_SLV_REG0_OFFSET, Reg0);
		PWM_LED_mWriteReg(XPAR_PWM_LED_0_S00_AXI_BASEADDR, PWM_LED_S00_AXI_SLV_REG1_OFFSET, Reg1);
		PWM_LED_mWriteReg(XPAR_PWM_LED_0_S00_AXI_BASEADDR, PWM_LED_S00_AXI_SLV_REG2_OFFSET, Reg2);
		PWM_LED_mWriteReg(XPAR_PWM_LED_0_S00_AXI_BASEADDR, PWM_LED_S00_AXI_SLV_REG3_OFFSET, Reg3);
		PWM_LED_mWriteReg(XPAR_PWM_LED_0_S00_AXI_BASEADDR, PWM_LED_S00_AXI_SLV_REG4_OFFSET, Reg4);
		// Send current state of switches to ARM via Mailbox
		Reg32Data1 = Reg32Data0;
		XMbox_WriteBlocking(&Mbox1,(u32*)(&Reg32Data1),4);

		// Sleep for 10 us
		usleep(10);
	}

	return 0;
}
