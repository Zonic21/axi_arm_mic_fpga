/*
 * own_functions.c
 *
 *  Created on: 18.05.2017
 *      Author: Zonic
 */
#include <stdio.h>
#include <string.h>
#include "own_functions.h"


char * payloadToString(void *pointer, char *pointer2)
{
	 pointer2 = (char*)pointer; //OK in both C and C++
	 //pointer2 = pointer;        //OK in C, convertion is implicit
	 return pointer2;
}
// hex should be from 0 to f = 0 to 15
 int hextoint(char hex)
 {
	 int temp = (int)hex;
	 if ((temp >= 48)||(temp <= 57))
	 {
		 return temp - 48;
	 }
	 if ((temp >= 97)||(temp <= 102))
	 {
		 return temp - 87;
	 }
	 return 100; //error number
 }
 void inttostring(struct datasend *point)
 {
	 sprintf(point->data, "%d", point->value);
	 point->datalen = strlen(point->data);
 }


