/*
 * own_functions.h
 *
 *  Created on: 19.05.2017
 *      Author: Zonic
 */

#ifndef SRC_OWN_FUNCTIONS_H_
#define SRC_OWN_FUNCTIONS_H_

struct datasend
{
	int datalen;
	int value;
	char data[10];
};

char * payloadToString(void *pointer, char *pointer2);
int hextoint(char hex);
void inttostring(struct datasend *point);

#endif /* SRC_OWN_FUNCTIONS_H_ */
