/******************************************************************************
 *
 * Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Use of the Software is limited solely to applications:
 * (a) running on a Xilinx device, or
 * (b) that interact with a Xilinx device through a bus or interconnect.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
 * OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Except as contained in this notice, the name of the Xilinx shall not be used
 * in advertising or otherwise to promote the sale, use or other dealings in
 * this Software without prior written authorization from Xilinx.
 *
 ******************************************************************************/

#include <stdio.h>
#include <string.h>
#include "own_functions.h"
#include "sleep.h"
#include "lwip/err.h"
#include "lwip/tcp.h"
#if defined (__arm__) || defined (__aarch64__)
#include "xil_printf.h"

#endif
#include "xmbox.h" // Mailbox driver
static XMbox Mbox0;
static XMbox Mbox1;
static XMbox_Config *ConfigPtr0;
static XMbox_Config *ConfigPtr1;
static int Status;
static u32 Reg32Data0 = 0; // data from the ethernet interface
static u32 Reg32Data1 = 0; // momentan status of the LED


void initmboxs()
{
	// the initialisazion part for the two Mailboxes




	// Configure Mailbox0
	ConfigPtr0 = XMbox_LookupConfig(XPAR_MBOX_0_DEVICE_ID );
	if (ConfigPtr0 == (XMbox_Config *)NULL) {
		printf ("LookupConfig0 Failed.\r\n");
		return XST_FAILURE;
	}
	// Configure Mailbox1
	ConfigPtr1 = XMbox_LookupConfig(XPAR_MBOX_1_DEVICE_ID );
	if (ConfigPtr1 == (XMbox_Config *)NULL) {
		printf ("LookupConfig1 Failed.\r\n");
		return XST_FAILURE;
	}
	// Initialize Mailbox0
	Status = XMbox_CfgInitialize(&Mbox0, ConfigPtr0, ConfigPtr0->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	// Initialize Mailbox1
	Status = XMbox_CfgInitialize(&Mbox1, ConfigPtr1, ConfigPtr1->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	// end of the initialisation of the two Mailboxes
}
int transfer_data() {
	return 0;
}

void print_app_header()
{
	xil_printf("\n\r\n\r-----lwIP TCP echo server ------\n\r");
	xil_printf("TCP packets sent to port 6001 will be echoed back\n\r");
}

err_t recv_callback(void *arg, struct tcp_pcb *tpcb,
		struct pbuf *p, err_t err)
{
	u32 Mailchar   = 0; // 32 bit wert der �ber mail zum microblaze gesendet wird
							   // erste 8 bit ist nichts, dann LEDID, LED mode, Value

	/* do not read the packet if we are not in ESTABLISHED state */
	if (!p) {
		tcp_close(tpcb);
		tcp_recv(tpcb, NULL);
		return ERR_OK;
	}

	/* indicate that the packet has been received */
	tcp_recved(tpcb, p->len);

	/* echo back the payload */
	/* in this case, we assume that the payload is < TCP_SND_BUF */
	if (tcp_sndbuf(tpcb) > p->len) {
		/* 	ethernet nachricht solte so sein.
			LED1_MODEPIN_1  (Selected LED, Mode of led, led or duration)
			LED2_MODEPWM_156
			Selected LED : LED1, LED2, LED3, LED4
			Mode of led  : MODEPIN, MODEPWM
			led or duration by MODEPIN : 0 = off, 1 = on
			led or duration by MODEPWM : 0 low to 255 bright
		*/

		char *payloadpointer = NULL;
		payloadpointer = payloadToString(p->payload, payloadpointer);
		char buffer[20]; // buffer wo die ethernet nachricht drin stehen soll
		for(int i=0; i<p->len; i++) {
			buffer[i] = *payloadpointer;
			payloadpointer++;
		}
		int bufferlen = strlen(buffer);
		int numlen = 0;
		u32 duty;
		if (bufferlen == 17) {
			numlen = 1;
			char numbuf;
			numbuf = buffer[13];
			//duty =  numbuf - '0' ;
			int dutytemp = atoi(numbuf);
			duty = dutytemp;
		} else if ( bufferlen == 18) {
			numlen = 2;
			char numbuf[2];
			numbuf[0] = buffer[13];
			numbuf[1] = buffer[14];
			//duty = strtoul(numbuf, 0L, 10);
			int dutytemp = atoi(numbuf);
			duty = dutytemp;
		} else if (bufferlen == 19){
			numlen = 3;
			char numbuf[3];
			numbuf[0] = buffer[13];
			numbuf[1] = buffer[14];
			numbuf[2] = buffer[15];
			//duty = strtoul(numbuf, 0L, 10);
			int dutytemp = atoi(numbuf);
			duty = dutytemp;
		}
		Mailchar = Mailchar | duty; // oder verkn�pft mailchar mit dem duty

		if(buffer[3] == '1'){
			if(buffer[10] == 'I'){
				Mailchar = Mailchar | 0x00000100; // 1 ist f�r erste led , 0 ist f�r Pin mode
			} else if (buffer[10] == 'W'){
				Mailchar = Mailchar | 0x00010100; // 1 ist f�r erste led , 1 ist f�r Pin mode
			}
		} else if (buffer[3] == '2'){
			if(buffer[10] == 'I'){
				Mailchar = Mailchar | 0x00020000; // 2 ist f�r erste led , 0 ist f�r Pin mode
			} else if (buffer[10] == 'W'){
				Mailchar = Mailchar | 0x00020100; // 2 ist f�r erste led , 1 ist f�r Pin mode
			}
		} else if (buffer[3] == '3'){
			if(buffer[10] == 'I'){
				Mailchar = Mailchar | 0x00030000; // 3 ist f�r erste led , 0 ist f�r Pin mode
			} else if (buffer[10] == 'W'){
				Mailchar = Mailchar | 0x00030100; // 3 ist f�r erste led , 1 ist f�r Pin mode
			}
		} else if (buffer[3] == '4'){
			if(buffer[10] == 'I'){
				Mailchar = Mailchar | 0x00040000; // 4 ist f�r erste led , 0 ist f�r Pin mode
			} else if (buffer[10] == 'W'){
				Mailchar = Mailchar | 0x00040100; // 4 ist f�r erste led , 1 ist f�r Pin mode
			}
		}

		// Send current state of switches to ARM via Mailbox
		XMbox_WriteBlocking(&Mbox0,(u32*)(&Mailchar),4);
		// Sleep for 10 us
		usleep(10);
		// Wait for new data received from the Mailbox
		XMbox_ReadBlocking(&Mbox1,(u32*)(&Reg32Data1),4);
		// Sleep for 10 us
		usleep(10);
		void *pointerdata;
		struct datasend *sendingdata;
		sendingdata->value = Reg32Data1; 	//empfangene daten fvom microblaze
		inttostring(sendingdata); 			//beschreibd die datasent struktur
		pointerdata = malloc(sendingdata->datalen * sizeof(char));
		pointerdata = sendingdata->data;
		err = tcp_write(tpcb, pointerdata, sendingdata->datalen, 1);
	} else
		xil_printf("no space in tcp_sndbuf\n\r");

	/* free the received pbuf */
	pbuf_free(p);

	return ERR_OK;
}

err_t accept_callback(void *arg, struct tcp_pcb *newpcb, err_t err)
{
	static int connection = 1;

	/* set the receive callback for this connection */
	tcp_recv(newpcb, recv_callback);

	/* just use an integer number indicating the connection id as the
	   callback argument */
	tcp_arg(newpcb, (void*)(UINTPTR)connection);

	/* increment for subsequent accepted connections */
	connection++;

	return ERR_OK;
}


int start_application()
{
	struct tcp_pcb *pcb;
	err_t err;
	unsigned port = 7;

	/* create new TCP PCB structure */
	pcb = tcp_new();
	if (!pcb) {
		xil_printf("Error creating PCB. Out of Memory\n\r");
		return -1;
	}

	/* bind to specified @port */
	err = tcp_bind(pcb, IP_ADDR_ANY, port);
	if (err != ERR_OK) {
		xil_printf("Unable to bind to port %d: err = %d\n\r", port, err);
		return -2;
	}

	/* we do not need any arguments to callback functions */
	tcp_arg(pcb, NULL);

	/* listen for connections */
	pcb = tcp_listen(pcb);
	if (!pcb) {
		xil_printf("Out of memory while tcp_listen\n\r");
		return -3;
	}

	/* specify callback to use for incoming connections */
	tcp_accept(pcb, accept_callback);

	xil_printf("TCP echo server started @ port %d\n\r", port);

	return 0;
}

