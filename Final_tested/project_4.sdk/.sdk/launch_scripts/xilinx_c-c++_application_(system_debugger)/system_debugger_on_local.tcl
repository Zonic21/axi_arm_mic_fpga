connect -url tcp:127.0.0.1:3121
source C:/Users/Zonic/project_4/project_4.sdk/design_1_wrapper_hw_platform_0/ps7_init.tcl
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Digilent Zybo 210279772213A"} -index 0
rst -system
after 3000
targets -set -filter {jtag_cable_name =~ "Digilent Zybo 210279772213A" && level==0} -index 1
fpga -file C:/Users/Zonic/project_4/project_4.sdk/design_1_wrapper_hw_platform_0/design_1_wrapper.bit
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Digilent Zybo 210279772213A"} -index 0
loadhw C:/Users/Zonic/project_4/project_4.sdk/design_1_wrapper_hw_platform_0/system.hdf
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Digilent Zybo 210279772213A"} -index 0
ps7_init
ps7_post_config
targets -set -nocase -filter {name =~ "microblaze*#0" && bscan=="USER2"  && jtag_cable_name =~ "Digilent Zybo 210279772213A"} -index 1
dow C:/Users/Zonic/project_4/project_4.sdk/microblaze_part3/Debug/microblaze_part3.elf
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "Digilent Zybo 210279772213A"} -index 0
dow C:/Users/Zonic/project_4/project_4.sdk/Echo_example/Debug/Echo_example.elf
bpadd -addr &main
